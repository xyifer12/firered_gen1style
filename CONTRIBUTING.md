
There are no hard rules, direct contribution is so rare that I haven't even needed a CONTRIBUTING file until now.

My general idea for what would work best is this;
 * Fork or Clone the project.
 * Make your edits either in the 'collaboration' branch, or make your own branch if permissions allow.
 * If necessary for GitLab permissions, request that I add you as Developer.
 * Submit a merge request once your contribution is tested and working.

 Alternatively, if you wish to avoid GitLab for one reason or another, send me a message through PokeCommunity, PRET Discord, or Aqua Hideout Discord. I'll manually implement the changes and credit your name in the commit message itself, as well as the usual locations.