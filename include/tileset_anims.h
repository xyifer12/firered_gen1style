#ifndef GUARD_TILESET_ANIMS_H
#define GUARD_TILESET_ANIMS_H

void InitTilesetAnimations(void);
void InitSecondaryTilesetAnimation(void);
void UpdateTilesetAnimations(void);
void TransferTilesetAnimsBuffer(void);

void InitTilesetAnim_RB_General(void);
void InitTilesetAnim_RB_Cavern(void);
void InitTilesetAnim_RB_Plant(void);
void InitTilesetAnim_RB_Lab(void);
void InitTilesetAnim_RB_Forest(void);
void InitTilesetAnim_RB_Ship(void);

#endif // GUARD_TILESET_ANIMS_H
