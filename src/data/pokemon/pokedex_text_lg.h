const u8 gDummyPokedexText[] = _(
    "This is a newly discovered POKéMON. It is\n"
    "currently under investigation. No detailed\n"
    "information is available at this time.");

const u8 gDummyPokedexTextUnused[] = _("");

const u8 gBulbasaurPokedexText[] = _(
    "A strange seed was planted\n"
    "on its back at birth.\n"
    "The plant sprouts and grows\n"
    "with this POKéMON.");

const u8 gBulbasaurPokedexTextUnused[] = _("");

const u8 gIvysaurPokedexText[] = _(
    "When the bulb on its back\n"
    "grows large, it appears to\n"
    "lose the ability to stand on\n"
    "its hind legs.");

const u8 gIvysaurPokedexTextUnused[] = _("");

const u8 gVenusaurPokedexText[] = _(
    "The plant blooms when it is\n"
    "absorbing solar energy.\n"
    "It stays on the move to seek\n"
    "sunlight.");

const u8 gVenusaurPokedexTextUnused[] = _("");

const u8 gCharmanderPokedexText[] = _(
    "Obviously prefers hot\n"
    "places. When is rains, steam\n"
    "is said to spout from the\n"
    "tip of its tail.");

const u8 gCharmanderPokedexTextUnused[] = _("");

const u8 gCharmeleonPokedexText[] = _(
    "When it swings its burning\n"
    "tail, it elevates the\n"
    "temperature to unbearably\n"
    "high levels.");

const u8 gCharmeleonPokedexTextUnused[] = _("");

const u8 gCharizardPokedexText[] = _(
    "Spits fire that is hot\n"
    "enough to melt boulders.\n"
    "Known to cause forest fires\n"
    "unintentionally.");

const u8 gCharizardPokedexTextUnused[] = _("");

const u8 gSquirtlePokedexText[] = _(
    "After birth, its back swells\n"
    "and hardens into a shell.\n"
    "Powerfully sprays foam from\n"
    "its mouth.");

const u8 gSquirtlePokedexTextUnused[] = _("");

const u8 gWartortlePokedexText[] = _(
    "Often hides in water to\n"
    "stalk unwary prey. For\n"
    "swimming fast, it moves its\n"
    "ears to maintain balance.");

const u8 gWartortlePokedexTextUnused[] = _("");

const u8 gBlastoisePokedexText[] = _(
    "A brutal POKéMON with\n"
    "pressurized water jets on\n"
    "its shell. They are used\n"
    "for high speed tackles.");

const u8 gBlastoisePokedexTextUnused[] = _("");

const u8 gCaterpiePokedexText[] = _(
    "Its short feet are tipped\n"
    "with suction pads that\n"
    "enable it to tirelessly\n"
    "climb slopes and walls.");

const u8 gCaterpiePokedexTextUnused[] = _("");

const u8 gMetapodPokedexText[] = _(
    "This POKéMON is vulnerable\n"
    "to attack while its shell\n"
    "is soft, exposing its\n"
    "weak and tender body.");

const u8 gMetapodPokedexTextUnused[] = _("");

const u8 gButterfreePokedexText[] = _(
    "In battle, it flaps its\n"
    "wings at high speed to\n"
    "release highly toxic\n"
    "dust into the air.");

const u8 gButterfreePokedexTextUnused[] = _("");

const u8 gWeedlePokedexText[] = _(
    "Often found in forests,\n"
    "eating leaves.\n"
    "It has a sharp venomous\n"
    "stinger on its head.");

const u8 gWeedlePokedexTextUnused[] = _("");

const u8 gKakunaPokedexText[] = _(
    "Almost incapable of moving,\n"
    "this POKéMON can only\n"
    "harden to protect itself\n"
    "from predators.");

const u8 gKakunaPokedexTextUnused[] = _("");

const u8 gBeedrillPokedexText[] = _(
    "Flies at high speed and\n"
    "attacks using its large\n"
    "venomous stingers on its\n"
    "forelegs and tail.");

const u8 gBeedrillPokedexTextUnused[] = _("");

const u8 gPidgeyPokedexText[] = _(
    "A common sight in forests\n"
    "and woods. It flaps its\n"
    "wings at ground level to\n"
    "kick up blinding sand.");

const u8 gPidgeyPokedexTextUnused[] = _("");

const u8 gPidgeottoPokedexText[] = _(
    "Very protective of its\n"
    "territorial area, this\n"
    "POKéMON will fiercely\n"
    "peck at any intruder.");

const u8 gPidgeottoPokedexTextUnused[] = _("");

const u8 gPidgeotPokedexText[] = _(
    "When hunting, it skims the\n"
    "surface of water at high\n"
    "speed to pick off the unwary\n"
    "prey such as MAGIKARP.");

const u8 gPidgeotPokedexTextUnused[] = _("");

const u8 gRattataPokedexText[] = _(
    "Bites anything when it\n"
    "attacks. Small and very\n"
    "quick, it is a common\n"
    "sight in many places.");

const u8 gRattataPokedexTextUnused[] = _("");

const u8 gRaticatePokedexText[] = _(
    "It uses its whiskers to\n"
    "maintain its balance.\n"
    "It apparently slows down\n"
    "if they are cut off.");

const u8 gRaticatePokedexTextUnused[] = _("");

const u8 gSpearowPokedexText[] = _(
    "Eats bugs in grassy areas.\n"
    "It has to flap its short\n"
    "wings at high speed\n"
    "to stay airborn.");

const u8 gSpearowPokedexTextUnused[] = _("");

const u8 gFearowPokedexText[] = _(
    "With its huge and\n"
    "magnificent wings, it can\n"
    "keep aloft without ever\n"
    "having to land for rest.");

const u8 gFearowPokedexTextUnused[] = _("");

const u8 gEkansPokedexText[] = _(
    "Moves silently and\n"
    "stealthily. Eats the eggs\n"
    "of birds, such as PIDGEY\n"
    "and SPEAROW, whole.");

const u8 gEkansPokedexTextUnused[] = _("");

const u8 gArbokPokedexText[] = _(
    "It is rumored that the\n"
    "ferocious warning markings\n"
    "on its belly differ from\n"
    "area to area.");

const u8 gArbokPokedexTextUnused[] = _("");

const u8 gPikachuPokedexText[] = _(
    "When several of these\n"
    "POKéMON gather, their\n"
    "electricity could build and\n"
    "cause lightning storms.");

const u8 gPikachuPokedexTextUnused[] = _("");

const u8 gRaichuPokedexText[] = _(
    "Its long tail serves as\n"
    "a ground to protect\n"
    "itself from its own\n"
    "high voltage power.");

const u8 gRaichuPokedexTextUnused[] = _("");

const u8 gSandshrewPokedexText[] = _(
    "Burrows deep underground in\n"
    "arid locations far from\n"
    "water. It only emerges to\n"
    "hunt for food.");

const u8 gSandshrewPokedexTextUnused[] = _("");

const u8 gSandslashPokedexText[] = _(
    "Curls up into a spiny ball\n"
    "when threatened.\n"
    "It can roll while curled up\n"
    "to attack or escape.");

const u8 gSandslashPokedexTextUnused[] = _("");

const u8 gNidoranFPokedexText[] = _(
    "Although small, its venemous\n"
    "barbs render this POKéMON\n"
    "dangerous. The female has\n"
    "smaller horns.");

const u8 gNidoranFPokedexTextUnused[] = _("");

const u8 gNidorinaPokedexText[] = _(
    "The femaleÙ horn develops\n"
    "slowly. Prefers physical\n"
    "attacks such as clawing and\n"
    "biting.");

const u8 gNidorinaPokedexTextUnused[] = _("");

const u8 gNidoqueenPokedexText[] = _(
    "Its hard scales provide\n"
    "strong protection. It uses\n"
    "its hefty bulk to execute\n"
    "powerful moves.");

const u8 gNidoqueenPokedexTextUnused[] = _("");

const u8 gNidoranMPokedexText[] = _(
    "Stiffens its ears to sense\n"
    "danger. The larger its\n"
    "horns, the more powerful\n"
    "its secreted venom.");

const u8 gNidoranMPokedexTextUnused[] = _("");

const u8 gNidorinoPokedexText[] = _(
    "An aggressive POKéMON that\n"
    "is quick to attack.\n"
    "The horn on its head\n"
    "secretes a powerful venom.");

const u8 gNidorinoPokedexTextUnused[] = _("");

const u8 gNidokingPokedexText[] = _(
    "It uses its powerful tail in\n"
    "battle to smash, constrict,\n"
    "then break the preyÙ bones.");

const u8 gNidokingPokedexTextUnused[] = _("");

const u8 gClefairyPokedexText[] = _(
    "Its magical and cute appeal\n"
    "has many admirers.\n"
    "It is rare and found only\n"
    "in certain areas.");

const u8 gClefairyPokedexTextUnused[] = _("");

const u8 gClefablePokedexText[] = _(
    "A timid fairy POKéMON that\n"
    "is rarely seen.\n"
    "It will run and hide the\n"
    "moment it senses people.");

const u8 gClefablePokedexTextUnused[] = _("");

const u8 gVulpixPokedexText[] = _(
    "At the time of birth, it has\n"
    "just one tail.\n"
    "The tail splits from its\n"
    "tip as it grows older.");

const u8 gVulpixPokedexTextUnused[] = _("");

const u8 gNinetalesPokedexText[] = _(
    "Very smart and very\n"
    "vengeful. Grabbing one of\n"
    "its many tails could result\n"
    "in a 1000-year curse.");

const u8 gNinetalesPokedexTextUnused[] = _("");

const u8 gJigglypuffPokedexText[] = _(
    "When its huge eyes light up,\n"
    "it sings a mysteriously\n"
    "soothing melody that lulls\n"
    "its prey to sleep.");

const u8 gJigglypuffPokedexTextUnused[] = _("");

const u8 gWigglytuffPokedexText[] = _(
    "The body is soft and\n"
    "rubbery. When angered, it\n"
    "will inflate itself to\n"
    "an enormous size.");

const u8 gWigglytuffPokedexTextUnused[] = _("");

const u8 gZubatPokedexText[] = _(
    "Forms colonies in perpetual-\n"
    "ly dark places. Uses\n"
    "ultrasonic waves to identify\n"
    "and approach targets.");

const u8 gZubatPokedexTextUnused[] = _("");

const u8 gGolbatPokedexText[] = _(
    "Once it strikes, it will\n"
    "not stop draining energy\n"
    "from the victim even if it\n"
    "gets too heavy to fly.");

const u8 gGolbatPokedexTextUnused[] = _("");

const u8 gOddishPokedexText[] = _(
    "During the day, it keeps its\n"
    "face buried in the ground.\n"
    "At night, it wanders around\n"
    "around sowing its seeds.");

const u8 gOddishPokedexTextUnused[] = _("");

const u8 gGloomPokedexText[] = _(
    "The fluid that oozes from\n"
    "its mouth isnÑ drool.\n"
    "It is a nectar that is\n"
    "used to attract prey.");

const u8 gGloomPokedexTextUnused[] = _("");

const u8 gVileplumePokedexText[] = _(
    "The larger its petals, the\n"
    "more toxic pollen it\n"
    "contains. Its big head is\n"
    "heavy and hard to hold up.");

const u8 gVileplumePokedexTextUnused[] = _("");

const u8 gParasPokedexText[] = _(
    "Burrows to suck tree roots.\n"
    "The mushrooms on its back\n"
    "grow by drawing nutrients\n"
    "from the bug host.");

const u8 gParasPokedexTextUnused[] = _("");

const u8 gParasectPokedexText[] = _(
    "A host-parasite pair in\n"
    "which the parasite mushroom\n"
    "has taken over the host\n"
    "bug. Prefers damp places.");

const u8 gParasectPokedexTextUnused[] = _("");

const u8 gVenonatPokedexText[] = _(
    "Lives in the shadows of tall\n"
    "trees where it eats insects.\n"
    "It is attracted by\n"
    "light at night.");

const u8 gVenonatPokedexTextUnused[] = _("");

const u8 gVenomothPokedexText[] = _(
    "The dust-like scales\n"
    "covering its wings are color\n"
    "coded to indicate the kinds\n"
    "of poison it has.");

const u8 gVenomothPokedexTextUnused[] = _("");

const u8 gDiglettPokedexText[] = _(
    "Lives about one yard\n"
    "underground where it feeds\n"
    "on plant roots. It sometimes\n"
    "appears above ground.");

const u8 gDiglettPokedexTextUnused[] = _("");

const u8 gDugtrioPokedexText[] = _(
    "A team of DIGLETT triplets.\n"
    "It triggers huge earthquakes\n"
    "by burrowing 50 miles\n"
    "underground.");

const u8 gDugtrioPokedexTextUnused[] = _("");

const u8 gMeowthPokedexText[] = _(
    "Adores circular objects.\n"
    "Wanders the streets on a\n"
    "nightly basis to look for\n"
    "dropped loose change.");

const u8 gMeowthPokedexTextUnused[] = _("");

const u8 gPersianPokedexText[] = _(
    "Although its fur has many\n"
    "admirers, it is tough to\n"
    "raise as a pet because of\n"
    "its fickle meanness.");

const u8 gPersianPokedexTextUnused[] = _("");

const u8 gPsyduckPokedexText[] = _(
    "While lulling its enemies\n"
    "with its vacant look, this\n"
    "wily POKéMON will use\n"
    "psychokinetic powers.");

const u8 gPsyduckPokedexTextUnused[] = _("");

const u8 gGolduckPokedexText[] = _(
    "Often seen swimming\n"
    "elegantly by lake shores.\n"
    "It is often mistaken for the\n"
    "Japanese monster, Kappa.");

const u8 gGolduckPokedexTextUnused[] = _("");

const u8 gMankeyPokedexText[] = _(
    "Extremely quick to anger.\n"
    "It could be docile one\n"
    "moment then thrashing away\n"
    "away the next instant.");

const u8 gMankeyPokedexTextUnused[] = _("");

const u8 gPrimeapePokedexText[] = _(
    "Always furious and tenacious\n"
    "to boot. It will not abandon\n"
    "chasing its quarry until\n"
    "it is caught.");

const u8 gPrimeapePokedexTextUnused[] = _("");

const u8 gGrowlithePokedexText[] = _(
    "Very protective of its\n"
    "territory. It will bark and\n"
    "bite to repel intruders\n"
    "from its space.");

const u8 gGrowlithePokedexTextUnused[] = _("");

const u8 gArcaninePokedexText[] = _(
    "A POKéMON that has been\n"
    "admired since the past for\n"
    "its beauty. It runs agilely\n"
    "as if on wings.");

const u8 gArcaninePokedexTextUnused[] = _("");

const u8 gPoliwagPokedexText[] = _(
    "Its newly grown legs prevent\n"
    "it from running. It appears\n"
    "to prefer swimming than\n"
    "trying to stand.");

const u8 gPoliwagPokedexTextUnused[] = _("");

const u8 gPoliwhirlPokedexText[] = _(
    "Capable of living in or\n"
    "out of water. When out of\n"
    "water, it sweats to keep\n"
    "its body slimy.");

const u8 gPoliwhirlPokedexTextUnused[] = _("");

const u8 gPoliwrathPokedexText[] = _(
    "An adept swimmer at both the\n"
    "front crawl and breast\n"
    "stroke. Easily overtakes\n"
    "the best human swimmers.");

const u8 gPoliwrathPokedexTextUnused[] = _("");

const u8 gAbraPokedexText[] = _(
    "Using its ability to read\n"
    "minds, it will identify\n"
    "impending danger and\n"
    "TELEPORT to safety.");

const u8 gAbraPokedexTextUnused[] = _("");

const u8 gKadabraPokedexText[] = _(
    "It emits special alpha waves\n"
    "from its body that induce\n"
    "headaches just by\n"
    "being close by.");

const u8 gKadabraPokedexTextUnused[] = _("");

const u8 gAlakazamPokedexText[] = _(
    "Its brain can outperform a\n"
    "super-computer. Its\n"
    "intelligence quotient is\n"
    "said to be over 5,000.");

const u8 gAlakazamPokedexTextUnused[] = _("");

const u8 gMachopPokedexText[] = _(
    "Loves to build its muscles.\n"
    "It trains in all styles of\n"
    "martial arts to become\n"
    "even stronger.");

const u8 gMachopPokedexTextUnused[] = _("");

const u8 gMachokePokedexText[] = _(
    "Its muscular body is so\n"
    "powerful, it must wear a\n"
    "power save belt to be able\n"
    "to regulate its motions.");

const u8 gMachokePokedexTextUnused[] = _("");

const u8 gMachampPokedexText[] = _(
    "Using its heavy muscles, it\n"
    "throws powerful punches that\n"
    "can send the victim clear\n"
    "over the horizon.");

const u8 gMachampPokedexTextUnused[] = _("");

const u8 gBellsproutPokedexText[] = _(
    "A carnivorous POKéMON that\n"
    "traps and eats bugs. It\n"
    "uses its root feet to soak\n"
    "up needed moisture.");

const u8 gBellsproutPokedexTextUnused[] = _("");

const u8 gWeepinbellPokedexText[] = _(
    "It spits out POISONPOWDER to\n"
    "immobalize the enemy and\n"
    "then finishes it with a\n"
    "spray of ACID.");

const u8 gWeepinbellPokedexTextUnused[] = _("");

const u8 gVictreebelPokedexText[] = _(
    "Said to live in huge\n"
    "colonies deep in jungles,\n"
    "although no one has ever\n"
    "returned from there.");

const u8 gVictreebelPokedexTextUnused[] = _("");

const u8 gTentacoolPokedexText[] = _(
    "Drifts in shallow seas.\n"
    "Anglers who hook them by\n"
    "accident are often punished\n"
    "by its stinging acid.");

const u8 gTentacoolPokedexTextUnused[] = _("");

const u8 gTentacruelPokedexText[] = _(
    "The tentacles are normally\n"
    "kept short. On hunts, they\n"
    "are extended to ensnare\n"
    "and immobilize prey.");

const u8 gTentacruelPokedexTextUnused[] = _("");

const u8 gGeodudePokedexText[] = _(
    "Found in fields and\n"
    "mountains. Mistaking them\n"
    "for boulders, people often\n"
    "step or trip on them.");

const u8 gGeodudePokedexTextUnused[] = _("");

const u8 gGravelerPokedexText[] = _(
    "Rolls down slopes to move.\n"
    "It rolls over any obstacle\n"
    "without slowing or changing\n"
    "its direction.");

const u8 gGravelerPokedexTextUnused[] = _("");

const u8 gGolemPokedexText[] = _(
    "Its boulder-like body is\n"
    "extremely hard. It can easi-\n"
    "ly withstand dynamite blasts\n"
    "without taking damage.");

const u8 gGolemPokedexTextUnused[] = _("");

const u8 gPonytaPokedexText[] = _(
    "Its hooves are 10 times\n"
    "harder than diamonds. It can\n"
    "trample anything completely\n"
    "flat in little time.");

const u8 gPonytaPokedexTextUnused[] = _("");

const u8 gRapidashPokedexText[] = _(
    "Very competitive, this\n"
    "POKéMON will chase anything\n"
    "that moves fast in the\n"
    "hopes of racing it.");

const u8 gRapidashPokedexTextUnused[] = _("");

const u8 gSlowpokePokedexText[] = _(
    "Incredibly slow and dopey.\n"
    "It takes 5 seconds for it to\n"
    "feel pain when under attack.");

const u8 gSlowpokePokedexTextUnused[] = _("");

const u8 gSlowbroPokedexText[] = _(
    "The SHELLDER that latched\n"
    "onto its tail is said to\n"
    "feed on the hostÙ left\n"
    "over scraps.");

const u8 gSlowbroPokedexTextUnused[] = _("");

const u8 gMagnemitePokedexText[] = _(
    "Uses anti-gravity to stay\n"
    "suspended. Appears without\n"
    "warning and uses THUNDER\n"
    "WAVE and similar moves.");

const u8 gMagnemitePokedexTextUnused[] = _("");

const u8 gMagnetonPokedexText[] = _(
    "Formed by several MAGNEMITEs\n"
    "linked together. They\n"
    "frequently appear when\n"
    "sunspots flare up.");

const u8 gMagnetonPokedexTextUnused[] = _("");

const u8 gFarfetchdPokedexText[] = _(
    "The sprig of green onions\n"
    "it holds is its weapon.\n"
    "It is used much like a\n"
    "metal sword.");

const u8 gFarfetchdPokedexTextUnused[] = _("");

const u8 gDoduoPokedexText[] = _(
    "A bird that makes up for\n"
    "its poor flying with its\n"
    "fast foot speed.\n"
    "Leaves giant footprints.");

const u8 gDoduoPokedexTextUnused[] = _("");

const u8 gDodrioPokedexText[] = _(
    "Uses its three brains to\n"
    "execute complex plans.\n"
    "While two heads sleep, one\n"
    "head stays awake.");

const u8 gDodrioPokedexTextUnused[] = _("");

const u8 gSeelPokedexText[] = _(
    "The protruding horn on its\n"
    "head is very hard.\n"
    "It is used for bashing\n"
    "through thick ice.");

const u8 gSeelPokedexTextUnused[] = _("");

const u8 gDewgongPokedexText[] = _(
    "Stores thermal energy in its\n"
    "body. Swims at a steady 8\n"
    "knots even in intensely\n"
    "cold waters.");

const u8 gDewgongPokedexTextUnused[] = _("");

const u8 gGrimerPokedexText[] = _(
    "Appears in filthy areas.\n"
    "Thrives by sucking up\n"
    "polluted sludge that is\n"
    "pumped out of factories.");

const u8 gGrimerPokedexTextUnused[] = _("");

const u8 gMukPokedexText[] = _(
    "Thickly covered with a\n"
    "filthy, vile sludge.\n"
    "It is so toxic, even its\n"
    "footprints contain poison.");

const u8 gMukPokedexTextUnused[] = _("");

const u8 gShellderPokedexText[] = _(
    "Its hard shell repels any\n"
    "kind of attack. It is\n"
    "vulnerable only when\n"
    "its shell is open.");

const u8 gShellderPokedexTextUnused[] = _("");

const u8 gCloysterPokedexText[] = _(
    "When attacked, it launches\n"
    "its horns in quick volleys.\n"
    "Its innards have never\n"
    "been seen.");

const u8 gCloysterPokedexTextUnused[] = _("");

const u8 gGastlyPokedexText[] = _(
    "Almost invisible, this\n"
    "gaseous POKéMON cloaks the\n"
    "target and puts it to\n"
    "sleep without notice.");

const u8 gGastlyPokedexTextUnused[] = _("");

const u8 gHaunterPokedexText[] = _(
    "Because of its ability to\n"
    "slip through block walls,\n"
    "it is said to be from\n"
    "another dimension.");

const u8 gHaunterPokedexTextUnused[] = _("");

const u8 gGengarPokedexText[] = _(
    "Under a full moon, this\n"
    "POKéMON likes to mimic the\n"
    "shadows of people and laugh\n"
    "at their fright.");

const u8 gGengarPokedexTextUnused[] = _("");

const u8 gOnixPokedexText[] = _(
    "As it grows, the stone\n"
    "portions of its body harden\n"
    "to become similar to\n"
    "diamond, but colored black.");

const u8 gOnixPokedexTextUnused[] = _("");

const u8 gDrowzeePokedexText[] = _(
    "Put enemies to sleep then\n"
    "eats their dreams.\n"
    "Occasionally gets sick\n"
    "from eating bad dreams.");

const u8 gDrowzeePokedexTextUnused[] = _("");

const u8 gHypnoPokedexText[] = _(
    "When it locks eyes with\n"
    "an enemy, it will use a mix\n"
    "of PSI moves such as\n"
    "HYPNOSIS and CONFUSION.");

const u8 gHypnoPokedexTextUnused[] = _("");

const u8 gKrabbyPokedexText[] = _(
    "Its pincers are not only\n"
    "powerful weapons, they are\n"
    "used for balance when\n"
    "walking sideways.");

const u8 gKrabbyPokedexTextUnused[] = _("");

const u8 gKinglerPokedexText[] = _(
    "The large pincer has 10000\n"
    "hp of crushing power.\n"
    "However, its huge size makes\n"
    "it unwieldy to use.");

const u8 gKinglerPokedexTextUnused[] = _("");

const u8 gVoltorbPokedexText[] = _(
    "Usually found in power\n"
    "plants. Easily mistaken for\n"
    "a POKé BALL, they have\n"
    "zapped many people.");

const u8 gVoltorbPokedexTextUnused[] = _("");

const u8 gElectrodePokedexText[] = _(
    "It stores electric energy\n"
    "under very high pressure.\n"
    "It often explodes with\n"
    "little or no provocation.");

const u8 gElectrodePokedexTextUnused[] = _("");

const u8 gExeggcutePokedexText[] = _(
    "Oftn mistaken for eggs.\n"
    "When disturbed, they quickly\n"
    "gather in swarms and attack.");

const u8 gExeggcutePokedexTextUnused[] = _("");

const u8 gExeggutorPokedexText[] = _(
    "Legend has it that on rare\n"
    "occasions, one of its heads\n"
    "will drop off and continue\n"
    "on as an EXEGGCUTE.");

const u8 gExeggutorPokedexTextUnused[] = _("");

const u8 gCubonePokedexText[] = _(
    "Because it never removes its\n"
    "skull helmet, no one has\n"
    "has ever seen this POKéMONÙ\n"
    "real face.");

const u8 gCubonePokedexTextUnused[] = _("");

const u8 gMarowakPokedexText[] = _(
    "The bone it holds is its key\n"
    "weapon. It throws the bone\n"
    "skillfully like a boomerang\n"
    "to KO targets.");

const u8 gMarowakPokedexTextUnused[] = _("");

const u8 gHitmonleePokedexText[] = _(
    "When in a hurry, its legs\n"
    "lengthen progressively.\n"
    "It runs smoothly with extra\n"
    "long, loping strides.");

const u8 gHitmonleePokedexTextUnused[] = _("");

const u8 gHitmonchanPokedexText[] = _(
    "While apparently doing\n"
    "nothing, it fires punches in\n"
    "lightning fast volleys that\n"
    "are impossible to see.");

const u8 gHitmonchanPokedexTextUnused[] = _("");

const u8 gLickitungPokedexText[] = _(
    "Its tongue can be extended\n"
    "like a chameleonÙ. it\n"
    "leaves a tingling sensation\n"
    "when it licks enemies.");

const u8 gLickitungPokedexTextUnused[] = _("");

const u8 gKoffingPokedexText[] = _(
    "Because it stores several\n"
    "kinds of toxic gases in its\n"
    "body, it is prone to\n"
    "exploding without warning.");

const u8 gKoffingPokedexTextUnused[] = _("");

const u8 gWeezingPokedexText[] = _(
    "Where two kinds of poison\n"
    "gases meet, 2 KOFFINGs\n"
    "can fuse into a WEEZING\n"
    "over many years.");

const u8 gWeezingPokedexTextUnused[] = _("");

const u8 gRhyhornPokedexText[] = _(
    "Its massive bones are 1000\n"
    "times harder than human\n"
    "bones. It can easily knock\n"
    "a trailer flying.");

const u8 gRhyhornPokedexTextUnused[] = _("");

const u8 gRhydonPokedexText[] = _(
    "Protected by an armor-like\n"
    "hide, it is capable of\n"
    "living in molten lava of\n"
    "3,600 degrees.");

const u8 gRhydonPokedexTextUnused[] = _("");

const u8 gChanseyPokedexText[] = _(
    "A rare and elusive POKéMON\n"
    "that is said to bring\n"
    "happiness to those who\n"
    "manage to get it.");

const u8 gChanseyPokedexTextUnused[] = _("");

const u8 gTangelaPokedexText[] = _(
    "The whole body is swathed\n"
    "with wide vines that are\n"
    "similar to seaweed. Its\n"
    "vines shake as it walks.");

const u8 gTangelaPokedexTextUnused[] = _("");

const u8 gKangaskhanPokedexText[] = _(
    "The infant rarely ventures\n"
    "out of its motherÙ\n"
    "protective pouch until\n"
    "it is 3 years old.");

const u8 gKangaskhanPokedexTextUnused[] = _("");

const u8 gHorseaPokedexText[] = _(
    "Known to shoot down flying\n"
    "bugs with precision blasts\n"
    "of ink from the surface\n"
    "of the water.");

const u8 gHorseaPokedexTextUnused[] = _("");

const u8 gSeadraPokedexText[] = _(
    "Capable of swimming backward\n"
    "by rapidly flapping its\n"
    "wing-like pectoral fins\n"
    "and stout tail.");

const u8 gSeadraPokedexTextUnused[] = _("");

const u8 gGoldeenPokedexText[] = _(
    "Its tail fin billows like an\n"
    "elegant ballroom dress,\n"
    "giving it the nickname of\n"
    "the water Queen.");

const u8 gGoldeenPokedexTextUnused[] = _("");

const u8 gSeakingPokedexText[] = _(
    "In the autumn spawning\n"
    "season, they can be seen\n"
    "swimming powerfully up\n"
    "rivers and creeks.");

const u8 gSeakingPokedexTextUnused[] = _("");

const u8 gStaryuPokedexText[] = _(
    "An enigmatic POKéMON that\n"
    "can effortlessly regenerate\n"
    "any appendage it loses\n"
    "in battle.");

const u8 gStaryuPokedexTextUnused[] = _("");

const u8 gStarmiePokedexText[] = _(
    "Its central core glows with\n"
    "the colors of the rainbow.\n"
    "Some people value the\n"
    "core as a gem.");

const u8 gStarmiePokedexTextUnused[] = _("");

const u8 gMrmimePokedexText[] = _(
    "If interrupted while it is\n"
    "miming, it will slap around\n"
    "the offender with its\n"
    "broad hands.");

const u8 gMrmimePokedexTextUnused[] = _("");

const u8 gScytherPokedexText[] = _(
    "With ninja-like agility and\n"
    "speed, is can create the\n"
    "illusion that there is\n"
    "more than one.");

const u8 gScytherPokedexTextUnused[] = _("");

const u8 gJynxPokedexText[] = _(
    "It seductively wiggles its\n"
    "hips as it walks. It can\n"
    "cause people to dance in\n"
    "unison with it.");

const u8 gJynxPokedexTextUnused[] = _("");

const u8 gElectabuzzPokedexText[] = _(
    "Normally found near power\n"
    "plants, they can wander away\n"
    "and cause major blackouts\n"
    "in cities.");

const u8 gElectabuzzPokedexTextUnused[] = _("");

const u8 gMagmarPokedexText[] = _(
    "Its body always burns with\n"
    "an orange glow that enables\n"
    "it to hide perfectly\n"
    "among flames.");

const u8 gMagmarPokedexTextUnused[] = _("");

const u8 gPinsirPokedexText[] = _(
    "If it fails to crush the\n"
    "victim in its pincers,\n"
    "it will swing it around\n"
    "and toss it hard.");

const u8 gPinsirPokedexTextUnused[] = _("");

const u8 gTaurosPokedexText[] = _(
    "When it targets an enemy, it\n"
    "charges furiously while\n"
    "whipping its body with\n"
    "its long tails.");

const u8 gTaurosPokedexTextUnused[] = _("");

const u8 gMagikarpPokedexText[] = _(
    "In the distant past, it was\n"
    "somewhat stonger than the\n"
    "horribly weak descendants\n"
    "that exist today.");

const u8 gMagikarpPokedexTextUnused[] = _("");

const u8 gGyaradosPokedexText[] = _(
    "Rarely seen in the wild.\n"
    "Huge and vicious, it is\n"
    "capable of destroying entire\n"
    "cities in a rage.");

const u8 gGyaradosPokedexTextUnused[] = _("");

const u8 gLaprasPokedexText[] = _(
    "A POKéMON that has been\n"
    "overhunted almost to\n"
    "extinction. It can ferry\n"
    "people across the water.");

const u8 gLaprasPokedexTextUnused[] = _("");

const u8 gDittoPokedexText[] = _(
    "Capable of copying an\n"
    "enemyÙ genetic code to\n"
    "transform itself into a\n"
    "duplicate of the enemy.");

const u8 gDittoPokedexTextUnused[] = _("");

const u8 gEeveePokedexText[] = _(
    "Its genetic code is\n"
    "irregular. It may mutate if\n"
    "it is exposed to radiation\n"
    "from element STONEs.");

const u8 gEeveePokedexTextUnused[] = _("");

const u8 gVaporeonPokedexText[] = _(
    "Lives close to water. Its\n"
    "long tail is ridged with a\n"
    "fin which is often mistaken\n"
    "for a mermaidÙ.");

const u8 gVaporeonPokedexTextUnused[] = _("");

const u8 gJolteonPokedexText[] = _(
    "It accumulates negative ions\n"
    "in the atmosphere to\n"
    "blast out 10000-volt\n"
    "lightning bolts.");

const u8 gJolteonPokedexTextUnused[] = _("");

const u8 gFlareonPokedexText[] = _(
    "When storing thermal energy\n"
    "in its body, its temperature\n"
    "could soar to over\n"
    "1600 degrees.");

const u8 gFlareonPokedexTextUnused[] = _("");

const u8 gPorygonPokedexText[] = _(
    "A POKéMON that consists\n"
    "entirely of programming\n"
    "code. Capable of moving\n"
    "freely in cyberspace.");

const u8 gPorygonPokedexTextUnused[] = _("");

const u8 gOmanytePokedexText[] = _(
    "Although long extinct, in\n"
    "rare cases, it can be\n"
    "genetically resurrected\n"
    "from fossils.");

const u8 gOmanytePokedexTextUnused[] = _("");

const u8 gOmastarPokedexText[] = _(
    "A prehistoric POKéMON that\n"
    "died out when its heavy\n"
    "shell made it impossible\n"
    "to catch prey.");

const u8 gOmastarPokedexTextUnused[] = _("");

const u8 gKabutoPokedexText[] = _(
    "A POKéMON that was\n"
    "resurrected from a fossil\n"
    "found in what was once\n"
    "the ocean long ago.");

const u8 gKabutoPokedexTextUnused[] = _("");

const u8 gKabutopsPokedexText[] = _(
    "Its sleek shape is perfect\n"
    "for swimming. It slashes\n"
    "prey with its claws and\n"
    "drains the body fluids.");

const u8 gKabutopsPokedexTextUnused[] = _("");

const u8 gAerodactylPokedexText[] = _(
    "A ferocious prehistoric\n"
    "POKéMON that goes for the\n"
    "enemyÙ throat with its\n"
    "serrated saw-like fangs.");

const u8 gAerodactylPokedexTextUnused[] = _("");

const u8 gSnorlaxPokedexText[] = _(
    "Very lazy. Just eats and\n"
    "sleeps. As its rotund bulk\n"
    "builds, it becomes steadily\n"
    "more slothful.");

const u8 gSnorlaxPokedexTextUnused[] = _("");

const u8 gArticunoPokedexText[] = _(
    "A legendary bird POKéMON\n"
    "that is said to appear to\n"
    "doomed people who are lost\n"
    "in icy mountains.");

const u8 gArticunoPokedexTextUnused[] = _("");

const u8 gZapdosPokedexText[] = _(
    "A legendary bird POKéMON\n"
    "that is said to appear from\n"
    "clouds while dropping\n"
    "enormous lightning bolts.");

const u8 gZapdosPokedexTextUnused[] = _("");

const u8 gMoltresPokedexText[] = _(
    "Known as the legendary bird\n"
    "of fire. Every flap of its\n"
    "wings creates a dazzling\n"
    "flash of flames.");

const u8 gMoltresPokedexTextUnused[] = _("");

const u8 gDratiniPokedexText[] = _(
    "Long considered a mythical\n"
    "POKéMON until recently when\n"
    "a small colony was found\n"
    "living underwater.");

const u8 gDratiniPokedexTextUnused[] = _("");

const u8 gDragonairPokedexText[] = _(
    "A mystical POKéMON that\n"
    "exudes a gentle aura.\n"
    "Has the ability to change\n"
    "climate conditions.");

const u8 gDragonairPokedexTextUnused[] = _("");

const u8 gDragonitePokedexText[] = _(
    "An extremely rarely seen\n"
    "marine POKéMON.\n"
    "Its intelligence is said\n"
    "to match that of humans.");

const u8 gDragonitePokedexTextUnused[] = _("");

const u8 gMewtwoPokedexText[] = _(
    "It was created by a\n"
    "scientist after years of\n"
    "horrific gene splicing and\n"
    "DNA engineering experiments.");

const u8 gMewtwoPokedexTextUnused[] = _("");

const u8 gMewPokedexText[] = _(
    "So rare that it is still\n"
    "said to be a mirage by many\n"
    "experts. Only a few people\n"
    "have seen it worldwide.");

const u8 gMewPokedexTextUnused[] = _("");

const u8 gChikoritaPokedexText[] = _(
#if REVISION == 0
    "Its pleasantly aromatic leaves have the\n"
    #else
    "Its pleasantly aromatic leaf has the\n"
    #endif
    "ability to check the humidity and\n"
    "temperature."
);

const u8 gChikoritaPokedexTextUnused[] = _("");

const u8 gBayleefPokedexText[] = _(
    "A spicy aroma emanates from around its\n"
    "neck. The aroma acts as a stimulant to\n"
    "restore health.");

const u8 gBayleefPokedexTextUnused[] = _("");

const u8 gMeganiumPokedexText[] = _(
    "MEGANIUMÙ breath has the power to revive\n"
    "dead grass and plants. It can make them\n"
    "healthy again.");

const u8 gMeganiumPokedexTextUnused[] = _("");

const u8 gCyndaquilPokedexText[] = _(
    "It usually stays hunched over. If it is\n"
    "angry or surprised, it shoots flames out\n"
    "of its back.");

const u8 gCyndaquilPokedexTextUnused[] = _("");

const u8 gQuilavaPokedexText[] = _(
    "This POKéMON is fully covered by\n"
    "nonflammable fur. It can withstand any\n"
    "kind of fire attack.");

const u8 gQuilavaPokedexTextUnused[] = _("");

const u8 gTyphlosionPokedexText[] = _(
    "It has a secret, devastating move. It\n"
    "rubs its blazing fur together to cause\n"
    "huge explosions.");

const u8 gTyphlosionPokedexTextUnused[] = _("");

const u8 gTotodilePokedexText[] = _(
    "It is small but rough and tough. It wonÑ\n"
    "hesitate to take a bite out of anything\n"
    "that moves.");

const u8 gTotodilePokedexTextUnused[] = _("");

const u8 gCroconawPokedexText[] = _(
    "It opens its huge jaws wide when\n"
    "attacking. If it loses any fangs while\n"
    "biting, they grow back in.");

const u8 gCroconawPokedexTextUnused[] = _("");

const u8 gFeraligatrPokedexText[] = _(
    "It is hard for it to support its own\n"
    "weight out of water, so it gets down on\n"
    "all fours. But it moves fast.");

const u8 gFeraligatrPokedexTextUnused[] = _("");

const u8 gSentretPokedexText[] = _(
    "It stands on its tail so it can see a long\n"
    "way. If it spots an enemy, it cries loudly\n"
    "to warn its kind.");

const u8 gSentretPokedexTextUnused[] = _("");

const u8 gFurretPokedexText[] = _(
    "There is no telling where its tail begins.\n"
    "Despite its short legs, it is quick at\n"
    "hunting RATTATA.");

const u8 gFurretPokedexTextUnused[] = _("");

const u8 gHoothootPokedexText[] = _(
    "It has a perfect sense of time. Whatever\n"
    "happens, it keeps rhythm by precisely\n"
    "tilting its head in time.");

const u8 gHoothootPokedexTextUnused[] = _("");

const u8 gNoctowlPokedexText[] = _(
    "When it needs to think, it rotates its head\n"
    "180 degrees to sharpen its intellectual\n"
    "power.");

const u8 gNoctowlPokedexTextUnused[] = _("");

const u8 gLedybaPokedexText[] = _(
    "When the weather turns cold, numerous\n"
    "LEDYBA gather from everywhere to cluster\n"
    "and keep each other warm.");

const u8 gLedybaPokedexTextUnused[] = _("");

const u8 gLedianPokedexText[] = _(
    "The star patterns on its back grow larger\n"
    "or smaller depending on the number of\n"
    "stars in the night sky.");

const u8 gLedianPokedexTextUnused[] = _("");

const u8 gSpinarakPokedexText[] = _(
    "It spins a web using fine--but durable--\n"
    "thread. It then waits patiently for prey\n"
    "to be trapped.");

const u8 gSpinarakPokedexTextUnused[] = _("");

const u8 gAriadosPokedexText[] = _(
    "A single strand of a special string is\n"
    "endlessly spun out of its rear. The string\n"
    "leads back to its nest.");

const u8 gAriadosPokedexTextUnused[] = _("");

const u8 gCrobatPokedexText[] = _(
    "The development of wings on its legs\n"
    "enables it to fly fast but also makes it\n"
    "tough to stop and rest.");

const u8 gCrobatPokedexTextUnused[] = _("");

const u8 gChinchouPokedexText[] = _(
    "On the dark ocean floor, its only means\n"
    "of communication is its constantly\n"
    "flashing lights.");

const u8 gChinchouPokedexTextUnused[] = _("");

const u8 gLanturnPokedexText[] = _(
    "It blinds prey with an intense burst of\n"
    "light, then swallows the immobilized prey\n"
    "in a single gulp.");

const u8 gLanturnPokedexTextUnused[] = _("");

const u8 gPichuPokedexText[] = _(
    "Despite its small size, it can zap even\n"
    "adult humans. However, if it does so, it\n"
    "also surprises itself.");

const u8 gPichuPokedexTextUnused[] = _("");

const u8 gCleffaPokedexText[] = _(
    "When numerous meteors illuminate the\n"
    "night sky, sightings of CLEFFA strangely\n"
    "increase.");

const u8 gCleffaPokedexTextUnused[] = _("");

const u8 gIgglybuffPokedexText[] = _(
    "Its extremely flexible and elastic body\n"
    "makes it bounce continuously--anytime,\n"
    "anywhere.");

const u8 gIgglybuffPokedexTextUnused[] = _("");

const u8 gTogepiPokedexText[] = _(
    "A proverb claims that happiness will come\n"
    "to anyone who can make a sleeping TOGEPI\n"
    "stand up.");

const u8 gTogepiPokedexTextUnused[] = _("");

const u8 gTogeticPokedexText[] = _(
    "It grows dispirited if it is not with kind\n"
    "people. It can float in midair without\n"
    "moving its wings.");

const u8 gTogeticPokedexTextUnused[] = _("");

const u8 gNatuPokedexText[] = _(
    "It usually forages for food on the ground\n"
    "but may, on rare occasions, hop onto\n"
    "branches to peck at shoots.");

const u8 gNatuPokedexTextUnused[] = _("");

const u8 gXatuPokedexText[] = _(
    "In South America, it is said that its right\n"
    "eye sees the future and its left eye\n"
    "views the past.");

const u8 gXatuPokedexTextUnused[] = _("");

const u8 gMareepPokedexText[] = _(
    "Its fleece grows continually. In the\n"
    "summer, the fleece is fully shed, but it\n"
    "grows back in a week.");

const u8 gMareepPokedexTextUnused[] = _("");

const u8 gFlaaffyPokedexText[] = _(
    "Its fluffy fleece easily stores\n"
    "electricity. Its rubbery hide keeps it\n"
    "from being electrocuted.");

const u8 gFlaaffyPokedexTextUnused[] = _("");

const u8 gAmpharosPokedexText[] = _(
    "The bright light on its tail can be seen\n"
    "far away. It has been treasured since\n"
    "ancient times as a beacon.");

const u8 gAmpharosPokedexTextUnused[] = _("");

const u8 gBellossomPokedexText[] = _(
    "Plentiful in the tropics. When it dances,\n"
    "its petals rub together and make a\n"
    "pleasant ringing sound.");

const u8 gBellossomPokedexTextUnused[] = _("");

const u8 gMarillPokedexText[] = _(
    "The end of its tail serves as a buoy that\n"
    "keeps it from drowning, even in a vicious\n"
    "current.");

const u8 gMarillPokedexTextUnused[] = _("");

const u8 gAzumarillPokedexText[] = _(
    "When it plays in water, it rolls up its\n"
    "elongated ears to prevent their insides\n"
    "from getting wet.");

const u8 gAzumarillPokedexTextUnused[] = _("");

const u8 gSudowoodoPokedexText[] = _(
    "It disguises itself as a tree to avoid\n"
    "attack. It hates water, so it will\n"
    "disappear if it starts raining.");

const u8 gSudowoodoPokedexTextUnused[] = _("");

const u8 gPolitoedPokedexText[] = _(
    "Whenever three or more of these get\n"
    "together, they sing in a loud voice that\n"
    "sounds like bellowing.");

const u8 gPolitoedPokedexTextUnused[] = _("");

const u8 gHoppipPokedexText[] = _(
    "Its body is so light, it must grip the\n"
    "ground firmly with its feet to keep from\n"
    "being blown away.");

const u8 gHoppipPokedexTextUnused[] = _("");

const u8 gSkiploomPokedexText[] = _(
    "It spreads its petals to absorb sunlight.\n"
    "It also floats in the air to get closer to\n"
    "the sun.");

const u8 gSkiploomPokedexTextUnused[] = _("");

const u8 gJumpluffPokedexText[] = _(
    "It drifts on seasonal winds and spreads\n"
    "its cotton-like spores all over the world \n"
    "to make more offspring.");

const u8 gJumpluffPokedexTextUnused[] = _("");

const u8 gAipomPokedexText[] = _(
    "It lives atop tall trees. When leaping\n"
    "from branch to branch, it deftly uses its\n"
    "tail for balance.");

const u8 gAipomPokedexTextUnused[] = _("");

const u8 gSunkernPokedexText[] = _(
    "It lives by drinking only dewdrops from\n"
    "under the leaves of plants. It is said\n"
    "that it eats nothing else.");

const u8 gSunkernPokedexTextUnused[] = _("");

const u8 gSunfloraPokedexText[] = _(
    "In the daytime, it rushes about in a\n"
    "hectic manner, but it comes to a complete\n"
    "stop when the sun sets.");

const u8 gSunfloraPokedexTextUnused[] = _("");

const u8 gYanmaPokedexText[] = _(
    "Its large eyes can scan 360 degrees.\n"
    "It looks in all directions to seek out\n"
    "insects as its prey.");

const u8 gYanmaPokedexTextUnused[] = _("");

const u8 gWooperPokedexText[] = _(
    "When it walks around on the ground,\n"
    "it coats its body with a slimy, poisonous\n"
    "film.");

const u8 gWooperPokedexTextUnused[] = _("");

const u8 gQuagsirePokedexText[] = _(
    "Due to its relaxed and carefree attitude,\n"
    "it often bumps its head on boulders and\n"
    "boat hulls as it swims.");

const u8 gQuagsirePokedexTextUnused[] = _("");

const u8 gEspeonPokedexText[] = _(
    "By reading air currents, it can predict\n"
    "things such as the weather or its foeÙ\n"
    "next move.");

const u8 gEspeonPokedexTextUnused[] = _("");

const u8 gUmbreonPokedexText[] = _(
    "When darkness falls, the rings on its body\n"
    "begin to glow, striking fear in the hearts\n"
    "of anyone nearby.");

const u8 gUmbreonPokedexTextUnused[] = _("");

const u8 gMurkrowPokedexText[] = _(
    "It is said that when chased, it lures its\n"
    "attacker onto dark mountain trails where\n"
    "the foe will get lost.");

const u8 gMurkrowPokedexTextUnused[] = _("");

const u8 gSlowkingPokedexText[] = _(
    "When its head was bitten, toxins entered\n"
    "SLOWPOKEÙ head and unlocked an\n"
    "extraordinary power.");

const u8 gSlowkingPokedexTextUnused[] = _("");

const u8 gMisdreavusPokedexText[] = _(
    "It loves to bite and yank peopleÙ hair\n"
    "from behind without warning, just to see\n"
    "their shocked reactions.");

const u8 gMisdreavusPokedexTextUnused[] = _("");

const u8 gUnownPokedexText[] = _(
    "Its flat, thin body is always stuck on\n"
    "walls. Its shape appears to have some\n"
    "meaning.");

const u8 gUnownPokedexTextUnused[] = _("");

const u8 gWobbuffetPokedexText[] = _(
    "To keep its pitch-black tail hidden, it\n"
    "lives quietly in the darkness. It is never\n"
    "first to attack.");

const u8 gWobbuffetPokedexTextUnused[] = _("");

const u8 gGirafarigPokedexText[] = _(
    "Its tail, which also contains a small\n"
    "brain, may bite on its own if it notices an\n"
    "alluring smell.");

const u8 gGirafarigPokedexTextUnused[] = _("");

const u8 gPinecoPokedexText[] = _(
    "It hangs and waits for flying-insect prey\n"
    "to come near. It does not move about\n"
    "much on its own.");

const u8 gPinecoPokedexTextUnused[] = _("");

const u8 gForretressPokedexText[] = _(
    "It remains immovably rooted to its tree.\n"
    "It scatters pieces of its hard shell to\n"
    "drive its enemies away.");

const u8 gForretressPokedexTextUnused[] = _("");

const u8 gDunsparcePokedexText[] = _(
    "If spotted, it escapes by burrowing with\n"
    "its tail. It can hover just slightly using\n"
    "its wings.");

const u8 gDunsparcePokedexTextUnused[] = _("");

const u8 gGligarPokedexText[] = _(
    "It usually clings to cliffs. When it spots\n"
    "its prey, it spreads its wings and glides\n"
    "down to attack.");

const u8 gGligarPokedexTextUnused[] = _("");

const u8 gSteelixPokedexText[] = _(
    "It is said that if an ONIX lives for over\n"
    "100 years, its composition changes to\n"
    "become diamond-like.");

const u8 gSteelixPokedexTextUnused[] = _("");

const u8 gSnubbullPokedexText[] = _(
    "It has an active, playful nature. Many\n"
    "women like to frolic with it because of\n"
    "its affectionate ways.");

const u8 gSnubbullPokedexTextUnused[] = _("");

const u8 gGranbullPokedexText[] = _(
    "Because its fangs are too heavy, it\n"
    "always keeps its head tilted down.\n"
    "However, its BITE is powerful.");

const u8 gGranbullPokedexTextUnused[] = _("");

const u8 gQwilfishPokedexText[] = _(
    "The small spikes covering its body\n"
    "developed from scales. They inject a\n"
    "toxin that causes fainting.");

const u8 gQwilfishPokedexTextUnused[] = _("");

const u8 gScizorPokedexText[] = _(
    "Its wings are not used for flying.\n"
    "They are flapped at high speed to adjust\n"
    "its body temperature.");

const u8 gScizorPokedexTextUnused[] = _("");

const u8 gShucklePokedexText[] = _(
    "It stores BERRIES inside its shell.\n"
    "To avoid attacks, it hides beneath rocks\n"
    "and remains completely still.");

const u8 gShucklePokedexTextUnused[] = _("");

const u8 gHeracrossPokedexText[] = _(
    "Usually docile, but if disturbed while\n"
    "sipping honey, it chases off the intruder\n"
    "with its horn.");

const u8 gHeracrossPokedexTextUnused[] = _("");

const u8 gSneaselPokedexText[] = _(
    "Vicious in nature, it drives PIDGEY from\n"
    "their nests and feasts on the eggs that\n"
    "are left behind.");

const u8 gSneaselPokedexTextUnused[] = _("");

const u8 gTeddiursaPokedexText[] = _(
    "Before food becomes scarce in wintertime,\n"
    "its habit is to hoard food in many hidden\n"
    "locations.");

const u8 gTeddiursaPokedexTextUnused[] = _("");

const u8 gUrsaringPokedexText[] = _(
    "With its ability to distinguish any smell,\n"
    "it unfailingly finds all food buried deep\n"
    "underground.");

const u8 gUrsaringPokedexTextUnused[] = _("");

const u8 gSlugmaPokedexText[] = _(
    "A common sight in volcanic areas, it\n"
    "slowly slithers around in a constant\n"
    "search for warm places.");

const u8 gSlugmaPokedexTextUnused[] = _("");

const u8 gMagcargoPokedexText[] = _(
    "Its brittle shell occasionally spouts\n"
    "intense flames that circulate throughout\n"
    "its body.");

const u8 gMagcargoPokedexTextUnused[] = _("");

const u8 gSwinubPokedexText[] = _(
    "If it smells something enticing, it dashes\n"
    "off headlong to find the source of the\n"
    "aroma.");

const u8 gSwinubPokedexTextUnused[] = _("");

const u8 gPiloswinePokedexText[] = _(
    "If it charges at an enemy, the hairs on\n"
    "its back stand up straight. It is very\n"
    "sensitive to sound.");

const u8 gPiloswinePokedexTextUnused[] = _("");

const u8 gCorsolaPokedexText[] = _(
    "In a south sea nation, the people live in\n"
    "communities that are built on groups of\n"
    "these POKéMON.");

const u8 gCorsolaPokedexTextUnused[] = _("");

const u8 gRemoraidPokedexText[] = _(
    "Using its dorsal fin as a suction pad, it\n"
    "clings to a MANTINEÙ underside to\n"
    "scavenge for leftovers.");

const u8 gRemoraidPokedexTextUnused[] = _("");

const u8 gOctilleryPokedexText[] = _(
    "It instinctively sneaks into rocky holes.\n"
    "If it gets sleepy, it steals the nest of a\n"
    "fellow OCTILLERY.");

const u8 gOctilleryPokedexTextUnused[] = _("");

const u8 gDelibirdPokedexText[] = _(
    "It nests at the edge of sharp cliffs.\n"
    "It spends all day carrying food to its\n"
    "awaiting chicks.");

const u8 gDelibirdPokedexTextUnused[] = _("");

const u8 gMantinePokedexText[] = _(
    "Swimming freely in open seas, it may fly\n"
    "out of the water and over the waves if it\n"
    "builds up enough speed.");

const u8 gMantinePokedexTextUnused[] = _("");

const u8 gSkarmoryPokedexText[] = _(
    "After nesting in bramble bushes, the wings\n"
    "of its chicks grow hard from scratches by\n"
    "thorns.");

const u8 gSkarmoryPokedexTextUnused[] = _("");

const u8 gHoundourPokedexText[] = _(
    "To corner prey, they check each otherÙ\n"
    "location using barks that only they can\n"
    "understand.");

const u8 gHoundourPokedexTextUnused[] = _("");

const u8 gHoundoomPokedexText[] = _(
    "Upon hearing its eerie howls, other\n"
    "POKéMON get the shivers and head straight\n"
    "back to their nests.");

const u8 gHoundoomPokedexTextUnused[] = _("");

const u8 gKingdraPokedexText[] = _(
    "It sleeps deep on the ocean floor to\n"
    "build its energy. It is said to cause\n"
    "tornadoes as it wakes.");

const u8 gKingdraPokedexTextUnused[] = _("");

const u8 gPhanpyPokedexText[] = _(
    "As a sign of affection, it bumps with its\n"
    "snout. However, it is so strong, it may\n"
    "send you flying.");

const u8 gPhanpyPokedexTextUnused[] = _("");

const u8 gDonphanPokedexText[] = _(
    "The longer and bigger its tusks, the\n"
    "higher its rank in its herd. The tusks take\n"
    "a long time to grow.");

const u8 gDonphanPokedexTextUnused[] = _("");

const u8 gPorygon2PokedexText[] = _(
    "Further research enhanced its abilities.\n"
    "Sometimes, it may exhibit motions that\n"
    "were not programmed.");

const u8 gPorygon2PokedexTextUnused[] = _("");

const u8 gStantlerPokedexText[] = _(
    "Those who stare at its antlers will\n"
    "gradually lose control of their senses\n"
    "and be unable to stand.");

const u8 gStantlerPokedexTextUnused[] = _("");

const u8 gSmearglePokedexText[] = _(
    "Once it becomes an adult, it has a\n"
    "tendency to let its comrades plant\n"
    "footprints on its back.");

const u8 gSmearglePokedexTextUnused[] = _("");

const u8 gTyroguePokedexText[] = _(
    "Even though it is small, it canÑ be\n"
    "ignored because it will slug any handy\n"
    "target without warning.");

const u8 gTyroguePokedexTextUnused[] = _("");

const u8 gHitmontopPokedexText[] = _(
    "It launches kicks while spinning. If it\n"
    "spins at high speed, it may bore its way\n"
    "into the ground.");

const u8 gHitmontopPokedexTextUnused[] = _("");

const u8 gSmoochumPokedexText[] = _(
    "It always rocks its head slowly backwards\n"
    "and forwards as if it is trying to kiss\n"
    "someone.");

const u8 gSmoochumPokedexTextUnused[] = _("");

const u8 gElekidPokedexText[] = _(
    "Even in the most vicious storm, this\n"
    "POKéMON plays happily if thunder rumbles\n"
    "in the sky.");

const u8 gElekidPokedexTextUnused[] = _("");

const u8 gMagbyPokedexText[] = _(
    "It is found in volcanic craters. Its body\n"
    "heat exceeds 1,100 degrees Fahrenheit,\n"
    "so donÑ underestimate it.");

const u8 gMagbyPokedexTextUnused[] = _("");

const u8 gMiltankPokedexText[] = _(
    "If it has just had a baby, the milk it\n"
    "produces contains much more nutrition than\n"
    "usual.");

const u8 gMiltankPokedexTextUnused[] = _("");

const u8 gBlisseyPokedexText[] = _(
    "It has a very compassionate nature. If it\n"
    "sees a sick POKéMON, it will nurse the\n"
    "sufferer back to health.");

const u8 gBlisseyPokedexTextUnused[] = _("");

const u8 gRaikouPokedexText[] = _(
    "This POKéMON races across the\n"
    "land while barking a cry that sounds\n"
    "like crashing thunder.");

const u8 gRaikouPokedexTextUnused[] = _("");

const u8 gEnteiPokedexText[] = _(
    "A POKéMON that races across the land.\n"
    "It is said that one is born every time a\n"
    "new volcano appears.");

const u8 gEnteiPokedexTextUnused[] = _("");

const u8 gSuicunePokedexText[] = _(
    "This POKéMON races across the land.\n"
    "It is said that north winds will somehow\n"
    "blow whenever it appears.");

const u8 gSuicunePokedexTextUnused[] = _("");

const u8 gLarvitarPokedexText[] = _(
    "It is born deep underground. It canÑ\n"
    "emerge until it has entirely consumed the\n"
    "soil around it.");

const u8 gLarvitarPokedexTextUnused[] = _("");

const u8 gPupitarPokedexText[] = _(
    "Even sealed in its shell, it can move\n"
    "freely. Hard and fast, it has outstanding\n"
    "destructive power.");

const u8 gPupitarPokedexTextUnused[] = _("");

const u8 gTyranitarPokedexText[] = _(
#if REVISION == 0
    "Its body canÑ be harmed by any sort of\n"
    "attack, so it is very eager to make\n"
    "challenges against enemies."
#else
"It has an impudent nature. Having great\n"
    "strength, it can even change surrounding\n"
    "landforms."
#endif
);

const u8 gTyranitarPokedexTextUnused[] = _("");

const u8 gLugiaPokedexText[] = _(
    "It is said to be the guardian of the seas.\n"
    "It is rumored to have been seen on the\n"
    "night of a storm.");

const u8 gLugiaPokedexTextUnused[] = _("");

const u8 gHoOhPokedexText[] = _(
    "A legend says that its body glows in\n"
    "seven colors. A rainbow is said to form\n"
    "behind it when it flies.");

const u8 gHoOhPokedexTextUnused[] = _("");

const u8 gCelebiPokedexText[] = _(
    "When CELEBI disappears deep in a forest,\n"
    "it is said to leave behind an egg it\n"
    "brought from the future.");

const u8 gCelebiPokedexTextUnused[] = _("");

const u8 gTreeckoPokedexText[] = _(
    "It quickly scales even vertical walls.\n"
    "It senses humidity with its tail to predict\n"
    "the next dayÙ weather.");

const u8 gTreeckoPokedexTextUnused[] = _("");

const u8 gGrovylePokedexText[] = _(
    "Its strongly developed thigh muscles\n"
    "give it astounding agility and jumping\n"
    "performance.");

const u8 gGrovylePokedexTextUnused[] = _("");

const u8 gSceptilePokedexText[] = _(
    "The leaves on its forelegs are as sharp\n"
    "as swords. It agilely leaps about the\n"
    "branches of trees to strike.");

const u8 gSceptilePokedexTextUnused[] = _("");

const u8 gTorchicPokedexText[] = _(
    "It has a flame sac inside its belly that\n"
    "perpetually burns. It feels warm if it is\n"
    "hugged.");

const u8 gTorchicPokedexTextUnused[] = _("");

const u8 gCombuskenPokedexText[] = _(
    "It boosts its concentration by emitting\n"
    "harsh cries. Its kicks have outstanding\n"
    "destructive power.");

const u8 gCombuskenPokedexTextUnused[] = _("");

const u8 gBlazikenPokedexText[] = _(
    "When facing a tough foe, it looses flames\n"
    "from its wrists. Its powerful legs let it\n"
    "jump clear over buildings.");

const u8 gBlazikenPokedexTextUnused[] = _("");

const u8 gMudkipPokedexText[] = _(
    "Its large tail fin propels it through\n"
    "water with powerful acceleration. It is\n"
    "strong in spite of its size.");

const u8 gMudkipPokedexTextUnused[] = _("");

const u8 gMarshtompPokedexText[] = _(
    "It is at its best when on muddy ground\n"
    "with poor footing. It quickly overwhelms\n"
    "foes struggling in mud.");

const u8 gMarshtompPokedexTextUnused[] = _("");

const u8 gSwampertPokedexText[] = _(
    "Its arms are rock-hard. With one swing,\n"
    "they can batter down its foe. It makes its\n"
    "nest on beautiful beaches.");

const u8 gSwampertPokedexTextUnused[] = _("");

const u8 gPoochyenaPokedexText[] = _(
    "It has a very tenacious nature. Its acute\n"
    "sense of smell lets it chase a chosen\n"
    "prey without ever losing track.");

const u8 gPoochyenaPokedexTextUnused[] = _("");

const u8 gMightyenaPokedexText[] = _(
    "It will always obey the commands of a\n"
    "skilled TRAINER. Its behavior arises from\n"
    "its living in packs in ancient times.");

const u8 gMightyenaPokedexTextUnused[] = _("");

const u8 gZigzagoonPokedexText[] = _(
    "A POKéMON with abundant curiosity.\n"
    "It shows an interest in everything, so it\n"
    "always zigs and zags.");

const u8 gZigzagoonPokedexTextUnused[] = _("");

const u8 gLinoonePokedexText[] = _(
    "When running in a straight line, it can top\n"
    "60 miles per hour. However, it has a\n"
    "tough time with curved roads.");

const u8 gLinoonePokedexTextUnused[] = _("");

const u8 gWurmplePokedexText[] = _(
    "It lives amidst tall grass and in forests.\n"
    "When attacked, it resists by pointing its\n"
    "venomous spikes at the foe.");

const u8 gWurmplePokedexTextUnused[] = _("");

const u8 gSilcoonPokedexText[] = _(
    "It conserves its energy by moving as\n"
    "little as possible. It awaits evolution\n"
    "while drinking only a little rainwater.");

const u8 gSilcoonPokedexTextUnused[] = _("");

const u8 gBeautiflyPokedexText[] = _(
    "Despite its appearance, it has an\n"
    "aggressive nature. It attacks by jabbing\n"
    "with its long, thin mouth.");

const u8 gBeautiflyPokedexTextUnused[] = _("");

const u8 gCascoonPokedexText[] = _(
    "Its body, which is made of soft silk,\n"
    "hardens over time. When cracks appear,\n"
    "evolution is near.");

const u8 gCascoonPokedexTextUnused[] = _("");

const u8 gDustoxPokedexText[] = _(
    "It scatters horribly toxic dust when it\n"
    "senses danger. They tend to gather in the\n"
    "glow of streetlamps at night.");

const u8 gDustoxPokedexTextUnused[] = _("");

const u8 gLotadPokedexText[] = _(
    "It searches about for clean water. If it\n"
    "does not drink water for too long, the\n"
    "leaf on its head wilts.");

const u8 gLotadPokedexTextUnused[] = _("");

const u8 gLombrePokedexText[] = _(
    "It lives at the waterÙ edge where it is\n"
    "sunny. It sleeps on a bed of water grass\n"
    "by day and becomes active at night.");

const u8 gLombrePokedexTextUnused[] = _("");

const u8 gLudicoloPokedexText[] = _(
    "The rhythm of bright, festive music\n"
    "activates LUDICOLOÙ cells, making it more\n"
    "powerful.");

const u8 gLudicoloPokedexTextUnused[] = _("");

const u8 gSeedotPokedexText[] = _(
    "If it remains still, it becomes impossible\n"
    "to distinguish from real nuts. It delights\n"
    "in surprising foraging PIDGEY.");

const u8 gSeedotPokedexTextUnused[] = _("");

const u8 gNuzleafPokedexText[] = _(
    "They live in holes bored in large trees.\n"
    "The sound of NUZLEAFÙ grass flute fills\n"
    "listeners with dread.");

const u8 gNuzleafPokedexTextUnused[] = _("");

const u8 gShiftryPokedexText[] = _(
    "A POKéMON that was feared as a forest\n"
    "guardian. It can read the foeÙ mind and\n"
    "take preemptive action.");

const u8 gShiftryPokedexTextUnused[] = _("");

const u8 gTaillowPokedexText[] = _(
    "It dislikes cold seasons. They migrate to\n"
    "other lands in search of warmth, flying\n"
    "over 180 miles a day.");

const u8 gTaillowPokedexTextUnused[] = _("");

const u8 gSwellowPokedexText[] = _(
    "If its two tail feathers are standing at\n"
    "attention, it is proof of good health.\n"
    "It soars elegantly in the sky.");

const u8 gSwellowPokedexTextUnused[] = _("");

const u8 gWingullPokedexText[] = _(
    "It rides upon ocean winds as if it were\n"
    "a glider. In the winter, it hides food\n"
    "around its nest.");

const u8 gWingullPokedexTextUnused[] = _("");

const u8 gPelipperPokedexText[] = _(
    "It is a flying transporter that carries\n"
    "small POKéMON in its beak. It bobs on the\n"
    "waves to rest its wings.");

const u8 gPelipperPokedexTextUnused[] = _("");

const u8 gRaltsPokedexText[] = _(
    "It is highly attuned to the emotions of\n"
    "people and POKéMON. It hides if it senses\n"
    "hostility.");

const u8 gRaltsPokedexTextUnused[] = _("");

const u8 gKirliaPokedexText[] = _(
    "The cheerful spirit of its TRAINER gives\n"
    "it energy for its psychokinetic power.\n"
    "It spins and dances when happy.");

const u8 gKirliaPokedexTextUnused[] = _("");

const u8 gGardevoirPokedexText[] = _(
    "It has the power to predict the future.\n"
    "Its power peaks when it is protecting its\n"
    "TRAINER.");

const u8 gGardevoirPokedexTextUnused[] = _("");

const u8 gSurskitPokedexText[] = _(
    "They usually live on ponds, but after an\n"
    "evening shower, they may appear on\n"
    "puddles in towns.");

const u8 gSurskitPokedexTextUnused[] = _("");

const u8 gMasquerainPokedexText[] = _(
    "The antennae have distinctive patterns\n"
    "that look like eyes. When it rains, they\n"
    "grow heavy, making flight impossible.");

const u8 gMasquerainPokedexTextUnused[] = _("");

const u8 gShroomishPokedexText[] = _(
    "It prefers damp places. By day it remains\n"
    "still in the forest shade. It releases\n"
    "toxic powder from its head.");

const u8 gShroomishPokedexTextUnused[] = _("");

const u8 gBreloomPokedexText[] = _(
    "The seeds on its tail are made of toxic\n"
    "spores. It knocks out foes with quick,\n"
    "virtually invisible punches.");

const u8 gBreloomPokedexTextUnused[] = _("");

const u8 gSlakothPokedexText[] = _(
    "It sleeps for 20 hours every day. Making\n"
    "drowsy those that see it is one of\n"
    "its abilities.");

const u8 gSlakothPokedexTextUnused[] = _("");

const u8 gVigorothPokedexText[] = _(
    "It is always hungry because it wonÑ stop\n"
    "rampaging. Even while it is eating, it\n"
    "canÑ keep still.");

const u8 gVigorothPokedexTextUnused[] = _("");

const u8 gSlakingPokedexText[] = _(
    "It is the worldÙ most slothful POKéMON.\n"
    "However, it can exert horrifying power by\n"
    "releasing pent-up energy all at once.");

const u8 gSlakingPokedexTextUnused[] = _("");

const u8 gNincadaPokedexText[] = _(
    "Because it lived almost entirely\n"
    "underground, it is nearly blind.\n"
    "It uses its antennae instead.");

const u8 gNincadaPokedexTextUnused[] = _("");

const u8 gNinjaskPokedexText[] = _(
    "This POKéMON is so quick, it is said to\n"
    "be able to avoid any attack. It loves to\n"
    "feed on tree sap.");

const u8 gNinjaskPokedexTextUnused[] = _("");

const u8 gShedinjaPokedexText[] = _(
    "A most peculiar POKéMON that somehow\n"
    "appears in a POKé BALL when a NINCADA\n"
    "evolves.");

const u8 gShedinjaPokedexTextUnused[] = _("");

const u8 gWhismurPokedexText[] = _(
    "It usually murmurs, but starts crying\n"
    "loudly if it senses danger. It stops when\n"
    "its ear covers are shut.");

const u8 gWhismurPokedexTextUnused[] = _("");

const u8 gLoudredPokedexText[] = _(
    "When it stamps its feet and bellows, it\n"
    "generates ultrasonic waves that can blow\n"
    "apart a house.");

const u8 gLoudredPokedexTextUnused[] = _("");

const u8 gExploudPokedexText[] = _(
    "It emits a variety of sounds from the \n"
    "holes all over its body. Its loud cries\n"
    "can be heard from over six miles away.");

const u8 gExploudPokedexTextUnused[] = _("");

const u8 gMakuhitaPokedexText[] = _(
    "It grows stronger by enduring harsh\n"
    "training. It is a gutsy POKéMON that can\n"
    "withstand any attack.");

const u8 gMakuhitaPokedexTextUnused[] = _("");

const u8 gHariyamaPokedexText[] = _(
    "It stomps on the ground to build power.\n"
    "It can send a 10-ton truck flying with a\n"
    "straight-arm punch.");

const u8 gHariyamaPokedexTextUnused[] = _("");

const u8 gAzurillPokedexText[] = _(
    "It battles by flinging around its tail,\n"
    "which is bigger than its body. The\n"
    "tail is a flotation device in water.");

const u8 gAzurillPokedexTextUnused[] = _("");

const u8 gNosepassPokedexText[] = _(
    "Its magnetic nose consistently faces\n"
    "north. Travelers check NOSEPASS to get\n"
    "their bearings.");

const u8 gNosepassPokedexTextUnused[] = _("");

const u8 gSkittyPokedexText[] = _(
    "It is said to be difficult to earn its\n"
    "trust. However, it is extremely popular\n"
    "for its cute looks and behavior.");

const u8 gSkittyPokedexTextUnused[] = _("");

const u8 gDelcattyPokedexText[] = _(
    "The favorite of trend-conscious\n"
    "female TRAINERS, they are used in\n"
    "competition for their style and fur.");

const u8 gDelcattyPokedexTextUnused[] = _("");

const u8 gSableyePokedexText[] = _(
    "It feeds on gemstone crystals.\n"
    "In darkness, its eyes sparkle with the\n"
    "glitter of jewels.");

const u8 gSableyePokedexTextUnused[] = _("");

const u8 gMawilePokedexText[] = _(
    "It uses its docile-looking face to lull\n"
    "foes into complacency, then bites with its\n"
    "huge, relentless jaws.");

const u8 gMawilePokedexTextUnused[] = _("");

const u8 gAronPokedexText[] = _(
    "It eats iron to build its steel body.\n"
    "It is a pest that descends from mountains\n"
    "to eat bridges and train tracks.");

const u8 gAronPokedexTextUnused[] = _("");

const u8 gLaironPokedexText[] = _(
    "It habitually shows off its strength with\n"
    "the size of sparks it creates by ramming\n"
    "its steel body into boulders.");

const u8 gLaironPokedexTextUnused[] = _("");

const u8 gAggronPokedexText[] = _(
    "It claims a large mountain as its sole\n"
    "territory. It mercilessly thrashes those\n"
    "that violate its space.");

const u8 gAggronPokedexTextUnused[] = _("");

const u8 gMedititePokedexText[] = _(
    "It never skips its daily yoga training.\n"
    "It heightens its inner strength through\n"
    "meditation.");

const u8 gMedititePokedexTextUnused[] = _("");

const u8 gMedichamPokedexText[] = _(
    "It elegantly avoids attacks with dance-\n"
    "like steps, then launches a devastating\n"
    "blow in the same motion.");

const u8 gMedichamPokedexTextUnused[] = _("");

const u8 gElectrikePokedexText[] = _(
    "It stores static electricity in its fur\n"
    "for discharging. It gives off sparks if a\n"
    "storm approaches.");

const u8 gElectrikePokedexTextUnused[] = _("");

const u8 gManectricPokedexText[] = _(
    "It rarely appears before people.\n"
    "It is said to nest where lightning has\n"
    "fallen.");

const u8 gManectricPokedexTextUnused[] = _("");

const u8 gPluslePokedexText[] = _(
    "It cheers on partners while scattering\n"
    "sparks from its body. It climbs telephone\n"
    "poles to absorb electricity.");

const u8 gPluslePokedexTextUnused[] = _("");

const u8 gMinunPokedexText[] = _(
    "Its dislike of water makes it take shelter\n"
    "under the eaves of houses in rain. It uses\n"
    "pom-poms made of sparks for cheering.");

const u8 gMinunPokedexTextUnused[] = _("");

const u8 gVolbeatPokedexText[] = _(
    "It lives around clean ponds. At night,\n"
    "its rear lights up. It converses with\n"
    "others by flashing its light.");

const u8 gVolbeatPokedexTextUnused[] = _("");

const u8 gIllumisePokedexText[] = _(
    "It guides VOLBEAT to draw signs in night\n"
    "skies. There are scientists that study the\n"
    "patterns it creates.");

const u8 gIllumisePokedexTextUnused[] = _("");

const u8 gRoseliaPokedexText[] = _(
    "Its flowers give off a relaxing fragrance.\n"
    "The stronger its aroma, the healthier\n"
    "the ROSELIA is.");

const u8 gRoseliaPokedexTextUnused[] = _("");

const u8 gGulpinPokedexText[] = _(
    "There is nothing its stomach canÑ digest.\n"
    "While it is digesting, vile, overpowering\n"
    "gases are expelled.");

const u8 gGulpinPokedexTextUnused[] = _("");

const u8 gSwalotPokedexText[] = _(
    "It can swallow a tire whole in one gulp.\n"
    "It secretes a horribly toxic fluid from\n"
    "the pores on its body.");

const u8 gSwalotPokedexTextUnused[] = _("");

const u8 gCarvanhaPokedexText[] = _(
    "It lives in massive rivers that course\n"
    "through jungles. It swarms prey that\n"
    "enter its territory.");

const u8 gCarvanhaPokedexTextUnused[] = _("");

const u8 gSharpedoPokedexText[] = _(
    "The ruffian of the seas, it has fangs that\n"
    "crunch through iron. It swims by jetting\n"
    "water from its rear.");

const u8 gSharpedoPokedexTextUnused[] = _("");

const u8 gWailmerPokedexText[] = _(
    "When it sucks in a large volume of\n"
    "seawater, it becomes like a big, bouncy\n"
    "ball. It eats a ton of food daily.");

const u8 gWailmerPokedexTextUnused[] = _("");

const u8 gWailordPokedexText[] = _(
    "It is among the largest of all POKéMON.\n"
    "It herds prey in a pack then swallows the\n"
    "massed prey in one gulp.");

const u8 gWailordPokedexTextUnused[] = _("");

const u8 gNumelPokedexText[] = _(
    "Magma of almost 2,200 degrees Fahrenheit\n"
    "courses through its body. When it grows\n"
    "cold, the magma hardens and slows it.");

const u8 gNumelPokedexTextUnused[] = _("");

const u8 gCameruptPokedexText[] = _(
    "If angered, the humps on its back erupt\n"
    "in a shower of molten lava. It lives in\n"
    "the craters of volcanoes.");

const u8 gCameruptPokedexTextUnused[] = _("");

const u8 gTorkoalPokedexText[] = _(
    "It burns coal inside its shell. If it is\n"
    "attacked, it belches thick, black smoke\n"
    "and flees.");

const u8 gTorkoalPokedexTextUnused[] = _("");

const u8 gSpoinkPokedexText[] = _(
    "It apparently dies if it stops bouncing\n"
    "about. It carries a pearl from CLAMPERL\n"
    "on its head.");

const u8 gSpoinkPokedexTextUnused[] = _("");

const u8 gGrumpigPokedexText[] = _(
    "It can gain control over foes by doing\n"
    "odd dance steps. The black pearls on its\n"
    "forehead are precious gems.");

const u8 gGrumpigPokedexTextUnused[] = _("");

const u8 gSpindaPokedexText[] = _(
    "No two SPINDA are said to have identical\n"
    "patterns. It confuses foes with its\n"
    "stumbling motions.");

const u8 gSpindaPokedexTextUnused[] = _("");

const u8 gTrapinchPokedexText[] = _(
    "It lives in arid deserts. It makes a\n"
    "sloping pit trap in sand where it\n"
    "patiently awaits prey.");

const u8 gTrapinchPokedexTextUnused[] = _("");

const u8 gVibravaPokedexText[] = _(
    "It generates ultrasonic waves by violently\n"
    "flapping its wings. After making its prey\n"
    "faint, it melts the prey with acid.");

const u8 gVibravaPokedexTextUnused[] = _("");

const u8 gFlygonPokedexText[] = _(
    "It hides itself by kicking up desert sand\n"
    "with its wings. Red covers shield its eyes\n"
    "from sand.");

const u8 gFlygonPokedexTextUnused[] = _("");

const u8 gCacneaPokedexText[] = _(
    "It prefers harsh environments such as\n"
    "deserts. It can survive for 30 days on\n"
    "water stored in its body.");

const u8 gCacneaPokedexTextUnused[] = _("");

const u8 gCacturnePokedexText[] = _(
    "It lives in deserts. It becomes active at\n"
    "night when it hunts for prey exhausted\n"
    "from the desertÙ heat.");

const u8 gCacturnePokedexTextUnused[] = _("");

const u8 gSwabluPokedexText[] = _(
    "It constantly grooms its cotton-like\n"
    "wings. It takes a shower to clean\n"
    "itself if it becomes dirty.");

const u8 gSwabluPokedexTextUnused[] = _("");

const u8 gAltariaPokedexText[] = _(
    "If you hear a beautiful melody trilling\n"
    "deep among mountains far from people,\n"
    "it is ALTARIAÙ humming.");

const u8 gAltariaPokedexTextUnused[] = _("");

const u8 gZangoosePokedexText[] = _(
    "If it comes across a SEVIPER, its fur\n"
    "bristles and it assumes its battle pose.\n"
    "Its sharp claws are its best weapon.");

const u8 gZangoosePokedexTextUnused[] = _("");

const u8 gSeviperPokedexText[] = _(
    "It sharpens its swordlike tail on hard\n"
    "rocks. It hides in tall grass and strikes\n"
    "unwary prey with venomous fangs.");

const u8 gSeviperPokedexTextUnused[] = _("");

const u8 gLunatonePokedexText[] = _(
    "Its health ebbs and flows with the lunar\n"
    "cycle. It brims with power when exposed\n"
    "to the light of the full moon.");

const u8 gLunatonePokedexTextUnused[] = _("");

const u8 gSolrockPokedexText[] = _(
    "It absorbs solar energy during the day.\n"
    "Always expressionless, it can sense what\n"
    "its foe is thinking.");

const u8 gSolrockPokedexTextUnused[] = _("");

const u8 gBarboachPokedexText[] = _(
    "It probes muddy riverbeds with its two\n"
    "long whiskers. A slimy film protects its\n"
    "body.");

const u8 gBarboachPokedexTextUnused[] = _("");

const u8 gWhiscashPokedexText[] = _(
    "It makes its nest at the bottom of \n"
    "swamps. It will eat anything - if it is\n"
    "alive, WHISCASH will eat it.");

const u8 gWhiscashPokedexTextUnused[] = _("");

const u8 gCorphishPokedexText[] = _(
    "It came from overseas. It is a very hardy\n"
    "creature that will quickly proliferate,\n"
    "even in polluted streams.");

const u8 gCorphishPokedexTextUnused[] = _("");

const u8 gCrawdauntPokedexText[] = _(
    "A rough customer that wildly flails its\n"
    "giant claws. It is said to be extremely\n"
    "hard to raise.");

const u8 gCrawdauntPokedexTextUnused[] = _("");

const u8 gBaltoyPokedexText[] = _(
    "It was discovered in ancient ruins.\n"
    "While moving, it constantly spins. It\n"
    "stands on one foot even when asleep.");

const u8 gBaltoyPokedexTextUnused[] = _("");

const u8 gClaydolPokedexText[] = _(
    "It appears to have been born from clay\n"
    "dolls made by ancient people. It uses\n"
    "telekinesis to float and move.");

const u8 gClaydolPokedexTextUnused[] = _("");

const u8 gLileepPokedexText[] = _(
    "It became extinct roughly 100 million\n"
    "years ago. It was regenerated from a\n"
    "fossil using advanced techniques.");

const u8 gLileepPokedexTextUnused[] = _("");

const u8 gCradilyPokedexText[] = _(
    "It ensnares prey with its eight tentacles.\n"
    "It then melts the prey with a strong acid\n"
    "before feeding.");

const u8 gCradilyPokedexTextUnused[] = _("");

const u8 gAnorithPokedexText[] = _(
    "It is a kind of POKéMON progenitor.\n"
    "It uses its extending claws to catch prey\n"
    "hiding among rocks on the seafloor.");

const u8 gAnorithPokedexTextUnused[] = _("");

const u8 gArmaldoPokedexText[] = _(
    "Protected by a hard shell, its body is\n"
    "very sturdy. It skewers prey with its\n"
    "claws to feed.");

const u8 gArmaldoPokedexTextUnused[] = _("");

const u8 gFeebasPokedexText[] = _(
    "Ridiculed for its shabby appearance,\n"
    "it is ignored by researchers. It lives in\n"
    "ponds choked with weeds.");

const u8 gFeebasPokedexTextUnused[] = _("");

const u8 gMiloticPokedexText[] = _(
    "MILOTIC is breathtakingly beautiful.\n"
    "Those that see it are said to forget their\n"
    "combative spirits.");

const u8 gMiloticPokedexTextUnused[] = _("");

const u8 gCastformPokedexText[] = _(
    "It has the ability to change its form into\n"
    "the sun, the rain, or a snow cloud, \n"
    "depending on the weather.");

const u8 gCastformPokedexTextUnused[] = _("");

const u8 gKecleonPokedexText[] = _(
    "It changes body color to blend in with\n"
    "its surroundings. It also changes color if\n"
    "it is happy or sad.");

const u8 gKecleonPokedexTextUnused[] = _("");

const u8 gShuppetPokedexText[] = _(
    "It loves to feed on feelings like envy and\n"
    "malice. Its upright horn catches the\n"
    "emotions of people.");

const u8 gShuppetPokedexTextUnused[] = _("");

const u8 gBanettePokedexText[] = _(
    "Strong feelings of hatred turned a puppet\n"
    "into a POKéMON. If it opens its mouth,\n"
    "its cursed energy escapes.");

const u8 gBanettePokedexTextUnused[] = _("");

const u8 gDuskullPokedexText[] = _(
    "Making itself invisible, it silently sneaks\n"
    "up to prey. It has the ability to slip\n"
    "through thick walls.");

const u8 gDuskullPokedexTextUnused[] = _("");

const u8 gDusclopsPokedexText[] = _(
    "Its body is entirely hollow. When it opens\n"
    "its mouth, it sucks everything in as if it\n"
    "were a black hole.");

const u8 gDusclopsPokedexTextUnused[] = _("");

const u8 gTropiusPokedexText[] = _(
    "It lives in tropical jungles. The bunch of\n"
    "fruit around its neck is delicious.\n"
    "The fruit grows twice a year.");

const u8 gTropiusPokedexTextUnused[] = _("");

const u8 gChimechoPokedexText[] = _(
    "It travels by riding on winds. It cleverly\n"
    "uses its long tail to pluck nuts and\n"
    "berries, which it loves to eat.");

const u8 gChimechoPokedexTextUnused[] = _("");

const u8 gAbsolPokedexText[] = _(
    "It appears when it senses an impending\n"
    "natural disaster. As a result, it was\n"
    "mistaken as a doom-bringer.");

const u8 gAbsolPokedexTextUnused[] = _("");

const u8 gWynautPokedexText[] = _(
    "It tends to move in a pack with others.\n"
    "They cluster in a tight group to sleep in\n"
    "a cave.");

const u8 gWynautPokedexTextUnused[] = _("");

const u8 gSnoruntPokedexText[] = _(
    "It is said that a home visited by a\n"
    "SNORUNT will prosper. It can withstand\n"
    "cold of minus 150 degrees Fahrenheit.");

const u8 gSnoruntPokedexTextUnused[] = _("");

const u8 gGlaliePokedexText[] = _(
    "It has a body of ice that wonÑ melt,\n"
    "even with fire. It can instantly freeze\n"
    "moisture in the atmosphere.");

const u8 gGlaliePokedexTextUnused[] = _("");

const u8 gSphealPokedexText[] = _(
    "Its body is covered in fluffy fur. The\n"
    "fur keeps it from feeling cold while\n"
    "it is rolling on ice.");

const u8 gSphealPokedexTextUnused[] = _("");

const u8 gSealeoPokedexText[] = _(
    "It touches new things with its nose to\n"
    "test for smell and feel. It plays by\n"
    "spinning SPHEAL on its nose.");

const u8 gSealeoPokedexTextUnused[] = _("");

const u8 gWalreinPokedexText[] = _(
    "It swims through icy seas while shattering\n"
    "ice floes with its large tusks. It is\n"
    "protected by its thick blubber.");

const u8 gWalreinPokedexTextUnused[] = _("");

const u8 gClamperlPokedexText[] = _(
    "It is protected by a sturdy shell.\n"
    "Once in a lifetime, it makes a magnificent\n"
    "pearl.");

const u8 gClamperlPokedexTextUnused[] = _("");

const u8 gHuntailPokedexText[] = _(
    "It lives deep in the sea where no light\n"
    "ever filters down. It lights up its small\n"
    "fishlike tail to attract prey.");

const u8 gHuntailPokedexTextUnused[] = _("");

const u8 gGorebyssPokedexText[] = _(
    "Its swimming form is exquisitely elegant.\n"
    "With its thin mouth, it feeds on seaweed\n"
    "that grows between rocks.");

const u8 gGorebyssPokedexTextUnused[] = _("");

const u8 gRelicanthPokedexText[] = _(
    "It has remained unchanged for 100\n"
    "million years. It was discovered\n"
    "during a deep-sea exploration.");

const u8 gRelicanthPokedexTextUnused[] = _("");

const u8 gLuvdiscPokedexText[] = _(
    "During the spawning season, countless\n"
    "LUVDISC congregate at coral reefs,\n"
    "turning the waters pink.");

const u8 gLuvdiscPokedexTextUnused[] = _("");

const u8 gBagonPokedexText[] = _(
    "Its steel-hard head can shatter boulders.\n"
    "It longingly hopes for wings to grow so it\n"
    "can fly.");

const u8 gBagonPokedexTextUnused[] = _("");

const u8 gShelgonPokedexText[] = _(
    "Its armored body makes all attacks bounce\n"
    "off. The armor is too tough, however,\n"
    "making it heavy and somewhat sluggish.");

const u8 gShelgonPokedexTextUnused[] = _("");

const u8 gSalamencePokedexText[] = _(
    "It becomes uncontrollable if it is\n"
    "enraged. It destroys everything with\n"
    "shredding claws and fire.");

const u8 gSalamencePokedexTextUnused[] = _("");

const u8 gBeldumPokedexText[] = _(
    "It uses magnetic waves to converse with\n"
    "its kind. All the cells in its body are\n"
    "magnetic.");

const u8 gBeldumPokedexTextUnused[] = _("");

const u8 gMetangPokedexText[] = _(
    "It floats midair using magnetism. Its body\n"
    "is so tough, even a crash with a jet\n"
    "plane wonÑ leave a scratch.");

const u8 gMetangPokedexTextUnused[] = _("");

const u8 gMetagrossPokedexText[] = _(
    "It is formed by two METANG fusing.\n"
    "Its four brains are said to be superior\n"
    "to a supercomputer.");

const u8 gMetagrossPokedexTextUnused[] = _("");

const u8 gRegirockPokedexText[] = _(
    "It is entirely composed of rocks with no\n"
    "sign of a brain or heart. It is a mystery\n"
    "even to modern scientists.");

const u8 gRegirockPokedexTextUnused[] = _("");

const u8 gRegicePokedexText[] = _(
    "Research revealed that its body is made\n"
    "of the same kind of ice that is found at\n"
    "the South Pole.");

const u8 gRegicePokedexTextUnused[] = _("");

const u8 gRegisteelPokedexText[] = _(
    "It is sturdier than any kind of metal.\n"
    "It hardened due to pressure underground\n"
    "over tens of thousands of years.");

const u8 gRegisteelPokedexTextUnused[] = _("");

const u8 gLatiasPokedexText[] = _(
    "It can telepathically communicate with\n"
    "people. It changes its appearance using\n"
    "its down that refracts light.");

const u8 gLatiasPokedexTextUnused[] = _("");

const u8 gLatiosPokedexText[] = _(
    "It has a docile temperament and dislikes\n"
    "fighting. Tucking in its forelegs, it can\n"
    "fly faster than a jet plane.");

const u8 gLatiosPokedexTextUnused[] = _("");

const u8 gKyogrePokedexText[] = _(
    "This POKéMON is said to have\n"
    "expanded the sea by bringing heavy rains.\n"
    "It has the power to control water.");

const u8 gKyogrePokedexTextUnused[] = _("");

const u8 gGroudonPokedexText[] = _(
    "This legendary POKéMON is said to\n"
    "represent the land. It went to sleep after\n"
    "dueling KYOGRE.");

const u8 gGroudonPokedexTextUnused[] = _("");

const u8 gRayquazaPokedexText[] = _(
    "It has lived for hundreds of millions of\n"
    "years in the ozone layer. Its flying form\n"
    "looks like a meteor.");

const u8 gRayquazaPokedexTextUnused[] = _("");

const u8 gJirachiPokedexText[] = _(
    "It is said to make any wish come true.\n"
    "It is awake for only seven days out of\n"
    "a thousand years.");

const u8 gJirachiPokedexTextUnused[] = _("");

const u8 gDeoxysPokedexText[] = _(
    "This DEOXYS has transformed into its\n"
    "aggressive guise. It can fool enemies by\n"
    "altering its appearance.");

const u8 gDeoxysPokedexTextUnused[] = _("");
