gOtherText_NewName::
    .string "NEW NAME$"

gNameChoice_BLUE::
    .string "BLUE$"

gNameChoice_RED::
    .string "RED$"

gNameChoice_GARY::
    .string "GARY$"

gNameChoice_JOHN::
    .string "JOHN$"

gNameChoice_ASH::
    .string "ASH$"

gNameChoice_JACK::
    .string "JACK$"

gNameChoice_TEST1::
    .string "TEST1$"

gNameChoice_TEST2::
    .string "TEST2$"

gNameChoice_TEST3::
    .string "TEST3$"

gNameChoice_TEST4::
    .string "TEST4$"

gNameChoice_TEST5::
    .string "TEST5$"

gNameChoice_TEST6::
    .string "TEST6$"

gNameChoice_TEST7::
    .string "TEST7$"

gNameChoice_TEST8::
    .string "TEST8$"

gNameChoice_TEST9::
    .string "TEST9$"

gNameChoice_TEST10::
    .string "TEST10$"

gNameChoice_TEST11::
    .string "TEST11$"

gNameChoice_TEST12::
    .string "TEST12$"

gControlsGuide_Text_Intro::
    .string "The various buttons will be explained in\n"
    .string "the order of their importance.$"

gControlsGuide_Text_DPad::
    .string "Moves the main character.\n"
    .string "Also used to choose various data\n"
    .string "headings.$"

gControlsGuide_Text_AButton::
    .string "Used to confirm a choice, check\n"
    .string "things, chat, and scroll text.$"

gControlsGuide_Text_BButton::
    .string "Used to exit, cancel a choice,\n"
    .string "and cancel a mode.$"

gControlsGuide_Text_StartButton::
    .string "Press this button to open the\n"
    .string "MENU.$"

gControlsGuide_Text_SelectButton::
    .string "Used to shift items and to use\n"
    .string "a registered item.$"

gControlsGuide_Text_LRButtons::
    .string "If you need help playing the\n"
    .string "game, or on how to do things,\n"
    .string "press the L or R Button.$"

gOakSpeech_Text_AskPlayerGender::
    .string "Now tell me. Are you a boy?\n"
    .string "Or are you a girl?$"

gPikachuIntro_Text_Page1::

gPikachuIntro_Text_Page2::

gPikachuIntro_Text_Page3::

gOakSpeech_Text_WelcomeToTheWorld::
    .string "Hello there!\n"
    .string "Welcome to the\l"
    .string "world of POKéMON!\p"
    
    .string "My name is OAK!\n"
    .string "People call me\l"
    .string "the POKéMON PROF!\p"

    .string "$"

gOakSpeech_Text_ThisWorld::
    .string "This world is\n"
    .string "inhabited by\l"
    .string "creatures called\l"
    .string "POKéMON!$"

gOakSpeech_Text_IsInhabitedFarAndWide::
    .string "WIP$"

gOakSpeech_Text_IStudyPokemon::
    .string "For some people,\n"
    .string "POKéMON are\l"
    .string "pets. Others use\l"
    .string "them for fights.\p"

    .string "Myself...\n"
    .string "I study POKéMON\l"
    .string "as a profession.\p"

    .string "$"

gOakSpeech_Text_TellMeALittleAboutYourself::
    .string "But first, tell me a\n"
    .string "little about yourself.\p"

    .string "$"

gOakSpeech_Text_AskPlayersName::
    .string "First, what is\n"
    .string "your name?\p"

    .string "$"

gOakSpeech_Text_SoYourNameIsPlayer::
    .string "Right! So your\n"
    .string "name is {PLAYER}!$"

gOakSpeech_Text_WhatWasHisName::
    .string "This is my grand-\n"
    .string "son. HeÙ been\l"
    .string "your rival since\l"
    .string "you were a baby.\p"

    .string "...Erm, what is\n"
    .string "his name again?$"

gOakSpeech_Text_YourRivalsNameWhatWasIt::
    .string "Your rivalÙ name,\n"
    .string "what was it now?$"

gOakSpeech_Text_ConfirmPlayerName::
    .string "...Er, was it\n"
    .string "{PLAYER}?$"

gOakSpeech_Text_ConfirmRivalName::
    .string "...Er, was it\n"
    .string "{RIVAL}?$"

gOakSpeech_Text_RememberRivalsName::
    .string "ThatÙ right! I\n"
    .string "remember now! His\l"
    .string "name is {RIVAL}!\p"

    .string "$"

gOakSpeech_Text_LetsGo::
    .string "{PLAYER}!\p"

    .string "Your very own\n"
    .string "POKéMON legend is\l"
    .string "about to unfold!\p"

    .string "A world of dreams\n"
    .string "and adventures\l"
    .string "with POKéMON\l"
    .string "awaits! LetÙ go!$"
