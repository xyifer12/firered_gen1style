@ For the NPC in Pallet Town that talks about the Trainer Tips sign

PalletTown_Text_SignLady::
	.string "I'm raising\n"
    .string "POKéMON, too!\p"
    .string "When they get\n"
    .string "strong, they can\l"
    .string "protect me.$"
