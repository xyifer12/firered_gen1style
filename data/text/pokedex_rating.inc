PokedexRating_Text_HowIsPokedexComingAlong::
	.string "OAK: Good to see\n"
	.string "you! How is your\l"
	.string "POKéDEX coming?\l"
	.string "Here, let me take\l"
	.string "a look!$"

PokedexRating_Text_SeenXOwnedY::
	.string "POKéDEX completion\n"
	.string "is:\p"
	.string "{STR_VAR_1} POKéMON seen\n"
	.string "{STR_VAR_2} POKéMON owned.\p"
	.string "PROF.OAKÙ rating:$"

PokedexRating_Text_LessThan10::
	.string "You still have\n"
	.string "lots to do.\l"
	.string "Look for POKéMON\l"
	.string "in grassy areas!$"

PokedexRating_Text_LessThan20::
	.string "You're on the\n"
	.string "right track!\l"
	.string "Get a FLASH HM\l"
	.string "from my AIDE!$"

PokedexRating_Text_LessThan30::
	.string "You still need\n"
	.string "more POKéMON!\l"
	.string "Try to catch\l"
	.string "other species!$"

PokedexRating_Text_LessThan40::
	.string "Good, you're\n"
	.string "trying hard!\l"
	.string "Get an ITEMFINDER\l"
	.string "from my AIDE!$"

PokedexRating_Text_LessThan50::
	.string "Looking good!\n"
	.string "Go find my AIDE\l"
	.string "when you get 50!$"

PokedexRating_Text_LessThan60::
	.string "You finally got at\n"
	.string "least 50 species!\l"
	.string "Be sure to get\l"
	.string "EXP.ALL from my\l"
	.string "AIDE!$"

PokedexRating_Text_LessThan70::
	.string "Ho! This is geting\n"
	.string "even better!$"

PokedexRating_Text_LessThan80::
	.string "Very good!\n"
	.string "Go fish for some\l"
	.string "marine POKéMON!$"

PokedexRating_Text_LessThan90::
	.string "Wonderful!\n"
	.string "Do you like to\l"
	.string "collect things?$"

PokedexRating_Text_LessThan100::
	.string "I'm impressed!\n"
	.string "It must have been\l"
	.string "difficult to do!$"

PokedexRating_Text_LessThan110::
	.string "You finally got at\n"
	.string "least 100 species!\l"
	.string "I can't believe\l"
	.string "how good you are!$"

PokedexRating_Text_LessThan120::
	.string "You even have the\n"
	.string "evolved forms of\l"
	.string "POKéMON! Super!$"

PokedexRating_Text_LessThan130::
	.string "Excellent! Trade\n"
	.string "with friends to\l"
	.string "get some more!$"

PokedexRating_Text_LessThan140::
	.string "Outstanding!\n"
	.string "You've become a\l"
	.string "real pro at this!$"

PokedexRating_Text_LessThan150::
	.string "I have nothing\n"
	.string "left to say!\l"
	.string "You're the\l"
	.string "authority now!$"

PokedexRating_Text_Complete::
	.string "Your POKéDEX is\n"
	.string "entirely complete!\l"
	.string "Congratulations!!$"

PokedexRating_Text_NationalDexSeenXOwnedY::
	.string "And your NATIONAL POKéDEX is:\p"
	.string "{STR_VAR_1} POKéMON seen and\n"
	.string "{STR_VAR_2} POKéMON owned.$"

PokedexRating_Text_LookForwardToFilledNationalDex::
	.string "I'll be looking forward to seeing\n"
	.string "you fill the NATIONAL POKéDEX!$"

PokedexRating_Text_YouveCompletedDex::
	.string "Finally…\p"
	.string "You've finally completed the\n"
	.string "POKéDEX!\p"
	.string "It's magnificent!\n"
	.string "Truly, this is a fantastic feat!$"

PokedexRating_Text_Wroooaaarrr::
	.string "Wroooooooaaaaaarrrr!$"

PokedexRating_Text_HaHa::
	.string "はあ はあ‥$"

PokedexRating_Text_ThankYouMadeDreamReality::
	.string "Thank you, {PLAYER}!\n"
	.string "Sincerely, thank you!\l"
	.string "You've made my dream a reality!$"

PokedexRating_Text_LoveSeeingYourPokedex::
	.string "OAK: Ah, welcome!\p"
	.string "Tell me, how is your POKéDEX\n"
	.string "coming along?\p"
	.string "Wahaha!\p"
	.string "Actually, I know how it is, but I\n"
	.string "love seeing it anyway!\p"
	.string "Let's see…$"
