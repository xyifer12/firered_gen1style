Aide_Text_HaventCaughtEnoughMonsForItem::
	.string "Let's see...\n"
	.string "Uh-oh! You have \l"
	.string "caught only {STR_VAR_3}\l"
	.string "kinds of POKéMON!\p"
	.string "You need {STR_VAR_1}\n"
	.string "kinds if you want\l"
	.string "the {STR_VAR_2}.$"

Aide_Text_GetEnoughMonsComeBackForItem::
	.string "Oh. I see.\n"
	.string "When you get {STR_VAR_1}\l"
	.string "kinds, come back\l"
	.string "for {STR_VAR_2}.$"

Aide_Text_DontHaveAnyRoomForItem::
	.string "Oh! I see you\n"
	.string "donÑ have any\l"
	.string "room for the\l"
	.string "{STR_VAR_2}.$"
