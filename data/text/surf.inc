Text_WantToSurf::
	.string "Would you like\n"
	.string "to SURF?$"

Text_UsedSurf::
	.string "{PLAYER} got on\n"
	.string "{STR_VAR_1}!$"

Text_CurrentTooFast::
	.string "The current is much too fast!\n"
	.string "SURF can't be used here...$"
