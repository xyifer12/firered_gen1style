Text_OnlySkilledTrainersAllowedThrough::
	.string "Only truly skilled\n"
	.string "TRAINERS are\l"
	.string "allowed through.\p"

	.string "You don't have the\n"
	.string "{STR_VAR_1} yet!$"

Text_CantLetYouPass::
	.string "The rules are\n"
	.string "rules. I can't\l"
	.string "let you pass.$"

Text_OhThatsBadgeGoRightAhead::
	.string "Oh! That is the\n"
	.string "{STR_VAR_1}!\l"
	.string "Go right ahead!$"

Text_OnlyPassWithBadgeDontHaveYet::
	.string "Only truly skilled\n"
	.string "trainers are\l"
	.string "allowed through.\p"

	.string "You don't have the\n"
	.string "{STR_VAR_1} yet!$"

Text_OnlyPassWithBadgeOhGoAhead::
	.string "Oh! That is the\n"
	.string "{STR_VAR_1}!{PAUSE_MUSIC}{PLAY_BGM}{MUS_LEVEL_UP}{PAUSE 0x60}{RESUME_MUSIC}\l"
	.string "Go right ahead!$"
