Text_ObtainedTheX::
	.string "Obtained the\n"
	.string "{STR_VAR_2}!$"

Text_GotX::
	.string "{PLAYER} got\n"
	.string "{STR_VAR_2}!$"

Text_BagIsFull::
	.string "The BAG is full...$"

Text_PutItemAway::
	.string "{PLAYER} put the\n"
	.string "{STR_VAR_2} in\l"
	.string "the {STR_VAR_3}.$"

Text_FoundOneItem::
	.string "{PLAYER} found\n"
	.string "{STR_VAR_2}!$"

Text_TooBadBagFull::
	.string "Too bad!\n"
	.string "The BAG is full...$"

Text_FoundXCoins::
	.string "{PLAYER} found\n"
	.string "{STR_VAR_1} {STR_VAR_2}!$"

Text_PutCoinsAwayInCoinCase::
	.string "{PLAYER} put the\n"
	.string "COINS away in\l"
	.string "the COIN CASE.$"

Text_CoinCaseIsFull::
	.string "Too bad!\n"
	.string "The COIN CASE is full...$"

Text_NothingToPutThemIn::
	.string "Too bad!\n"
	.string "There's nothing to put them in...$"

Text_FoundXItems::
	.string "{PLAYER} found\n"
	.string "{STR_VAR_1} {STR_VAR_2}(s)!$"

Text_ObtainedTheDecor::
	.string "Obtained the\n"
	.string "{STR_VAR_2}!$"

Text_NoRoomForAnotherDecor::
	.string "Too bad! There's no room left for\n"
	.string "another {STR_VAR_2}…$"

Text_DecorTransferredToPC::
	.string "The {STR_VAR_2} was transferred\n"
	.string "to the PC.$"
