gText_WouldYouLikeToSaveTheGame::
	.string "Would you like to\n" 
	.string "SAVE the game?$"

gText_AlreadySaveFile_WouldLikeToOverwrite::
	.string "There is already\n"
	.string "a saved file.\l"
	.string "Is it okay to\l"
	.string "overwrite it?$"

gText_SavingDontTurnOffThePower::
	.string "Now saving...$"

gText_PlayerSavedTheGame::
	.string "{PLAYER} saved\n"
	.string "the game!$"

gText_SaveFailed2::
	.string "セーブに　しっぱい　しました‥$"

gText_DifferentGameFile::
	.string "The older file\n"
	.string "will be erased\l"
	.string "to save. Okay?$"
