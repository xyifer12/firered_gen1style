Trade_Text_LookingForMonWannaTradeForMon::
	.string "Iá looking for\n"
	.string "{STR_VAR_1}!\p"

	.string "Wanna trade one\n"
	.string "for {STR_VAR_2}?$"

Trade_Text_AwwOhWell::
	.string "Awww!\n"
	.string "Oh well...$"

Trade_Text_WhatThatsNoMon::
	.string "What? ThatÙ not\n"
	.string "{STR_VAR_1}!\p"

	.string "If you get one,\n"
	.string "come back here!$"

Trade_Text_HeyThanks::
	.string "Hey, thanks!$"

Trade_Text_IsntMyOldMonGreat::
	.string "IsnÑ my old\n"
	.string "{STR_VAR_2} great?$"

Trade_Text_DoYouHaveMonWouldYouTradeForMon::
	.string "Hello there! Do\n"
	.string "you want to trade\l"
	.string "your {STR_VAR_1}\l"
	.string "for {STR_VAR_2}?$"

Trade_Text_WellIfYouDontWantTo::
	.string "Well, if you\n"
	.string "donÑ want to...$"

Trade_Text_ThisIsntMon::
	.string "Hmmm? This isnÑ\n"
	.string "{STR_VAR_1}.\p"

	.string "Think of me when\n"
	.string "you get one.$"

Trade_Text_Thanks::
	.string "Thanks!$"

Trade_Text_HasTradedMonGrownStronger::
	.string "The {STR_VAR_2}\n"
	.string "that I traded you,\l"
	.string "has it grown\l"
	.string "stronger?$"

Trade_Text_DoYouHaveMonWantToTradeForMon::
	.string "Hi! Do you have\n"
	.string "{STR_VAR_1}?\p"

	.string "Want to trade it\n"
	.string "for {STR_VAR_2}?$"

Trade_Text_ThatsTooBad::
	.string "ThatÙ too bad.$"

Trade_Text_ThisIsNoMon::
	.string "...This is no\n"
	.string "{STR_VAR_1}.\p"

	.string "If you get one,\n"
	.string "trade it with me!$"

Trade_Text_ThanksYoureAPal::
	.string "Thanks pal!$"

Trade_Text_HowIsMyOldMon::
	.string "How is my old\n"
	.string "{STR_VAR_2}?\p"

	.string "{STR_VAR_1} is\n"
	.string "doing great!$"
