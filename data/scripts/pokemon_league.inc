PokemonLeague_EventScript_OpenDoor::
	applymovement OBJ_EVENT_ID_PLAYER, Common_Movement_Delay32
	waitmovement 0
	playse SE_RS_DOOR
	call PokemonLeague_EventScript_SetDoorOpen
	special DrawWholeMapView
	setflag FLAG_TEMP_4
	return

PokemonLeague_EventScript_AgathasOpenDoor::
	applymovement OBJ_EVENT_ID_PLAYER, Common_Movement_Delay32
	waitmovement 0
	playse SE_RS_DOOR
	call PokemonLeague_EventScript_AgathasSetDoorOpen
	special DrawWholeMapView
	setflag FLAG_TEMP_4
	return

PokemonLeague_EventScript_EnterRoom::
	applymovement OBJ_EVENT_ID_PLAYER, Common_Movement_WalkUp6
	waitmovement 0
	setflag FLAG_TEMP_2
	playse SE_UNLOCK
	call PokemonLeague_EventScript_CloseEntry
	special DrawWholeMapView
	return

PokemonLeague_EventScript_EnterAgathasRoom::
	applymovement OBJ_EVENT_ID_PLAYER, Common_Movement_WalkUp6
	waitmovement 0
	setflag FLAG_TEMP_2
	playse SE_UNLOCK
	call PokemonLeague_EventScript_AgathasCloseEntry
	special DrawWholeMapView
	return

PokemonLeague_EventScript_SetDoorOpen::
	setmetatile 4, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	setmetatile 5, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	return

PokemonLeague_EventScript_AgathasSetDoorOpen::
	setmetatile 4, 0, METATILE_PokemonLeague_Door_Mid_Open, 0
	setmetatile 5, 0, METATILE_PokemonLeague_Door_Mid_Open, 0
	return

PokemonLeague_EventScript_PreventExit::
	lockall
	msgbox Text_VoiceRangOutDontRunAway
	closemessage
	applymovement OBJ_EVENT_ID_PLAYER, PokemonLeague_Movement_ForcePlayerIn
	waitmovement 0
	releaseall
	end

PokemonLeague_EventScript_OpenDoorLance::
	applymovement OBJ_EVENT_ID_PLAYER, Common_Movement_Delay32
	waitmovement 0
	playse SE_RS_DOOR
	setmetatile 5, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	setmetatile 6, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	special DrawWholeMapView
	setflag FLAG_TEMP_4
	return

PokemonLeague_EventScript_SetDoorOpenLance::
	setmetatile 5, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	setmetatile 6, 0, METATILE_PokemonLeague_Door_Top_Open, 0
	return

PokemonLeague_EventScript_CloseEntry::
	setmetatile 4, 0, METATILE_PokemonLeague_Entry_TopLeft_Closed, 1
	setmetatile 5, 0, METATILE_PokemonLeague_Entry_TopLeft_Closed, 1
	return

PokemonLeague_EventScript_AgathasCloseEntry::
	setmetatile 4, 0, METATILE_PokemonLeague_Entry_TopRight_Closed, 1
	setmetatile 5, 0, METATILE_PokemonLeague_Entry_TopRight_Closed, 1
	return

PokemonLeague_Movement_ForcePlayerIn::
	walk_up
	step_end

PokemonLeague_EventScript_DoLightingEffect::
	special DoPokemonLeagueLightingEffect
	return
