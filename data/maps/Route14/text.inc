Route14_Text_CarterIntro::
    .string "You need to use\n"
    .string "TMs to teach good\l"
    .string "moves to POKéMON!$"

Route14_Text_CarterDefeat::
    .string "BIRD KEEPER: Not\n"
    .string "good enough!$"

Route14_Text_CarterPostBattle::
    .string "You have some HMs\n"
    .string "right? POKéMON\l"
    .string "canÑ ever forget\l"
    .string "those moves.$"

Route14_Text_MitchIntro::
    .string "My bird POKéMON\n"
    .string "should be ready\l"
    .string "for battle.$"

Route14_Text_MitchDefeat::
    .string "BIRD KEEPER: Not\n"
    .string "ready yet!$"

Route14_Text_MitchPostBattle::
    .string "They need to learn\n"
    .string "better moves.$"

Route14_Text_BeckIntro::
    .string "TMs are on sale\n"
    .string "in CELEDON!\p"

    .string "But, only a few\n"
    .string "people have HMs!$"

Route14_Text_BeckDefeat::
    .string "BIRD KEEPER: Aww,\n"
    .string "bummer!$"

Route14_Text_BeckPostBattle::
    .string "Teach POKéMON\n"
    .string "moves of the same\l"
    .string "element type for\l"
    .string "more power.$"

Route14_Text_MarlonIntro::
    .string "Have you taught\n"
    .string "your bird POKéMON\l"
    .string "how to FLY?$"

Route14_Text_MarlonDefeat::
    .string "BIRD KEEPER: Shot\n"
    .string "down in flames!$"

Route14_Text_MarlonPostBattle::
    .string "Bird POKéMON are\n"
    .string "my true love!$"

Route14_Text_DonaldIntro::
    .string "Have you heard of\n"
    .string "the legendary\l"
    .string "POKéMON?$"

Route14_Text_DonaldDefeat::
    .string "BIRD KEEPER: Why?\n"
    .string "WhyÚ I lose?$"

Route14_Text_DonaldPostBattle::
    .string "The 3 legendary\n"
    .string "POKéMON are all\l"
    .string "birds of prey.$"

Route14_Text_BennyIntro::
    .string "Iá not into it,\n"
    .string "but OK! LetÙ go!$"

Route14_Text_BennyDefeat::
    .string "BIRD KEEPER: I\n"
    .string "knew it!$"

Route14_Text_BennyPostBattle::
    .string "Winning, losing,\n"
    .string "it doesnÑ matter\l"
    .string "in the long run!$"

Route14_Text_LukasIntro::
    .string "Cáon, cáon.\n"
    .string "LetÙ go, letÙ\l"
    .string "go, letÙ go!$"

Route14_Text_LukasDefeat::
    .string "BIKER: Arrg!\n"
    .string "Lost! Get lost!$"

Route14_Text_LukasPostBattle::
    .string "What, what, what?\n"
    .string "What do you want?$"

Route14_Text_IsaacIntro::
    .string "Perfect!\n"
    .string "I need to\l"
    .string "burn some time!$"

Route14_Text_IsaacDefeat::
    .string "BIKER: What?\n"
    .string "You!?$"

Route14_Text_IsaacPostBattle::
    .string "Raising POKéMON\n"
    .string "is a drag, man.$"

Route14_Text_GeraldIntro::
    .string "We ride out here\n"
    .string "because thereÙ\l"
    .string "more room!$"

Route14_Text_GeraldDefeat::
    .string "BIKER: Wipe out!$"

Route14_Text_GeraldPostBattle::
    .string "ItÙ cool you\n"
    .string "made your POKéMON\l"
    .string "so strong!\p"
    
    .string "Might is right!\n"
    .string "And you know it!$"

Route14_Text_MalikIntro::
    .string "POKéMON fight?\n"
    .string "Cool! Rumble!$"

Route14_Text_MalikDefeat::
    .string "BIKER: Blown\n"
    .string "away!$"

Route14_Text_MalikPostBattle::
    .string "You know whoÚ\n"
    .string "win, you and me\l"
    .string "one on one!$"

Route14_Text_RouteSign::
    .string "ROUTE 14\n"
    .string "West to FUCHSIA\l"
    .string "CITY$"

Route14_Text_KiriIntro::
    .string "KIRI: JAN, letÙ try really,\n"
    .string "really hard together.$"

Route14_Text_KiriDefeat::
    .string "KIRI: Whimper…\n"
    .string "We lost, didnÑ we?$"

Route14_Text_KiriPostBattle::
    .string "KIRI: Did we lose because of me?$"

Route14_Text_KiriNotEnoughMons::
    .string "KIRI: We can battle if you have\n"
    .string "two POKéMON.$"

Route14_Text_JanIntro::
    .string "JAN: KIRI, here we go!\n"
    .string "We have to try hard!$"

Route14_Text_JanDefeat::
    .string "JAN: Eeeeh!\n"
    .string "No fair!$"

Route14_Text_JanPostBattle::
    .string "JAN: KIRI, donÑ cry!\n"
    .string "WeÛl just try harder next time.$"

Route14_Text_JanNotEnoughMons::
    .string "JAN: You want to battle?\n"
    .string "You donÑ have enough POKéMON.$"
