Route20_Text_BarryIntro::
    .string "The water is\n"
    .string "shallow here.$"

Route20_Text_BarryDefeat::
    .string "SWIMMER: Splash!$"

Route20_Text_BarryPostBattle::
    .string "I wish I could\n"
    .string "ride my POKéMON.$"

Route20_Text_ShirleyIntro::
    .string "SEAFOAM is a\n"
    .string "quiet getaway!$"

Route20_Text_ShirleyDefeat::
    .string "BEAUTY: Quit it!$"

Route20_Text_ShirleyPostBattle::
    .string "ThereÙ a huge\n"
    .string "cavern underneath\l"
    .string "this island.$"

Route20_Text_TiffanyIntro::
    .string "I love floating\n"
    .string "with the fishes!$"

Route20_Text_TiffanyDefeat::
    .string "BEAUTY: Yowch!$"

Route20_Text_TiffanyPostBattle::
    .string "Want to float\n"
    .string "with me?$"

Route20_Text_IreneIntro::
    .string "Are you on\n"
    .string "vacation, too?$"

Route20_Text_IreneDefeat::
    .string "SWIMMER: No\n"
    .string "mercy at all!$"

Route20_Text_IrenePostBattle::
    .string "SEAFOAM used to\n"
    .string "be a single island!$"

Route20_Text_DeanIntro::
    .string "Check out my buff\n"
    .string "physique!$"

Route20_Text_DeanDefeat::
    .string "SWIMMER: Wimpy!$"

Route20_Text_DeanPostBattle::
    .string "I shouldße been\n"
    .string "buffing up my\l"
    .string "POKéMON, not me!$"

Route20_Text_DarrinIntro::
    .string "Why are you\n"
    .string "riding a POKéMON?\l"
    .string "CanÑ you swim?$"

Route20_Text_DarrinDefeat::
    .string "SWIMMER: Ouch!\n"
    .string "Torpedoed!$"

Route20_Text_DarrinPostBattle::
    .string "Riding a POKéMON\n"
    .string "sure looks fun!$"

Route20_Text_RogerIntro::
    .string "I rode my bird\n"
    .string "POKéMON here!$"

Route20_Text_RogerDefeat::
    .string "BIRD KEEPER: Oh,\n"
    .string "no!$"

Route20_Text_RogerPostBattle::
    .string "My birds canÑ\n"
    .string "FLY me back!$"

Route20_Text_NoraIntro::
    .string "My boy friend gave\n"
    .string "me big pearls!$"

Route20_Text_NoraDefeat::
    .string "BEAUTY: DonÑ\n"
    .string "touch my pearls!$"

Route20_Text_NoraPostBattle::
    .string "Will my pearls\n"
    .string "grow bigger\l"
    .string "inside CLOYSTER?$"

Route20_Text_MissyIntro::
    .string "I swam here from\n"
    .string "CINNABAR ISLAND.$"

Route20_Text_MissyDefeat::
    .string "BEAUTY: Iá\n"
    .string "so disappointed!$"

Route20_Text_MissyPostBattle::
    .string "POKéMON have\n"
    .string "taken over an\l"
    .string "abandoned mansion\l"
    .string "on CINNABAR!$"

Route20_Text_MelissaIntro::
    .string "CINNABAR, in the\n"
    .string "west, has a LAB\l"
    .string "for POKéMON.$"

Route20_Text_MelissaDefeat::
    .string "BEAUTY: Wait!$"

Route20_Text_MelissaPostBattle::
    .string "CINNABAR is a \n"
    .string "volcanic island!$"

Route20_Text_SeafoamIslands::
    .string "SEAFOAM ISLANDS$"

Route20_Text_MistyTrainsHere::
    .string "Strong TRAINERS and WATER POKéMON\n"
    .string "are common sights in these parts.\p"
    .string "They say that MISTY of the\n"
    .string "CERULEAN GYM trains here.$"
