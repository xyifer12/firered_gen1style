Route2_ViridianForest_NorthEntrance_Text_ManyMonsOnlyInForests::
    .string "Many POKéMON live\n"
    .string "only in forests\l"
    .string "and caves.\p"
    .string "You need to look\n"
    .string "everywhere to get\l"
    .string "different kinds!$"

Route2_ViridianForest_NorthEntrance_Text_CanCutSkinnyTrees::
    .string "Have you noticed\n"
    .string "the bushes on the\l"
    .string "roadside?\p"
    .string "They can be cut\n"
    .string "down by a special\l"
    .string "POKéMON move.$"

Route2_ViridianForest_NorthEntrance_Text_CanCancelEvolution::
    .string "Do you know the evolution-cancel\n"
    .string "technique?\p"
    .string "When a POKéMON is evolving, you\n"
    .string "can stop the process.\p"
    .string "ItÙ a technique for raising\n"
    .string "POKéMON the way they are.$"

