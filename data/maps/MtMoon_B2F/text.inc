MtMoon_B2F_Text_MiguelIntro::
    .string "Hey, stop!\p"
    
    .string "I found these\n"
    .string "fossils! Theyàe\l"
    .string "both mine!$"

MtMoon_B2F_Text_MiguelDefeat::
    .string "SUPER NERD: Ok!\n"
    .string "IÛl share!$"

MtMoon_B2F_Text_WellEachTakeAFossil::
    .string "WeÛl each take\n"
    .string "one!\l"
    .string "No being greedy!$"

MtMoon_B2F_Text_ThenThisFossilIsMine::
    .string "All right. Then\n"
    .string "this is mine!$"

MtMoon_B2F_Text_LabOnCinnabarRegeneratesFossils::
    .string "Far away, on\n"
    .string "CINNABAR ISLAND,\l"
    .string "thereÙ a POKéMON\l"
    .string "LAB.\p"

    .string "They do research\n"
    .string "on regenerating\l"
    .string "fossils.$"

MtMoon_B2F_Text_Grunt1Intro::
    .string "TEAM ROCKET will\n"
    .string "find the fossils,\l"
    .string "revive and sell\l"
    .string "them for cash!$"

MtMoon_B2F_Text_Grunt1Defeat::
    .string "ROCKET: Urgh!\n"
    .string "Now Iá mad!$"

MtMoon_B2F_Text_Grunt1PostBattle::
    .string "You made me mad!\n"
    .string "TEAM ROCKET will\l"
    .string "blacklist you!$"

MtMoon_B2F_Text_Grunt2Intro::
    .string "We, TEAM ROCKET,\n"
    .string "are POKéMON\l"
    .string "gangsters!$"

MtMoon_B2F_Text_Grunt2Defeat::
    .string "ROCKET: I blew\n"
    .string "it!$"

MtMoon_B2F_Text_Grunt2PostBattle::
    .string "Darn it all! My\n"
    .string "associates wonÑ\l"
    .string "stand for this!$"

MtMoon_B2F_Text_Grunt3Intro::
    .string "Weàe pulling a\n"
    .string "big job here!\l"
    .string "Get lost, kid!$"

MtMoon_B2F_Text_Grunt3Defeat::
    .string "So, you\n"
    .string "are good...$"

MtMoon_B2F_Text_Grunt3PostBattle::
    .string "If you find a\n"
    .string "fossil, give it\l"
    .string "to me and scram!$"

MtMoon_B2F_Text_Grunt4Intro::
    .string "Little kids\n"
    .string "should leave\l"
    .string "grown-ups alone!$"

MtMoon_B2F_Text_Grunt4Defeat::
    .string "ROCKET: Iá\n"
    .string "steamed!$"

MtMoon_B2F_Text_Grunt4PostBattle::
    .string "POKéMON lived\n"
    .string "here long before\l"
    .string "people came.$"

MtMoon_B2F_Text_YouWantDomeFossil::
    .string "You want the\n"
    .string "DOME FOSSIL?$"

MtMoon_B2F_Text_YouWantHelixFossil::
    .string "You want the\n"
    .string "HELIX FOSSIL?$"

MtMoon_B2F_Text_ObtainedHelixFossil::
    .string "{PLAYER} got the\n" 
    .string "HELIX FOSSIL!$"

MtMoon_B2F_Text_ObtainedDomeFossil::
    .string "{PLAYER} got the\n"
    .string "DOME FOSSIL!$"

