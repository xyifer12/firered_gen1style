SafariZone_SecretHouse_Text_CongratsYouveWon::
    .string "Ah! Finally!\p"
    .string "Youàe the first\n"
    .string "person to reach\l"
    .string "the SECRET HOUSE!\p"
    .string "I was getting\n"
    .string "worried that no\l"
    .string "one would win our\l"
    .string "campaign prize.\p"
    .string "Congratulations!\n"
    .string "You have won!$"

SafariZone_SecretHouse_Text_ReceivedHM03FromAttendant::
    .string "{PLAYER} received\n"
    .string "HM03!$"

SafariZone_SecretHouse_Text_ExplainSurf::
    .string "HM03 is SURF!\p"
    .string "POKéMON will be\n"
    .string "able to ferry you\l"
    .string "across water!\p"
    .string "And, this HM isnÑ\n"
    .string "disposable! You\l"
    .string "can use it over\l"
    .string "and over!\p"
    .string "Youàe super lucky\n"
    .string "for winning this\l"
    .string "fabulous prize!$"

SafariZone_SecretHouse_Text_DontHaveRoomForPrize::
    .string "You donÑ have\n"
    .string "room for this\l"
    .string "fabulous prize!$"

