SilphCo_6F_Text_RocketsTookOverBuilding::
    .string "The ROCKETs came\n"
    .string "and took over the\l"
    .string "building!$"

SilphCo_6F_Text_BetterGetBackToWork::
    .string "Well, better get\n"
    .string "back to work!$"

SilphCo_6F_Text_HelpMePlease::
    .string "Oh dear, oh dear.\n"
    .string "Help me please!$"

SilphCo_6F_Text_WeGotEngaged::
    .string "We got engaged!\n"
    .string "Heheh!$"

SilphCo_6F_Text_ThatManIsSuchACoward::
    .string "Look at him! HeÙ\n"
    .string "such a coward!$"

SilphCo_6F_Text_NeedsMeToLookAfterHim::
    .string "I feel so sorry\n"
    .string "for him, I have"
    .string "to marry him!$"

SilphCo_6F_Text_RocketsTryingToConquerWorld::
    .string "TEAM ROCKET is\n"
    .string "trying to conquer\l"
    .string "the world with\l"
    .string "POKéMON!$"

SilphCo_6F_Text_RocketsRanAwayBecauseOfYou::
    .string "TEAM ROCKET ran\n"
    .string "because of you!$"

SilphCo_6F_Text_TargetedSilphForOurMonProducts::
    .string "They must have\n"
    .string "targeted SILPH\l"
    .string "for our POKéMON\l"
    .string "products.$"

SilphCo_6F_Text_ComeWorkForSilphWhenYoureOlder::
    .string "Come work for\n"
    .string "SILPH when you\l"
    .string "get older!$"

SilphCo_6F_Text_Grunt1Intro::
    .string "I am one of the 4\n"
    .string "ROCKET BROTHERS!$"

SilphCo_6F_Text_Grunt1Defeat::
    .string "ROCKET: Flame\n"
    .string "out!$"

SilphCo_6F_Text_Grunt1PostBattle::
    .string "No matter!\n"
    .string "My brothers will\l"
    .string "avenge me!$"

SilphCo_6F_Text_TaylorIntro::
    .string "That rotten\n"
    .string "PRESIDENT!\p"
    .string "He shouldnÑ have\n"
    .string "sent me to the\l"
    .string "TIKSI BRANCH!$"

SilphCo_6F_Text_TaylorDefeat::
    .string "SCIENTIST: Shoot!$"

SilphCo_6F_Text_TaylorPostBattle::
    .string "TIKSI BRANCH?\n"
    .string "ItÙ in Russian\l"
    .string "no manÙ-land!$"

SilphCo_6F_Text_Grunt2Intro::
    .string "You dare betray\n"
    .string "TEAM ROCKET?$"

SilphCo_6F_Text_Grunt2Defeat::
    .string "ROCKET: You\n"
    .string "traitor!$"

SilphCo_6F_Text_Grunt2PostBattle::
    .string "If you stand for\n"
    .string "justice, you\l"
    .string "betray evil!$"

SilphCo_6F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "6F$"

