Route11_Text_HugoIntro::
    .string "Win, lose or draw!$"

Route11_Text_HugoDefeat::
    .string "GAMBLER: Atcha!\n"
    .string "DidnÑ go my way!$"

Route11_Text_HugoPostBattle::
    .string "POKéMON is life!\n"
    .string "And to live is to\l"
    .string "gamble!$"

Route11_Text_JasperIntro::
    .string "Competition! I\n"
    .string "canÑ get enough!$"

Route11_Text_JasperDefeat::
    .string "GAMBLER: I had\n"
    .string "a chance!$"

Route11_Text_JasperPostBattle::
    .string "You canÑ be a\n"
    .string "coward in the\l"
    .string "world of POKéMON!$"

Route11_Text_EddieIntro::
    .string "LetÙ go, but\n"
    .string "donÑ cheat!$"

Route11_Text_EddieDefeat::
    .string "YOUNGSTER: Huh?\n"
    .string "ThatÙ not right!$"

Route11_Text_EddiePostBattle::
    .string "I did my best! I\n"
    .string "have no regrets!$"

Route11_Text_BraxtonIntro::
    .string "Careful!\n"
    .string "Iá laying down\l"
    .string "some cables!$"

Route11_Text_BraxtonDefeat::
    .string "ENGINEER: That\n"
    .string "was electric!$"

Route11_Text_BraxtonPostBattle::
    .string "Spread the word\n"
    .string "to save energy!$"

Route11_Text_DillonIntro::
    .string "I just became a\n"
    .string "trainer! But, I\l"
    .string "think I can win!$"

Route11_Text_DillonDefeat::
    .string "YOUNGSTER: My\n"
    .string "POKéMON couldnÑ!$"

Route11_Text_DillonPostBattle::
    .string "What do you want?\n"
    .string "Leave me alone!$"

Route11_Text_DirkIntro::
    .string "Fwahaha!  I have\n"
    .string "never lost!$"

Route11_Text_DirkDefeat::
    .string "GAMBLER: My\n"
    .string "first loss!$"

Route11_Text_DirkPostBattle::
    .string "Luck of the draw!\n"
    .string "Just luck!$"

Route11_Text_DarianIntro::
    .string "I have never won\n"
    .string "before...$"

Route11_Text_DarianDefeat::
    .string "GAMBLER: I saw\n"
    .string "this coming...$"

Route11_Text_DarianPostBattle::
    .string "ItÙ just luck.\n"
    .string "Luck of the draw.$"

Route11_Text_YasuIntro::
    .string "Iá the best in\n"
    .string "my class!$"

Route11_Text_YasuDefeat::
    .string "YOUNGSTER: Darn!\n"
    .string "I need to make my\l"
    .string "POKéMON stronger!$"

Route11_Text_YasuPostBattle::
    .string "ThereÙ a fat\n"
    .string "POKéMON that\l"
    .string "comes down from\l"
    .string "the mountains.\p"
    .string "ItÙ strong if\n"
    .string "you can get it.$"

Route11_Text_BernieIntro::
    .string "Watch out for\n"
    .string "live wires!$"

Route11_Text_BernieDefeat::
    .string "ENGINEER: Whoa!\n"
    .string "You spark plug!$"

Route11_Text_BerniePostBattle::
    .string "Well, better get\n"
    .string "back to work.$"

Route11_Text_DaveIntro::
    .string "My POKéMON should\n"
    .string "be ready by now!$"

Route11_Text_DaveDefeat::
    .string "YOUNGSTER: Too\n"
    .string "much, too young!$"

Route11_Text_DavePostBattle::
    .string "I better go find\n"
    .string "stronger ones!$"

Route11_Text_DiglettsCave::
    .string "DIGLETT'S CAVE$"

