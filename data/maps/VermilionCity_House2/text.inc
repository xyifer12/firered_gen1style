VermilionCity_House2_Text_DoYouHaveMonWantToTradeForMyMon::
	.string "Hi! Do you have\n"
	.string "{STR_VAR_1}?\p"
	
	.string "Want to trade it\n"
	.string "for {STR_VAR_2}?$"

VermilionCity_House2_Text_ThatsTooBad::
	.string "ThatÙ too bad.$"

VermilionCity_House2_Text_ThisIsNoMon::
	.string "...This is no\n"
	.string "{STR_VAR_1}.\p"

	.string "If you get one,\n"
	.string "trade it with me!$"

VermilionCity_House2_Text_ThankYou::
	.string "Thank you!$"

VermilionCity_House2_Text_HowIsMyOldMon::
	.string "How is my old\n"
	.string "{STR_VAR_2}?\p"

	.string "My {STR_VAR_1}\n"
	.string "is doing great!$"
