CeruleanCity_BikeShop_Text_WelcomeToBikeShop::
    .string "Hi! Welcome to\n"
    .string "our BIKE SHOP.\p"
    .string "Have we got just\n"
    .string "the BIKE for you!\p"
    .string "ItÙ a cool BIKE!\n"
    .string "Do you want it?$"

CeruleanCity_BikeShop_Text_SorryYouCantAffordIt::
    .string "Sorry! You canÑ\n"
    .string "afford it!$"

CeruleanCity_BikeShop_Text_OhBikeVoucherHereYouGo::
    .string "Oh, thatÙ...\p"
    .string "A BIKE VOUCHER!\p"
    .string "OK! Here you go!$"

CeruleanCity_BikeShop_Text_ExchangedVoucherForBicycle::
    .string "{PLAYER} exchanged\n"
    .string "the BIKE VOUCHER\l"
    .string "for a BICYCLE.$"

CeruleanCity_BikeShop_Text_ThankYouComeAgain::
    .string "Come back again\n"
    .string "some time!$"

CeruleanCity_BikeShop_Text_HowDoYouLikeNewBicycle::
    .string "How do you like\n"
    .string "your new BICYCLE?\p"
    .string "You can take it\n"
    .string "on CYCLING ROAD\l"
    .string "and in caves!$"

CeruleanCity_BikeShop_Text_MakeRoomForBicycle::
    .string "You better make\n"
    .string "room for this!$"

CeruleanCity_BikeShop_Text_CityBikeGoodEnoughForMe::
    .string "A plain city BIKE\n"
    .string "is good enough\l"
    .string "for me!\p"
    .string "You canÑ put a\n"
    .string "shopping basket\l"
    .string "on an MTB!$"

CeruleanCity_BikeShop_Text_BikesCoolButExpensive::
    .string "These BIKEs are\n"
    .string "cool, but theyàe\l"
    .string "way expensive!$"

CeruleanCity_BikeShop_Text_WowYourBikeIsCool::
    .string "Wow. Your BIKE is\n"
    .string "really cool!$"

@ Unused
CeruleanCity_BikeShop_Text_GermanFoldableBicyleFinallyOnMarket::
    .string "ついに　はつばい！\p"
    .string "ドイツ　せい　さいこうきゅう\n"
    .string "おりたたみ　じてんしゃ！$"

CeruleanCity_BikeShop_Text_ShinyNewBicycle::
    .string "A shiny new\n"
    .string "BICYCLE!$"

