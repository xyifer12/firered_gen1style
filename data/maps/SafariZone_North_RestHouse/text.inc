SafariZone_East_Text_KeepAnyItemFoundOnSafari::
    .string "You can keep any\n"
    .string "item you find on\l"
    .string "the ground here.\p"
    .string "But, youÛl run\n"
    .string "out of time if\l"
    .string "you try for all\l"
    .string "of them at once!$"

SafariZone_East_Text_PrizeInDeepestPartOfSafariZone::
    .string "Go to the deepest\n"
    .string "part of the\l"
    .string "SAFARI ZONE. You\l"
    .string "will win a prize!$"

SafariZone_East_Text_MyEeveeEvolvedIntoFlareon::
    .string "My EEVEE evolved\n"
    .string "into FLAREON!\p"
    .string "But, a friendÙ\n"
    .string "EEVEE turned into\l"
    .string "a VAPOREON!\l"
    .string "I wonder why?$"

