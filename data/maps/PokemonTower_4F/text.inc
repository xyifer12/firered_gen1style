PokemonTower_4F_Text_PaulaIntro::
    .string "Ghost! No!\n"
    .string "Kwaaah!$"

PokemonTower_4F_Text_PaulaDefeat::
    .string "CHANNELER: Where\n"
    .string "is the ghost?$"

PokemonTower_4F_Text_PaulaPostBattle::
    .string "I must have been\n"
    .string "dreaming...$"

PokemonTower_4F_Text_LaurelIntro::
    .string "Be cursed with\n"
    .string "me! Kwaaah!$"

PokemonTower_4F_Text_LaurelDefeat::
    .string "CHANNELER: What!$"

PokemonTower_4F_Text_LaurelPostBattle::
    .string "We canÑ crack\n"
    .string "the identity of\l"
    .string "the GHOSTs.$"

PokemonTower_4F_Text_JodyIntro::
    .string "Huhuhu...\n"
    .string "Beat me not!$"

PokemonTower_4F_Text_JodyDefeat::
    .string "CHANNELER: Huh?\n"
    .string "Who? What?$"

PokemonTower_4F_Text_JodyPostBattle::
    .string "May the departed\n"
    .string "souls of POKéMON\l"
    .string "rest in peace...$"

