SSAnne_1F_Room2_Text_TylerIntro::
    .string "I love POKéMON!\n"
    .string "Do you?$"

SSAnne_1F_Room2_Text_TylerDefeat::
    .string "YOUNGSTER: Wow!\n"
    .string "Youàe great!$"

SSAnne_1F_Room2_Text_TylerPostBattle::
    .string "Let me be your\n"
    .string "friend, OK?\p"
    .string "Then we can trade\n"
    .string "POKéMON!$"

SSAnne_1F_Room2_Text_AnnIntro::
    .string "I collected these\n"
    .string "POKéMON from all\l"
    .string "around the world!$"

SSAnne_1F_Room2_Text_AnnDefeat::
    .string "LASS: Oh no!\n"
    .string "I went around the\l"
    .string "world for these!$"

SSAnne_1F_Room2_Text_AnnPostBattle::
    .string "You hurt my poor\n"
    .string "worldly POKéMON!\p"
    .string "I demand that you\n"
    .string "heal them at a\l"
    .string "POKéMON CENTER!$"

SSAnne_1F_Room2_Text_CruisingAroundWorld::
    .string "We are cruising\n"
    .string "around the world.$"

