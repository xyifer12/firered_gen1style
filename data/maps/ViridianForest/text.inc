ViridianForest_Text_FriendsItchingToBattle::
    .string "I came here with\n"
    .string "some friends!\p"
    
    .string "Theyàe out for\n"
    .string "POKéMON fights!$"

ViridianForest_Text_RickIntro::
    .string "Hey! You have\n"
    .string "POKéMON! Come on!\l"
    .string "LetÙ battle'em!$"

ViridianForest_Text_RickDefeat::
    .string "BUG CATCHER: No!\n"
    .string "CATERPIE canÑ\l"
    .string "cut it!$"

ViridianForest_Text_RickPostBattle::
    .string "Ssh! YouÛl scare\n"
    .string "the bugs away!$"

ViridianForest_Text_DougIntro::
    .string "Yo! You canÑ jam\n"
    .string "out if youàe a\l"
    .string "POKéMON trainer!$"

ViridianForest_Text_DougDefeat::
    .string "BUG CATCHER: Huh?\n"
    .string "I ran out of\l"
    .string "POKéMON!$"

ViridianForest_Text_DougPostBattle::
    .string "Darn! Iá going\n"
    .string "to catch some\l"
    .string "stronger ones!$"

ViridianForest_Text_SammyIntro::
    .string "Hey, wait up!\n"
    .string "WhatÙ the hurry?$"

ViridianForest_Text_SammyDefeat::
    .string "BUG CATCHER: I\n"
    .string "give! Youàe good\l"
    .string "at this!$"

ViridianForest_Text_SammyPostBattle::
    .string "Sometimes, you\n"
    .string "can find stuff on\l"
    .string "the ground!\p"

    .string "Iá looking for\n"
    .string "the stuff I\l"
    .string "dropped!$"

ViridianForest_Text_AnthonyIntro::
    .string "I might be little, but I wonÑ like\n"
    .string "it if you go easy on me!$"

ViridianForest_Text_AnthonyDefeat::
    .string "Oh, boo.\n"
    .string "Nothing went right.$"

ViridianForest_Text_AnthonyPostBattle::
    .string "I lost some of my allowance…$"

ViridianForest_Text_CharlieIntro::
    .string "Did you know that POKéMON evolve?$"

ViridianForest_Text_CharlieDefeat::
    .string "Oh!\n"
    .string "I lost!$"

ViridianForest_Text_CharliePostBattle::
    .string "BUG POKéMON evolve quickly.\n"
    .string "Theyàe a lot of fun!$"

ViridianForest_Text_RanOutOfPokeBalls::
    .string "I ran out of POKé\n"
    .string "BALLs to catch\l"
    .string "POKéMON with!\p"

    .string "You should carry\n"
    .string "extras!$"

ViridianForest_Text_AvoidGrassyAreasWhenWeak::
    .string "TRAINER TIPS\p"

    .string "If you want to\n"
    .string "avoid battles,\l"
    .string "stay away from\l"
    .string "grassy areas!$"

ViridianForest_Text_UseAntidoteForPoison::
    .string "For poison, use\n"
    .string "ANTIDOTE! Get it\l"
    .string "at POKéMON MARTs!$"

ViridianForest_Text_ContactOakViaPCToRatePokedex::
    .string "TRAINER TIPS\p"

    .string "Contact PROF.OAK\n"
    .string "via a PC to get\l"
    .string "your POKéDEX\l"
    .string "evaluated!$"

ViridianForest_Text_CantCatchOwnedMons::
    .string "TRAINER TIPS\p"

    .string "No stealing of\n"
    .string "POKéMON from\l"
    .string "other trainers!\l"
    .string "Catch only wild\l"
    .string "POKéMON!$"

ViridianForest_Text_WeakenMonsBeforeCapture::
    .string "TRAINER TIPS\p"

    .string "Weaken POKéMON\n"
    .string "before attempting\l"
    .string "capture!\l"
    .string "When healthy,\l"
    .string "they may escape!$"

ViridianForest_Text_LeavingViridianForest::
    .string "LEAVING\n"
    .string "VIRIDIAN FOREST\l"
    .string "PEWTER CITY AHEAD$"

