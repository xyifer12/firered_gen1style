Route18_Text_WiltonIntro::
    .string "I always check\n"
    .string "every grassy area\l"
    .string "for new POKéMON.$"

Route18_Text_WiltonDefeat::
    .string "BIRD KEEPER: Tch!$"

Route18_Text_WiltonPostBattle::
    .string "I wish I had a\n"
    .string "BIKE!$"

Route18_Text_RamiroIntro::
    .string "Kurukkoo!\n"
    .string "How do you like\l"
    .string "my bird call?$"

Route18_Text_RamiroDefeat::
    .string "BIRD KEEPER: I\n"
    .string "had to bug you!$"

Route18_Text_RamiroPostBattle::
    .string "I also collect sea\n"
    .string "POKéMON on\l"
    .string "weekends!$"

Route18_Text_JacobIntro::
    .string "This is my turf!\n"
    .string "Get out of here!$"

Route18_Text_JacobDefeat::
    .string "BIRD KEEPER: Darn!$"

Route18_Text_JacobPostBattle::
    .string "This is my fave\n"
    .string "POKéMON hunting\l"
    .string "area!$"

Route18_Text_RouteSign::
    .string "ROUTE 18\n"
    .string "CELADON CITY -\l"
    .string "FUCHSIA CITY$"

Route18_Text_CyclingRoadSign::
    .string "CYCLING ROAD\n"
    .string "No pedestrians\l"
    .string "permitted!$"

