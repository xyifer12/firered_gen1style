Route16_NorthEntrance_2F_Text_OnBikeRideWithGirlfriend::
    .string "Iá going for a\n"
    .string "ride with my girl\l"
    .string "friend!$"

Route16_NorthEntrance_2F_Text_RidingTogetherOnNewBikes::
    .string "Weàe going\n"
    .string "riding together!$"

Route16_NorthEntrance_2F_Text_ItsCeladonDeptStore::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "ItÙ the CELADON DEPT.\n"
    .string "STORE!$"

Route16_NorthEntrance_2F_Text_LongPathOverWater::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "ThereÙ a long\n"
    .string "path over water!$"

Route16_NorthEntrance_2F_Text_GiveAmuletCoinIfCaught40::
    .string "Hi! Remember me?\n"
    .string "Iá PROF.OAKÙ AIDE!\p"
    .string "If your POKéDEX has complete data\n"
    .string "on 40 species, Iá supposed to\p"
    .string "give you a reward.\l"
    .string "So, {PLAYER}, let me ask you.\n"
    .string "Have you gathered data on at least\p"
    .string "40 kinds of POKéMON?$"

Route16_NorthEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route16_NorthEntrance_2F_Text_ReceivedAmuletCoinFromAide::
    .string "{PLAYER} got the AMULET COIN!$"

Route16_NorthEntrance_2F_Text_ExplainAmuletCoin::
    .string "An AMULET COIN is an item to be\n"
    .string "held by a POKéMON.\p"
    .string "If the POKéMON appears in a winning\n"
    .string "battle, you will earn more money.$"

