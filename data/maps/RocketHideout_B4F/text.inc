RocketHideout_B4F_Text_GiovanniIntro::
    .string "So! I must say, I\n"
    .string "am impressed you\l"
    .string "got here.$"

RocketHideout_B4F_Text_GiovanniDefeat::
    .string "WHAT!\n"
    .string "This canÑ be!$"

RocketHideout_B4F_Text_GiovanniPostBattle::
    .string "I see that you\n"
    .string "raise POKéMON\l"
    .string "with utmost care.\p"
    .string "A child like you\n"
    .string "would never\l"
    .string "understand what I\l"
    .string "hope to achieve.\p"
    .string "I shall step\n"
    .string "aside this time!\p"
    .string "I hope we meet\n"
    .string "again...$"

RocketHideout_B4F_Text_Grunt2Intro::
    .string "I know you! You\n"
    .string "ruined our plans\l"
    .string "at MT.MOON!$"

RocketHideout_B4F_Text_Grunt2Defeat::
    .string "ROCKET: Burned\n"
    .string "again!$"

RocketHideout_B4F_Text_Grunt2PostBattle::
    .string "Do you have\n"
    .string "something against\l"
    .string "TEAM ROCKET?$"

RocketHideout_B4F_Text_Grunt3Intro::
    .string "How can you not\n"
    .string "see the beauty of\l"
    .string "our evil?$"

RocketHideout_B4F_Text_Grunt3Defeat::
    .string "ROCKET: Ayaya!$"

RocketHideout_B4F_Text_Grunt3PostBattle::
    .string "BOSS! Iá sorry I\n"
    .string "failed you!$"

RocketHideout_B4F_Text_Grunt1Intro::
    .string "The elevator\n"
    .string "doesnÑ work? Who\l"
    .string "has the LIFT KEY?$"

RocketHideout_B4F_Text_Grunt1Defeat::
    .string "ROCKET: No!$"

RocketHideout_B4F_Text_Grunt1PostBattle::
    .string "Oh, no! I dropped\n"
    .string "the LIFT KEY!$"

