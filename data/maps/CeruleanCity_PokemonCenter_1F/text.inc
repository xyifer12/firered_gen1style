CeruleanCity_PokemonCenter_1F_Text_BillDoesWhateverForRareMons::
    .string "That BILL!\p"
    .string "I heard that\n"
    .string "heÛl do whatever\l"
    .string "it takes to get\l"
    .string "rare POKéMON!$"

CeruleanCity_PokemonCenter_1F_Text_EveryoneCallsBillPokemaniac::
    .string "Have you heard\n"
    .string "about BILL?\p"
    .string "Everyone calls\n"
    .string "him a POKéMANIAC!\p"
    .string "I think people\n"
    .string "are just jealous\l"
    .string "of BILL, though.\p"
    .string "Who wouldnÑ want\n"
    .string "to boast about\l"
    .string "their POKéMON?$"

CeruleanCity_PokemonCenter_1F_Text_BillCollectsRareMons::
    .string "BILL has lots of\n"
    .string "POKéMON!\p"
    .string "He collects rare\n"
    .string "ones too!$"

CeruleanCity_PokemonCenter_1F_Text_TryTradingUpstairs::
    .string "Why donÑ you go upstairs and try\n"
    .string "trading POKéMON with your friends?\p"
    .string "You could get a lot more variety\n"
    .string "by trading.\p"
    .string "The POKéMON you get in trades grow\n"
    .string "quickly, too.$"

