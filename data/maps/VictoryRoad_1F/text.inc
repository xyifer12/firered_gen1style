VictoryRoad_1F_Text_NaomiIntro::
    .string "I wonder if you\n"
    .string "are good enough\l"
    .string "for me!$"

VictoryRoad_1F_Text_NaomiDefeat::
    .string "COOLTRAINER♀: I\n"
    .string "lost out!$"

VictoryRoad_1F_Text_NaomiPostBattle::
    .string "I never wanted to\n"
    .string "lose to anybody!$"

VictoryRoad_1F_Text_RolandoIntro::
    .string "I can see youàe\n"
    .string "good! Let me see\l"
    .string "exactly how good!$"

VictoryRoad_1F_Text_RolandoDefeat::
    .string "COOLTRAINER♂: I\n"
    .string "had a chance...$"

VictoryRoad_1F_Text_RolandoPostBattle::
    .string "I concede, youàe\n"
    .string "better than me!$"

