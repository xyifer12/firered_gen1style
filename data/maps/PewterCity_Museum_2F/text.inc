Text_SeismicTossTeach::
    .string "The secrets of space…\n"
    .string "The mysteries of earth…\p"
    .string "There are so many things about\n"
    .string "which we know so little.\p"
    .string "But that should spur us to study\n"
    .string "harder, not toss in the towel.\p"
    .string "The only thing you should toss…\p"
    .string "Well, how about SEISMIC TOSS?\n"
    .string "Should I teach that to a POKéMON?$"

Text_SeismicTossDeclined::
    .string "Is that so?\n"
    .string "Iá sure youÛl be back for it.$"

Text_SeismicTossWhichMon::
    .string "Which POKéMON wants to learn\n"
    .string "SEISMIC TOSS?$"

Text_SeismicTossTaught::
    .string "I hope you wonÑ toss in the towel.\n"
    .string "Keep it up.$"

PewterCity_Museum_1F_Text_WhatsSpecialAboutMoonStone::
    .string "MOON STONE?\p"
    .string "WhatÙ so special\n"
    .string "about it?$"

PewterCity_Museum_1F_Text_BoughtColorTVForMoonLanding::
    .string "July 20, 1969!\p"
    .string "The 1st lunar\n"
    .string "landing!\p"
    .string "I bought a color\n"
    .string "TV to watch it!$"

PewterCity_Museum_1F_Text_RunningSpaceExhibitThisMonth::
    .string "We have a space\n"
    .string "exhibit now.$"

PewterCity_Museum_1F_Text_AskedDaddyToCatchPikachu::
    .string "I want a PIKACHU!\n"
    .string "ItÙ so cute!\p"
    .string "I asked my Daddy\n"
    .string "to catch me one!$"

PewterCity_Museum_1F_Text_PikachuSoonIPromise::
    .string "Yeah, a PIKACHU\n"
    .string "soon, I promise!$"

PewterCity_Museum_1F_Text_SpaceShuttle::
    .string "SPACE SHUTTLE\n"
    .string "COLOMBIA$"

PewterCity_Museum_1F_Text_MeteoriteThatFellOnMtMoon::
    .string "Meteorite that\n"
    .string "fell on MT.MOON.\l"
    .string "(MOON STONE?)$"

