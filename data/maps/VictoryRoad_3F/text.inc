Text_DoubleEdgeTeach::
    .string "You should be proud of yourself,\n"
    .string "having battled your way through\l"
    .string "VICTORY ROAD so courageously.\p"
    .string "In recognition of your feat,\n"
    .string "IÛl teach you DOUBLE-EDGE.\p"
    .string "Would you like me to teach that\n"
    .string "technique?$"

Text_DoubleEdgeDeclined::
    .string "IÛl teach you the technique\n"
    .string "anytime.$"

Text_DoubleEdgeWhichMon::
    .string "Which POKéMON should I teach\n"
    .string "DOUBLE-EDGE?$"

Text_DoubleEdgeTaught::
    .string "Keep that drive going for the\n"
    .string "POKéMON LEAGUE!\p"
    .string "Take a run at them and knock 'em\n"
    .string "out!$"

VictoryRoad_3F_Text_GeorgeIntro::
    .string "I heard rumors of\n"
    .string "a child prodigy!$"

VictoryRoad_3F_Text_GeorgeDefeat::
    .string "COOLTRAINER♂: The\n"
    .string "rumors were true!$"

VictoryRoad_3F_Text_GeorgePostBattle::
    .string "You beat GIOVANNI\n"
    .string "of TEAM ROCKET?$"

VictoryRoad_3F_Text_AlexaIntro::
    .string "TRAINERS live to\n"
    .string "seek stronger\l"
    .string "opponents!$"

VictoryRoad_3F_Text_AlexaDefeat::
    .string "COOLTRAINER♀: Oh!\n"
    .string "So strong!$"

VictoryRoad_3F_Text_AlexaPostBattle::
    .string "By fighting tough\n"
    .string "battles, you get\l"
    .string "stronger!$"

VictoryRoad_3F_Text_CarolineIntro::
    .string "IÛl show you just\n"
    .string "how good you are!$"

VictoryRoad_3F_Text_CarolineDefeat::
    .string "COOLTRAINER♀: Iá\n"
    .string "furious!$"

VictoryRoad_3F_Text_CarolinePostBattle::
    .string "You showed me just\n"
    .string "how good I was!$"

VictoryRoad_3F_Text_ColbyIntro::
    .string "Only the chosen\n"
    .string "can pass here!$"

VictoryRoad_3F_Text_ColbyDefeat::
    .string "COOLTRAINER♂: I\n"
    .string "donÑ believe it!$"

VictoryRoad_3F_Text_ColbyPostBattle::
    .string "All trainers here\n"
    .string "are headed to the\l"
    .string "POKéMON LEAGUE!\l"
    .string "Be careful!$"

VictoryRoad_3F_Text_RayIntro::
    .string "RAY: Together, the two of us are\n"
    .string "destined for greatness!$"

VictoryRoad_3F_Text_RayDefeat::
    .string "RAY: Ludicrous!\n"
    .string "This canÑ be!$"

VictoryRoad_3F_Text_RayPostBattle::
    .string "RAY: Youße beaten us.\n"
    .string "Greatness remains elusive…$"

VictoryRoad_3F_Text_RayNotEnoughMons::
    .string "RAY: Together, the two of us are\n"
    .string "striving for the pinnacle.\p"
    .string "We need you to bring two POKéMON\n"
    .string "into battle with us.$"

VictoryRoad_3F_Text_TyraIntro::
    .string "TYRA: Weàe trying to become\n"
    .string "champions together.$"

VictoryRoad_3F_Text_TyraDefeat::
    .string "TYRA: Oh, but…$"

VictoryRoad_3F_Text_TyraPostBattle::
    .string "TYRA: Youße taught me that power\n"
    .string "can be infinite in shape and form.$"

VictoryRoad_3F_Text_TyraNotEnoughMons::
    .string "TYRA: You canÑ battle with us if\n"
    .string "you have only one POKéMON.$"

