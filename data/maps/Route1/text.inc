Route1_Text_WorkAtPokeMartTakeSample::
    .string "Hi! I work at a\n"
    .string "POKéMON MART.\p"
    .string "ItÙ a convenient\n"
    .string "shop, so please\l"
    .string "visit us in\l"
    .string "VIRIDIAN CITY.\p"
    .string "I know, IÛl give\n"
    .string "you a sample!\l"
    .string "Here you go!$"

Route1_Text_ComeSeeUsIfYouNeedPokeBalls::
    .string "We also carry\n"
    .string "POKé BALLS for\l"
    .string "catching POKéMON!$"

Route1_Text_PutPotionAway::
    .string "{PLAYER} put the POTION away in\n"
    .string "the BAGÙ ITEMS POCKET.$"

Route1_Text_CanJumpFromLedges::
    .string "See those ledges\n"
    .string "along the road?\p"
    .string "ItÙ a bit scary,\n"
    .string "but you can jump\l"
    .string "from them.\p"
    .string "You can get back\n"
    .string "to PALLET TOWN\l"
    .string "quicker that way.$"

Route1_Text_RouteSign::
    .string "ROUTE 1\n"
    .string "PALLET TOWN -\l"
    .string "VIRIDIAN CITY$"

