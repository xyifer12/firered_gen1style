CeruleanCity_House1_Text_BadgesHaveAmazingSecrets::
    .string "POKéMON BADGEs\n"
    .string "are owned only by\l"
    .string "skilled trainers.\p"

    .string "I see you have\n"
    .string "at least one.\p"

    .string "Those BADGEs have\n"
    .string "amazing secrets!$"

CeruleanCity_House1_Text_DescribeWhichBadge::
    .string "Now then...\p"

    .string "Which of the 8\n"
    .string "BADGEs should I\l"
    .string "describe?$"

CeruleanCity_House1_Text_ComeVisitAnytime::
    .string "Come visit me any\n"
    .string "time you wish.$"

CeruleanCity_House1_Text_AttackStatFlash::
    .string "The ATTACK of all\n"
    .string "POKéMON increases\l"
    .string "a little bit.\p"

    .string "It also lets you\n"
    .string "use FLASH any\l"
    .string "time you desire.$"

CeruleanCity_House1_Text_ObeyLv30Cut::
    .string "POKéMON up to L30\n"
    .string "will obey you.\p"

    .string "Any higher, they\n"
    .string "become unruly!\p"

    .string "It also lets you\n"
    .string "use CUT outside\l"
    .string "of battle.$"

CeruleanCity_House1_Text_SpeedStatFly::
    .string "The SPEED of all\n"
    .string "POKéMON increases\l"
    .string "a little bit.\p"

    .string "It also lets you\n"
    .string "use FLY outside\l"
    .string "of battle.$"

CeruleanCity_House1_Text_ObeyLv50Strength::
    .string "POKéMON up to L50\n"
    .string "will obey you.\p"

    .string "Any higher, they\n"
    .string "become unruly!\p"

    .string "It also lets you\n"
    .string "use STRENGTH out-\l"
    .string "side of battle.$"

CeruleanCity_House1_Text_DefenseStatSurf::
    .string "The DEFENSE of all\n"
    .string "POKéMON increases\l"
    .string "a little bit.\p"

    .string "It also lets you\n"
    .string "use SURF outside\l"
    .string "of battle.$"

CeruleanCity_House1_Text_ObeyLv70RockSmash::
    .string "POKéMON up to L70\n"
    .string "will obey you.\p"
    
    .string "Any higher, they\n"
    .string "become unruly!$"

CeruleanCity_House1_Text_SpStatsWaterfall::
    .string "Your POKéMONÙ\n"
    .string "SPECIAL abilities\l"
    .string "increase a bit.$"

CeruleanCity_House1_Text_AllMonsWillObeyYou::
    .string "All POKéMON will\n"
    .string "obey you!$"

