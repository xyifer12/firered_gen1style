PokemonTower_7F_Text_Grunt1Intro::
    .string "What do you want?\n"
    .string "Why are you here?$"

PokemonTower_7F_Text_Grunt1Defeat::
    .string "ROCKET: I give up!$"

PokemonTower_7F_Text_Grunt1PostBattle::
    .string "Iá not going to\n"
    .string "forget this!$"

PokemonTower_7F_Text_Grunt2Intro::
    .string "This old guy came\n"
    .string "and complained\l"
    .string "about us harming\l"
    .string "useless POKéMON!\p"
    .string "Weàe talking it\n"
    .string "over as adults!$"

PokemonTower_7F_Text_Grunt2Defeat::
    .string "ROCKET: Please!\n"
    .string "No more!$"

PokemonTower_7F_Text_Grunt2PostBattle::
    .string "POKéMON are only\n"
    .string "good for making\l"
    .string "money!\p"
    .string "Stay out of our\n"
    .string "business!$"

PokemonTower_7F_Text_Grunt3Intro::
    .string "Youàe not saving\n"
    .string "anyone, kid!$"

PokemonTower_7F_Text_Grunt3Defeat::
    .string "ROCKET: DonÑ\n"
    .string "fight us ROCKETs!$"

PokemonTower_7F_Text_Grunt3PostBattle::
    .string "Youàe not getting\n"
    .string "away with this!$"

PokemonTower_7F_Text_MrFujiThankYouFollowMe::
    .string "MR.FUJI: Heh? You\n"
    .string "came to save me?\p"
    .string "Thank you. But, I\n"
    .string "came here of my\l"
    .string "own free will.\p"
    .string "I came to calm\n"
    .string "the soul of\l"
    .string "CUBONEÙ mother.\p"
    .string "I think MAROWAKÙ\n"
    .string "spirit has gone\l"
    .string "to the afterlife.\p"
    .string "I must thank you\n"
    .string "for your kind\l"
    .string "concern!\p"
    .string "Follow me to my\n"
    .string "home, POKéMON\l"
    .string "HOUSE, at the foot\l"
    .string "of this tower.$"

