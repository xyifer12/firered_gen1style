VermilionCity_PokemonFanClub_Text_AdmirePikachusTail::
    .string "WonÑ you admire\n"
    .string "my PIKACHUÙ\l"
    .string "adorable tail?$"

VermilionCity_PokemonFanClub_Text_PikachuTwiceAsCute::
    .string "Humph! My PIKACHU\n"
    .string "is twice as cute\l"
    .string "as that one!$"

VermilionCity_PokemonFanClub_Text_AdoreMySeel::
    .string "I just love my\n"
    .string "SEEL!\p"

    .string "It squeals when I\n"
    .string "hug it!$"

VermilionCity_PokemonFanClub_Text_SeelFarMoreAttractive::
    .string "Oh dear!\p"

    .string "My SEEL is far\n"
    .string "more attractive!$"

VermilionCity_PokemonFanClub_Text_Pikachu::
    .string "PIKACHU: Chu!\n"
    .string "Pikachu!$"

VermilionCity_PokemonFanClub_Text_Seel::
    .string "SEEL: Kyuoo!$"

VermilionCity_PokemonFanClub_Text_DidYouComeToHearAboutMyMons::
    .string "I chair the\n"
    .string "POKéMON Fan Club!\p"

    .string "I have collected\n"
    .string "over 100 POKéMON!\p"

    .string "Iá very fussy\n"
    .string "when it comes to\l"
    .string "POKéMON!\p"

    .string "So...\p"

    .string "Did you come\n"
    .string "visit to hear\l"
    .string "about my POKéMON?$"

VermilionCity_PokemonFanClub_Text_ChairmansStory::
    .string "Good!\n"
    .string "Then listen up!\p"
    
    .string "My favorite\n"
    .string "RAPIDASH...\p"

    .string "ItÙ...cute...\n"
    .string "lovely...smart...\l"
    .string "plus...amazing...\l"
    .string "you think so?...\l"
    .string "oh yes...it...\l"
    .string "stunning...\l"
    .string "kindly...\l"
    .string "love it!\p"

    .string "Hug it...when...\n"
    .string "sleeping...warm\l"
    .string "and cuddly...\l"
    .string "spectacular...\l"
    .string "ravishing...\l"
    .string "...Oops! Look at\l"
    .string "the time! I kept\l"
    .string "you too long!\p"

    .string "Thanks for hearing\n"
    .string "me out! I want\l"
    .string "you to have this!$"

VermilionCity_PokemonFanClub_Text_ReceivedBikeVoucherFromChairman::
    .string "{PLAYER} received a\n"
    .string "BIKE VOUCHER!$"

VermilionCity_PokemonFanClub_Text_ExplainBikeVoucher::
    .string "Exchange that for\n"
    .string "a BICYCLE!\p"

    .string "DonÑ worry, my\n"
    .string "FEAROW will FLY\l"
    .string "me anywhere!\p"

    .string "So, I donÑ need a\n"
    .string "BICYCLE!\p"

    .string "I hope you like\n"
    .string "cycling!$"

VermilionCity_PokemonFanClub_Text_ComeBackToHearStory::
    .string "Oh. Come back\n"
    .string "when you want to\l"
    .string "hear my story!$"

VermilionCity_PokemonFanClub_Text_DidntComeToSeeAboutMonsAgain::
    .string "Hello, {PLAYER}!\p"

    .string "Did you come see\n"
    .string "me about my\l"
    .string "POKéMON again?\p"

    .string "No? Too bad!$"

VermilionCity_PokemonFanClub_Text_MakeRoomForThis::
    .string "Make room for\n"
    .string "this!$"

VermilionCity_PokemonFanClub_Text_ChairmanVeryVocalAboutPokemon::
    .string "Our Chairman is\n"
    .string "very vocal about\l"
    .string "POKéMON.$"

VermilionCity_PokemonFanClub_Text_ListenPolitelyToOtherTrainers::
    .string "LetÙ all listen\n"
    .string "politely to other\l"
    .string "trainers!$"

VermilionCity_PokemonFanClub_Text_SomeoneBragsBragBack::
    .string "If someone brags,\n"
    .string "brag right back!$"

VermilionCity_PokemonFanClub_Text_ChairmanReallyAdoresHisMons::
    .string "Our CHAIRMAN really does adore his\n"
    .string "POKéMON.\p"

    .string "But the person who is most liked by\n"
    .string "POKéMON is DAISY, I think.$"
