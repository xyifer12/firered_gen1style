ViridianCity_Gym_Text_GiovanniIntro::
    .string "Fwahahaha! This is\n"
    .string "my hideout!\p"
    .string "I planned to\n"
    .string "resurrect TEAM\l"
    .string "ROCKET here!\p"
    .string "But, you have\n"
    .string "caught me again!\l"
    .string "So be it! This\l"
    .string "time, Iá not\l"
    .string "holding back!\p"
    .string "Once more, you\n"
    .string "shall face\l"
    .string "GIOVANNI, the\l"
    .string "greatest trainer!{PLAY_BGM}{MUS_ENCOUNTER_ROCKET}$"

ViridianCity_Gym_Text_GiovanniDefeat::
    .string "GIOVANNI: Ha!\n"
    .string "That was a truly\l"
    .string "intense fight!\l"
    .string "You have won!\l"
    .string "As proof, here is\l"
    .string "the EARTHBADGE!\p"
    .string "{PAUSE_MUSIC}{PLAY_BGM}{MUS_OBTAIN_BADGE}{PAUSE 0xFE}{PAUSE 0x56}{RESUME_MUSIC}$"

ViridianCity_Gym_Text_GiovanniPostBattle::
    .string "Having lost, I\n"
    .string "cannot face my\l"
    .string "underlings!\l"
    .string "TEAM ROCKET is\l"
    .string "finished forever!\p"
    .string "I will dedicate my\n"
    .string "life to the study\l"
    .string "of POKéMON!\p"
    .string "Let us meet again\n"
    .string "some day!\l"
    .string "Farewell!$"

ViridianCity_Gym_Text_ExplainEarthBadgeTakeThis::
    .string "The EARTHBADGE\n"
    .string "makes POKéMON of\l"
    .string "any level obey!\p"
    .string "It is evidence of\n"
    .string "your mastery as a\l"
    .string "POKéMON trainer!\p"
    .string "With it, you can\n"
    .string "enter the POKéMON\l"
    .string "LEAGUE!\p"
    .string "It is my gift for\n"
    .string "your POKéMON\l"
    .string "LEAGUE challenge!$"

ViridianCity_Gym_Text_ReceivedTM27FromGiovanni::
    .string "{PLAYER} received\n"
    .string "TM27!$"

ViridianCity_Gym_Text_ExplainTM27::
    .string "TM27 is FISSURE!\n"
    .string "It will take out\l"
    .string "POKéMON with just\l"
    .string "one hit!\p"
    .string "I made it when I\n"
    .string "ran the GYM here,\l"
    .string "too long ago...$"

ViridianCity_Gym_Text_YouDoNotHaveSpace::
    .string "You do not have\n"
    .string "space for this!$"

ViridianCity_Gym_Text_YujiIntro::
    .string "Heh! You must be\n"
    .string "running out of\l"
    .string "steam by now!$"

ViridianCity_Gym_Text_YujiDefeat::
    .string "COOLTRAINER: I\n"
    .string "ran out of gas!$"

ViridianCity_Gym_Text_YujiPostBattle::
    .string "You need power to\n"
    .string "keep up with our\l"
    .string "GYM LEADER!$"

ViridianCity_Gym_Text_AtsushiIntro::
    .string "Rrrroar! Iá\n"
    .string "working myself\l"
    .string "into a rage!$"

ViridianCity_Gym_Text_AtsushiDefeat::
    .string "BLACKBELT: Wargh!$"

ViridianCity_Gym_Text_AtsushiPostBattle::
    .string "Iá still not\n"
    .string "worthy!$"

ViridianCity_Gym_Text_JasonIntro::
    .string "POKéMON and I, we\n"
    .string "make wonderful\l"
    .string "music together!$"

ViridianCity_Gym_Text_JasonDefeat::
    .string "ROCKER: You are in\n"
    .string "perfect harmony!$"

ViridianCity_Gym_Text_JasonPostBattle::
    .string "Do you know the\n"
    .string "identity of our\l"
    .string "GYM LEADER?$"

ViridianCity_Gym_Text_KiyoIntro::
    .string "Karate is the\n"
    .string "ultimate form of\l"
    .string "martial arts!$"

ViridianCity_Gym_Text_KiyoDefeat::
    .string "BLACKBELT: Atcho!$"

ViridianCity_Gym_Text_KiyoPostBattle::
    .string "If my POKéMON\n"
    .string "were as good at\l"
    .string "Karate as I...$"

ViridianCity_Gym_Text_WarrenIntro::
    .string "The truly talented\n"
    .string "win with style!$"

ViridianCity_Gym_Text_WarrenDefeat::
    .string "COOLTRAINER: I\n"
    .string "lost my grip!$"

ViridianCity_Gym_Text_WarrenPostBattle::
    .string "The LEADER will\n"
    .string "scold me!$"

ViridianCity_Gym_Text_TakashiIntro::
    .string "Iá the KARATE\n"
    .string "KING! Your fate\l"
    .string "rests with me!$"

ViridianCity_Gym_Text_TakashiDefeat::
    .string "BLACKBELT: Ayah!$"

ViridianCity_Gym_Text_TakashiPostBattle::
    .string "POKéMON LEAGUE?\n"
    .string "You? DonÑ get\l"
    .string "cocky!$"

ViridianCity_Gym_Text_ColeIntro::
    .string "Your POKéMON will\n"
    .string "cower at the\l"
    .string "crack of my whip!$"

ViridianCity_Gym_Text_ColeDefeat::
    .string "TAMER: Yowch!\n"
    .string "Whiplash!$"

ViridianCity_Gym_Text_ColePostBattle::
    .string "Wait! I was just\n"
    .string "careless!$"

ViridianCity_Gym_Text_SamuelIntro::
    .string "VIRIDIAN GYM was\n"
    .string "closed for a long\l"
    .string "time, but now our\l"
    .string "LEADER is back!$"

ViridianCity_Gym_Text_SamuelDefeat::
    .string "COOLTRAINER♂: I\n"
    .string "was beaten?$"

ViridianCity_Gym_Text_SamuelPostBattle::
    .string "You can go onto\n"
    .string "POKéMON LEAGUE\l"
    .string "only by defeating\l"
    .string "our GYM LEADER!$"

ViridianCity_Gym_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"
    .string "Even I donÑ know\n"
    .string "VIRIDIAN LEADERÙ\l"
    .string "identity!\p"
    .string "This will be the\n"
    .string "toughest of all\l"
    .string "the GYM LEADERs!\p"
    .string "I heard that the\n"
    .string "trainers here\l"
    .string "like ground-type\l"
    .string "POKéMON!$"

ViridianCity_Gym_Text_GymGuyPostVictory::
    .string "Blow me away!\n"
    .string "GIOVANNI was the\l"
    .string "GYM LEADER here?$"

ViridianCity_Gym_Text_GymStatue::
    .string "VIRIDIAN CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: GIOVANNI\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

ViridianCity_Gym_Text_GymStatuePlayerWon::
    .string "VIRIDIAN CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: GIOVANNI\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

