PokemonTower_2F_Text_RivalIntro::
    .string "{RIVAL}: Hey,\n"
    .string "{PLAYER}! What\l"
    .string "brings you here?\l"
    .string "Your POKéMON\l"
    .string "donÑ look dead!\p"
    .string "I can at least\n"
    .string "make them faint!\l"
    .string "LetÙ go, pal!$"

PokemonTower_2F_Text_RivalDefeat::
    .string "{RIVAL}: What?\n"
    .string "You stinker!\p"
    .string "I took it easy on\n"
    .string "you too!$"

@ Unused. Translated below
@ Aw, man! They really kicked the bucket! Weak! Do them a favor and raise them more properly.
PokemonTower_2F_Text_RivalVictory::
    .string "{RIVAL}“あーあ‥！\n"
    .string "ほんとに　くたばっちまったぞ！\l"
    .string "よわいなー！\l"
    .string "もっと　ちゃんと　そだてて　やれよ$"

PokemonTower_2F_Text_RivalPostBattle::
    .string "HowÙ your POKéDEX\n"
    .string "coming, pal?\l"
    .string "I just caught a\l"
    .string "CUBONE!\p"
    .string "I canÑ find the\n"
    .string "grown-up MAROWAK\l"
    .string "yet!\p"
    .string "I doubt there are\n"
    .string "any left! Well, I\l"
    .string "better get going!\l"
    .string "Iße got a lot to\l"
    .string "accomplish, pal!\p"
    .string "Smell ya later!$"

PokemonTower_2F_Text_SilphScopeCouldUnmaskGhosts::
    .string "Even we could not\n"
    .string "identify the\l"
    .string "wayward GHOSTs!\p"
    .string "A SILPH SCOPE\n"
    .string "might be able to\l"
    .string "unmask them.$"

