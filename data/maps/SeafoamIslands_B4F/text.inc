Text_RockSlideTeach::
    .string "When youàe up on a rocky mountain\n"
    .string "like this, rockslides are a threat.\p"
    .string "Can you imagine?\n"
    .string "Boulders tumbling down on you?\p"
    .string "ThatÚ be, like, waaaaaaaaaaah!\n"
    .string "Total terror!\p"
    .string "You donÑ seem to be scared.\n"
    .string "Want to try using ROCK SLIDE?$"

Text_RockSlideDeclined::
    .string "Oh, so you are scared after all.$"

Text_RockSlideWhichMon::
    .string "Which POKéMON should I teach\n"
    .string "ROCK SLIDE?$"

Text_RockSlideTaught::
    .string "It might be scary to use it in this\n"
    .string "tunnel…$"

SeafoamIslands_B4F_Text_BouldersMightChangeWaterFlow::
    .string "Boulders might\n"
    .string "change the flow\l"
    .string "of water!$"

SeafoamIslands_B4F_Text_DangerFastCurrent::
    .string "DANGER\n"
    .string "Fast current!$"

