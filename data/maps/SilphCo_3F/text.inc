SilphCo_3F_Text_WhatAmIToDo::
    .string "I work for SILPH.\n"
    .string "What am I to do?$"

SilphCo_3F_Text_YouAndYourMonsSavedUs::
    .string "{PLAYER}! You and\n"
    .string "your POKéMON\l"
    .string "saved us!$"

SilphCo_3F_Text_GruntIntro::
    .string "Quit messing with\n"
    .string "us, kid!$"

SilphCo_3F_Text_GruntDefeat::
    .string "ROCKET: I give\n"
    .string "up!$"

SilphCo_3F_Text_GruntPostBattle::
    .string "A hint? You can\n"
    .string "open doors with a\l"
    .string "CARD KEY!$"

SilphCo_3F_Text_JoseIntro::
    .string "I support TEAM\n"
    .string "ROCKET more than\l"
    .string "I support SILPH!$"

SilphCo_3F_Text_JoseDefeat::
    .string "SCIENTIST: You\n"
    .string "really got me!$"

SilphCo_3F_Text_JosePostBattle::
    .string "Humph...\p"
    .string "TEAM ROCKET said\n"
    .string "that if I helped\l"
    .string "them, theyÚ let\l"
    .string "me study POKéMON!$"

SilphCo_3F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "3F$"

