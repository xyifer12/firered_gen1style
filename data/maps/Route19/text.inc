Route19_Text_RichardIntro::
    .string "Have to warm up\n"
    .string "before my swim!$"

Route19_Text_RichardDefeat::
    .string "SWIMMER: All\n"
    .string "warmed up!$"

Route19_Text_RichardPostBattle::
    .string "Thanks, kid! Iá\n"
    .string "ready for a swim!$"

Route19_Text_ReeceIntro::
    .string "Wait!\n"
    .string "YouÛl have\l"
    .string "a heart attack!$"

Route19_Text_ReeceDefeat::
    .string "SWIMMER: Ooh!\n"
    .string "ThatÙ chilly!$"

Route19_Text_ReecePostBattle::
    .string "Watch out for\n"
    .string "TENTACOOL!$"

Route19_Text_MatthewIntro::
    .string "I love swimming!\n"
    .string "What about you?$"

Route19_Text_MatthewDefeat::
    .string "SWIMMER: Belly\n"
    .string "flop!$"

Route19_Text_MatthewPostBattle::
    .string "I can beat POKéMON\n"
    .string "at swimming!$"

Route19_Text_DouglasIntro::
    .string "WhatÙ beyond the\n"
    .string "horizon?$"

Route19_Text_DouglasDefeat::
    .string "SWIMMER: Glub!$"

Route19_Text_DouglasPostBattle::
    .string "I see a couple of\n"
    .string "islands!$"

Route19_Text_DavidIntro::
    .string "I tried diving\n"
    .string "for POKéMON, but\l"
    .string "it was a no go!$"

Route19_Text_DavidDefeat::
    .string "SWIMMER: Help!$"

Route19_Text_DavidPostBattle::
    .string "You have to fish\n"
    .string "for sea POKéMON!$"

Route19_Text_TonyIntro::
    .string "I look at the\n"
    .string "sea to forget!$"

Route19_Text_TonyDefeat::
    .string "Ooh!\n"
    .string "Traumatic!$"

Route19_Text_TonyPostBattle::
    .string "Iá looking at the\n"
    .string "sea to forget!$"

Route19_Text_AnyaIntro::
    .string "Oh, I just love\n"
    .string "your ride! Can I\l"
    .string "have it if I win?$"

Route19_Text_AnyaDefeat::
    .string "BEAUTY: Oh!\n"
    .string "I lost!$"

Route19_Text_AnyaPostBattle::
    .string "ItÙ still a long\n"
    .string "way to go to\l"
    .string "SEAFOAM ISLANDS.$"

Route19_Text_AliceIntro::
    .string "SwimmingÙ great!\n"
    .string "Sunburns arenÑ!$"

Route19_Text_AliceDefeat::
    .string "BEAUTY: Shocker!$"

Route19_Text_AlicePostBattle::
    .string "My boy friend\n"
    .string "wanted to swim to\l"
    .string "SEAFOAM ISLANDS.$"

Route19_Text_AxleIntro::
    .string "These waters are\n"
    .string "treacherous!$"

Route19_Text_AxleDefeat::
    .string "SWIMMER: Ooh!\n"
    .string "Dangerous!$"

Route19_Text_AxlePostBattle::
    .string "I got a cramp!\n"
    .string "Glub, glub...$"

Route19_Text_ConnieIntro::
    .string "I swam here, but\n"
    .string "Iá tired.$"

Route19_Text_ConnieDefeat::
    .string "BEAUTY: Iá\n"
    .string "exhausted...$"

Route19_Text_ConniePostBattle::
    .string "LAPRAS is so big,\n"
    .string "it must keep you\l"
    .string "dry on water.$"

Route19_Text_RouteSign::
    .string "SEA ROUTE 19\n"
    .string "FUCHSIA CITY -\l"
    .string "SEAFOAM ISLANDS$"

Route19_Text_LiaIntro::
    .string "LIA: Iá looking after my brother.\n"
    .string "He just became a TRAINER.$"

Route19_Text_LiaDefeat::
    .string "LIA: ThatÙ no way to treat my\n"
    .string "little brother!$"

Route19_Text_LiaPostBattle::
    .string "LIA: Do you have a younger\n"
    .string "brother?\p"
    .string "I hope youàe teaching him all\n"
    .string "sorts of things.$"

Route19_Text_LiaNotEnoughMons::
    .string "LIA: I want to battle together\n"
    .string "with my little brother.\p"
    .string "DonÑ you have two POKéMON?$"

Route19_Text_LucIntro::
    .string "LUC: My big sis taught me how\n"
    .string "to swim and train POKéMON.$"

Route19_Text_LucDefeat::
    .string "LUC: Oh, wow!\n"
    .string "Someone tougher than my big sis!$"

Route19_Text_LucPostBattle::
    .string "LUC: My big sis is strong and nice.\n"
    .string "I think sheÙ awesome!$"

Route19_Text_LucNotEnoughMons::
    .string "LUC: I donÑ want to if I canÑ\n"
    .string "battle you with my big sis.\p"
    .string "DonÑ you have two POKéMON?$"
