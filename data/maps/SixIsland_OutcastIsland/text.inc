SixIsland_OutcastIsland_Text_RocketIntro::
    .string "Thereàe no rare POKéMON around\n"
    .string "these parts! Not a one!\p"
    .string "That burns me up, man.\n"
    .string "IÛl take it out on you!$"

SixIsland_OutcastIsland_Text_RocketDefeat::
    .string "…Huh?$"

SixIsland_OutcastIsland_Text_RocketPostBattle::
    .string "So listen, you havenÑ seen any\n"
    .string "rare POKéMON, have you?$"

SixIsland_OutcastIsland_Text_TylorIntro::
    .string "Iá having no luck at all.\n"
    .string "A battleÚ be a change of pace!$"

SixIsland_OutcastIsland_Text_TylorDefeat::
    .string "Nope, no luck at all…$"

SixIsland_OutcastIsland_Text_TylorPostBattle::
    .string "I canÑ very well go home without\n"
    .string "catching something, though.$"

SixIsland_OutcastIsland_Text_MymoIntro::
    .string "Gasp… Gasp…\p"
    .string "I swam here from SIX ISLE PORT\n"
    .string "in one go.$"

SixIsland_OutcastIsland_Text_MymoDefeat::
    .string "Gasp…\n"
    .string "Gasp…$"

SixIsland_OutcastIsland_Text_MymoPostBattle::
    .string "Iá only at the halfway point…\n"
    .string "Iá beat…$"

SixIsland_OutcastIsland_Text_NicoleIntro::
    .string "ItÙ not so easy sending POKéMON\n"
    .string "out while swimming, you know?$"

SixIsland_OutcastIsland_Text_NicoleDefeat::
    .string "I didnÑ lose to you at swimming.\n"
    .string "This doesnÑ bother me.$"

SixIsland_OutcastIsland_Text_NicolePostBattle::
    .string "Are you headed for the island up\n"
    .string "past here?\p"
    .string "I didnÑ see anything interesting\n"
    .string "there.$"

SixIsland_OutcastIsland_Text_AvaIntro::
    .string "AVA: LetÙ have a two-on-two\n"
    .string "marine battle!$"

SixIsland_OutcastIsland_Text_AvaDefeat::
    .string "AVA: Oh, youàe amazing!\n"
    .string "Even better, youàe on your own!$"

SixIsland_OutcastIsland_Text_AvaPostBattle::
    .string "AVA: You know, I do prefer the\n"
    .string "sea over any pool.$"

SixIsland_OutcastIsland_Text_AvaNotEnoughMons::
    .string "AVA: Youàe challenging us to\n"
    .string "a battle?\p"
    .string "YouÛl need at least two POKéMON\n"
    .string "if you want to do that.$"

SixIsland_OutcastIsland_Text_GebIntro::
    .string "GEB: Big Sister, help!\n"
    .string "Please battle with me!$"

SixIsland_OutcastIsland_Text_GebDefeat::
    .string "GEB: Wow, Big Sister, this personÙ\n"
    .string "really good!$"

SixIsland_OutcastIsland_Text_GebPostBattle::
    .string "GEB: Iá hanging onto my sister\n"
    .string "because I canÑ touch the bottom.$"

SixIsland_OutcastIsland_Text_GebNotEnoughMons::
    .string "GEB: Please battle against me and\n"
    .string "my sister!\p"
    .string "…Oh, you donÑ have two POKéMON?$"
