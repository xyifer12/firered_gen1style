CeladonCity_Text_GotMyKoffingInCinnabar::
    .string "I got my KOFFING\n"
    .string "in CINNABAR!\p"
    .string "ItÙ nice, but it\n"
    .string "breathes poison\l"
    .string "when itÙ angry!$"

CeladonCity_Text_GymIsGreatFullOfWomen::
    .string "Heheh! This GYM\n"
    .string "is great! ItÙ\l"
    .string "full of women!$"

CeladonCity_Text_GameCornerIsBadForCitysImage::
    .string "The GAME CORNER\n"
    .string "is bad for our\l"
    .string "cityÙ image.$"

CeladonCity_Text_BlewItAllAtSlots::
    .string "Moan! I blew it\n"
    .string "all at the slots!\p"
    .string "I knew I should\n"
    .string "have cashed in my\l"
    .string "coins for prizes!$"

CeladonCity_Text_MyTrustedPalPoliwrath::
    .string "This is my trusted\n"
    .string "pal, POLIWRATH!\p"
    .string "It evolved from\n"
    .string "POLIWHIRL when I\l"
    .string "used WATER STONE!$"

CeladonCity_Text_Poliwrath::
    .string "POLIWRATH: Ribi\n"
    .string "ribit!$"

CeladonCity_Text_GetLostOrIllPunchYou::
    .string "What are you\n"
    .string "staring at?$"

CeladonCity_Text_KeepOutOfTeamRocketsWay::
    .string "Keep out of TEAM\n"
    .string "ROCKETÙ way!$"

CeladonCity_Text_ExplainXAccuracyDireHit::
    .string "TRAINER TIPS\p"
    .string "X ACCURACY boosts\n"
    .string "the accuracy of\l"
    .string "techniques.\p"
    .string "DIRE HIT jacks up\n"
    .string "the likelihood of\l"
    .string "critical hits.\p"
    .string "Get your items at\n"
    .string "CELDON DEPT.\l"
    .string "STORE!$"

CeladonCity_Text_CitySign::
    .string "CELADON CITY\n"
    .string "The City of\l"
    .string "Rainbow Dreams$"

CeladonCity_Text_GymSign::
    .string "CELADON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: ERIKA\p"
    .string "The Nature Loving\n"
    .string "Princess!$"

CeladonCity_Text_MansionSign::
    .string "CELADON MANSION$"

CeladonCity_Text_DeptStoreSign::
    .string "Find what you\n"
    .string "need at CELADON\l"
    .string "DEPT. STORE!$"

CeladonCity_Text_GuardSpecProtectsFromStatus::
    .string "TRAINER TIPS\p"
    .string "GUARD SPEC.\n"
    .string "protects POKéMON\l"
    .string "against SPECIAL\l"
    .string "attacks such as\l"
    .string "fire and water!\p"
    .string "Get your items at\n"
    .string "CELADON DEPT.\l"
    .string "STORE!$"

CeladonCity_Text_PrizeExchangeSign::
    .string "Coins exchanged\n"
    .string "for prizes!\l"
    .string "PRIZE EXCHANGE$"

CeladonCity_Text_GameCornerSign::
    .string "ROCKET GAME CORNER\n"
    .string "The playground\l"
    .string "for grown-ups!$"

CeladonCity_Text_ScaldedTongueOnTea::
    .string "Aaaagh, ow...\n"
    .string "I scalded my tongue!\p"
    .string "This nice old lady in the MANSION\n"
    .string "gave me some TEA.\p"
    .string "But it was boiling hot!\n"
    .string "Gotta cool it to drink it.$"

CeladonCity_Text_TakeTM41::
    .string "Hello, there!\p"
    .string "Iße seen you,\n"
    .string "but I never had a\l"
    .string "chance to talk!\p"
    .string "HereÙ a gift for\n"
    .string "dropping by!$"

CeladonCity_Text_PutTM41Away::
    .string "{PLAYER} put TM41 away in\n"
    .string "the BAGÙ TM CASE.$"

CeladonCity_Text_TM41_Contains::
    .string "TM41 teaches\n"
    .string "SOFTBOILED!\p"
    .string "Only one POKéMON\n"
    .string "can use it!\p"
    .string "That POKéMON is\n"
    .string "CHANSEY!$"

CeladonCity_Text_SomeoneStoleSilphScope::
    .string "Oh, what am I to do…\p"
    .string "Someone stole our SILPH SCOPE.\p"
    .string "The thief came running this way,\n"
    .string "Iá sure of it.\p"
    .string "But I lost sight of him!\n"
    .string "WhereÚ he go?$"

