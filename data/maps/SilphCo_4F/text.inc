SilphCo_4F_Text_CantYouSeeImHiding::
    .string "Sssh! CanÑ you\n"
    .string "see Iá hiding?$"

SilphCo_4F_Text_TeamRocketIsGone::
    .string "Huh? TEAM ROCKET\n"
    .string "is gone?$"

SilphCo_4F_Text_Grunt1Intro::
    .string "TEAM ROCKET has\n"
    .string "taken command of\l"
    .string "SILPH CO.!$"

SilphCo_4F_Text_Grunt1Defeat::
    .string "ROCKET: Arrgh!$"

SilphCo_4F_Text_Grunt1PostBattle::
    .string "Fwahahaha!\n"
    .string "My BOSS has been\l"
    .string "after this place!$"

SilphCo_4F_Text_RodneyIntro::
    .string "My POKéMON are my\n"
    .string "loyal soldiers!$"

SilphCo_4F_Text_RodneyDefeat::
    .string "SCIENTIST: Darn!\n"
    .string "You weak POKéMON!$"

SilphCo_4F_Text_RodneyPostBattle::
    .string "The doors are\n"
    .string "electronically\l"
    .string "locked! A CARD\p"
    .string "KEY opens them!$"

SilphCo_4F_Text_Grunt2Intro::
    .string "Intruder spotted!$"

SilphCo_4F_Text_Grunt2Defeat::
    .string "GRUNT: Who\n"
    .string "are you?$"

SilphCo_4F_Text_Grunt2PostBattle::
    .string "I better tell the\n"
    .string "BOSS on 11F!$"

SilphCo_4F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "4F$"

