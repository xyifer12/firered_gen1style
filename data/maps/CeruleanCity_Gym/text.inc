CeruleanCity_Gym_Text_MistyIntro::
    .string "Hi, youàe a new\n"
    .string "face!\p"
    .string "Trainers who want\n"
    .string "to turn pro have\l"
    .string "to have a policy\l"
    .string "about POKéMON!\p"
    .string "What is your\n"
    .string "approach when you\l"
    .string "catch POKéMON?\p"
    .string "My policy is an\n"
    .string "all-out offensive\l"
    .string "with WATER-type\l"
    .string "POKéMON!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

CeruleanCity_Gym_Text_ExplainTM11::
    .string "TM11 teaches\n"
    .string "BUBBLEBEAM!\p"
    .string "Use it on an\n"
    .string "aquatic POKéMON!$"

CeruleanCity_Gym_Text_ExplainCascadeBadge::
    .string "The CASCADEBADGE\n"
    .string "makes all POKéMON\l"
    .string "up to L30 obey!\p"
    .string "That includes\n"
    .string "even outsiders!\p"
    .string "ThereÙ more, you\n"
    .string "can now use CUT\l"
    .string "any time!\p"
    .string "You can CUT down\n"
    .string "small bushes to\l"
    .string "open new paths!\p"
    .string "You can also have\n"
    .string "my favorite TM!$"

CeruleanCity_Gym_Text_ReceivedTM11FromMisty::
    .string "{PLAYER} received\n"
    .string "TM11!$"

CeruleanCity_Gym_Text_BetterMakeRoomForThis::
    .string "You better make\n"
    .string "room for this!$"

CeruleanCity_Gym_Text_MistyDefeat::
    .string "MISTY: Wow!\n"
    .string "Youàe too much!\p"
    .string "All right!\p"
    .string "You can have the\n"
    .string "CASCADEBADGE to\l"
    .string "show you beat me!$"

CeruleanCity_Gym_Text_DianaIntro::
    .string "Iá more than good\n"
    .string "enough for you!\p"
    .string "MISTY can wait!$"

CeruleanCity_Gym_Text_DianaDefeat::
    .string "JR.TRAINER♀: You\n"
    .string "overwhelmed me!$"

CeruleanCity_Gym_Text_DianaPostBattle::
    .string "You have to face\n"
    .string "other trainers to\l"
    .string "find out how good\l"
    .string "you really are.$"

CeruleanCity_Gym_Text_LuisIntro::
    .string "Splash!\p"
    .string "Iá first up!\n"
    .string "LetÙ do it!$"

CeruleanCity_Gym_Text_LuisDefeat::
    .string "SWIMMER: That\n"
    .string "canÑ be!$"

CeruleanCity_Gym_Text_LuisPostBattle::
    .string "MISTY is going to\n"
    .string "keep improving.\p"
    .string "She wonÑ lose to\n"
    .string "someone like you!$"

CeruleanCity_Gym_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"
    .string "HereÙ my advice!\p"
    .string "The LEADER, MISTY,\n"
    .string "is a pro who uses\l"
    .string "water POKéMON!\p"
    .string "You can drain all\n"
    .string "their water with\l"
    .string "plant POKéMON!\p"
    .string "Or, zap them with\n"
    .string "electricity!$"

CeruleanCity_Gym_Text_WeMakePrettyGoodTeam::
    .string "You beat MISTY!\n"
    .string "WhatÚ I tell ya?\p"
    .string "You and me kid,\n"
    .string "we make a pretty\l"
    .string "darn good team!$"

CeruleanCity_Gym_Text_GymStatue::
    .string "CERULEAN CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: MISTY\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CeruleanCity_Gym_Text_GymStatuePlayerWon::
    .string "CERULEAN CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: MISTY\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

