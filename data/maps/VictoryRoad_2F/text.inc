VictoryRoad_2F_Text_DawsonIntro::
    .string "If you can get\n"
    .string "through here, you\l"
    .string "can go meet the\l"
    .string "ELITE FOUR.$"

VictoryRoad_2F_Text_DawsonDefeat::
    .string "SUPER NERD: No!\n"
    .string "Unbelievable!$"

VictoryRoad_2F_Text_DawsonPostBattle::
    .string "I can beat you\n"
    .string "when it comes to\l"
    .string "knowledge about\l"
    .string "POKéMON!$"

VictoryRoad_2F_Text_DaisukeIntro::
    .string "VICTORY ROAD is\n"
    .string "the final test\l"
    .string "for trainers!$"

VictoryRoad_2F_Text_DaisukeDefeat::
    .string "BLACKBELT: Aiyah!$"

VictoryRoad_2F_Text_DaisukePostBattle::
    .string "If you get stuck,\n"
    .string "try moving some\l"
    .string "boulders around!$"

VictoryRoad_2F_Text_NelsonIntro::
    .string "Ah, so you wish\n"
    .string "to challenge the\l"
    .string "ELITE FOUR?$"

VictoryRoad_2F_Text_NelsonDefeat::
    .string "JUGGLER: You\n"
    .string "got me!$"

VictoryRoad_2F_Text_NelsonPostBattle::
    .string "{RIVAL} also came\n"
    .string "through here!$"

VictoryRoad_2F_Text_VincentIntro::
    .string "Come on!\n"
    .string "IÛl whip you!$"

VictoryRoad_2F_Text_VincentDefeat::
    .string "TAMER: I got\n"
    .string "whipped!$"

VictoryRoad_2F_Text_VincentPostBattle::
    .string "You earned the\n"
    .string "right to be on\l"
    .string "VICTORY ROAD!$"

VictoryRoad_2F_Text_GregoryIntro::
    .string "Is VICTORY ROAD\n"
    .string "too tough?$"

VictoryRoad_2F_Text_GregoryDefeat::
    .string "JUGGLER: Well\n"
    .string "done!$"

VictoryRoad_2F_Text_GregoryPostBattle::
    .string "Many trainers give\n"
    .string "up the challenge\l"
    .string "here.$"

@ Unused, old text for Moltres
VictoryRoad_2F_Text_Gyaoo::
    .string "ギヤーオ！$"

