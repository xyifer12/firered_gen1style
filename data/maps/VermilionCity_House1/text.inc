VermilionCity_House1_Text_ImFishingGuruDoYouLikeToFish::
    .string "Iá the FISHING\n"
    .string "GURU!\p"
    .string "I simply Looove\n"
    .string "fishing!\p"
    .string "Do you like to\n"
    .string "fish?$"

VermilionCity_House1_Text_TakeThisAndFish::
    .string "Grand! I like\n"
    .string "your style.\p"
    .string "Take this and\n"
    .string "fish, young one!$"

VermilionCity_House1_Text_ReceivedOldRodFromFishingGuru::
    .string "{PLAYER} got an\n"
    .string "OLD ROD!$"

VermilionCity_House1_Text_FishingIsAWayOfLife::
    .string "Fishing is a way\n"
    .string "of life!\p"
    .string "From the seas to\n"
    .string "rivers, go out\l"
    .string "and land the big\l"
    .string "one, young one!$"

VermilionCity_House1_Text_OhThatsSoDisappointing::
    .string "Oh... ThatÙ so\n"
    .string "disappointing...$"

VermilionCity_House1_Text_HowAreTheFishBiting::
    .string "Hello there,\n"
    .string "{PLAYER}!\p"
    .string "How are the fish\n"
    .string "biting?$"

VermilionCity_House1_Text_NoRoomForNiceGift::
    .string "Oh, no!\p"
    .string "You have no room\n"
    .string "for my gift!$"

