ThreeIsland_Text_IslandSign::
    .string "THREE ISLAND\n"
    .string "Kin Island of Family Bonding$"

ThreeIsland_Text_IslandBelongsToUs::
    .string "Hyahoo!\p"
    .string "From this day on, this island\n"
    .string "belongs to us!$"

ThreeIsland_Text_GoBackToKanto::
    .string "We donÑ need you people bringing\n"
    .string "your noise and trouble here!\p"
    .string "Weàe asking you to go back to\n"
    .string "KANTO!$"

ThreeIsland_Text_BossIsOnHisWay::
    .string "Hey, go cry somewhere else.\n"
    .string "Our boss is on his way.\p"
    .string "When he gets here, weÛl give you\n"
    .string "a k-rad motorbike show you wonÑ\l"
    .string "soon forget!$"

ThreeIsland_Text_GetOffIslandNow::
    .string "W-what!? Not on your life!\n"
    .string "Get off the island now!$"

ThreeIsland_Text_WhosGonnaMakeMe::
    .string "WhoÙ gonna make me?$"

ThreeIsland_Text_AreYouBossGoBackToKanto::
    .string "Are you the boss?\n"
    .string "Go back to KANTO right now!$"

ThreeIsland_Text_JustGotHerePal::
    .string "Hah?\p"
    .string "I just got here, pal.\p"
    .string "WhatÙ with the hostile attitude?\n"
    .string "ItÙ mighty cold of you!$"

ThreeIsland_Text_FollowersRaisingHavoc::
    .string "Your gang of followers have been\n"
    .string "raising havoc on their bikes.\p"
    .string "Do you have any idea how much\n"
    .string "trouble theyße caused us on the\l"
    .string "island?$"

ThreeIsland_Text_OughtToBeThankingUs::
    .string "No, man, I donÑ get it at all.\p"
    .string "Look at this place.\n"
    .string "What do you do for entertainment?\p"
    .string "You ought to be thanking us for\n"
    .string "livening up this sleepy village.\p"
    .string "But hey, if you insist, you can try\n"
    .string "making us leave.$"

ThreeIsland_Text_YouCowardsToughInPack::
    .string "Grr… You cowards…\n"
    .string "So tough in a pack…$"

ThreeIsland_Text_WannaMakeSomethingOfYourStaring::
    .string "You, what are you staring at?\n"
    .string "DonÑ you know itÙ not polite?\p"
    .string "You wanna make something of it\n"
    .string "or what?$"

ThreeIsland_Text_ThatsSmart::
    .string "ThatÙ smart.\n"
    .string "Keep your nose out of this.$"

ThreeIsland_Text_Biker1Intro::
    .string "Heh, I like your guts.\n"
    .string "YouÛl be losing money to me, but…$"

ThreeIsland_Text_Biker1Defeat::
    .string "Wha…\n"
    .string "What is this kid?!$"

ThreeIsland_Text_Biker1PostBattle::
    .string "Aww, man…\n"
    .string "DonÑ you dare laugh!$"

ThreeIsland_Text_Biker2Intro::
    .string "ArenÑ you from KANTO?\n"
    .string "You should be on our side!$"

ThreeIsland_Text_Biker2Defeat::
    .string "Stop fooling around!$"

ThreeIsland_Text_Biker2PostBattle::
    .string "WhatÙ the matter with you,\n"
    .string "getting all hot like that?\p"
    .string "Totally uncool, man!$"

ThreeIsland_Text_Biker3Intro::
    .string "We invited the boss out here,\n"
    .string "but you had to mess it up!\p"
    .string "You embarrassed us, man!$"

ThreeIsland_Text_Biker3Defeat::
    .string "… … …   … … …$"

ThreeIsland_Text_Biker3PostBattle::
    .string "Boss, Iá telling you, youße gotta\n"
    .string "do something about this kid!$"

ThreeIsland_Text_PaxtonIntro::
    .string "Iße been watching you, and IÚ say\n"
    .string "youße done enough.\p"
    .string "What are you, their friend or\n"
    .string "something?\p"
    .string "Then I guess youÛl be battling me\n"
    .string "in their place.$"

ThreeIsland_Text_PaxtonDefeat::
    .string "All right, enough!\n"
    .string "WeÛl leave like you wanted!\p"
    .string "WeÛl be happy to see the last of\n"
    .string "this boring island!$"

ThreeIsland_Text_PaxtonPostBattle::
    .string "Humph! Yeah, go right on hanging\n"
    .string "around with these hayseeds!$"

ThreeIsland_Text_ThankYouOhYourMonGotHurt::
    .string "Thank you! Those goons were\n"
    .string "nothing but bad trouble.\p"
    .string "Oh, look, your POKéMON got hurt\n"
    .string "on my account.$"

ThreeIsland_Text_GivenFullRestore::
    .string "{PLAYER} was given\n"
    .string "a FULL RESTORE.$"

ThreeIsland_Text_OhYourBagIsFull::
    .string "Oh?\n"
    .string "Your BAG is full.$"

ThreeIsland_Text_YouveGotImpressiveMons::
    .string "Youße got seriously impressive\n"
    .string "POKéMON with you.$"

ThreeIsland_Text_LostelleWentOffTowardsBondBridge::
    .string "ItÚ be fantastic if someone as\n"
    .string "strong as you lived here.\p"
    .string "I hope youÛl at least stay here\n"
    .string "a while.\p"
    .string "…I beg your pardon?\n"
    .string "Youàe looking for LOSTELLE?\p"
    .string "LOSTELLE went off towards BOND\n"
    .string "BRIDGE a while ago.$"

ThreeIsland_Text_WouldntWantToSeeBikersHereAgain::
    .string "THREE ISLAND is actually the most\n"
    .string "populous of the islands here.\p"
    .string "Still, it could be less sleepy.\p"
    .string "But I wouldnÑ want to see goons\n"
    .string "like those BIKERS here again.$"

ThreeIsland_Text_WhenDodouEvolvesGoingToPlayGame::
    .string "Iá going to train my DODUO in the\n"
    .string "BERRY FOREST.\p"
    .string "When it evolves, Iá going to play\n"
    .string "a game on TWO ISLAND.$"

ThreeIsland_Text_Doduo::
    .string "DODUO: Gigiih!$"

