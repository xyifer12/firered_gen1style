CeladonCity_DepartmentStore_2F_Text_SuperRepelMorePowerfulRepel::
    .string "SUPER REPEL keeps\n"
    .string "weak POKéMON at\l"
    .string "bay...\p"
    .string "Hmm, itÙ a more\n"
    .string "powerful REPEL!$"

CeladonCity_DepartmentStore_2F_Text_BuyReviveForLongOutings::
    .string "For long outings,\n"
    .string "you should buy\l"
    .string "REVIVE.$"

CeladonCity_DepartmentStore_3F_Text_TakeTM18::
    .string "Oh, hi! I finally\n"
    .string "finished POKéMON!\p"
    .string "Not done yet?\n"
    .string "This might be\l"
    .string "useful!$"

CeladonCity_DepartmentStore_3F_Text_PutTM18Away::
    .string "{PLAYER} put TM18 away in\n"
    .string "the BAGÙ TM CASE.$"

CeladonCity_DepartmentStore_3F_Text_TM18_Contains::
    .string "TM18 is COUNTER!\n"
    .string "Not like the one\l"
    .string "Iá leaning on,\l"
    .string "mind you!$"

CeladonCity_DepartmentStore_2F_Text_FloorSign::
    .string "Top Grade Items\n"
    .string "for TRAINERS!\p"
    .string "2F: TRAINER'S\n"
    .string "    MARKET$"

CeladonCity_DepartmentStore_2F_Text_LanceComesToBuyCapes::
    .string "We have a customer, LANCE, who\n"
    .string "occasionally comes.\p"
    .string "He always buys capes.\l"
    .string "I wonder... Does he have many\n"
    .string "identical capes at home?$"
