CeladonCity_Condominiums_3F_Text_ImTheProgrammer::
    .string "Me? Iá the\n"
    .string "programmer!$"

CeladonCity_Condominiums_3F_Text_ImTheGraphicArtist::
    .string "Iá the graphic\n"
    .string "artist!\l"
    .string "I drew you!$"

CeladonCity_Condominiums_3F_Text_IWroteTheStory::
    .string "I wrote the story!\n"
    .string "IsnÑ ERIKA cute?\p"
    .string "I like MISTY a\n"
    .string "lot too!\p"
    .string "Oh, and SABRINA,\n"
    .string "I like her!$"

CeladonCity_Condominiums_3F_Text_ImGameDesignerShowMeFinishedPokedex::
    .string "Is that right?\p"
    .string "Iá the game\n"
    .string "designer!\p"
    .string "Filling up your\n"
    .string "POKéDEX is tough,\l"
    .string "but donÑ quit!\p"
    .string "When you finish,\n"
    .string "come tell me!$"

CeladonCity_Condominiums_3F_Text_CompletedPokedexCongratulations::
    .string "Wow! Excellent!\n"
    .string "You completed\l"
    .string "your POKéDEX!\l"
    .string "Congratulations!\l"
    .string "...$"

CeladonCity_Condominiums_3F_Text_ItsTheGameProgram::
    .string "ItÙ the game\n"
    .string "program! Messing\l"
    .string "with it could bug\l"
    .string "out the game!$"

CeladonCity_Condominiums_3F_Text_SomeonesPlayingGame::
    .string "SomeoneÙ playing\n"
    .string "a game instead of\l"
    .string "working!$"

CeladonCity_Condominiums_3F_Text_ItsScriptBetterNotLookAtEnding::
    .string "ItÙ the script!\n"
    .string "Better not look\l"
    .string "at the ending!$"

CeladonCity_Condominiums_3F_Text_GameFreakDevelopmentRoom::
    .string "GAME FREAK\n"
    .string "Development Room$"

