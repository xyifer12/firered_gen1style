Route7_EastEntrance_Text_ThirstyOnGuardDuty::
    .string "Iá on guard duty.\n"
    .string "Gee, Iá thirsty,\l"
    .string "though!\p"
    .string "Oh wait there,\n"
    .string "the roadÙ closed.$"

Route7_EastEntrance_Text_ThatTeaLooksTasty::
    .string "Whoa, boy!\n"
    .string "Iá parched!$"

Route7_EastEntrance_Text_ThanksIllShareTeaWithGuards::
    .string "...\n"
    .string "Huh? I can have\l"
    .string "this drink?\p"
    .string "Gee, thanks!\n"
    .string "...\l"
    .string "Glug, glug...\l"
    .string "...\l"
    .string "Gulp...\p"
    .string "If you want to go\n"
    .string "to SAFFRON CITY...\l"
    .string "...\p"
    .string "You can go on\n"
    .string "through. IÛl\l"
    .string "share this with\l"
    .string "the other guards!$"

Route7_EastEntrance_Text_HiHowsItGoing::
    .string "Hi, thanks for\n"
    .string "the cool drinks!$"

