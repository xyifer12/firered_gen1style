LavenderTown_Text_DoYouBelieveInGhosts::
    .string "Do you believe in\n"
    .string "GHOSTs?$"

LavenderTown_Text_SoThereAreBelievers::
    .string "Really? So there\n"
    .string "are believers...$"

LavenderTown_Text_JustImaginingWhiteHand::
    .string "Hahaha, I guess\n"
    .string "not.\p"
    .string "That white hand\n"
    .string "on your shoulder,\l"
    .string "itÙ not real.$"

LavenderTown_Text_TownKnownAsMonGraveSite::
    .string "This town is known\n"
    .string "as the grave site\l"
    .string "of POKéMON.\p"
    .string "Memorial services\n"
    .string "are held in\l"
    .string "POKéMON TOWER.$"

LavenderTown_Text_GhostsAppearedInTower::
    .string "GHOSTs appeared\n"
    .string "in POKéMON TOWER.\p"
    .string "I think theyàe\n"
    .string "the spirits of\l"
    .string "POKéMON that the\l"
    .string "ROCKETs killed.$"

LavenderTown_Text_TownSign::
    .string "LAVENDER TOWN\n"
    .string "The Noble Purple\l"
    .string "Town$"

LavenderTown_Text_SilphScopeNotice::
    .string "New SILPH SCOPE!\p"
    .string "Make the Invisible\n"
    .string "Plain to See!\l"
    .string "SILPH CO.$"

LavenderTown_Text_VolunteerPokemonHouse::
    .string "LAVENDER VOLUNTEER\n"
    .string "POKéMON HOUSE$"

LavenderTown_Text_PokemonTowerSign::
    .string "May the Souls of\n"
    .string "POKéMON Rest Easy\p"
    .string "POKéMON TOWER$"

