SafariZone_West_Text_KogaPatrolsSafariEverySoOften::
    .string "The SAFARI ZONEÙ huge, wouldnÑ\n"
    .string "you say?\p"
    .string "FUCHSIAÙ GYM LEADER, KOGA, \n"
    .string "patrols the grounds every so often.\p"
    .string "Thanks to him, we can play here\n"
    .string "knowing that weàe safe.$"

SafariZone_West_Text_RocksMakeMonRunButEasierCatch::
    .string "Tossing ROCKs at\n"
    .string "POKéMON might\l"
    .string "make them run,\l"
    .string "but theyÛl be\l"
    .string "easier to catch.$"

SafariZone_West_Text_BaitMakesMonStickAround::
    .string "Using BAIT will\n"
    .string "make POKéMON\l"
    .string "easier to catch.$"

SafariZone_West_Text_HikedLotsDidntSeeMonIWanted::
    .string "I hiked a lot, but\n"
    .string "I didnÑ see any\l"
    .string "POKéMON I wanted.$"

