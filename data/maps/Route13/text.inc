Route13_Text_SebastianIntro::
    .string "My bird POKéMON\n"
    .string "want to scrap!$"

Route13_Text_SebastianDefeat::
    .string "BIRD KEEPER: My\n"
    .string "bird combo lost?$"

Route13_Text_SebastianPostBattle::
    .string "My POKéMON look\n"
    .string "happy even though\l"
    .string "they lost.$"

Route13_Text_SusieIntro::
    .string "Iá told Iá good\n"
    .string "for a kid!$"

Route13_Text_SusieDefeat::
    .string "JR.TRAINER♀: Ohh!\n"
    .string "I lost!$"

Route13_Text_SusiePostBattle::
    .string "I want to become\n"
    .string "a good trainer.\l"
    .string "IÛl train hard.$"

Route13_Text_ValerieIntro::
    .string "Wow! Your BADGEs\n"
    .string "are too cool!$"

Route13_Text_ValerieDefeat::
    .string "JR.TRAINER♀: Not\n"
    .string "enough!$"

Route13_Text_ValeriePostBattle::
    .string "You got those\n"
    .string "BADGEs from GYM\l"
    .string "LEADERs. I know!$"

Route13_Text_GwenIntro::
    .string "My cute POKéMON\n"
    .string "wish to make your\l"
    .string "acquaintance.$"

Route13_Text_GwenDefeat::
    .string "JR.TRAINER♀: Wow!\n"
    .string "You totally won!$"

Route13_Text_GwenPostBattle::
    .string "You have to make\n"
    .string "POKéMON fight to\l"
    .string "toughen them up.$"

Route13_Text_AlmaIntro::
    .string "I found CARBOS in\n"
    .string "a cave once.$"

Route13_Text_AlmaDefeat::
    .string "JR.TRAINER♀: Just\n"
    .string "messed up!$"

Route13_Text_AlmaPostBattle::
    .string "CARBOS boosted\n"
    .string "the SPEED of my\l"
    .string "POKéMON.$"

Route13_Text_PerryIntro::
    .string "The windÙ blowing\n"
    .string "my way!$"

Route13_Text_PerryDefeat::
    .string "BIRD KEEPER: The\n"
    .string "wind turned!$"

Route13_Text_PerryPostBattle::
    .string "Iá beat. I guess\n"
    .string "IÛl FLY home.$"

Route13_Text_LolaIntro::
    .string "Sure, IÛl play\n"
    .string "with you!$"

Route13_Text_LolaDefeat::
    .string "BEAUTY: Oh!\n"
    .string "You little brute!$"

Route13_Text_LolaPostBattle::
    .string "I wonder which is\n"
    .string "stronger, male or\l"
    .string "female POKéMON?$"

Route13_Text_SheilaIntro::
    .string "Do you want to\n"
    .string "POKéMON with me?$"

Route13_Text_SheilaDefeat::
    .string "BEAUTY: ItÙ over\n"
    .string "already?$"

Route13_Text_SheilaPostBattle::
    .string "I donÑ know\n"
    .string "anything about\l"
    .string "POKéMON. I just\l"
    .string "like cool ones!$"

Route13_Text_JaredIntro::
    .string "Whatàe you\n"
    .string "lookin' at?$"

Route13_Text_JaredDefeat::
    .string "BIKER: Dang!\n"
    .string "Stripped gears!$"

Route13_Text_JaredPostBattle::
    .string "Get lost!$"

Route13_Text_RobertIntro::
    .string "I always go with\n"
    .string "bird POKéMON!$"

Route13_Text_RobertDefeat::
    .string "BIRD KEEPER: Out\n"
    .string "of power!$"

Route13_Text_RobertPostBattle::
    .string "I wish I could\n"
    .string "fly like PIDGEY\l"
    .string "and PIDGEOTTO...$"

Route13_Text_LookToLeftOfThatPost::
    .string "TRAINER TIPS\p"
    .string "Look to the left\n"
    .string "of that post!$"

Route13_Text_SelectToSwitchItems::
    .string "TRAINER TIPS\p"
    .string "Use SELECT to\n"
    .string "switch items in\l"
    .string "the ITEM window.$"

Route13_Text_RouteSign::
    .string "ROUTE 13\n"
    .string "North to SILENCE\l"
    .string "BRIDGE$"

