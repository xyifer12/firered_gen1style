Route2_EastBuilding_Text_GiveHM05IfSeen10Mons::
    .string "Hi! Remember me?\n"
    .string "Iá PROF.OAKÙ\l"
    .string "AIDE!\p"

    .string "If you caught 10\n"
    .string "kinds of POKéMON,\l"
    .string "Iá supposed to\l"
    .string "give you an\l"
    .string "HM05!\p"

    .string "So, {PLAYER}! Have\n"
    .string "you caught at\l"
    .string "least 10 kinds of\l"
    .string "POKéMON?$"

Route2_EastBuilding_Text_GreatHereYouGo::
    .string "Great! You have\n"
    .string "caught {STR_VAR_3} kinds\l"
    .string "of POKéMON!\p"

    .string "Congratulations!\n"
    .string "Here you go!$"

Route2_EastBuilding_Text_ReceivedHM05FromAide::
    .string "{PLAYER} got the\n"
    .string "HM05!$"

Route2_EastBuilding_Text_ExplainHM05::
    .string "The HM FLASH\n"
    .string "lights even the\l"
    .string "darkest dungeons.$"

Route2_EastBuilding_Text_CanGetThroughRockTunnel::
    .string "Once a POKéMON\n"
    .string "learns FLASH, you\l"
    .string "can get through\l"
    .string "ROCK TUNNEL.$"

