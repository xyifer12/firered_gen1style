Route16_NorthEntrance_1F_Text_NoPedestriansOnCyclingRoad::
    .string "No pedestrians\n"
    .string "are allowed on\l"
    .string "CYCLING ROAD!$"

Route16_NorthEntrance_1F_Text_CyclingRoadIsDownhillCourse::
    .string "CYCLING ROAD is a\n"
    .string "downhill course\l"
    .string "by the sea. ItÙ\l"
    .string "a great ride.$"

Route16_NorthEntrance_1F_Text_ExcuseMeWaitUp::
    .string "Excuse me! Wait\n"
    .string "up, please!$"

Route16_NorthEntrance_1F_Text_HowdYouGetInGoodEffort::
    .string "HowÚ you get in?\n"
    .string "Good effort!$"

