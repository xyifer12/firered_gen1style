SSAnne_CaptainsOffice_Text_CaptainIFeelSeasick::
    .string "CAPTAIN: Ooargh...\n"
    .string "I feel hideous...\l"
    .string "Urrp! Seasick...$"

SSAnne_CaptainsOffice_Text_RubbedCaptainsBack::
    .string "{PLAYER} rubbed\n"
    .string "the CAPTAINÙ\l"
    .string "back!\p"
    .string "Rub-rub...\n"
    .string "Rub-rub...$"

SSAnne_CaptainsOffice_Text_ThankYouHaveHMForCut::
    .string "CAPTAIN: Whew!\n"
    .string "Thank you! I\l"
    .string "feel much better!\p"
    .string "You want to see\n"
    .string "my CUT technique?\p"
    .string "I could show you\n"
    .string "if I wasnÑ ill...\p"
    .string "I know! You can\n"
    .string "have this!\p"
    .string "Teach it to your\n"
    .string "POKéMON and you\l"
    .string "can see it CUT\l"
    .string "any time!$"

SSAnne_CaptainsOffice_Text_ObtainedHM01FromCaptain::
    .string "{PLAYER} got\n"
    .string "HM01!$"

SSAnne_CaptainsOffice_Text_ExplainCut::
    .string "Using CUT, you can chop down\n"
    .string "small trees.\p"
    .string "Why not try it with the trees\n"
    .string "around VERMILION CITY?$"

SSAnne_CaptainsOffice_Text_SSAnneWillSetSailSoon::
    .string "CAPTAIN: Whew!\p"
    .string "Now that Iá not\n"
    .string "sick any more, I\l"
    .string "guess itÙ time.$"

SSAnne_CaptainsOffice_Text_YouHaveNoRoomForThis::
    .string "Oh, no!\n"
    .string "You have no room\l"
    .string "for this!$"

SSAnne_CaptainsOffice_Text_YuckShouldntHaveLooked::
    .string "Yuck! ShouldnÑ\n"
    .string "have looked!$"

SSAnne_CaptainsOffice_Text_HowToConquerSeasickness::
    .string "How to Conquer\n"
    .string "Seasickness...\l"
    .string "The CAPTAINÙ\l"
    .string "reading this!$"

