PokemonTower_1F_Text_ErectedInMemoryOfDeadMons::
    .string "POKéMON TOWER was\n"
    .string "erected in the\l"
    .string "memory of POKéMON\l"
    .string "that had died.$"

PokemonTower_1F_Text_ComeToPayRespectsSon::
    .string "Did you come to\n"
    .string "pay respects?\l"
    .string "Bless you!$"

PokemonTower_1F_Text_ComeToPayRespectsGirl::
    .string "Did you come to\n"
    .string "pay respects?\l"
    .string "Bless you!$"

PokemonTower_1F_Text_CameToPrayForDepartedClefairy::
    .string "I came to pray\n"
    .string "for my CLEFAIRY.\p"
    .string "Sniff! I canÑ\n"
    .string "stop crying...$"

PokemonTower_1F_Text_GrowlitheWhyDidYouDie::
    .string "My GROWLITHE...\n"
    .string "Why did you die?$"

PokemonTower_1F_Text_SenseSpiritsUpToMischief::
    .string "I am a CHANNELER!\n"
    .string "There are spirits\l"
    .string "up to mischief!$"

