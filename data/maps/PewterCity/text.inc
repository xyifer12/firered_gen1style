PewterCity_Text_ClefairyCameFromMoon::
    .string "ItÙ rumored that\n"
    .string "CLEFAIRYs came\l"
    .string "from the moon!\p"
    .string "They appeared\n"
    .string "after MOON STONE\l"
    .string "fell on MT.MOON.$"

PewterCity_Text_BrockOnlySeriousTrainerHere::
    .string "There arenÑ many\n"
    .string "serious POKéMON\l"
    .string "trainers here!\p"
    .string "Theyàe all like\n"
    .string "BUG CATCHERs,\l"
    .string "but PEWTER GYMÙ\l"
    .string "BROCK is totally\l"
    .string "into it!$"

PewterCity_Text_DidYouCheckOutMuseum::
    .string "Did you check out\n"
    .string "the MUSEUM?$"

PewterCity_Text_WerentThoseFossilsAmazing::
    .string "WerenÑ those\n"
    .string "fossils from MT.\l"
    .string "MOON amazing?$"

PewterCity_Text_ReallyYouHaveToGo::
    .string "Really?\n"
    .string "You absolutely\l"
    .string "have to go!$"

PewterCity_Text_ThisIsTheMuseum::
    .string "ItÙ right here!\n"
    .string "You have to pay\l"
    .string "to get in, but\l"
    .string "itÙ worth it!\l"
    .string "See you around!$"

PewterCity_Text_DoYouKnowWhatImDoing::
    .string "Psssst!\n"
    .string "Do you know what\l"
    .string "Iá doing?$"

PewterCity_Text_ThatsRightItsHardWork::
    .string "ThatÙ right!\n"
    .string "ItÙ hard work!$"

PewterCity_Text_SprayingRepelToKeepWildMonsOut::
    .string "Iá spraying REPEL\n"
    .string "to keep POKéMON\l"
    .string "out of my garden!$"

PewterCity_Text_BrocksLookingForChallengersFollowMe::
    .string "Youàe a trainer\n"
    .string "right? BROCKÙ\l"
    .string "looking for new\l"
    .string "challengers!\l"
    .string "Follow me!$"

PewterCity_Text_GoTakeOnBrock::
    .string "If you have the\n"
    .string "right stuff, go\l"
    .string "take on BROCK!$"

PewterCity_Text_TrainerTipsEarningEXP::
    .string "TRAINER TIPS\p"
    .string "Any POKéMON that\n"
    .string "takes part in\l"
    .string "battle, however\l"
    .string "short, earns EXP!$"

PewterCity_Text_CallPoliceIfInfoOnThieves::
    .string "NOTICE!\p"
    .string "Thieves have been\n"
    .string "stealing POKéMON\l"
    .string "fossils at MT.\l"
    .string "MOON! Please call\l"
    .string "PEWTER POLICE\l"
    .string "with any info!$"

PewterCity_Text_MuseumOfScience::
    .string "PEWTER MUSEUM\n"
    .string "OF SCIENCE$"

PewterCity_Text_GymSign::
    .string "PEWTER CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: BROCK\p"
    .string "The Rock Solid\n"
    .string "POKéMON Trainer!$"

PewterCity_Text_CitySign::
    .string "PEWTER CITY\n"
    .string "A Stone Gray\l"
    .string "City$"

@ The below 3 JP texts are unused. Possibly a cut NPC meant to give the player the Berry Pouch
PewterCity_Text_DefeatedBrockYouCanHaveTreasure::
    .string "あッれー！\n"
    .string "その　ジムバッジ‥‥\l"
    .string "すげえな　タケシに　かったのかよ！\p"
    .string "かんどう　したから\n"
    .string "おれの　たからもの　やるよ！$"

PewterCity_Text_BerriesInsideUseCarefully::
    .string "なかには　きのみが　はいってるぜ\p"
    .string "やくに　たつ　きのみも\n"
    .string "はいって　いるから\l"
    .string "だいじに　つかって　くれよ！$"

PewterCity_Text_MonsWillUseHeldBerriesOnTheirOwn::
    .string "ポケモンに　きのみを\n"
    .string "もたせて　おけば\l"
    .string "たたかっている　ときに\l"
    .string "かってに　つかって　くれるんだ\p"
    .string "キズぐすり　とか　どくけし　より\n"
    .string "てがるで　べんり　だろ？$"

PewterCity_Text_OhPlayer::
    .string "Oh, {PLAYER}{KUN}!$"

PewterCity_Text_AskedToDeliverThis::
    .string "Iá glad I caught up to you.\n"
    .string "Iá PROF. OAKÙ AIDE.\p"
    .string "Iße been asked to deliver this,\n"
    .string "so here you go.$"

PewterCity_Text_ReceivedRunningShoesFromAide::
    .string "{PLAYER} received the\n"
    .string "RUNNING SHOES from the AIDE.$"

PewterCity_Text_SwitchedShoesWithRunningShoes::
    .string "{PLAYER} switched shoes with the\n"
    .string "RUNNING SHOES.$"

PewterCity_Text_ExplainRunningShoes::
    .string "Press the B Button to run.\n"
    .string "But only where thereÙ room to run!$"

PewterCity_Text_MustBeGoingBackToLab::
    .string "Well, I must be going back to\n"
    .string "the LAB.\p"
    .string "Bye-bye!$"

PewterCity_Text_RunningShoesLetterFromMom::
    .string "ThereÙ a letter attached…\p"
    .string "Dear {PLAYER},\p"
    .string "Here is a pair of RUNNING SHOES\n"
    .string "for my beloved challenger.\p"
    .string "Remember, IÛl always cheer for\n"
    .string "you! DonÑ ever give up!\p"
    .string "From Mom$"

