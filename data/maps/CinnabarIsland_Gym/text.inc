CinnabarIsland_Gym_Text_BlaineIntro::
    .string "Hah!\p"

    .string "I am BLAINE! I\n"
    .string "am the LEADER of\l"
    .string "CINNEBAR GYM!\p"

    .string "My fiery POKéMON\n"
    .string "will incinerate\l"
    .string "all challengers!\p"

    .string "Hah! You better\n"
    .string "have BURN HEAL!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

CinnabarIsland_Gym_Text_BlaineDefeat::
    .string "BLAINE: I have\n"
    .string "burnt out!\p"

    .string "You have earned\n"
    .string "the VOLCANOBADGE!$"

CinnabarIsland_Gym_Text_FireBlastIsUltimateFireMove::
    .string "FIRE BLAST is the\n"
    .string "ultimate fire\l"
    .string "technique!\p"

    .string "DonÑ waste it on\n"
    .string "water POKéMON!$"

CinnabarIsland_Gym_Text_ExplainVolcanoBadge::
    .string "Hah!\p"

    .string "The VOLCANOBADGE\n"
    .string "heightens the\l"
    .string "SPECIAL abilities\l"
    .string "of your POKéMON!\p"

    .string "Here, you can\n"
    .string "have this too!$"

CinnabarIsland_Gym_Text_ReceivedTM38FromBlaine::
    .string "{PLAYER} received\n"
    .string "TM38!$"

CinnabarIsland_Gym_Text_BlainePostBattle::
    .string "TM38 contains\n"
    .string "FIRE BLAST!\p"

    .string "Teach it to fire-\n"
    .string "type POKéMON!\p"

    .string "CHARMELEON or\n"
    .string "PONYTA would be\l"
    .string "good bets!$"

CinnabarIsland_Gym_Text_MakeSpaceForThis::
    .string "Make rrom for my\n"
    .string "gift!$"

CinnabarIsland_Gym_Text_ErikIntro::
    .string "Do you know how\n"
    .string "hot POKéMON fire\l"
    .string "breath can get?$"

CinnabarIsland_Gym_Text_ErikDefeat::
    .string "SUPER NERD: Yow!\n"
    .string "Hot, hot, hot!$"

CinnabarIsland_Gym_Text_ErikPostBattle::
    .string "Fire, or to be\n"
    .string "more precise,\l"
    .string "combustion...\p"

    .string "Blah, blah, blah,\n"
    .string "blah...$"

CinnabarIsland_Gym_Text_QuinnIntro::
    .string "I was a thief, but\n"
    .string "I became straight\l"
    .string "as a trainer!$"

CinnabarIsland_Gym_Text_QuinnDefeat::
    .string "BURGLAR: I\n"
    .string "surrender!$"

CinnabarIsland_Gym_Text_QuinnPostBattle::
    .string "I canÑ help\n"
    .string "stealing other\l"
    .string "peopleÙ POKéMON!$"

CinnabarIsland_Gym_Text_AveryIntro::
    .string "You canÑ win!\n"
    .string "I have studied\l"
    .string "POKéMON totally!$"

CinnabarIsland_Gym_Text_AveryDefeat::
    .string "SUPER NERD: Waah!\n"
    .string "My studies!$"

CinnabarIsland_Gym_Text_AveryPostBattle::
    .string "My theories are\n"
    .string "too complicated\l"
    .string "for you!$"

CinnabarIsland_Gym_Text_RamonIntro::
    .string "I just like using\n"
    .string "fire POKéMON!$"

CinnabarIsland_Gym_Text_RamonDefeat::
    .string "BURGLAR: Too hot\n"
    .string "to handle!$"

CinnabarIsland_Gym_Text_RamonPostBattle::
    .string "I wish there was\n"
    .string "a thief POKéMON!\l"
    .string "IÚ use that!$"

CinnabarIsland_Gym_Text_DerekIntro::
    .string "I know why BLAINE\n"
    .string "became a trainer!$"

CinnabarIsland_Gym_Text_DerekDefeat::
    .string "SUPER NERD: Ow!$"

CinnabarIsland_Gym_Text_DerekPostBattle::
    .string "BLAINE was lost\n"
    .string "in the mountains\l"
    .string "when a fiery bird\l"
    .string "POKéMON appeared.\p"

    .string "Its light enabled\n"
    .string "BLAINE to find\l"
    .string "his way down!$"

CinnabarIsland_Gym_Text_DustyIntro::
    .string "Iße been to many\n"
    .string "GYMs, but this is\l"
    .string "my favorite!$"

CinnabarIsland_Gym_Text_DustyDefeat::
    .string "BURGLAR: Yowza!\n"
    .string "Too hot!$"

CinnabarIsland_Gym_Text_DustyPostBattle::
    .string "Us fire POKéMON\n"
    .string "fans like PONYTA\l"
    .string "and NINETALES!$"

CinnabarIsland_Gym_Text_ZacIntro::
    .string "Fire is weak\n"
    .string "against H2O!$"

CinnabarIsland_Gym_Text_ZacDefeat::
    .string "SUPER NERD: Oh!\n"
    .string "Snuffed out!$"

CinnabarIsland_Gym_Text_ZacPostBattle::
    .string "Water beats fire!\p"

    .string "But, fire melts\n"
    .string "ice POKéMON!$"

CinnabarIsland_Gym_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"

    .string "The hot-headed\n"
    .string "BLAINE is a fire\l"
    .string "POKéMON pro!\p"

    .string "Douse his spirits\n"
    .string "with water!\p"

    .string "YouÚ better take\n"
    .string "some BURN HEALs!$"

CinnabarIsland_Gym_Text_GymGuyPostVictory::
    .string "{PLAYER}! You beat\n"
    .string "that fire brand!$"

CinnabarIsland_Gym_Text_GymStatue::
    .string "CINNABAR ISLAND\n"
    .string "POKéMON GYM\l"
    .string "LEADER: BLAINE\p"

    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CinnabarIsland_Gym_Text_GymStatuePlayerWon::
    .string "CINNABAR ISLAND\n"
    .string "POKéMON GYM\l"
    .string "LEADER: BLAINE\p"

    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

CinnabarIsland_Gym_Text_PokemonQuizRules::
    .string "POKéMON Quiz!\p"

    .string "Get it right and\n"
    .string "the door opens to\l"
    .string "the next room!\p"

    .string "Get it wrong and\n"
    .string "face a trainer!\p"

    .string "If you want to\n"
    .string "conserve your\l"
    .string "POKéMON for the\l"
    .string "GYM LEADER...\p"

    .string "Then get it right!\n"
    .string "Here we go!$"

CinnabarIsland_Gym_Text_QuizQuestion1::
    .string "CATERPIE evolves\n"
    .string "into BUTTERFREE?$"

CinnabarIsland_Gym_Text_QuizQuestion2::
    .string "There are 9\n"
    .string "certified POKéMON\l"
    .string "LEAGUE BADGEs?$"

CinnabarIsland_Gym_Text_QuizQuestion3::
    .string "POLIWAG evolves 3\n"
    .string "times?$"

CinnabarIsland_Gym_Text_QuizQuestion4::
    .string "Are thunder moves\n"
    .string "effective against\l"
    .string "ground element-\l"
    .string "type POKéMON?$"

CinnabarIsland_Gym_Text_QuizQuestion5::
    .string "POKéMON of the\n"
    .string "same kind and\l"
    .string "level are not\l"
    .string "identical?$"

CinnabarIsland_Gym_Text_QuizQuestion6::
    .string "TM28 contains\n"
    .string "TOMBSTONER?$"

CinnabarIsland_Gym_Text_CorrectGoOnThrough::
    .string "Youàe absolutely\n"
    .string "correct!\p"

    .string "Go on through!$"

CinnabarIsland_Gym_Text_SorryBadCall::
    .string "Sorry! Bad call!$"

