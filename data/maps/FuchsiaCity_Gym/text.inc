FuchsiaCity_Gym_Text_KogaIntro::
    .string "KOGA: Fwahahaha!\p"
    .string "A mere child like\n"
    .string "you dares to\l"
    .string "challenge me?\p"
    .string "Very well, I\n"
    .string "shall show you\l"
    .string "true terror as a\l"
    .string "ninja master!\p"
    .string "You shall feel\n"
    .string "the despair of\l"
    .string "poison and sleep\l"
    .string "techniques!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

FuchsiaCity_Gym_Text_KogaDefeat::
    .string "KOGA: Humph!\n"
    .string "You have proven\l"
    .string "your worth!\p"
    .string "Here! Take this\n"
    .string "SOULBADGE!$"

FuchsiaCity_Gym_Text_KogaPostBattle::
    .string "When afflicted by\n"
    .string "TOXIC, POKéMON\l"
    .string "suffer more and\l"
    .string "more as battle\l"
    .string "progresses!\p"
    .string "It will surely\n"
    .string "terrorize foes!$"

FuchsiaCity_Gym_Text_KogaExplainSoulBadge::
    .string "Now that you have\n"
    .string "the SOULBADGE,\l"
    .string "the DEFENSE of\l"
    .string "your POKéMON\l"
    .string "increases!\p"
    .string "It also lets you\n"
    .string "SURF outside of\l"
    .string "battle!\p"
    .string "Ah! Take this,\n"
    .string "too!$"

FuchsiaCity_Gym_Text_ReceivedTM06FromKoga::
    .string "{PLAYER} received\n"
    .string "TM06!$"

FuchsiaCity_Gym_Text_KogaExplainTM06::
    .string "TM06 contains\n"
    .string "TOXIC!\p"
    .string "It is a secret\n"
    .string "technique over\l"
    .string "400 years old!$"

FuchsiaCity_Gym_Text_MakeSpaceForThis::
    .string "Make space for\n"
    .string "this, child!$"

FuchsiaCity_Gym_Text_KaydenIntro::
    .string "Strength isnÑ\n"
    .string "the key for\l"
    .string "POKéMON!\p"
    .string "ItÙ strategy!\p"
    .string "IÛl show you how\n"
    .string "strategy can beat\l"
    .string "brute strength!$"

FuchsiaCity_Gym_Text_KaydenDefeat::
    .string "ROCKER: What?\n"
    .string "Extraordinary!$"

FuchsiaCity_Gym_Text_KaydenPostBattle::
    .string "So, you mix brawn\n"
    .string "with brains?\l"
    .string "Good strategy!$"

FuchsiaCity_Gym_Text_KirkIntro::
    .string "I wanted to become\n"
    .string "a ninja, so I\l"
    .string "joined this GYM!$"

FuchsiaCity_Gym_Text_KirkDefeat::
    .string "ROCKER: Iá done\n"
    .string "for!$"

FuchsiaCity_Gym_Text_KirkPostBattle::
    .string "I will keep on\n"
    .string "training under\l"
    .string "KOGA, my ninja\l"
    .string "master!$"

FuchsiaCity_Gym_Text_NateIntro::
    .string "LetÙ see you\n"
    .string "beat my special\l"
    .string "techniques!$"

FuchsiaCity_Gym_Text_NateDefeat::
    .string "ROCKER: You\n"
    .string "had me fooled!$"

FuchsiaCity_Gym_Text_NatePostBattle::
    .string "I like poison and\n"
    .string "sleep techniques,\l"
    .string "as they linger\l"
    .string "after battle!$"

FuchsiaCity_Gym_Text_PhilIntro::
    .string "Stop right there!\p"
    .string "Our invisible\n"
    .string "walls have you\l"
    .string "frustrated?$"

FuchsiaCity_Gym_Text_PhilDefeat::
    .string "TAMER: Whoa!\n"
    .string "HeÙ got it!$"

FuchsiaCity_Gym_Text_PhilPostBattle::
    .string "You impressed me!\n"
    .string "HereÙ a hint!\p"
    .string "Look very closely\n"
    .string "for gaps in the\l"
    .string "invisible walls!$"

FuchsiaCity_Gym_Text_EdgarIntro::
    .string "I also study the\n"
    .string "way of the ninja\l"
    .string "with master KOGA!\p"
    .string "Ninja have a long\n"
    .string "history of using\l"
    .string "animals!$"

FuchsiaCity_Gym_Text_EdgarDefeat::
    .string "TAMER: Awoo!$"

FuchsiaCity_Gym_Text_EdgarPostBattle::
    .string "I still have much\n"
    .string "to learn!$"

FuchsiaCity_Gym_Text_ShawnIntro::
    .string "Master KOGA comes\n"
    .string "from a long line\l"
    .string "of ninjas!\p"
    .string "What did you\n"
    .string "descend from?$"

FuchsiaCity_Gym_Text_ShawnDefeat::
    .string "ROCKER: Dropped\n"
    .string "my balls!$"

FuchsiaCity_Gym_Text_ShawnPostBattle::
    .string "Where there is\n"
    .string "light, there is\l"
    .string "shadow!\p"
    .string "Light and shadow!\n"
    .string "Which do you\l"
    .string "choose?$"

FuchsiaCity_Gym_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"
    .string "FUCHSIA GYM is\n"
    .string "riddled with\l"
    .string "invisible walls!\p"
    .string "KOGA might appear\n"
    .string "close, but heÙ\l"
    .string "blocked off!\p"
    .string "You have to find\n"
    .string "gaps in the walls\l"
    .string "to reach him!$"

FuchsiaCity_Gym_Text_GymGuyPostVictory::
    .string "ItÙ amazing how\n"
    .string "ninja can terrify\l"
    .string "even now!$"

FuchsiaCity_Gym_Text_GymStatue::
    .string "FUCHSIA CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: KOGA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

FuchsiaCity_Gym_Text_GymStatuePlayerWon::
    .string "FUCHSIA CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: KOGA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

