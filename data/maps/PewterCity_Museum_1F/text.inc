PewterCity_Museum_1F_Text_Its50YForChildsTicket::
    .string "ItÙ ¥50 for a\n"
    .string "childÙ ticket.\p"
    .string "Would you like to\n"
    .string "come in?$"

PewterCity_Museum_1F_Text_ComeAgain::
    .string "Come again!$"

PewterCity_Museum_1F_Text_Right50YThankYou::
    .string "Right, ¥50!\n"
    .string "Thank you!$"

PewterCity_Museum_1F_Text_DontHaveEnoughMoney::
    .string "You donÑ have\n"
    .string "enough money.$"

PewterCity_Museum_1F_Text_PleaseEnjoyYourself::
    .string "Please enjoy\n"
    .string "yourself.$"

PewterCity_Museum_1F_Text_DoYouKnowWhatAmberIs::
    .string "You canÑ sneak\n"
    .string "in the back way!\p"
    .string "Oh, whatever!\n"
    .string "Do you know what\l"
    .string "AMBER is?$"

PewterCity_Museum_1F_Text_AmberContainsGeneticMatter::
    .string "ThereÙ a lab\n"
    .string "somewhere trying\l"
    .string "to resurrect\l"
    .string "ancient POKéMON\l"
    .string "from AMBER.$"

PewterCity_Museum_1F_Text_AmberIsFossilizedSap::
    .string "AMBER is fossil-\n"
    .string "ized tree sap.$"

@ Unused
PewterCity_Museum_1F_Text_PleaseGoAround::
    .string "あちらへ　おまわりください$"

PewterCity_Museum_1F_Text_ShouldBeGratefulForLongLife::
    .string "That is one\n"
    .string "magnificent\l"
    .string "fossil!$"

PewterCity_Museum_1F_Text_WantYouToGetAmberExamined::
    .string "Ssh! I think that\n"
    .string "this chunk of\l"
    .string "AMBER contains\l"
    .string "POKéMON DNA!\p"
    .string "It would be great\n"
    .string "if POKéMON could\l"
    .string "be resurrected\l"
    .string "from it!\p"
    .string "But, my colleagues\n"
    .string "just ignore me!\p"
    .string "So I have a favor\n"
    .string "to ask!\p"
    .string "Take this to a\n"
    .string "POKéMON LAB and\l"
    .string "get it examined!$"

PewterCity_Museum_1F_Text_ReceivedOldAmberFromMan::
    .string "{PLAYER} received\n"
    .string "OLD AMBER!$"

PewterCity_Museum_1F_Text_GetOldAmberChecked::
    .string "Ssh! Get the OLD\n"
    .string "AMBER checked!$"

PewterCity_Museum_1F_Text_DontHaveSpaceForThis::
    .string "You donÑ have\n"
    .string "space for this!$"

PewterCity_Museum_1F_Text_WeHaveTwoFossilsOnExhibit::
    .string "We are proud of 2\n"
    .string "fossils of very\l"
    .string "rare, prehistoric\l"
    .string "POKéMON!$"

PewterCity_Museum_1F_Text_BeautifulPieceOfAmber::
    .string "The AMBER is\n"
    .string "clear and gold!$"

PewterCity_Museum_1F_Text_AerodactylFossil::
    .string "AERODACTYL Fossil\n"
    .string "A primitive and\l"
    .string "rare POKéMON.$"

PewterCity_Museum_1F_Text_KabutopsFossil::
    .string "KABUTOPS Fossil\n"
    .string "A primitive and\l"
    .string "rare POKéMON.$"

