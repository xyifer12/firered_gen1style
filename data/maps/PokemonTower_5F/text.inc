PokemonTower_5F_Text_RestHereInPurifiedSpace::
    .string "Come, child! I\n"
    .string "sealed this space\l"
    .string "with white magic!\p"
    .string "You can rest here!$"

PokemonTower_5F_Text_TammyIntro::
    .string "Give...me...\n"
    .string "your...soul...$"

PokemonTower_5F_Text_TammyDefeat::
    .string "CHANNELER: Gasp!$"

PokemonTower_5F_Text_TammyPostBattle::
    .string "I was under\n"
    .string "possession!$"

PokemonTower_5F_Text_RuthIntro::
    .string "You...shall...\n"
    .string "join...us...$"

PokemonTower_5F_Text_RuthDefeat::
    .string "CHANNELER: What\n"
    .string "a nightmare!$"

PokemonTower_5F_Text_RuthPostBattle::
    .string "I was possessed!$"

PokemonTower_5F_Text_KarinaIntro::
    .string "Zombies!$"

PokemonTower_5F_Text_KarinaDefeat::
    .string "CHANNELER: Ha?$"

PokemonTower_5F_Text_KarinaPostBattle::
    .string "I regained my\n"
    .string "senses!$"

PokemonTower_5F_Text_JanaeIntro::
    .string "Urgah...\n"
    .string "Urff....$"

PokemonTower_5F_Text_JanaeDefeat::
    .string "CHANNELER: Whoo!$"

PokemonTower_5F_Text_JanaePostBattle::
    .string "I fell to evil\n"
    .string "spirits despite\l"
    .string "my training!$"

PokemonTower_5F_Text_PurifiedZoneMonsFullyHealed::
    .string "Entered purified,\n"
    .string "protected zone!\p"
    .string "{PLAYER}Ù POKéMON\n"
    .string "are fully healed!$"

