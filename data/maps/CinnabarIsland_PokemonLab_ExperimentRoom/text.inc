CinnabarIsland_PokemonLab_ExperimentRoom_Text_TakeTM35::
    .string "Tch-tch-tch!\n"
    .string "I made a cool TM!\p"

    .string "It can cause all\n"
    .string "kinds of fun!$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_TM35_Contains::
    .string "Tch-tch-tch!\n"
    .string "ThatÙ the sound\l"
    .string "of a METRONOME!\p"

    .string "It tweaks your\n"
    .string "POKéMONÙ brain\l"
    .string "into using moves\l"
    .string "it doesnÑ know!$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_HaveYouAFossilForMe::
    .string "Hiya!\p"

    .string "I am important\n"
    .string "doctor!\p"

    .string "I study here rare\n"
    .string "POKéMON fossils!\p"

    .string "You! Have you a\n"
    .string "fossil for me?$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_NoIsTooBad::
    .string "No! Is too bad!$"

@ Unused
CinnabarIsland_PokemonLab_ExperimentRoom_Text_TakesTimeGoForWalkJP::
    .string "ちょっと　じかん　かかるよ！\p"
    .string "そこらへんを　すこし\n"
    .string "ブラブラ　してくると　よろしー！$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_FossilMonBackToLife::
    .string "Where were you?\p"

    .string "Your fossil is\n"
    .string "back to life!\p"

    .string "It was {STR_VAR_1}\n"
    .string "like I think!$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_ReceivedMonFromDoctor::
    .string "{PLAYER} got\n"
    .string "{STR_VAR_1}!$"

@ Unused
CinnabarIsland_PokemonLab_ExperimentRoom_Text_NoRoomForPokemon::
    .string "ポケモン　いっぱいで　もてないね！$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_ThatFossilIsOfMonMakeItLiveAgain::
    .string "Oh! That is\n"
    .string "{STR_VAR_2}!\p"

    .string "It is fossil of\n"
    .string "{STR_VAR_1}, a\l"
    .string "POKéMON that is\l"
    .string "already extinct!\p"

    .string "My Resurrection\n"
    .string "Machine will make\l"
    .string "that POKéMON live\l"
    .string "again!$"

@ TODO: Resolve text dump error below?
CinnabarIsland_PokemonLab_ExperimentRoom_Text_HandedFossilToWeirdDoctor::
    .string "So! You hurry and\n"
    .string "give me that!\p"

    .string "{PLAYER} handed\n"
    .string "over {STR_VAR_2}.$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_TakesTimeGoForWalk::
    .string "I take a little\n"
    .string "time!\p"

    .string "You go for walk a\n"
    .string "little while!$"

CinnabarIsland_PokemonLab_ExperimentRoom_Text_YouComeAgain::
    .string "Aiyah! You come\n"
    .string "again!$"

