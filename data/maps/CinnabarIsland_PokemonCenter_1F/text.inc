CinnabarIsland_PokemonCenter_1F_Text_CinnabarGymLocked::
    .string "POKéMON can still\n"
    .string "learn techniques\l"
    .string "after cancelling\l"
    .string "evolution.\p"
    .string "Evolution can wait\n"
    .string "until new moves\l"
    .string "have been learned.$"

CinnabarIsland_PokemonCenter_1F_Text_VisitUnionRoom::
    .string "Do you have any\n"
    .string "friends?\p"
    .string "POKéMON you get\n"
    .string "in trades grow\l"
    .string "very quickly.\p"
    .string "I think itÙ\n"
    .string "worth a try!$"

CinnabarIsland_PokemonCenter_1F_Text_EvolutionCanWaitForNewMoves::
    .string "You can cancel\n"
    .string "evolution.\p"
    .string "When a POKéMON is\n"
    .string "evolving, you can\l"
    .string "stop it and leave\l"
    .string "it the way it is.$"

CinnabarIsland_PokemonCenter_1F_Text_ReadyToSailToOneIsland::
    .string "BILL: Hey, you kept me waiting!\n"
    .string "Ready to set sail to ONE ISLAND?$"

CinnabarIsland_PokemonCenter_1F_Text_OhNotDoneYet::
    .string "Oh, youàe still not done yet?$"

CinnabarIsland_PokemonCenter_1F_Text_LetsGo::
    .string "Well, thatÙ it.\n"
    .string "LetÙ go!$"

