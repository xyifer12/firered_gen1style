PalletTown_PlayersHouse_1F_Text_AllBoysLeaveOakLookingForYou::
    .string "MOM: Right.\n"
    .string "All boys leave\l"
    .string "home some day.\p"
    .string "It said so on TV.\n"
    .string "PROF.OAK, next\l"
    .string "door, is looking\l"
    .string "for you.$"

PalletTown_PlayersHouse_1F_Text_AllGirlsLeaveOakLookingForYou::
    .string "MOM: …Right.\n"
    .string "All girls dream of traveling.\l"
    .string "It said so on TV.\p"
    .string "Oh, yes. PROF. OAK, next door, was\n"
    .string "looking for you.$"

PalletTown_PlayersHouse_1F_Text_YouShouldTakeQuickRest::
    .string "MOM: {PLAYER}!\n"
    .string "You should take a\l"
    .string "quick rest.$"

PalletTown_PlayersHouse_1F_Text_LookingGreatTakeCare::
    .string "MOM: Oh good!\n"
    .string "You and your\l"
    .string "POKéMON are\l"
    .string "looking great!\l"
    .string "Take care now!$"

PalletTown_PlayersHouse_1F_Text_MovieOnTVFourBoysOnRailroad::
    .string "ThereÙ a movie\n"
    .string "on TV.\l"
    .string "Four boys\l"
    .string "are walking on\l"
    .string "railroad tracks.\p"
    .string "I better go too.$"

PalletTown_PlayersHouse_1F_Text_MovieOnTVGirlOnBrickRoad::
    .string "ThereÙ a movie on TV.\p"
    .string "A girl with her hair in\n"
    .string "pigtails is walking up a\l"
    .string "brick road.\p"
    .string "...I better go too.$"

PalletTown_PlayersHouse_1F_Text_OopsWrongSide::
    .string "Oops, wrong side.$"

