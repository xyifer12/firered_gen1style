Route15_Text_KindraIntro::
    .string "Let me try out the\n"
    .string "POKéMON I just\l"
    .string "got in a trade!$"

Route15_Text_KindraDefeat::
    .string "JR.TRAINER♀: Not\n"
    .string "good enough!$"

Route15_Text_KindraPostBattle::
    .string "You canÑ change\n"
    .string "the nickname of\l"
    .string "any POKéMON you\l"
    .string "get in a trade.\p"
    
    .string "Only the Original\n"
    .string "Trainer can.$"

Route15_Text_BeckyIntro::
    .string "You look gentle,\n"
    .string "so I think I can\l"
    .string "beat you!$"

Route15_Text_BeckyDefeat::
    .string "JR.TRAINER♀: No,\n"
    .string "wrong!$"

Route15_Text_BeckyPostBattle::
    .string "Iá afraid of\n"
    .string "BIKERs, they look\l"
    .string "so ugly and mean!$"

Route15_Text_EdwinIntro::
    .string "When I whistle, I\n"
    .string "can summon bird\l"
    .string "POKéMON.$"

Route15_Text_EdwinDefeat::
    .string "BIRD KEEPER: Ow!\n"
    .string "ThatÙ tragic!$"

Route15_Text_EdwinPostBattle::
    .string "Maybe Iá not cut\n"
    .string "out for battles.$"

Route15_Text_ChesterIntro::
    .string "Hmm? My birds are\n"
    .string "shivering! Youàe\l"
    .string "good, arenÑ you?$"

Route15_Text_ChesterDefeat::
    .string "BIRD KEEPER: Just\n"
    .string "as I thought!$"

Route15_Text_ChesterPostBattle::
    .string "Did you know moves\n"
    .string "like EARTHQUAKE\l"
    .string "donÑ have any\l"
    .string "effect on birds?$"

Route15_Text_GraceIntro::
    .string "Oh, youàe a\n"
    .string "little cutie!$"

Route15_Text_GraceDefeat::
    .string "BEAUTY: You looked\n"
    .string "so cute too!$"

Route15_Text_GracePostBattle::
    .string "I forgive you!\n"
    .string "I can take it!$"

Route15_Text_OliviaIntro::
    .string "I raise POKéMON\n"
    .string "because I live\l"
    .string "alone!$"

Route15_Text_OliviaDefeat::
    .string "BEAUTY: I didnÑ\n"
    .string "ask for this!$"

Route15_Text_OliviaPostBattle::
    .string "I just like going\n"
    .string "home to be with\l"
    .string "my POKéMON!$"

Route15_Text_ErnestIntro::
    .string "Hey, kid! Cáon!\n"
    .string "I just got these!$"

Route15_Text_ErnestDefeat::
    .string "BIKER: Why\n"
    .string "not?$"

Route15_Text_ErnestPostBattle::
    .string "You only live\n"
    .string "once, so I live\l"
    .string "as an outlaw!\l"
    .string "TEAM ROCKET RULES!$"

Route15_Text_AlexIntro::
    .string "Fork over all your\n"
    .string "cash when you\l"
    .string "lose to me, kid!$"

Route15_Text_AlexDefeat::
    .string "BIKER: That\n"
    .string "canÑ be true!$"

Route15_Text_AlexPostBattle::
    .string "I was just joking\n"
    .string "about the money!$"

Route15_Text_CeliaIntro::
    .string "WhatÙ cool?\n"
    .string "Trading POKéMON!$"

Route15_Text_CeliaDefeat::
    .string "JR.TRAINER♀: I\n"
    .string "said trade!$"

Route15_Text_CeliaPostBattle::
    .string "I trade POKéMON\n"
    .string "with my friends!$"

Route15_Text_YazminIntro::
    .string "Want to play with\n"
    .string "my POKéMON?$"

Route15_Text_YazminDefeat::
    .string "JR.TRAINER♀: I was\n"
    .string "too impatient!$"

Route15_Text_YazminPostBattle::
    .string "IÛl go train with\n"
    .string "weaker people.$"

Route15_Text_RouteSign::
    .string "ROUTE 15\n"
    .string "West to FUCHSIA\l"
    .string "CITY$"

Route15_Text_MyaIntro::
    .string "MYA: Youàe perfect.\n"
    .string "Help me train my little brother?$"

Route15_Text_MyaDefeat::
    .string "MYA: RON, you have to focus!\n"
    .string "Concentrate on what youàe doing!$"

Route15_Text_MyaPostBattle::
    .string "MYA: Okay, weÛl turn it up.\n"
    .string "IÛl add to our training menu!$"

Route15_Text_MyaNotEnoughMons::
    .string "MYA: Do you want to challenge us?\n"
    .string "YouÛl need two POKéMON, though.$"

Route15_Text_RonIntro::
    .string "RON: My sister gets scary when we\n"
    .string "lose.$"

Route15_Text_RonDefeat::
    .string "RON: Oh, no, no…\n"
    .string "Sis, Iá sorry!$"

Route15_Text_RonPostBattle::
    .string "RON: Oh, bleah…\n"
    .string "I wish I had a nice sister…$"

Route15_Text_RonNotEnoughMons::
    .string "RON: Did you want to battle with\n"
    .string "my sister and me?\p"
    .string "You need two POKéMON, then.$"
