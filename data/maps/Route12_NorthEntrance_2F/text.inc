@ Male and female text identical, differ (presumably) in JP
Route12_NorthEntrance_2F_Text_TakeTMDontNeedAnymoreMale::
    .string "My POKéMONÙ\n"
    .string "ashes are stored\l"
    .string "in POKéMON TOWER.\p"
    .string "You can have this\n"
    .string "TM. I donÑ need\l"
    .string "it any more...$"

Route12_NorthEntrance_2F_Text_TakeTMDontNeedAnymoreFemale::
    .string "My POKéMONÙ\n"
    .string "ashes are stored\l"
    .string "in POKéMON TOWER.\p"
    .string "You can have this\n"
    .string "TM. I donÑ need\l"
    .string "it any more...$"

Route12_NorthEntrance_2F_Text_ReceivedTM39FromLittleGirl::
    .string "{PLAYER} received\n"
    .string "TM39!$"

Route12_NorthEntrance_2F_Text_ExplainTM39::
    .string "TM39 is a move\n"
    .string "called SWIFT.\p"
    .string "ItÙ very accurate.\n"
    .string "so use it during\l"
    .string "battles you canÑ\l"
    .string "afford to lose.$"

Route12_NorthEntrance_2F_Text_DontHaveRoomForThis::
    .string "You donÑ have\n"
    .string "room for this.$"

Route12_NorthEntrance_2F_Text_TheresManFishing::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "A man fishing!$"

Route12_NorthEntrance_2F_Text_ItsPokemonTower::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "ItÙ POKéMON TOWER!$"

