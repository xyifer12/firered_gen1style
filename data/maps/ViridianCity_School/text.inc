ViridianCity_School_Text_TryingToMemorizeNotes::
    .string "Whew! Iá trying\n"
    .string "to memorize all\l"
    .string "my notes.$"

ViridianCity_School_Text_ReadBlackboardCarefully::
    .string "Okay!\p"
    .string "Be sure to read\n"
    .string "the blackboard\l"
    .string "carefully!$"

ViridianCity_School_Text_NotebookFirstPage::
    .string "Looked at the\n"
    .string "notebook!\l"
    .string "First page...\p"
    .string "POKé BALLs are\n"
    .string "used to catch\l"
    .string "POKéMON.\p"
    .string "Up to 6 POKéMON\n"
    .string "can be carried.\p"
    .string "People who raise\n"
    .string "and make POKéMON\l"
    .string "fight are called\l"
    .string "POKéMON trainers.$"

ViridianCity_School_Text_NotebookSecondPage::
    .string "Second page...\p"
    .string "A healthy POKéMON\n"
    .string "may be hard to\l"
    .string "catch, so weaken\l"
    .string "it first!\p"
    .string "Poison, burns and\n"
    .string "other damage are\l"
    .string "effective!$"

ViridianCity_School_Text_NotebookThirdPage::
    .string "Third page...\p"
    .string "POKéMON trainers\n"
    .string "seek others to\l"
    .string "engage in POKéMON\l"
    .string "fights.\p"
    .string "Battles are\n"
    .string "constantly fought\l"
    .string "at POKéMON GYMs.$"

ViridianCity_School_Text_NotebookFourthPage::
    .string "Fourth page...\p"
    .string "The goal for\n"
    .string "POKéMON trainers\l"
    .string "is to beat the\l"
    .string "top 8 POKéMON\l"
    .string "GYM LEADERs.\p"
    .string "Do so to earn the\n"
    .string "right to face...\l"
    .string "The ELITE FOUR of\l"
    .string "POKéMON LEAGUE!$"

ViridianCity_School_Text_TurnThePage::
    .string "Turn the page?$"

ViridianCity_School_Text_HeyDontLookAtMyNotes::
    .string "GIRL: Hey! DonÑ\n"
    .string "look at my notes!$"

ViridianCity_School_Text_BlackboardListsStatusProblems::
    .string "The blackboard\n"
    .string "describes POKéMON\l"
    .string "STATUS changes\l"
    .string "during battles.$"

ViridianCity_School_Text_ReadWhichTopic::
    .string "Which heading do\n"
    .string "you want to read?$"

ViridianCity_School_Text_ExplainSleep::
    .string "A POKéMON canÑ\n"
    .string "attack if itÙ\l"
    .string "asleep!\p"
    .string "POKéMON will stay\n"
    .string "asleep even after\l"
    .string "battles.\p"
    .string "Use AWAKENING to\n"
    .string "wake them up!$"

ViridianCity_School_Text_ExplainBurn::
    .string "A burn reduces\n"
    .string "power and speed.\l"
    .string "It also causes\l"
    .string "ongoing damage.\p"
    .string "Burns remain\n"
    .string "after battles.\p"
    .string "Use BURN HEAL to\n"
    .string "cure a burn!$"

ViridianCity_School_Text_ExplainPoison::
    .string "When poisoned, a\n"
    .string "POKéMONÙ health\l"
    .string "steadily drops.\p"
    .string "Poison lingers\n"
    .string "after battles.\p"
    .string "Use an ANTIDOTE\n"
    .string "to cure poison!$"

ViridianCity_School_Text_ExplainFreeze::
    .string "If frozen, a\n"
    .string "POKéMON becomes\l"
    .string "totally immobile!\p"
    .string "It stays frozen\n"
    .string "even after the\l"
    .string "battle ends.\p"
    .string "Use ICE HEAL to\n"
    .string "thaw out POKéMON!$"

ViridianCity_School_Text_ExplainParalysis::
    .string "Paralysis could\n"
    .string "make POKéMON\l"
    .string "moves misfire!\p"
    .string "Paralysis remains\n"
    .string "after battles.\p"
    .string "Use PARLYZ HEAL\n"
    .string "for treatment!$"

