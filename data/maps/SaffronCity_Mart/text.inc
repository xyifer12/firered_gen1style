SaffronCity_Mart_Text_MaxRepelMoreEffectiveThanSuper::
    .string "MAX REPEL lasts\n"
    .string "longer than SUPER\l"
    .string "REPEL for keeping\l"
    .string "weaker POKéMON\l"
    .string "away!$"

SaffronCity_Mart_Text_ReviveIsCostly::
    .string "REVIVE is costly,\n"
    .string "but it revives\l"
    .string "fainted POKéMON!$"

