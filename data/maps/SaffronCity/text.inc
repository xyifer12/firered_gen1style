SaffronCity_Text_WhatDoYouWantGetLost::
    .string "What do you want?\n"
    .string "Get lost!$"

SaffronCity_Text_BossTakeTownForTeamRocket::
    .string "BOSS said heÛl\n"
    .string "take this town!$"

SaffronCity_Text_DontGetDefiantOrIllHurtYou::
    .string "Get out of the\n"
    .string "way!$"

SaffronCity_Text_SaffronBelongsToTeamRocket::
    .string "SAFFRON belongs\n"
    .string "to TEAM ROCKET!$"

SaffronCity_Text_CriminalLifeMakesMeFeelAlive::
    .string "Being evil makes\n"
    .string "me feel so alive!$"

SaffronCity_Text_WatchWhereYoureWalking::
    .string "Ow! Watch where\n"
    .string "youàe walking!$"

SaffronCity_Text_WeCanExploitMonsAroundWorld::
    .string "With SILPH under\n"
    .string "control, we can\l"
    .string "exploit POKéMON\l"
    .string "around the world!$"

SaffronCity_Text_YouBeatTeamRocket::
    .string "You beat TEAM\n"
    .string "ROCKET all alone?\l"
    .string "ThatÙ amazing!$"

SaffronCity_Text_SafeToGoOutAgain::
    .string "Yeah! TEAM ROCKET\n"
    .string "is gone!\l"
    .string "ItÙ safe to go\l"
    .string "out again!$"

SaffronCity_Text_PeopleComingBackToSaffron::
    .string "People should be\n"
    .string "flocking back to\l"
    .string "SAFFRON now.$"

SaffronCity_Text_FlewHereOnPidgeot::
    .string "I flew here on my\n"
    .string "PIDGEOT when I\l"
    .string "read about SILPH.\p"
    .string "ItÙ already over?\n"
    .string "I missed the\l"
    .string "media action.$"

SaffronCity_Text_Pidgeot::
    .string "PIDGEOT: Bi bibii!$"

SaffronCity_Text_SawRocketBossEscaping::
    .string "I saw the ROCKET\n"
    .string "BOSS escaping\l"
    .string "SILPHÙ building.$"

SaffronCity_Text_ImASecurityGuard::
    .string "Iá a security\n"
    .string "guard.\p"
    .string "Suspicious kids I\n"
    .string "donÑ allow in!$"

SaffronCity_Text_HesTakingASnooze::
    .string "...\n"
    .string "Snore...\p"
    .string "Hah! HeÙ taking\n"
    .string "a snooze!$"

SaffronCity_Text_CitySign::
    .string "SAFFRON CITY\n"
    .string "Shining, Golden\l"
    .string "Land of Commerce$"

SaffronCity_Text_FightingDojo::
    .string "FIGHTING DOJO$"

SaffronCity_Text_GymSign::
    .string "SAFFRON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: SABRINA\p"
    .string "The Master of\n"
    .string "Psychic POKéMON!$"

SaffronCity_Text_FullHealCuresStatus::
    .string "TRAINER TIPS\p"
    .string "FULL HEAL cures\n"
    .string "all ailments like\l"
    .string "sleep and burns.\p"
    .string "It costs a bit\n"
    .string "more, but itÙ\l"
    .string "more convenient.$"

SaffronCity_Text_GreatBallImprovedCatchRate::
    .string "TRAINER TIPS\p"
    .string "New GREAT BALL\n"
    .string "offers improved\l"
    .string "capture rates.\p"
    .string "Try it on those\n"
    .string "hard-to-catch\l"
    .string "POKéMON.$"

SaffronCity_Text_SilphCoSign::
    .string "SILPH CO.\n"
    .string "OFFICE BUILDING$"

SaffronCity_Text_MrPsychicsHouse::
    .string "MR. PSYCHIC'S\n"
    .string "HOUSE$"

SaffronCity_Text_SilphsLatestProduct::
    .string "SILPHÙ latest\n"
    .string "product!\p"
    .string "Release to be\n"
    .string "determined...$"

SaffronCity_Text_TrainerFanClubSign::
    .string "POKéMON TRAINER FAN CLUB\p"
    .string "Many TRAINERS have scribbled their\n"
    .string "names on this sign.$"

SaffronCity_Text_HowCanClubNotRecognizeLance::
    .string "This FAN CLUB…\n"
    .string "No one here has a clue!\p"
    .string "How could they not recognize\n"
    .string "the brilliance that is LANCE?\p"
    .string "He stands for justice!\n"
    .string "HeÙ cool, and yet passionate!\l"
    .string "HeÙ the greatest, LANCE!$"

