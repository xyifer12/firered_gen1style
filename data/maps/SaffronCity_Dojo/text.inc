SaffronCity_Dojo_Text_MasterKoichiIntro::
    .string "Grunt!\p"
    
    .string "I am the KARATE\n"
    .string "MASTER! I am the\l"
    .string "LEADER here!\p"

    .string "You wish to\n"
    .string "challenge us?\l"
    .string "Expect no mercy!\p"

    .string "Fwaaa!$"

SaffronCity_Dojo_Text_MasterKoichiDefeat::
    .string "BLACKBELT: Hwa!\n"
    .string "Arrgh! Beaten!$"

SaffronCity_Dojo_Text_ChoosePrizedFightingMon::
    .string "Indeed, I have\n"
    .string "lost!\p"

    .string "But, I beseech\n"
    .string "you, do not take\l"
    .string "our emblem as\l"
    .string "your trophy!\p"

    .string "In return, I will\n"
    .string "give you a prized\l"
    .string "fighting POKéMON!\p"

    .string "Choose whichever\n"
    .string "one you like!$"

SaffronCity_Dojo_Text_StayAndTrainWithUs::
    .string "Ho!\p"

    .string "Stay and train at\n"
    .string "Karate with us!$"

SaffronCity_Dojo_Text_MikeIntro::
    .string "Hoargh! Take your\n"
    .string "shoes off!$"

SaffronCity_Dojo_Text_MikeDefeat::
    .string "BLACKBELT: I give\n"
    .string "up!$"

SaffronCity_Dojo_Text_MikePostBattle::
    .string "You wait Ñil you\n"
    .string "see our Master!\p"

    .string "Iá a small fry\n"
    .string "compared to him!$"

SaffronCity_Dojo_Text_HidekiIntro::
    .string "I hear youàe\n"
    .string "good! Show me!$"

SaffronCity_Dojo_Text_HidekiDefeat::
    .string "BLACKBELT: Judge!\n"
    .string "1 point!$"

SaffronCity_Dojo_Text_HidekiPostBattle::
    .string "Our Master is a\n"
    .string "pro fighter!$"

SaffronCity_Dojo_Text_AaronIntro::
    .string "Nothing tough\n"
    .string "frightens me!\p"

    .string "I break boulders\n"
    .string "for training!$"

SaffronCity_Dojo_Text_AaronDefeat::
    .string "BLACKBELT: Yow!\n"
    .string "Stubbed fingers!$"

SaffronCity_Dojo_Text_AaronPostBattle::
    .string "The only thing\n"
    .string "that frightens us\l"
    .string "is psychic power!$"

SaffronCity_Dojo_Text_HitoshiIntro::
    .string "Hoohah!\p"

    .string "Youàe trespassing\n"
    .string "in our FIGHTING\l"
    .string "DOJO!$"

SaffronCity_Dojo_Text_HitoshiDefeat::
    .string "BLACKBELT: Oof!\n"
    .string "I give up!$"

SaffronCity_Dojo_Text_HitoshiPostBattle::
    .string "The prime fighters\n"
    .string "across the land\l"
    .string "train here.$"

SaffronCity_Dojo_Text_YouWantHitmonlee::
    .string "You want the\n"
    .string "hard kicking\l"
    .string "HITMONLEE?$"

SaffronCity_Dojo_Text_ReceivedMonFromKarateMaster::
    .string "{PLAYER} got\n"
    .string "{STR_VAR_1}!$"

SaffronCity_Dojo_Text_YouWantHitmonchan::
    .string "You want the\n"
    .string "piston punching\l"
    .string "HITMONCHAN?$"

SaffronCity_Dojo_Text_ReceivedMonFromKarateMaster2::
    .string "{PLAYER}は　カラテ　だいおう　から\n"
    .string "{STR_VAR_1}を　もらった！$"

SaffronCity_Dojo_Text_BetterNotGetGreedy::
    .string "Better not get\n"
    .string "greedy...$"

SaffronCity_Dojo_Text_EnemiesOnEverySide::
    .string "Enemies on every\n"
    .string "side!$"

SaffronCity_Dojo_Text_GoesAroundComesAround::
    .string "What goes around\n"
    .string "comes around!$"

SaffronCity_Dojo_Text_FightingDojo::
    .string "FIGHTING DOJO$"

