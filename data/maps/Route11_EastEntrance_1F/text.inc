@ Unclear where this is originally from
Route11_EastEntrance_1F_Text_BagIsFull::
    .string "{PLAYER}{KUN}の　バッグ\n"
    .string "いっぱい　みたい　だね$"

Route11_EastEntrance_1F_Text_ManInLavenderRatesNames::
    .string "When you catch\n"
    .string "lots of POKéMON,\l"
    .string "isnÑ it hard to\l"
    .string "think up names?\p"
    .string "In LAVENDER TOWN,\n"
    .string "thereÙ a man who\l"
    .string "rates POKéMON\l"
    .string "nicknames.\p"
    .string "HeÛl help you\n"
    .string "rename them too!$"

Route11_EastEntrance_1F_Text_RockTunnelToReachLavender::
    .string "If youàe aiming to reach LAVENDER\n"
    .string "TOWN, take ROCK TUNNEL.\p"
    .string "You can get to ROCK TUNNEL from\n"
    .string "CERULEAN CITY.$"

