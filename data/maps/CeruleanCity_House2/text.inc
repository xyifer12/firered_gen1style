CeruleanCity_House2_Text_RocketsStoleTMForDig::
    .string "Those miserable\n"
    .string "ROCKETs!\p"
    .string "Look what they\n"
    .string "did here!\p"
    .string "They stole a TM\n"
    .string "for teaching\l"
    .string "POKéMON how to\l"
    .string "DIG holes!\p"
    .string "That cost me a\n"
    .string "bundle, it did!$"

CeruleanCity_House2_Text_TeachDiglettDigWithoutTM::
    .string "I figure whatÙ\n"
    .string "lost is lost!\p"
    .string "I decided to teach\n"
    .string "DIGLETT how to\l"
    .string "DIG without a TM!$"

CeruleanCity_House2_Text_TeamRocketTryingToDigIntoNoGood::
    .string "TEAM ROCKET must\n"
    .string "be trying to DIG\l"
    .string "their way into no\l"
    .string "good!$"

CeruleanCity_House2_Text_TeamRocketLeftWayOut::
    .string "TEAM ROCKET left\n"
    .string "a way out!$"

