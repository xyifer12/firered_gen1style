SixIsland_PatternBush_Text_BethanyIntro::
    .string "Iá curious, how do you raise your\n"
    .string "POKéMON?$"

SixIsland_PatternBush_Text_BethanyDefeat::
    .string "You raise your POKéMON with a\n"
    .string "whole lot of love!$"

SixIsland_PatternBush_Text_BethanyPostBattle::
    .string "In the same way your mother raised\n"
    .string "you full of love, you should raise\l"
    .string "your POKéMON.$"

SixIsland_PatternBush_Text_AllisonIntro::
    .string "Iá working to preserve the natural\n"
    .string "ecology of POKéMON here.$"

SixIsland_PatternBush_Text_AllisonDefeat::
    .string "Oh, for someone so young, you are\n"
    .string "tremendous!$"

SixIsland_PatternBush_Text_AllisonPostBattle::
    .string "Iá not saying that you shouldnÑ\n"
    .string "catch POKéMON.\p"
    .string "I just want people to raise their\n"
    .string "POKéMON responsibly.$"

SixIsland_PatternBush_Text_GarretIntro::
    .string "I caught a BUG POKéMON that lives\n"
    .string "only around here!$"

SixIsland_PatternBush_Text_GarretDefeat::
    .string "Heheh…\n"
    .string "IsnÑ my POKéMON awesome?$"

SixIsland_PatternBush_Text_GarretPostBattle::
    .string "ThereÙ a girl near the BUSH who\n"
    .string "measures HERACROSS for TRAINERS.$"

SixIsland_PatternBush_Text_JonahIntro::
    .string "You know, it just doesnÑ feel right\n"
    .string "if I donÑ use BUG POKéMON.$"

SixIsland_PatternBush_Text_JonahDefeat::
    .string "ItÙ okay, losing is a fun part of\n"
    .string "POKéMONÙ appeal, too.$"

SixIsland_PatternBush_Text_JonahPostBattle::
    .string "By the way, “appeal” is a weird\n"
    .string "word, if you think about it.\p"
    .string "Like, is it like an orange peel?\n"
    .string "Or like a ringing bell?$"

SixIsland_PatternBush_Text_VanceIntro::
    .string "Yeah, yeah, yeah!\n"
    .string "Look at all the BUG POKéMON!$"

SixIsland_PatternBush_Text_VanceDefeat::
    .string "I got beat while I was still busy\n"
    .string "celebrating!$"

SixIsland_PatternBush_Text_VancePostBattle::
    .string "Iá going to bring my little brother\n"
    .string "here next time.$"

SixIsland_PatternBush_Text_NashIntro::
    .string "Look, look! There are funny\n"
    .string "patterns on the ground.$"

SixIsland_PatternBush_Text_NashDefeat::
    .string "Super awesome!$"

SixIsland_PatternBush_Text_NashPostBattle::
    .string "The funny patterns on the ground…\p"
    .string "They look like the patterns on my\n"
    .string "grandpaÙ clothes.$"

SixIsland_PatternBush_Text_CordellIntro::
    .string "I bet you think Iá just some guy,\n"
    .string "donÑ you, eh?$"

SixIsland_PatternBush_Text_CordellDefeat::
    .string "I bet you think Iá weak,\n"
    .string "donÑ you, eh?$"

SixIsland_PatternBush_Text_CordellPostBattle::
    .string "Ayup, you townies come around all\n"
    .string "so dandy with a hat that flashy…\p"
    .string "How about giving me that?$"

SixIsland_PatternBush_Text_DaliaIntro::
    .string "Take a deep breath.\n"
    .string "IsnÑ the air delicious?$"

SixIsland_PatternBush_Text_DaliaDefeat::
    .string "If youÚ like, I can teach you how\n"
    .string "to breathe properly.$"

SixIsland_PatternBush_Text_DaliaPostBattle::
    .string "First, exhale.\n"
    .string "Blow everything out.\p"
    .string "When you canÑ exhale anymore,\n"
    .string "inhale the clean air!\p"
    .string "IsnÑ it refreshing?$"

SixIsland_PatternBush_Text_JoanaIntro::
    .string "I love BUG POKéMON.\n"
    .string "ThatÙ why Iá here all the time.\p"
    .string "Am I the only girl like that?$"

SixIsland_PatternBush_Text_JoanaDefeat::
    .string "I lost, but Iá still laughing.\n"
    .string "Am I the only girl like that?$"

SixIsland_PatternBush_Text_JoanaPostBattle::
    .string "Iá going to keep on collecting\n"
    .string "BUG POKéMON.\p"
    .string "Am I the only girl like that?$"

SixIsland_PatternBush_Text_RileyIntro::
    .string "This is a good spot.\n"
    .string "IÛl pitch my tent here.$"

SixIsland_PatternBush_Text_RileyDefeat::
    .string "Wimped out…$"

SixIsland_PatternBush_Text_RileyPostBattle::
    .string "Iá going to observe the night sky\n"
    .string "here.$"

SixIsland_PatternBush_Text_MarcyIntro::
    .string "Oh, yuck!\n"
    .string "I think a bug stung me!$"

SixIsland_PatternBush_Text_MarcyDefeat::
    .string "It wasnÑ a bug.\n"
    .string "I cut my shin on some grass.$"

SixIsland_PatternBush_Text_MarcyPostBattle::
    .string "A little cut like that…\n"
    .string "A little spitÙ enough to cure it!$"

SixIsland_PatternBush_Text_LaytonIntro::
    .string "Have you noticed something odd\n"
    .string "about these parts?$"

SixIsland_PatternBush_Text_LaytonDefeat::
    .string "Have you taken a good look around\n"
    .string "your feet?$"

SixIsland_PatternBush_Text_LaytonPostBattle::
    .string "There are places here in PATTERN\n"
    .string "BUSH where grass wonÑ grow.\p"
    .string "What could be the cause of such\n"
    .string "a phenomenon?$"

