VermilionCity_Text_GrimerMultipliesInSludge::
    .string "Weàe careful\n"
    .string "about pollution!\p"
    .string "Weße heard GRIMER\n"
    .string "multiplies in\l"
    .string "toxic sludge!$"

VermilionCity_Text_DidYouSeeSSAnneInHarbor::
    .string "Did you see S.S.\n"
    .string "ANNE moored in\l"
    .string "the harbor?$"

VermilionCity_Text_SSAnneHasDepartedForYear::
    .string "So, the S.S.ANNE has\n"
    .string "departed!\p"
    .string "SheÛl be back in\n"
    .string "about a year.$"

VermilionCity_Text_BuildingOnThisLand::
    .string "Iá putting up a\n"
    .string "building on this\l"
    .string "plot of land.\p"
    .string "My POKéMON is\n"
    .string "tamping the land.$"

VermilionCity_Text_Machop::
    .string "MACHOP: Guoh!\n"
    .string "Gogogoh!$"

VermilionCity_Text_MachopStompingLandFlat::
    .string "A MACHOP is\n"
    .string "stomping the land\l"
    .string "flat.$"

VermilionCity_Text_SSAnneVisitsOnceAYear::
    .string "S.S.ANNE is a\n"
    .string "famous luxury\l"
    .string "cruise ship.\p"
    .string "We visit VERMILION\n"
    .string "once a year.$"

VermilionCity_Text_CitySign::
    .string "VERMILION CITY\n"
    .string "The Port of\l"
    .string "Exquisite Sunsets$"

VermilionCity_Text_SnorlaxBlockingRoute12::
    .string "NOTICE!\p"
    .string "ROUTE 12 may be\n"
    .string "blocked off by a\l"
    .string "sleeping POKéMON.\p"
    .string "Detour through\n"
    .string "ROCK TUNNEL to\l"
    .string "LAVENDER TOWN.\p"
    .string "VERMILION POLICE$"

VermilionCity_Text_PokemonFanClubSign::
    .string "POKéMON FAN CLUB\n"
    .string "All POKéMON fans\l"
    .string "welcome!$"

VermilionCity_Text_GymSign::
    .string "VERMILION CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: LT.SURGE\p"
    .string "The Lightning\n"
    .string "American!$"

VermilionCity_Text_VermilionHarbor::
    .string "VERMILION HARBOR$"

VermilionCity_Text_WelcomeToTheSSAnne::
    .string "Welcome to S.S.\n"
    .string "ANNE!$"

VermilionCity_Text_DoYouHaveATicket::
    .string "Welcome to S.S.\n"
    .string "ANNE!\p"
    .string "Excuse me, do you\n"
    .string "have a ticket?$"

VermilionCity_Text_FlashedSSTicket::
    .string "{PLAYER} flashed\n"
    .string "the S.S.TICKET!\p"
    .string "Great! Welcome to\n"
    .string "S.S.ANNE!$"

VermilionCity_Text_DontHaveNeededSSTicket::
    .string "{PLAYER} doesnÑ\n"
    .string "have the needed\l"
    .string "S.S.TICKET.\p"
    .string "Sorry!\p"
    .string "You need a ticket\n"
    .string "to get aboard.$"

VermilionCity_Text_TheShipSetSail::
    .string "The ship set sail.$"

VermilionCity_Text_BoardSeagallopTriPass::
    .string "Ah, you have a TRI-PASS.\p"
    .string "Would you like to board\n"
    .string "a SEAGALLOP ferry?$"

VermilionCity_Text_Seagallop7Departing::
    .string "Okay, everythingÙ in order.\p"
    .string "SEAGALLOP HI-SPEED 7 will be\n"
    .string "departing immediately.$"

VermilionCity_Text_BoardSeagallopRainbowPass::
    .string "Ah, you have a RAINBOW PASS.\p"
    .string "Would you like to board\n"
    .string "a SEAGALLOP ferry?$"

VermilionCity_Text_OhMysticTicketTakeYouToNavelRock::
    .string "Oh! ThatÙ a MYSTICTICKET!\n"
    .string "Now that is rare.\p"
    .string "WeÛl be happy to take you to\n"
    .string "NAVEL ROCK anytime.$"

VermilionCity_Text_OhAuroraTicketTakeYouToBirthIsland::
    .string "Oh! ThatÙ an AURORATICKET!\n"
    .string "Now that is rare.\p"
    .string "WeÛl be happy to take you to\n"
    .string "BIRTH ISLAND anytime.$"

VermilionCity_Text_BoardSeagallopFerry::
    .string "Would you like to board\n"
    .string "a SEAGALLOP ferry?$"

VermilionCity_Text_Seagallop10Departing::
    .string "Okay, everythingÙ in order for you\n"
    .string "to board a special ferry.\p"
    .string "SEAGALLOP HI-SPEED 10 will be\n"
    .string "departing immediately.$"

VermilionCity_Text_Seagallop12Departing::
    .string "Okay, everythingÙ in order for you\n"
    .string "to board a special ferry.\p"
    .string "SEAGALLOP HI-SPEED 12 will be\n"
    .string "departing immediately.$"

VermilionCity_Text_Route2AideHasPackageForYou::
    .string "Oh, hello, {PLAYER}!\n"
    .string "How are you doing?\p"
    .string "ItÙ me, one of PROF. OAKÙ AIDES.\p"
    .string "Did you meet the other AIDE?\p"
    .string "He had a package from PROF. OAK\n"
    .string "for you, {PLAYER}.\p"
    .string "He said heÚ look for you around\n"
    .string "ROUTE 2, {PLAYER}.\p"
    .string "If youàe in the ROUTE 2 area,\n"
    .string "please look for him.$"

