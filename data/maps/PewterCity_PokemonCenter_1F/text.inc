PewterCity_PokemonCenter_1F_Text_TeamRocketMtMoonImOnPhone::
    .string "What!?\p"
    .string "TEAM ROCKET is\n"
    .string "at MT.MOON? Huh?\l"
    .string "Iá on the phone!\p"
    .string "Scram!$"

PewterCity_PokemonCenter_1F_Text_Jigglypuff::
    .string "JIGGLYPUFF: Puu\n"
    .string "pupuu!$"

PewterCity_PokemonCenter_1F_Text_WhenJiggylypuffSingsMonsGetDrowsy::
    .string "Yawn!\p"
    .string "When JIGGLYPUFF\n"
    .string "sings, POKéMON\l"
    .string "get drowsy...\p"
    .string "...Me too...\n"
    .string "Snore...$"

PewterCity_PokemonCenter_1F_Text_TradingMyClefairyForPikachu::
    .string "I really want a PIKACHU, so Iá\n"
    .string "trading my CLEFAIRY for one.$"

PewterCity_PokemonCenter_1F_Text_TradingPikachuWithKid::
    .string "Iá trading POKéMON with that kid\n"
    .string "there.\p"
    .string "I had two PIKACHU, so I figured\n"
    .string "I might as well trade one.$"

