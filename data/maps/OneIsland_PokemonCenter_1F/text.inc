OneIsland_PokemonCenter_1F_Text_BillHeyThereCelio::
    .string "BILL: Hey, there!\n"
    .string "CELIO!$"

OneIsland_PokemonCenter_1F_Text_CelioCantBelieveYouCameOut::
    .string "CELIO: BILL!\n"
    .string "I canÑ believe you came out here.$"

OneIsland_PokemonCenter_1F_Text_BillHowsYourResearchComing::
    .string "BILL: Well, absolutely!\n"
    .string "HowÙ your research coming along?\p"
    .string "…Oh, wait a sec.$"

OneIsland_PokemonCenter_1F_Text_ThisIsMyBuddyCelio::
    .string "{PLAYER}, this is my buddy CELIO.\n"
    .string "HeÙ one dedicated PC MANIAC!$"

OneIsland_PokemonCenter_1F_Text_PlayerIsRisingPokemonChamp::
    .string "CELIO, this is {PLAYER}, a rising\n"
    .string "contender as the POKéMON CHAMP!$"

OneIsland_PokemonCenter_1F_Text_PlayerIsReigningPokemonChamp::
    .string "CELIO, this is {PLAYER}, the\n"
    .string "reigning POKéMON CHAMP!$"

OneIsland_PokemonCenter_1F_Text_CelioThatsReallyImpressive::
    .string "CELIO: ThatÙ really impressive.\p"
    .string "I hate to say it, but I have zero\n"
    .string "aptitude for battling.\p"
    .string "Anyways, Iá glad to meet you.$"

OneIsland_PokemonCenter_1F_Text_BillBringMeUpToSpeed::
    .string "BILL: So, bring me up to speed.\n"
    .string "HowÙ your machine running?$"

OneIsland_PokemonCenter_1F_Text_CelioPCsCantLinkWithYours::
    .string "CELIO: ItÙ running fine, but weàe\n"
    .string "too remote out here.\p"
    .string "The PCs on this island just canÑ\n"
    .string "link with your PC, BILL.$"

OneIsland_PokemonCenter_1F_Text_BillLetMeHelpYou::
    .string "BILL: Oh, yeah?\n"
    .string "Okay, let me take a look-see.\p"
    .string "…Hang on here…\n"
    .string "I think we can make it work.\l"
    .string "Let me help you, okay?$"

OneIsland_PokemonCenter_1F_Text_CanYouDeliverThisMeteoritePlayer::
    .string "{PLAYER}, can I get you to wait for\n"
    .string "me just a bit?\p"
    .string "…Actually, can I get you to do\n"
    .string "me a favor?\p"
    .string "The island next to this oneÙ\n"
    .string "called TWO ISLAND.\p"
    .string "ThereÙ a guy there that runs\n"
    .string "a GAME CORNER.\p"
    .string "He has this thing for rare rocks\n"
    .string "and gems.\p"
    .string "We keep in touch, being fellow\n"
    .string "maniacs.\p"
    .string "So, can I get you to deliver this\n"
    .string "METEORITE to him?$"

OneIsland_PokemonCenter_1F_Text_AcceptedMeteoriteFromBill::
    .string "{PLAYER} accepted the METEORITE\n"
    .string "from BILL.$"

OneIsland_PokemonCenter_1F_Text_CelioPleaseTakeThis::
    .string "CELIO: {PLAYER}, if you are going\n"
    .string "to TWO ISLAND, please take this.$"

OneIsland_PokemonCenter_1F_Text_PassLetsYouTravelBetweenIslands::
    .string "ItÙ a PASS for the ferry service\n"
    .string "serving the local islands.\p"
    .string "It will let you travel between the\n"
    .string "ISLANDS ONE, TWO, and THREE.\p"
    .string "Oh, you should have this, too.$"

OneIsland_PokemonCenter_1F_Text_ReceivedExtraPageForTownMap::
    .string "{PLAYER} received an extra page\n"
    .string "for the TOWN MAP!$"

OneIsland_PokemonCenter_1F_Text_ReceivedTownMap::
    .string "{PLAYER} received\n"
    .string "a TOWN MAP!$"

OneIsland_PokemonCenter_1F_Text_BillCatchYouLater::
    .string "BILL: IÛl catch you later!\n"
    .string "Say hi to the guy for me!$"

OneIsland_PokemonCenter_1F_Text_HmmHowAboutLikeThis::
    .string "Hmm…\p"
    .string "How about we try this like this…$"

OneIsland_PokemonCenter_1F_Text_GotPCWorkingStrollAWhileMore::
    .string "Oh, hey, {PLAYER}!\p"
    .string "Did you see?\n"
    .string "We got the PC working!\p"
    .string "Iße got a few things to show\n"
    .string "CELIO here.\p"
    .string "Can you go out on a stroll or\n"
    .string "something for a while more?$"

OneIsland_PokemonCenter_1F_Text_SorryForBeingPoorHost::
    .string "Iá sorry for taking up so much of\n"
    .string "BILLÙ time.\p"
    .string "Iá also sorry for being such a \n"
    .string "poor host on your visit here.$"

OneIsland_PokemonCenter_1F_Text_UsualPCServicesUnavailable::
    .string "The usual PC services arenÑ\n"
    .string "available…$"

OneIsland_PokemonCenter_1F_Text_BillOhHeyPlayer::
    .string "BILL: Oh, hey!\n"
    .string "{PLAYER}!$"

OneIsland_PokemonCenter_1F_Text_BillWeGotItDone::
    .string "BILL: What kept you so long?\n"
    .string "Been out having a good time?\p"
    .string "We got it done.\n"
    .string "The PCs are up and running!$"

OneIsland_PokemonCenter_1F_Text_CelioJobWentQuick::
    .string "CELIO: The job went incredibly\n"
    .string "quick.\p"
    .string "BILL is one amazing guy…$"

OneIsland_PokemonCenter_1F_Text_BillYouveLearnedALot::
    .string "BILL: No, no! There was almost\n"
    .string "nothing left for me to do.\p"
    .string "CELIO, I have to hand it to you.\n"
    .string "Youße learned a lot.$"

OneIsland_PokemonCenter_1F_Text_CelioOhReallyEhehe::
    .string "CELIO: Oh, really?\n"
    .string "Ehehe…$"

OneIsland_PokemonCenter_1F_Text_BillWeShouldHeadBackToKanto::
    .string "BILL: Well, there you have it.\n"
    .string "Iá finished with the job.\l"
    .string "We should head back to KANTO.\p"
    .string "CELIO, IÛl be seeing you again.$"

OneIsland_PokemonCenter_1F_Text_CelioPromiseIllShowYouAroundSometime::
    .string "CELIO: {PLAYER}, Iá really sorry\n"
    .string "that we sent you off alone today.\p"
    .string "I promise, I will show you around\n"
    .string "these islands sometime.$"

OneIsland_PokemonCenter_1F_Text_CelioImModifyingMyNetworkMachine::
    .string "CELIO: Hello!\n"
    .string "You look awfully busy as always.\p"
    .string "How am I doing?\p"
    .string "Well, Iá modifying my Network\n"
    .string "Machine.\p"
    .string "When I get done with the machine,\n"
    .string "I hope youÛl be first to use it,\l"
    .string "{PLAYER}.$"

OneIsland_PokemonCenter_1F_Text_CelioCaughtMoreMonMaybeICanBeUseful::
    .string "CELIO: {PLAYER}, how have things\n"
    .string "been for you?\p"
    .string "Oh, is that right?\n"
    .string "Youße caught more POKéMON.\p"
    .string "Do you know what?\n"
    .string "Maybe I can be useful to you.$"

OneIsland_PokemonCenter_1F_Text_YoullBeTradingFromTrainersFarAway::
    .string "Iá modifying the Network Machine\n"
    .string "right now.\p"
    .string "Iá changing it so it can handle\n"
    .string "trades over long distances.\p"
    .string "When I get finished, youÛl be\n"
    .string "trading for exotic POKéMON from\l"
    .string "TRAINERS far away.$"

OneIsland_PokemonCenter_1F_Text_NeedsSpecialGemstone::
    .string "But, there is a slight catch.\p"
    .string "For the link to work, the Machine\n"
    .string "needs a special gemstone.\p"
    .string "ItÙ supposed to be on ONE ISLAND,\n"
    .string "but I havenÑ found one yet.\p"
    .string "Who knows where it could be.$"

OneIsland_PokemonCenter_1F_Text_TryingToFindGem::
    .string "I was trying to find the gem\n"
    .string "even while I was studying.\p"
    .string "As a result, Iße made no headway\n"
    .string "in both my search and studies…\p"
    .string "If I relied on BILL, Iá sure my\n"
    .string "research would progress.\p"
    .string "But this time, I want to try to do\n"
    .string "things by myself.$"

OneIsland_PokemonCenter_1F_Text_OhThats::
    .string "Oh!\n"
    .string "Th-thatÙ…$"

OneIsland_PokemonCenter_1F_Text_HandedRubyToCelio::
    .string "{PLAYER} handed the RUBY\n"
    .string "to CELIO.$"

OneIsland_PokemonCenter_1F_Text_MayIAskOneMoreFavor::
    .string "Thank you!\n"
    .string "{PLAYER}, youàe simply amazing.\p"
    .string "… … …  … … …\p"
    .string "Um… May I ask one more giant favor\n"
    .string "of you?$"

OneIsland_PokemonCenter_1F_Text_PleaseINeedYourHelp::
    .string "It… ItÙ not anything weird.\n"
    .string "Please, I need your help.$"

OneIsland_PokemonCenter_1F_Text_AnotherGemstoneInSeviiIslands::
    .string "While I was studying gemstones,\n"
    .string "I discovered something important.\p"
    .string "There is another gem that forms\n"
    .string "a pair with this RUBY.\p"
    .string "That other gemstone is supposed to\n"
    .string "be in the SEVII ISLANDS.\p"
    .string "{PLAYER}, please, I need you to go\n"
    .string "find the other gem.\p"
    .string "{PLAYER}, may I have your ferry\n"
    .string "PASS and the TOWN MAP?$"

OneIsland_PokemonCenter_1F_Text_ReturnedTriPassForRainbowPass::
    .string "{PLAYER} returned the TRI-PASS and\n"
    .string "received the RAINBOW PASS.$"

OneIsland_PokemonCenter_1F_Text_ObtainedExtraMapPage::
    .string "Obtained an extra page for the\n"
    .string "TOWN MAP!$"

OneIsland_PokemonCenter_1F_Text_PassLetYouGetToAllIslands::
    .string "This is my own ferry PASS.\p"
    .string "It will let you get to all the\n"
    .string "SEVII ISLANDS.\p"
    .string "{PLAYER}, please, I canÑ do\n"
    .string "it without your help.$"

OneIsland_PokemonCenter_1F_Text_HandedSapphireToCelio::
    .string "{PLAYER} handed the SAPPHIRE\n"
    .string "to CELIO.$"

OneIsland_PokemonCenter_1F_Text_ThankYouGiveMeTime::
    .string "CELIO: So this is the gem that\n"
    .string "forms a pair with the RUBY…\p"
    .string "{PLAYER}, youße gone through a lot\n"
    .string "to get this, didnÑ you?\p"
    .string "You donÑ have to tell me. I know\n"
    .string "it wasnÑ easy.\p"
    .string "Thank you so much!\p"
    .string "Now itÙ my turn to work for you!\n"
    .string "Please give me a little time.$"

OneIsland_PokemonCenter_1F_Text_OkayThisIsGood::
    .string "Okay, this is good…$"

OneIsland_PokemonCenter_1F_Text_LinkedUpWithLanette::
    .string "I did it!\n"
    .string "I linked up with LANETTE!$"

OneIsland_PokemonCenter_1F_Text_ManagedToLinkWithHoennThankYou::
    .string "{PLAYER}…\n"
    .string "{PLAYER}, I did it!\p"
    .string "Iße managed to link up with\n"
    .string "TRAINERS in the HOENN region!\p"
    .string "Finally, the Network Machine is\n"
    .string "fully operational!\p"
    .string "{PLAYER}, I owe it all to you!\p"
    .string "Thanks to you, my dream came\n"
    .string "true…$"

OneIsland_PokemonCenter_1F_Text_WishYouBestOfLuck::
    .string "I…\n"
    .string "Iá not crying.\p"
    .string "ThatÙ enough about me!\p"
    .string "{PLAYER}, youàe going to keep\n"
    .string "looking for exotic POKéMON, right?\p"
    .string "I wish you the best of luck!$"

OneIsland_PokemonCenter_1F_Text_CelioHearingRumorsAboutYou::
    .string "CELIO: Hello!\p"
    .string "{PLAYER}, Iße been hearing rumors\n"
    .string "about you.$"

OneIsland_PokemonCenter_1F_Text_BillsFirstMonWasAbra::
    .string "{PLAYER}, whatÙ your favorite kind\n"
    .string "of POKéMON?\p"
    .string "BILL is a POKéMANIAC, so he loves\n"
    .string "every kind.\p"
    .string "Apparently, the first one he caught\n"
    .string "was an ABRA.$"

OneIsland_PokemonCenter_1F_Text_BillsHometownInGoldenrod::
    .string "By the way, {PLAYER}, youàe from\n"
    .string "PALLET TOWN, arenÑ you?\p"
    .string "Iße heard that itÙ a quiet and\n"
    .string "pleasant place.\p"
    .string "BILLÙ hometown is GOLDENROD CITY,\n"
    .string "where his folks still live.\p"
    .string "Iße heard that itÙ quite the\n"
    .string "festive, bustling city.\p"
    .string "IÚ like to go there one day.$"

OneIsland_PokemonCenter_1F_Text_BillCantStomachMilk::
    .string "{PLAYER}, is there anything that\n"
    .string "you canÑ stand?\p"
    .string "Apparently, BILL simply canÑ\n"
    .string "stomach milk at all.$"

OneIsland_PokemonCenter_1F_Text_CameFromPalletDontKnowIt::
    .string "Oh, youàe a stranger here!\n"
    .string "Hi! Where did you come from?\p"
    .string "…PALLET TOWN?\n"
    .string "I donÑ know it!$"

OneIsland_PokemonCenter_1F_Text_EnormousVolcanoOnIsland::
    .string "On this island, thereÙ an enormous\n"
    .string "volcano.\p"
    .string "It hasnÑ erupted lately, so why\n"
    .string "not enjoy a hike?$"

OneIsland_PokemonCenter_1F_Text_WishICouldTradeWithBoyfriend::
    .string "I wish I could trade POKéMON with\n"
    .string "my boyfriend who lives far away…$"

OneIsland_PokemonCenter_1F_Text_TradedWithFarAwayBoyfriend::
    .string "I traded POKéMON with my boyfriend\n"
    .string "far away!\p"
    .string "EveryoneÙ saying that we can\n"
    .string "thank you and CELIO.\p"
    .string "So, thank you!$"

OneIsland_PokemonCenter_1F_Text_MachineUnderAdjustment::
    .string "Network Machine\n"
    .string "Link Level 0\p"
    .string "…POKéMON Storage System under\n"
    .string "adjustment…$"

OneIsland_PokemonCenter_1F_Text_MachineLinkedWithKanto::
    .string "Network Machine\n"
    .string "Link Level 1\p"
    .string "Link established with the KANTO\n"
    .string "region.$"

OneIsland_PokemonCenter_1F_Text_MachineLinkedWithKantoAndHoenn::
    .string "Network Machine\n"
    .string "Link Level 2\p"
    .string "Link established with the KANTO\n"
    .string "and HOENN regions.$"

OneIsland_PokemonCenter_1F_Text_ObtainedTriPass::
    .string "Obtained the TRI-PASS!$"

