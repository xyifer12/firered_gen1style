PewterCity_House1_Text_Nidoran::
    .string "NIDORAN: Bowbow!$"

PewterCity_House1_Text_NidoranSit::
    .string "NIDORAN sit!$"

PewterCity_House1_Text_TradeMonsAreFinicky::
    .string "Our POKéMONÙ an\n"
    .string "outsider, so itÙ\l"
    .string "hard to handle.\p"
    .string "An outsider is a\n"
    .string "POKéMON that you\l"
    .string "get in a trade.\p"
    .string "It grows fast, but\n"
    .string "it may ignore an\l"
    .string "unskilled trainer\l"
    .string "in battle!\p"
    .string "If only we had\n"
    .string "some BADGEs...$"

