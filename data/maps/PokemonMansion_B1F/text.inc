PokemonMansion_B1F_Text_LewisIntro::
    .string "Uh-oh. Where am\n"
    .string "I now?$"

PokemonMansion_B1F_Text_LewisDefeat::
    .string "BURGLAR: Awooh!$"

PokemonMansion_B1F_Text_LewisPostBattle::
    .string "You can find stuff\n"
    .string "lying around.$"

PokemonMansion_B1F_Text_IvanIntro::
    .string "This place is\n"
    .string "ideal for a lab.$"

PokemonMansion_B1F_Text_IvanDefeat::
    .string "SCIENTIST: What\n"
    .string "was that for?$"

PokemonMansion_B1F_Text_IvanPostBattle::
    .string "I like it here!\n"
    .string "ItÙ conducive\l"
    .string "to my studies!$"

PokemonMansion_B1F_Text_MewtwoIsFarTooPowerful::
    .string "Diary: Sept. 1\n"
    .string "MEWTWO is far too\l"
    .string "powerful.\p"
    .string "We have failed to\n"
    .string "curb its vicious\l"
    .string "tendencies...$"

