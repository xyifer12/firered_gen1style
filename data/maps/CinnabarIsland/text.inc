CinnabarIsland_Text_DoorIsLocked::
    .string "The door is\n"
    .string "locked...$"

CinnabarIsland_Text_BlaineLivedHereSinceBeforeLab::
    .string "CINNABAR GYMÙ\n"
    .string "BLAINE is an odd\l"
    .string "man who has lived\l"
    .string "here for decades.$"

CinnabarIsland_Text_ScientistsExperimentInMansion::
    .string "Scientists conduct\n"
    .string "experiments in\l"
    .string "the burned out\l"
    .string "building.$"

CinnabarIsland_Text_IslandSign::
    .string "CINNABAR ISLAND\n"
    .string "The Fiery Town of\l"
    .string "Burning Desire$"

CinnabarIsland_Text_PokemonLab::
    .string "POKéMON LAB$"

CinnabarIsland_Text_GymSign::
    .string "CINNABAR ISLAND\n"
    .string "POKéMON GYM\l"
    .string "LEADER: BLAINE\p"
    .string "The Hot-Headed\n"
    .string "Quiz Master!$"

CinnabarIsland_Text_HeyIfItIsntPlayer::
    .string "Huh?\n"
    .string "Hey, if it isnÑ {PLAYER}!$"

CinnabarIsland_Text_ComeWithMeToOneIsland::
    .string "Look, itÙ me, BILL.\n"
    .string "Long time no see!\p"
    .string "I hope youàe still using my\n"
    .string "PC system.\p"
    .string "Well, listen, since we met up here,\n"
    .string "how about spending time with me?\p"
    .string "ThereÙ this little island in the far\n"
    .string "south called ONE ISLAND.\p"
    .string "A friend invited me, so Iá on my\n"
    .string "way out there.\p"
    .string "How about it?\n"
    .string "Do you feel like coming with me?$"

CinnabarIsland_Text_AllRightLetsGo::
    .string "All right, then.\n"
    .string "LetÙ go!$"

CinnabarIsland_Text_IllBeWaitingInPokeCenter::
    .string "What, are you too busy?\p"
    .string "Well, all right.\n"
    .string "The boat hasnÑ arrived yet anyway.\p"
    .string "IÛl be waiting at the POKéMON\n"
    .string "CENTER over there.\p"
    .string "Come see me when youàe done with\n"
    .string "your business here.$"

CinnabarIsland_Text_MyPalsBoatArrived::
    .string "Looks like my palÙ boat arrived,\n"
    .string "too.\p"
    .string "He sent it specially here to\n"
    .string "CINNABAR to pick me up.$"

CinnabarIsland_Text_IfYouHaveTriPassYouCanGoAgain::
    .string "Hey, wasnÑ that a long cruise?\p"
    .string "My buddy CELIO seemed to enjoy\n"
    .string "your company.\p"
    .string "Iá sure heÚ welcome you if you\n"
    .string "were to visit him again.\p"
    .string "If you have a TRI-PASS, you can\n"
    .string "always take a ferry there from\l"
    .string "VERMILION PORT.\p"
    .string "All right, thanks for your company!$"

