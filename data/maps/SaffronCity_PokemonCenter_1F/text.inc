SaffronCity_PokemonCenter_1F_Text_GrowthRatesDifferBySpecies::
    .string "POKéMON growth\n"
    .string "rates differ from\l"
    .string "specie to specie.$"

SaffronCity_PokemonCenter_1F_Text_SilphCoVictimOfFame::
    .string "SILPH CO. is very\n"
    .string "famous. ThatÙ\l"
    .string "why it attracted\l"
    .string "TEAM ROCKET!$"

SaffronCity_PokemonCenter_1F_Text_GreatIfEliteFourCameBeatRockets::
    .string "It would be great\n"
    .string "if the ELITE FOUR\l"
    .string "came and stomped\l"
    .string "TEAM ROCKET!$"

SaffronCity_PokemonCenter_1F_Text_TeamRocketTookOff::
    .string "TEAM ROCKET took\n"
    .string "off! We can go\l"
    .string "out safely again!\l"
    .string "ThatÙ great!$"

