PalletTown_Text_OakDontGoOut::
    .string "OAK: Hey! Wait!\n"
    .string "DonÑ go out!$"

PalletTown_Text_OakGrassUnsafeNeedMon::
    .string "OAK: ItÙ unsafe!\n"
    .string "Wild POKéMON live\l"
    .string "in tall grass!\p"
    .string "You need your own\n"
    .string "POKéMON for your\l"
    .string "protection.\p"
    .string "I know!\n"
    .string "Here, come with\l"
    .string "me!$"

PalletTown_Text_RaisingMonsToo::
    .string "Iá raising\n"
    .string "POKéMON, too!\p"
    .string "When they get\n"
    .string "strong, they can\l"
    .string "protect me.$"

PalletTown_Text_CanStoreItemsAndMonsInPC::
    .string "Technology is\n"
    .string "incredible!\p"
    .string "You can now store\n"
    .string "and recall items\l"
    .string "and POKéMON as\l"
    .string "data via PC!$"

PalletTown_Text_OakPokemonResearchLab::
    .string "OAK POKéMON\n"
    .string "RESEARCH LAB$"

PalletTown_Text_PlayersHouse::
    .string "{PLAYER}Ù house$"

PalletTown_Text_RivalsHouse::
    .string "{RIVAL}Ù house$"

PalletTown_Text_TownSign::
    .string "PALLET TOWN\n"
    .string "Shades of your\p"
    .string "journey await!$"

PalletTown_Text_OakLetMeSeePokedex::
    .string "OAK: Ah, {PLAYER}!\n"
    .string "Youàe back, are you?\l"
    .string "How much have you filled\p"
    .string "in your POKéDEX?\n"
    .string "May I see it?\p"
    .string "LetÙ see...$"

PalletTown_Text_CaughtXPuttingInHonestEffort::
    .string "Youße caught {STR_VAR_2}...\p"
    .string "Hm, it looks as if\n"
    .string "youàe putting in an\n"
    .string "honest effort.\p"
    .string "When you manage to fill\n"
    .string "it some more, come show\l"
    .string "me, please.$"

PalletTown_Text_CaughtXImpressiveFollowMe::
    .string "Youße caught... {STR_VAR_2}!?\n"
    .string "Now, this is impressive!\p"
    .string "ThereÙ something I\n"
    .string "wanted to ask of you,\l"
    .string "{PLAYER}.\p"
    .string "Come.\n"
    .string "Follow me.$"

PalletTown_Text_OakYouEnjoyingTraveling::
    .string "OAK: Ah, {PLAYER}!\n"
    .string "You seem to be enjoying traveling.\p"
    .string "Knowing you, {PLAYER}, I can easily\n"
    .string "imagine you going out to even more\l"
    .string "exotic locales.\p"
    .string "Good for you, good for you.\n"
    .string "Hohoho.$"

