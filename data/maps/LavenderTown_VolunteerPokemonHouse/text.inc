LavenderTown_PokemonCenter_1F_Text_HearMrFujiNotFromAroundHere::
    .string "I recently moved to this town.\p"
    .string "I hear that MR. FUJIÙ not from\n"
    .string "these parts originally, either.$"

LavenderTown_VolunteerPokemonHouse_Text_WhereDidMrFujiGo::
    .string "ThatÙ odd, MR.FUJI\n"
    .string "isnÑ here.\l"
    .string "WhereÚ he go?$"

LavenderTown_VolunteerPokemonHouse_Text_MrFujiWasPrayingForCubonesMother::
    .string "MR.FUJI had been\n"
    .string "praying alone for\l"
    .string "CUBONEÙ mother.$"

LavenderTown_VolunteerPokemonHouse_Text_MrFujiLooksAfterOrphanedMons::
    .string "This is really\n"
    .string "MR.FUJIÙ house.\p"
    .string "HeÙ really kind!\p"
    .string "He looks after\n"
    .string "abandoned and\l"
    .string "orphaned POKéMON!$"

LavenderTown_VolunteerPokemonHouse_Text_MonsNiceToHug::
    .string "ItÙ so warm!\n"
    .string "POKéMON are so\l"
    .string "nice to hug.$"

LavenderTown_VolunteerPokemonHouse_Text_Nidorino::
    .string "NIDORINO: Gaoo!$"

LavenderTown_VolunteerPokemonHouse_Text_Psyduck::
    .string "PSYDUCK: Gwappa!$"

LavenderTown_VolunteerPokemonHouse_Text_IdLikeYouToHaveThis::
    .string "MR.FUJI: {PLAYER}.\p"
    .string "Your POKéDEX quest\n"
    .string "may fail without\l"
    .string "love for your\l"
    .string "POKéMON.\p"
    .string "I think this may\n"
    .string "help your quest.$"

LavenderTown_VolunteerPokemonHouse_Text_ReceivedPokeFluteFromMrFuji::
    .string "{PLAYER} received a\n"
    .string "POKé FLUTE!$"

LavenderTown_VolunteerPokemonHouse_Text_ExplainPokeFlute::
    .string "Upon hearing POKé\n"
    .string "FLUTE, sleeping\l"
    .string "POKéMON will\l"
    .string "spring awake.\p"
    .string "It works on all\n"
    .string "sleeping POKéMON.$"

LavenderTown_VolunteerPokemonHouse_Text_MustMakeRoomForThis::
    .string "You must make\n"
    .string "room for this!$"

LavenderTown_VolunteerPokemonHouse_Text_HasPokeFluteHelpedYou::
    .string "MR.FUJI: Has my\n"
    .string "FLUTE helped you?$"

LavenderTown_VolunteerPokemonHouse_Text_GrandPrizeDrawingClipped::
    .string "POKéMON Monthly\n"
    .string "Grand Prize\l"
    .string "Drawing!\p"
    .string "The application\n"
    .string "form is...\p"
    .string "Gone! ItÙ been\n"
    .string "clipped out!$"

LavenderTown_VolunteerPokemonHouse_Text_PokemonMagazinesLineShelf::
    .string "POKéMON magazines!\p"
    .string "POKéMON notebooks!\p"
    .string "POKéMON graphs!$"

