CeladonCity_DepartmentStore_Roof_Text_ImThirstyGiveHerDrink::
    .string "Iá thirsty!\n"
    .string "I want something\l"
    .string "to drink!\p"
    .string "Give her a drink?$"

CeladonCity_DepartmentStore_Roof_Text_GiveWhichDrink::
    .string "Give her which\n"
    .string "drink?$"

CeladonCity_DepartmentStore_Roof_Text_YayFreshWaterHaveThis::
    .string "Yay!\p"
    .string "FRESH WATER!\p"
    .string "Thank you!\p"
    .string "You can have this\n"
    .string "from me!$"

Text_ReceivedItemFromLittleGirl::
    .string "{PLAYER} received\n"
    .string "{STR_VAR_2}!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM13::
    .string "TM13 contains\n"
    .string "ICE BEAM!\p"
    .string "It can freeze the\n"
    .string "target sometimes!$"

CeladonCity_DepartmentStore_Roof_Text_YaySodaPopHaveThis::
    .string "Yay!\p"
    .string "SODA POP!\p"
    .string "Thank you!\p"
    .string "You can have this\n"
    .string "from me!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM48::
    .string "TM48 contains\n"
    .string "ROCK SLIDE!\p"
    .string "It can spook the\n"
    .string "target sometimes!$"

CeladonCity_DepartmentStore_Roof_Text_YayLemonadeHaveThis::
    .string "Yay!\p"
    .string "LEMONADE!\p"
    .string "Thank you!\p"
    .string "You can have this\n"
    .string "from me!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM49::
    .string "TM49 contains\n"
    .string "TRI ATTACK!$"

CeladonCity_DepartmentStore_Roof_Text_DontHaveSpaceForThis::
    .string "You donÑ have\n"
    .string "space for this!$"

CeladonCity_DepartmentStore_Roof_Text_ImNotThirstyAfterAll::
    .string "No thank you!\n"
    .string "Iá not thirsty\l"
    .string "after all!$"

CeladonCity_DepartmentStore_Roof_Text_MySisterIsImmature::
    .string "My sister is a\n"
    .string "trainer, believe\l"
    .string "it or not.\p"
    .string "But, sheÙ so\n"
    .string "immature, she\l"
    .string "drives me nuts!$"

CeladonCity_DepartmentStore_Roof_Text_ImThirstyIWantDrink::
    .string "Iá thirsty!\n"
    .string "I want something\l"
    .string "to drink!$"

CeladonCity_DepartmentStore_Roof_Text_FloorSign::
    .string "ROOFTOP SQUARE:\n"
    .string "VENDING MACHINES$"

CeladonCity_DepartmentStore_Roof_Text_VendingMachineWhatDoesItHave::
    .string "A vending machine!\n"
    .string "HereÙ the menu!$"

CeladonCity_DepartmentStore_Roof_Text_NotEnoughMoney::
    .string "Oops, not enough\n"
    .string "money!$"

CeladonCity_DepartmentStore_Roof_Text_DrinkCanPoppedOut::
    .string "{STR_VAR_1}\n"
    .string "popped out!$"

CeladonCity_DepartmentStore_Roof_Text_NoMoreRoomForStuff::
    .string "ThereÙ no more\n"
    .string "room for stuff!$"

CeladonCity_DepartmentStore_Roof_Text_NotThirsty::
    .string "Not thirsty!$"

