PalletTown_RivalsHouse_Text_HiBrothersAtLab::
    .string "DAISY: Hi {PLAYER}!\p"
    .string "{RIVAL} is out at\n"
    .string "GrandpaÙ lab.$"

PalletTown_RivalsHouse_Text_HeardYouBattledRival::
    .string "DAISY: Hi {PLAYER}!\n"
    .string "{RIVAL} is out at\l"
    .string "GrandpaÙ lab.$"

PalletTown_RivalsHouse_Text_ErrandForGrandpaThisWillHelp::
    .string "Grandpa asked you\n"
    .string "to run an errand?\l"
    .string "Here, this will\l"
    .string "help you!$"

PalletTown_RivalsHouse_Text_ReceivedTownMapFromDaisy::
    .string "{PLAYER} got a\n"
    .string "TOWN MAP!$"

PalletTown_RivalsHouse_Text_DontHaveSpaceForThis::
    .string "You donÑ have space\n"
    .string "for this.$"

PalletTown_RivalsHouse_Text_ExplainTownMap::
    .string "Use the TOWN MAP\n"
    .string "to find out where\l"
    .string "you are.$"

PalletTown_RivalsHouse_Text_PleaseGiveMonsRest::
    .string "POKéMON are living\n"
    .string "things! If they\l"
    .string "get tired, give\l"
    .string "them a rest.$"

PalletTown_RivalsHouse_Text_ItsBigMapOfKanto::
    .string "ItÙ a big map!\n"
    .string "This is useful!$"

PalletTown_RivalsHouse_Text_ShelvesCrammedFullOfBooks::
    .string "Crammed full of\n"
    .string "POKéMON books!$"

PalletTown_RivalsHouse_Text_LikeMeToGroomMon::
    .string "POKéMON are living things!\p"
    .string "If they get tired, give\n"
    .string "them a rest!$"

PalletTown_RivalsHouse_Text_DontNeedAnyGrooming::
    .string "You donÑ need any grooming done?\n"
    .string "Okay, weÛl just have tea.$"

PalletTown_RivalsHouse_Text_GroomWhichOne::
    .string "Which one should I groom?$"

PalletTown_RivalsHouse_Text_LookingNiceInNoTime::
    .string "DAISY: Okay, IÛl get it looking\n"
    .string "nice in no time.$"

PalletTown_RivalsHouse_Text_ThereYouGoAllDone::
    .string "{STR_VAR_1} looks dreamily content...\p"
    .string "DAISY: There you go! All done.\n"
    .string "See? DoesnÑ it look nice?\p"
    .string "Giggle...\n"
    .string "ItÙ such a cute POKéMON.$"

PalletTown_RivalsHouse_Text_CantGroomAnEgg::
    .string "Oh, sorry. I honestly canÑ\n"
    .string "groom an EGG.$"

PalletTown_RivalsHouse_Text_MayISeeFirstMon::
    .string "DAISY: Your POKéMON grow to love\n"
    .string "you if you raise them with love.\p"
    .string "For example, {PLAYER}, may I see\n"
    .string "your first POKéMON?$"

PalletTown_RivalsHouse_Text_CouldntLoveYouMore::
    .string "It couldnÑ possibly love you\n"
    .string "any more than it does now.\p"
    .string "Your POKéMON is happy beyond\n"
    .string "words.$"

PalletTown_RivalsHouse_Text_ItLooksVeryHappy::
    .string "It looks very happy.\p"
    .string "I wish {RIVAL} could see this and\n"
    .string "learn something from it.$"

PalletTown_RivalsHouse_Text_ItsQuiteFriendly::
    .string "ItÙ quite friendly with you.\n"
    .string "Keep being good to it!$"

PalletTown_RivalsHouse_Text_ItsWarmingUpToYou::
    .string "ItÙ warming up to you.\n"
    .string "Trust must be growing between you.$"

PalletTown_RivalsHouse_Text_NotFamiliarWithYouYet::
    .string "ItÙ not quite familiar with you\n"
    .string "yet.\p"
    .string "POKéMON are all quite wary when\n"
    .string "you first get them.$"

PalletTown_RivalsHouse_Text_DontLikeWayItGlaresAtYou::
    .string "{PLAYER}, I donÑ like the way it\n"
    .string "glares at you.\p"
    .string "Could you try being a little nicer\n"
    .string "to it?$"

PalletTown_RivalsHouse_Text_WhyWouldMonHateYouSoMuch::
    .string "…Um, itÙ not easy for me to say\n"
    .string "this, but…\p"
    .string "Is there some reason why your\n"
    .string "POKéMON would hate you so much?$"

PalletTown_RivalsHouse_Text_LovelyAndSweetClefairy::
    .string "“The lovely and sweet\n"
    .string "CLEFAIRY”$"

