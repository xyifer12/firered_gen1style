FuchsiaCity_Mart_Text_DontTheyHaveSafariZonePennants::
    .string "Do you have a\n"
    .string "SAFARI ZONE flag?\p"
    .string "What about cards\n"
    .string "or calendars?$"

FuchsiaCity_Mart_Text_DidYouTryXSpeed::
    .string "Did you try X\n"
    .string "SPEED? It speeds\l"
    .string "up a POKéMON in\l"
    .string "battle!$"

