SaffronCity_CopycatsHouse_1F_Text_DaughterIsSelfCentered::
    .string "My daughter is so\n"
    .string "self-centered.\l"
    .string "She only has a\l"
    .string "few friends.$"

SaffronCity_CopycatsHouse_1F_Text_DaughterLikesToMimicPeople::
    .string "My daughter likes\n"
    .string "to mimic people.\p"
    .string "Her mimicry has\n"
    .string "earned her the\l"
    .string "nickname COPYCAT\l"
    .string "around here!$"

SaffronCity_CopycatsHouse_1F_Text_Chansey::
    .string "CHANSEY: Chaan!\n"
    .string "Sii!$"

