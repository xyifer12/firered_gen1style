CeladonCity_DepartmentStore_1F_Text_WelcomeToDeptStore::
    .string "Hello! Welcome to\n"
    .string "CELADON DEPT.\l"
    .string "STORE.\p"
    .string "The board on the\n"
    .string "right describes\l"
    .string "the store layout.$"

CeladonCity_DepartmentStore_1F_Text_FloorDescriptions::
    .string "1F: SERVICE\n"
    .string "    COUNTER\p"
    .string "2F: TRAINER'S\n"
    .string "    MARKET\p"
    .string "3F: TV GAME SHOP\p"
    .string "4F: WISEMAN GIFTS\p"
    .string "5F: DRUG STORE\p"
    .string "ROOFTOP SQUARE:\n"
    .string "VENDING MACHINES$"

CeladonCity_DepartmentStore_1F_Text_ServiceCounter::
    .string "1F: SERVICE\n"
    .string "    COUNTER$"

