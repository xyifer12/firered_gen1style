Route10_Text_MarkIntro::
    .string "Wow, are you a\n"
    .string "POKéMANIAC too?\l"
    .string "Want to see my\l"
    .string "collection?$"

Route10_Text_MarkDefeat::
    .string "POKéMANIAC: Humph.\n"
    .string "Iá not angry!$"

Route10_Text_MarkPostBattle::
    .string "I have more rare\n"
    .string "POKéMON at home!$"

Route10_Text_ClarkIntro::
    .string "Ha-hahah-ah-ha!$"

Route10_Text_ClarkDefeat::
    .string "HIKER: Ha-haha!\n"
    .string "Not laughing!\l"
    .string "Ha-hay fever!\l"
    .string "Haha-ha-choo!$"

Route10_Text_ClarkPostBattle::
    .string "Haha-ha-choo!\n"
    .string "Ha-choo!\l"
    .string "Snort! Snivel!$"

Route10_Text_HermanIntro::
    .string "Hi kid, want to\n"
    .string "see my POKéMON?$"

Route10_Text_HermanDefeat::
    .string "POKéMANIAC: Oh no!\n"
    .string "My POKéMON!$"

Route10_Text_HermanPostBattle::
    .string "I donÑ like you\n"
    .string "for beating me!$"

Route10_Text_HeidiIntro::
    .string "Iße been to a\n"
    .string "POKéMON GYM a few\l"
    .string "times. But, I\l"
    .string "lost each time.$"

Route10_Text_HeidiDefeat::
    .string "JR.TRAINER♀: Ohh!\n"
    .string "Blew it again!$"

Route10_Text_HeidiPostBattle::
    .string "I noticed some\n"
    .string "POKéMANIACs\l"
    .string "prowling around.$"

Route10_Text_TrentIntro::
    .string "Ah!\n"
    .string "This mountain\l"
    .string "air is delicious!$"

Route10_Text_TrentDefeat::
    .string "HIKER: That\n"
    .string "cleared my head!$"

Route10_Text_TrentPostBattle::
    .string "I feel bloated on\n"
    .string "mountain air!$"

Route10_Text_CarolIntro::
    .string "Iá feeling a bit\n"
    .string "faint from this\l"
    .string "tough hike.$"

Route10_Text_CarolDefeat::
    .string "JR.TRAINER♂: Iá\n"
    .string "not up for it!$"

Route10_Text_CarolPostBattle::
    .string "The POKéMON here\n"
    .string "are so chunky!\p"
    .string "There should be a\n"
    .string "pink one with a\l"
    .string "floral pattern!$"

Route10_Text_RockTunnelDetourToLavender::
    .string "ROCK TUNNEL$"

Route10_Text_RockTunnel::
    .string "ROCK TUNNEL$"

Route10_Text_PowerPlant::
    .string "POWER PLANT$"

