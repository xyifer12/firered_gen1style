Route9_Text_AliciaIntro::
    .string "You have POKéMON\n"
    .string "with you!\l"
    .string "Youàe mine!$"

Route9_Text_AliciaDefeat::
    .string "JR.TRAINER♀: You\n"
    .string "deceived me!$"

Route9_Text_AliciaPostBattle::
    .string "You need light to\n"
    .string "get through that\l"
    .string "dark tunnel ahead.$"

Route9_Text_ChrisIntro::
    .string "WhoÙ that walking\n"
    .string "with those good\l"
    .string "looking POKéMON?$"

Route9_Text_ChrisDefeat::
    .string "JR.TRAINER♂: Out\n"
    .string "like a light!$"

Route9_Text_ChrisPostBattle::
    .string "Keep walking!$"

Route9_Text_DrewIntro::
    .string "Iá taking ROCK\n"
    .string "TUNNEL to go to\l"
    .string "LAVENDER...$"

Route9_Text_DrewDefeat::
    .string "JR.TRAINER♂: CanÑ\n"
    .string "measure up!$"

Route9_Text_DrewPostBattle::
    .string "Are you off to\n"
    .string "ROCK TUNNEL too?$"

Route9_Text_CaitlinIntro::
    .string "DonÑ you dare\n"
    .string "condescend me!$"

Route9_Text_CaitlinDefeat::
    .string "JR.TRAINER♀: No!\n"
    .string "Youàe too much!$"

Route9_Text_CaitlinPostBattle::
    .string "Youàe obviously\n"
    .string "talented! Good\l"
    .string "luck to you!$"

Route9_Text_JeremyIntro::
    .string "Bwahaha!\n"
    .string "Great! I was\l"
    .string "bored, eh!$"

Route9_Text_JeremyDefeat::
    .string "HIKER: Keep it\n"
    .string "coming, eh!\p"
    .string "Oh wait. Iá out\n"
    .string "of POKéMON!$"

Route9_Text_JeremyPostBattle::
    .string "You sure had guts\n"
    .string "standing up to\l"
    .string "me there, eh?$"

Route9_Text_BriceIntro::
    .string "Hahaha!\n"
    .string "ArenÑ you a\l"
    .string "little toughie!$"

Route9_Text_BriceDefeat::
    .string "HIKER: WhatÙ\n"
    .string "that?$"

Route9_Text_BricePostBattle::
    .string "Hahaha! Kids\n"
    .string "should be tough!$"

Route9_Text_BrentIntro::
    .string "I got up early\n"
    .string "every day to\l"
    .string "raise my POKéMON\l"
    .string "from cocoons!$"

Route9_Text_BrentDefeat::
    .string "BUG CATCHER: WHAT?\n"
    .string "What a total\l"
    .string "waste of time!$"

Route9_Text_BrentPostBattle::
    .string "I have to collect\n"
    .string "more than bugs to\l"
    .string "get stronger...$"

Route9_Text_AlanIntro::
    .string "Hahahaha!\n"
    .string "Come on, dude!$"

Route9_Text_AlanDefeat::
    .string "HIKER: Hahahaha!\n"
    .string "You beat me fair!$"

Route9_Text_AlanPostBattle::
    .string "Hahahaha!\n"
    .string "Us hearty guys\l"
    .string "always laugh!$"

Route9_Text_ConnerIntro::
    .string "Go, my super bug\n"
    .string "POKéMON!$"

Route9_Text_ConnerDefeat::
    .string "BUG CATCHER: My\n"
    .string "bugs...$"

Route9_Text_ConnerPostBattle::
    .string "If you donÑ like\n"
    .string "bug POKéMON, you\l"
    .string "bug me!$"

Route9_Text_RouteSign::
    .string "ROUTE 9\n"
    .string "CERULEAN CITY -\l"
    .string "ROCK TUNNEL$"

