Route11_EastEntrance_2F_Text_GiveItemfinderIfCaught30::
    .string "Hi! Remember me?\n"
    .string "Iá PROF.OAKÙ\l"
    .string "AIDE!\p"
    .string "If you caught {STR_VAR_1}\n"
    .string "kinds of POKéMON,\l"
    .string "Iá supposed to\l"
    .string "give you an\l"
    .string "ITEMFINDER!\p"
    .string "So, {PLAYER}! Have\n"
    .string "you caught at\l"
    .string "least {STR_VAR_1}\l"
    .string "kinds of POKéMON?$"

Route11_EastEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have\n"
    .string "caught {STR_VAR_3} kinds\l"
    .string "of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route11_EastEntrance_2F_Text_ReceivedItemfinderFromAide::
    .string "{PLAYER} got the\n"
    .string "{STR_VAR_2}!$"

Route11_EastEntrance_2F_Text_ExplainItemfinder::
    .string "There are items on\n"
    .string "the ground that\l"
    .string "canÑ be seen.\p"
    .string "ITEMFINDER will\n"
    .string "detect an item\l"
    .string "close to you.\p"
    .string "It canÑ pinpoint\n"
    .string "it, so you have\l"
    .string "to look yourself!$"

Route11_EastEntrance_2F_Text_BigMonAsleepOnRoad::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "A big POKéMON is\n"
    .string "asleep on a road!$"

Route11_EastEntrance_2F_Text_WhatABreathtakingView::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "ItÙ a beautiful\n"
    .string "view!$"

Route11_EastEntrance_2F_Text_RockTunnelGoodRouteToLavender::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "The only way to\n"
    .string "get from CERULEAN\l"
    .string "CITY to LAVENDER\l"
    .string "is by way of the\l"
    .string "ROCK TUNNEL.$"

