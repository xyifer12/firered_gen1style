LavenderTown_Mart_Text_SearchingForStatRaiseItems::
    .string "Iá searching for\n"
    .string "items that raise\l"
    .string "the abilities of\l"
    .string "POKéMON during a\l"
    .string "single battle.\p"
    .string "X ATTACK, X\n"
    .string "DEFEND, X SPEED\l"
    .string "and X SPECIAL are\l"
    .string "what Iá after.\p"
    .string "Do you know where\n"
    .string "I can get them?$"

LavenderTown_Mart_Text_DidYouBuyRevives::
    .string "You know REVIVE?\n"
    .string "It revives any\l"
    .string "fainted POKéMON!$"

LavenderTown_Mart_Text_TrainerDuosCanChallengeYou::
    .string "Sometimes, a TRAINER duo will\n"
    .string "challenge you with two POKéMON\l"
    .string "at the same time.\p"
    .string "If that happens, you have to send\n"
    .string "out two POKéMON to battle, too.$"

LavenderTown_Mart_Text_SoldNuggetFromMountainsFor5000::
    .string "I found a NUGGET\n"
    .string "in the mountains.\p"
    .string "I thought it was\n"
    .string "useless, but it\l"
    .string "sold for ¥5000!$"
