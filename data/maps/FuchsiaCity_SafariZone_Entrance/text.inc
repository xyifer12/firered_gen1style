FuchsiaCity_SafariZone_Entrance_Text_WelcomeToSafariZone::
    .string "Welcome to the\n"
    .string "SAFARI ZONE!$"

FuchsiaCity_SafariZone_Entrance_Text_PlaySafariGameFor500::
    .string "For just ¥500,\n"
    .string "you can catch all\l"
    .string "the POKéMON you\l"
    .string "want in the park!\p"
    .string "Would you like to\n"
    .string "join the hunt?$"

FuchsiaCity_SafariZone_Entrance_Text_ThatllBe500WeOnlyUseSpecialBalls::
    .string "ThatÛl be ¥500,\n"
    .string "please!\p"
    .string "We only use a\n"
    .string "special POKé BALL\l"
    .string "here.$"

FuchsiaCity_SafariZone_Entrance_Text_PlayerReceived30SafariBalls::
    .string "{PLAYER} received\n"
    .string "30 SAFARI BALLS!$"

FuchsiaCity_SafariZone_Entrance_Text_CallYouOnPAWhenYouRunOut::
    .string "WeÛl call you on\n"
    .string "the PA when you\l"
    .string "run out of time\l"
    .string "or SAFARI BALLS!$"

FuchsiaCity_SafariZone_Entrance_Text_OkayPleaseComeAgain::
    .string "OK! Please come\n"
    .string "again!$"

FuchsiaCity_SafariZone_Entrance_Text_OopsNotEnoughMoney::
    .string "Oops! Not enough\n"
    .string "money!$"

FuchsiaCity_SafariZone_Entrance_Text_GoingToLeaveSafariZoneEarly::
    .string "Leaving early?$"

FuchsiaCity_SafariZone_Entrance_Text_PleaseReturnSafariBalls::
    .string "Please return any\n"
    .string "SAFARI BALLS you\l"
    .string "may have left.$"

FuchsiaCity_SafariZone_Entrance_Text_GoodLuck::
    .string "Good luck!$"

FuchsiaCity_SafariZone_Entrance_Text_CatchFairShareComeAgain::
    .string "Did you get a\n"
    .string "good haul?\l"
    .string "Come again!$"

FuchsiaCity_SafariZone_Entrance_Text_FirstTimeAtSafariZone::
    .string "Hi! Is it your\n"
    .string "first time here?$"

FuchsiaCity_SafariZone_Entrance_Text_ExplainSafariZone::
    .string "SAFARI ZONE has 4\n"
    .string "zones in it.\p"
    .string "Each zone has\n"
    .string "different kinds\l"
    .string "of POKéMON. Use\l"
    .string "SAFARI BALLS to\l"
    .string "catch them!\p"
    .string "When you run out\n"
    .string "of time or SAFARI\l"
    .string "BALLS, itÙ game\l"
    .string "over for you!\p"
    .string "Before you go,\n"
    .string "open an unused\l"
    .string "POKéMON BOX so\l"
    .string "thereÙ room for\l"
    .string "new POKéMON!$"

FuchsiaCity_SafariZone_Entrance_Text_SorryYoureARegularHere::
    .string "Sorry, youàe a\n"
    .string "regular here!$"

