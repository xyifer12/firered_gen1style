FuchsiaCity_Text_DidYouTrySafariGame::
    .string "Did you try the\n"
    .string "SAFARI GAME? Some\l"
    .string "POKéMON can only\l"
    .string "be caught there.$"

FuchsiaCity_Text_SafariZoneZooInFront::
    .string "SAFARI ZONE has a\n"
    .string "zoo in front of\l"
    .string "the entrance.\p"
    .string "Out back is the\n"
    .string "SAFARI GAME for\l"
    .string "catching POKéMON.$"

FuchsiaCity_Text_WheresSara::
    .string "ERIK: WhereÙ\n"
    .string "SARA? I said IÚ\l"
    .string "meet her here.$"

FuchsiaCity_Text_ItemBallInThere::
    .string "That item ball in\n"
    .string "there is really a\l"
    .string "POKéMON.$"

FuchsiaCity_Text_CitySign::
    .string "FUCHSIA CITY\n"
    .string "Behold! ItÙ\l"
    .string "Passion Pink!$"

FuchsiaCity_Text_SafariZoneSign::
    .string "POKéMON PARADISE\n"
    .string "SAFARI ZONE$"

FuchsiaCity_Text_SafariGameSign::
    .string "SAFARI GAME\n"
    .string "POKéMON-U-CATCH!$"

FuchsiaCity_Text_WardensHomeSign::
    .string "SAFARI ZONE\n"
    .string "WARDEN'S HOME$"

FuchsiaCity_Text_SafariZoneOfficeSign::
    .string "POKéMON PARADISE!\n"
    .string "Welcome to the SAFARI ZONE!\l"
    .string "SAFARI ZONE OFFICE$"

FuchsiaCity_Text_GymSign::
    .string "FUCHSIA CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: KOGA\p"
    .string "The Poisonous\n"
    .string "Ninja Master$"

FuchsiaCity_Text_ChanseySign::
    .string "Name: CHANSEY\p"
    .string "Catching one is\n"
    .string "all up to chance.$"

FuchsiaCity_Text_VoltorbSign::
    .string "Name: VOLTORB\p"
    .string "The very image of\n"
    .string "a POKé BALL.$"

FuchsiaCity_Text_KangaskhanSign::
    .string "Name: KANGASKHAN\p"
    .string "A maternal POKéMON\n"
    .string "that raises its\l"
    .string "young in a pouch\l"
    .string "on its belly.$"

FuchsiaCity_Text_SlowpokeSign::
    .string "Name: SLOWPOKE\p"
    .string "Friendly and very\n"
    .string "slow moving.$"

FuchsiaCity_Text_LaprasSign::
    .string "Name: LAPRAS\p"
    .string "A.K.A. the king\n"
    .string "of the seas.$"

FuchsiaCity_Text_OmanyteSign::
    .string "Name: OMANYTE\p"
    .string "A POKéMON that\n"
    .string "was resurrected\l"
    .string "from a fossil.$"

FuchsiaCity_Text_KabutoSign::
    .string "Name: KABUTO\p"
    .string "A POKéMON that\n"
    .string "was resurrected\l"
    .string "from a fossil.$"

@ Unused. May have been meant to be the placeholder text for the zoo mons, similar to RBÙ "!" text
FuchsiaCity_Text_Ellipsis::
    .string "...$"

Text_SubstituteTeach::
    .string "Aww, I wish I was a KANGASKHAN\n"
    .string "baby.\p"
    .string "IÚ love to be a substitute for the\n"
    .string "baby…\p"
    .string "And snuggle in the mother\n"
    .string "KANGASKHANÙ belly pouch.\p"
    .string "But only POKéMON can use the\n"
    .string "technique SUBSTITUTE…\p"
    .string "Want me to teach SUBSTITUTE to\n"
    .string "one of your POKéMON?$"

Text_SubstituteDeclined::
    .string "Oh, really?\n"
    .string "SUBSTITUTE seems so fun…$"

Text_SubstituteWhichMon::
    .string "Which POKéMON wants to learn\n"
    .string "SUBSTITUTE?$"

Text_SubstituteTaught::
    .string "Boy, what IÚ give to crawl inside\n"
    .string "a KANGASKHAN belly pouch…$"

FuchsiaCity_Text_MyFatherIsGymLeader::
    .string "My father is the GYM LEADER of\n"
    .string "this town.\p"
    .string "Iá training to use POISON POKéMON\n"
    .string "as well as my father.$"

