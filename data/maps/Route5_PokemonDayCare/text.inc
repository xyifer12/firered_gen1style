Route5_PokemonDayCare_Text_WantMeToRaiseMon::
	.string "I run a DAYCARE.\n"
	.string "Would you like me\l"
	.string "to raise one of\l"
	.string "your POKéMON?$"

Route5_PokemonDayCare_Text_ComeAgain::
	.string "come again.$"

Route5_PokemonDayCare_Text_WhichMonShouldIRaise::
	.string "Which POKéMON\n"
	.string "should I raise?$"

Route5_PokemonDayCare_Text_ComeAnytimeYouLike::
	.string "All right then,\n"
	.string "come again.$"

Route5_PokemonDayCare_Text_LookAfterMonForAWhile::
	.string "Fine, IÛl look\n"
	.string "after {STR_VAR_1}\l"
	.string "for a while.$"

Route5_PokemonDayCare_Text_ComeSeeMeInAWhile::
	.string "Come see me in\n"
	.string "a while.$"

Route5_PokemonDayCare_Text_MonNeedsToSpendMoreTime::
	.string "Back already?\n"
	.string "Your {STR_VAR_1}\l"
	.string "needs some more\l"
	.string "time with me.$"

Route5_PokemonDayCare_Text_OweMeXForMonsReturn::
	.string "You owe me ¥{STR_VAR_2}\n"
	.string "for the return\l"
	.string "of this POKéMON.$"

Route5_PokemonDayCare_Text_ThankYouHeresMon::
	.string "Thank you! HereÙ\n"
	.string "your POKéMON!$"

Route5_PokemonDayCare_Text_PlayerGotMonBack::
	.string "{PLAYER} got\n"
	.string "{STR_VAR_1} back!$"

Route5_PokemonDayCare_Text_OnlyHaveOneMonWithYou::
	.string "You only have one\n"
	.string "POKéMON with you.$"

Route5_PokemonDayCare_Text_WhatWillYouBattleWith::
	.string "If you leave me\n"
	.string "that POKéMON, what\l"
	.string "will you battle with?$"

Route5_PokemonDayCare_Text_MonHasGrownByXLevels::
	.string "Your {STR_VAR_1}\n"
	.string "has grown a lot!\p"
	.string "By level, itÙ\n"
	.string "grown by {STR_VAR_2}!\l"
	.string "ArenÑ I great?$"

Route5_PokemonDayCare_Text_YouveGotNoRoomForIt::
	.string "You canÑ take this\n"
	.string "POKéMON back if youße\l"
	.string "got no room for it.$"

Route5_PokemonDayCare_Text_DontHaveEnoughMoney::
	.string "You donÑ have\n"
	.string "enough money.$"

Route5_PokemonDayCare_Text_WantMeToTakeALookAtYours::
    .string "きみの\n"
    .string "みてみるかね？$"

Route5_PokemonDayCare_Text_CantAcceptMonWithHM::
    .string "わるいけど　ひでんのわざを　もった\n"
    .string "ポケモンは　あずかれないなぁ$"
