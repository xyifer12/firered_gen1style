Route4_PokemonCenter_1F_Text_CanHaveSixMonsWithYou::
    .string "Iße 6 POKé BALLs\n"
    .string "set in my belt.\p"
    .string "At most, you can\n"
    .string "carry 6 POKéMON.$"

Route4_PokemonCenter_1F_Text_TeamRocketAttacksCerulean::
    .string "TEAM ROCKET\n"
    .string "attacks CERULEAN\l"
    .string "citizens...\p"
    .string "TEAM ROCKET is\n"
    .string "always in the\l"
    .string "news!$"

Route4_PokemonCenter_1F_Text_LaddieBuyMagikarpForJust500::
    .string "MAN: Hello there!\n"
    .string "Have I got a deal\l"
    .string "just for you!\p"
    .string "IÛl let you have\n"
    .string "a swell MAGIKARP\l"
    .string "for just ¥500!\l"
    .string "What do you say?$"

Route4_PokemonCenter_1F_Text_SweetieBuyMagikarpForJust500::
    .string "MAN: Hello there!\n"
    .string "Have I got a deal just for you!\p"
    .string "IÛl let you have a swell\n"
    .string "MAGIKARP for just ¥500!\p"
    .string "What do you say?$"

Route4_PokemonCenter_1F_Text_PaidOutrageouslyForMagikarp::
    .string "{PLAYER} paid an outrageous ¥500\n"
    .string "and bought the MAGIKARP...$"

Route4_PokemonCenter_1F_Text_OnlyDoingThisAsFavorToYou::
    .string "No? Iá only\n"
    .string "doing this as a\l"
    .string "favor to you!$"

Route4_PokemonCenter_1F_Text_NoRoomForMorePokemon::
    .string "ThereÙ no more room for any more\n"
    .string "POKéMON, it looks like.$"

Route4_PokemonCenter_1F_Text_YoullNeedMoreMoney::
    .string "YouÛl need more money than that!$"

Route4_PokemonCenter_1F_Text_IDontGiveRefunds::
    .string "MAN: Well, I donÑ\n"
    .string "give refunds!$"

Route4_PokemonCenter_1F_Text_ShouldStoreMonsUsingPC::
    .string "If you have too\n"
    .string "many POKéMON, you\l"
    .string "should store them\l"
    .string "via PC!$"

Route4_PokemonCenter_1F_Text_ItsANewspaper::
    .string "ItÙ a newspaper.$"

