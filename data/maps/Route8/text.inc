Route8_Text_AidanIntro::
    .string "You look good at\n"
    .string "POKéMON, but\l"
    .string "howÙ your chem?$"

Route8_Text_AidanDefeat::
    .string "SUPER NERD: Ow!\n"
    .string "Meltdown!$"

Route8_Text_AidanPostBattle::
    .string "I am better at\n"
    .string "school than this!$"

Route8_Text_StanIntro::
    .string "All right! LetÙ\n"
    .string "roll the dice!$"

Route8_Text_StanDefeat::
    .string "GAMBLER: Drat!\n"
    .string "Came up short!$"

Route8_Text_StanPostBattle::
    .string "Lady LuckÙ not\n"
    .string "with me today!$"

Route8_Text_GlennIntro::
    .string "You need strategy\n"
    .string "to win at this!$"

Route8_Text_GlennDefeat::
    .string "SUPER NERD: ItÙ\n"
    .string "not logical!$"

Route8_Text_GlennPostBattle::
    .string "Go with GRIMER\n"
    .string "first...and...\l"
    .string "and...then...$"

Route8_Text_PaigeIntro::
    .string "I like NIDORAN, so\n"
    .string "I collect them!$"

Route8_Text_PaigeDefeat::
    .string "LASS: Why? Why??$"

Route8_Text_PaigePostBattle::
    .string "When POKéMON grow\n"
    .string "up they get ugly!\l"
    .string "They shouldnÑ\l"
    .string "evolve!$"

Route8_Text_LeslieIntro::
    .string "School is fun, but\n"
    .string "so are POKéMON.$"

Route8_Text_LeslieDefeat::
    .string "SUPER NERD: IÛl\n"
    .string "stay with school.$"

Route8_Text_LesliePostBattle::
    .string "Weàe stuck here\n"
    .string "because of the\l"
    .string "gates at SAFFRON.$"

Route8_Text_AndreaIntro::
    .string "MEOWTH is so cute,\n"
    .string "meow, meow, meow!$"

Route8_Text_AndreaDefeat::
    .string "LASS: Meow!$"

Route8_Text_AndreaPostBattle::
    .string "I think PIDGEY\n"
    .string "and RATTATA\l"
    .string "are cute too!$"

Route8_Text_MeganIntro::
    .string "We must look\n"
    .string "silly standing\l"
    .string "here like this!$"

Route8_Text_MeganDefeat::
    .string "LASS: Look what\n"
    .string "you did!$"

Route8_Text_MeganPostBattle::
    .string "SAFFRONÙ gate\n"
    .string "keeper wonÑ let\l"
    .string "us through.\l"
    .string "HeÙ so mean!$"

Route8_Text_RichIntro::
    .string "Iá a rambling,\n"
    .string "gambling dude!$"

Route8_Text_RichDefeat::
    .string "GAMBLER: Missed\n"
    .string "the big score!$"

Route8_Text_RichPostBattle::
    .string "Gambling and\n"
    .string "POKéMON are like\l"
    .string "eating peanuts!\l"
    .string "Just canÑ stop!$"

Route8_Text_JuliaIntro::
    .string "WhatÙ a cute,\n"
    .string "round and fluffy\l"
    .string "POKéMON?$"

Route8_Text_JuliaDefeat::
    .string "LASS: Stop!\p"
    .string "DonÑ be so mean\n"
    .string "to my CLEFAIRY!$"

Route8_Text_JuliaPostBattle::
    .string "I heard that\n"
    .string "CLEFAIRY evolves\l"
    .string "when itÙ exposed\l"
    .string "to a MOON STONE.$"

Route8_Text_UndergroundPathSign::
    .string "UNDERGROUND PATH\n"
    .string "CELADON CITY -\l"
    .string "LAVENDER TOWN$"

Route8_Text_EliIntro::
    .string "ELI: Twin power is fantastic.\n"
    .string "Did you know?$"

Route8_Text_EliDefeat::
    .string "ELI: But…\n"
    .string "We used our twin power…$"

Route8_Text_EliPostBattle::
    .string "ELI: I caught my POKéMON with\n"
    .string "ANNE!$"

Route8_Text_EliNotEnoughMons::
    .string "ELI: We canÑ battle if you donÑ\n"
    .string "have two POKéMON.$"

Route8_Text_AnneIntro::
    .string "ANNE: WeÛl shock you with our twin\n"
    .string "power!$"

Route8_Text_AnneDefeat::
    .string "ANNE: Our twin power…$"

Route8_Text_AnnePostBattle::
    .string "ANNE: Iá raising POKéMON with\n"
    .string "ELI.$"

Route8_Text_AnneNotEnoughMons::
    .string "ANNE: Hi, hi! LetÙ battle!\n"
    .string "But bring two POKéMON.$"

Route8_Text_RicardoIntro::
    .string "My bikeÙ acting up, man.$"

Route8_Text_RicardoDefeat::
    .string "Aww, man.\n"
    .string "Iá not into this.$"

Route8_Text_RicardoPostBattle::
    .string "I got grass caught up in my\n"
    .string "spokes, man.$"

Route8_Text_JarenIntro::
    .string "Clear the way, or IÛl run you\n"
    .string "down!$"

Route8_Text_JarenDefeat::
    .string "You for real, kid?$"

Route8_Text_JarenPostBattle::
    .string "DonÑ think youàe all special and\n"
    .string "all just because of this.$"
