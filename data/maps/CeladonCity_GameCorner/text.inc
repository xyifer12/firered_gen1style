CeladonCity_GameCorner_Text_CanExchangeCoinsNextDoor::
    .string "Welcome!\p"
    .string "You can exchange\n"
    .string "your coins for\l"
    .string "fabulous prizes\l"
    .string "next door.$"

CeladonCity_GameCorner_Text_WelcomeBuySomeCoins::
    .string "Welcome to ROCKET\n"
    .string "GAME CORNER!\p"
    .string "Do you need some\n"
    .string "game coins?\p"
    .string "ItÙ ¥1000 for 50\n"
    .string "coins. Would you\l"
    .string "like some?$"

CeladonCity_GameCorner_Text_ComePlaySometime::
    .string "No? Please come\n"
    .string "play sometime!$"

CeladonCity_GameCorner_Text_SorryDontHaveCoinCase::
    .string "You donÑ have a\n"
    .string "COIN CASE!$"

CeladonCity_GameCorner_Text_CoinCaseIsFull::
    .string "Oops! Your COIN\n"
    .string "CASE is full.$"

CeladonCity_GameCorner_Text_CantAffordCoins::
    .string "You canÑ afford\n"
    .string "the COINS!$"

CeladonCity_GameCorner_Text_HereAreYour50Coins::
    .string "Thanks! Here are\n"
    .string "your 50 coins!$"

CeladonCity_GameCorner_Text_HereAreYour500Coins::
    .string "Thanks! Here are\n"
    .string "your 500 coins!$"

CeladonCity_GameCorner_Text_RumoredTeamRocketRunsThisPlace::
    .string "Keep this quiet.\p"
    .string "ItÙ rumored that\n"
    .string "this place is run\l"
    .string "by TEAM ROCKET.$"

CeladonCity_GameCorner_Text_ThinkMachinesHaveDifferentOdds::
    .string "I think these\n"
    .string "machines have\l"
    .string "different odds.$"

CeladonCity_GameCorner_Text_DoYouWantToPlay::
    .string "Kid, do you want\n"
    .string "to play?$"

CeladonCity_GameCorner_Text_Received10CoinsFromMan::
    .string "{PLAYER} received\n"
    .string "10 coins!$"

CeladonCity_GameCorner_Text_DontNeedMyCoins::
    .string "You donÑ need my\n"
    .string "coins!$"

CeladonCity_GameCorner_Text_WinsComeAndGo::
    .string "Wins seem to come\n"
    .string "and go.$"

CeladonCity_GameCorner_Text_WinOrLoseItsOnlyLuck::
    .string "Iá having a\n"
    .string "wonderful time!$"

CeladonCity_GameCorner_Text_GymGuyAdvice::
    .string "Hey!\p"
    .string "You have better\n"
    .string "things to do,\l"
    .string "champ in making!\p"
    .string "CELADON GYMÙ\n"
    .string "LEADER is ERIKA!\l"
    .string "She uses grass-\l"
    .string "type POKéMON!\p"
    .string "She might appear\n"
    .string "docile, but donÑ\l"
    .string "be fooled!$"

CeladonCity_GameCorner_Text_RareMonsForCoins::
    .string "They offer rare\n"
    .string "POKéMON that can\l"
    .string "be exchanged for\l"
    .string "your coins.\p"
    .string "But, I just canÑ\n"
    .string "seem to win!$"

CeladonCity_GameCorner_Text_SoEasyToGetHooked::
    .string "Games are scary!\n"
    .string "ItÙ so easy to\l"
    .string "get hooked!$"

CeladonCity_GameCorner_Text_WantSomeCoins::
    .string "WhatÙ up?\n"
    .string "Want some coins?$"

CeladonCity_GameCorner_Text_Received20CoinsFromNiceGuy::
    .string "{PLAYER} received\n"
    .string "10 coins!$"

CeladonCity_GameCorner_Text_YouHaveLotsOfCoins::
    .string "You have lots of\n"
    .string "coins!$"

CeladonCity_GameCorner_Text_NeedMoreCoinsForMonIWant::
    .string "Darn! I need more\n"
    .string "coins for the\l"
    .string "POKéMON I want!$"

CeladonCity_GameCorner_Text_HereAreSomeCoinsShoo::
    .string "Hey, what? Youàe\n"
    .string "throwing me off!\l"
    .string "Here are some\l"
    .string "coins, shoo!$"

CeladonCity_GameCorner_Text_Received20CoinsFromMan::
    .string "{PLAYER} received\n"
    .string "20 coins!$"

CeladonCity_GameCorner_Text_YouveGotPlentyCoins::
    .string "Youße got your\n"
    .string "own coins!$"

CeladonCity_GameCorner_Text_WatchReelsClosely::
    .string "The trick is to\n"
    .string "watch the reels\l"
    .string "closely.$"

CeladonCity_GameCorner_Text_GruntIntro::
    .string "Iá guarding this\n"
    .string "poster!\l"
    .string "Go away, or else!$"

CeladonCity_GameCorner_Text_GruntDefeat::
    .string "ROCKET: Dang!$"

CeladonCity_GameCorner_Text_GruntPostBattle::
    .string "Our hideout might\n"
    .string "be discovered! I\l"
    .string "better tell BOSS!$"

CeladonCity_GameCorner_Text_SwitchBehindPosterPushIt::
    .string "Hey!\p"
    .string "A switch behind\n"
    .string "the poster!?\l"
    .string "LetÙ push it!$"

CeladonCity_GameCorner_Text_CoinCaseIsRequired::
    .string "A COIN CASE is\n"
    .string "required!$"

CeladonCity_GameCorner_Text_DontHaveCoinCase::
    .string "Oops! Forgot the\n"
    .string "COIN CASE!$"

CeladonCity_GameCorner_Text_SlotMachineWantToPlay::
    .string "A slot machine!\n"
    .string "Want to play?$"

CeladonCity_GameCorner_Text_OutOfOrder::
    .string "OUT OF ORDER\n"
    .string "This is broken.$"

CeladonCity_GameCorner_Text_OutToLunch::
    .string "OUT TO LUNCH\n"
    .string "This is reserved.$"

CeladonCity_GameCorner_Text_SomeonesKeys::
    .string "SomeoneÙ keys!\n"
    .string "TheyÛl be back.$"

