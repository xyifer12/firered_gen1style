RockTunnel_1F_Text_LennyIntro::
    .string "This tunnel goes\n"
    .string "a long way, kid!$"

RockTunnel_1F_Text_LennyDefeat::
    .string "HIKER: Doh!\n"
    .string "You win!$"

RockTunnel_1F_Text_LennyPostBattle::
    .string "Watch for ONIX!\n"
    .string "It can put the\l"
    .string "squeeze on you!$"

RockTunnel_1F_Text_OliverIntro::
    .string "Hmm. Maybe Iá\n"
    .string "lost in here...$"

RockTunnel_1F_Text_OliverDefeat::
    .string "HIKER: Ease up!\n"
    .string "What am I doing?\l"
    .string "Which way is out?$"

RockTunnel_1F_Text_OliverPostBattle::
    .string "That sleeping\n"
    .string "POKéMON on ROUTE\l"
    .string "12 forced me to\l"
    .string "take this detour.$"

RockTunnel_1F_Text_LucasIntro::
    .string "Outsiders like\n"
    .string "you need to show\l"
    .string "me some respect!$"

RockTunnel_1F_Text_LucasDefeat::
    .string "HIKER: I give!$"

RockTunnel_1F_Text_LucasPostBattle::
    .string "Youàe talented\n"
    .string "enough to hike!$"

RockTunnel_1F_Text_AshtonIntro::
    .string "POKéMON fight!\n"
    .string "Ready, go!$"

RockTunnel_1F_Text_AshtonDefeat::
    .string "POKéMANIAC: Game\n"
    .string "over!$"

RockTunnel_1F_Text_AshtonPostBattle::
    .string "Oh well, IÛl get\n"
    .string "a ZUBAT as I go!$"

RockTunnel_1F_Text_LeahIntro::
    .string "Eek! DonÑ try \n"
    .string "anything funny in\l"
    .string "the dark!$"

RockTunnel_1F_Text_LeahDefeat::
    .string "JR.TRAINER♂: It\n"
    .string "was too dark!$"

RockTunnel_1F_Text_LeahPostBattle::
    .string "I saw a MACHOP\n"
    .string "in this tunnel!$"

RockTunnel_1F_Text_DanaIntro::
    .string "I came this far\n"
    .string "for POKéMON!$"

RockTunnel_1F_Text_DanaDefeat::
    .string "JR.TRAINER♂: Iá\n"
    .string "out of POKéMON!$"

RockTunnel_1F_Text_DanaPostBattle::
    .string "You looked cute\n"
    .string "and harmless!$"

RockTunnel_1F_Text_ArianaIntro::
    .string "You have POKéMON!\n"
    .string "LetÙ start!$"

RockTunnel_1F_Text_ArianaDefeat::
    .string "JR.TRAINER♂: You\n"
    .string "play hard!$"

RockTunnel_1F_Text_ArianaPostBattle::
    .string "Whew! Iá all\n"
    .string "sweaty now!$"

RockTunnel_1F_Text_RouteSign::
    .string "ROCK TUNNEL\n"
    .string "CERULEAN CITY -\l"
    .string "LAVENDER TOWN$"

