RocketHideout_B1F_Text_Grunt1Intro::
    .string "Who are you? How\n"
    .string "did you get here?$"

RocketHideout_B1F_Text_Grunt1Defeat::
    .string "ROCKET: Oww!\n"
    .string "Beaten!$"

RocketHideout_B1F_Text_Grunt1PostBattle::
    .string "Are you dissing\n"
    .string "TEAM ROCKET?$"

RocketHideout_B1F_Text_Grunt2Intro::
    .string "You broke into\n"
    .string "our operation?$"

RocketHideout_B1F_Text_Grunt2Defeat::
    .string "ROCKET: Burnt!$"

RocketHideout_B1F_Text_Grunt2PostBattle::
    .string "Youàe not going\n"
    .string "to get away with\l"
    .string "this, brat!$"

RocketHideout_B1F_Text_Grunt3Intro::
    .string "Intruder alert!$"

RocketHideout_B1F_Text_Grunt3Defeat::
    .string "ROCKET: I\n"
    .string "canÑ do it!$"

RocketHideout_B1F_Text_Grunt3PostBattle::
    .string "SILPH SCOPE?\n"
    .string "I donÑ know\l"
    .string "where it is!$"

RocketHideout_B1F_Text_Grunt4Intro::
    .string "Why did you come\n"
    .string "here?$"

RocketHideout_B1F_Text_Grunt4Defeat::
    .string "ROCKET: This\n"
    .string "wonÑ do!$"

RocketHideout_B1F_Text_Grunt4PostBattle::
    .string "Okay, IÛl talk!\n"
    .string "Take the elevator\l"
    .string "to see my BOSS!$"

RocketHideout_B1F_Text_Grunt5Intro::
    .string "Are you lost, you\n"
    .string "little rat?$"

RocketHideout_B1F_Text_Grunt5Defeat::
    .string "ROCKET: Why...?$"

RocketHideout_B1F_Text_Grunt5PostBattle::
    .string "Uh-oh, that fight\n"
    .string "opened the door!$"

