FuchsiaCity_House2_Text_DoYouLikeToFish::
    .string "Iá the FISHING\n"
    .string "GURUÙ older\l"
    .string "brother!\p"
    .string "I simply Looove\n"
    .string "fishing!\p"
    .string "Do you like to\n"
    .string "fish?$"

FuchsiaCity_House2_Text_LikeYourStyleTakeThis::
    .string "Grand! I like\n"
    .string "your style!\p"
    .string "Take this and\n"
    .string "fish, young one!$"

FuchsiaCity_House2_Text_ReceivedGoodRod::
    .string "{PLAYER} received a\n"
    .string "GOOD ROD!$"

FuchsiaCity_House2_Text_GoodRodCanCatchBetterMons::
    .string "Fishing is a way\n"
    .string "of life!\p"
    .string "From the seas to\n"
    .string "rivers, go out\l"
    .string "and land the big\l"
    .string "one!$"

FuchsiaCity_House2_Text_OhThatsDisappointing::
    .string "Oh... thatÙ so\n"
    .string "disappointing...$"

FuchsiaCity_House2_Text_HowAreTheFishBiting::
    .string "Hello there,\n"
    .string "{PLAYER}!\p"
    .string "How are the fish\n"
    .string "biting?$"

FuchsiaCity_House2_Text_YouHaveNoRoomForGift::
    .string "Oh no!\p"
    .string "You have no room\n"
    .string "for my gift!$"

