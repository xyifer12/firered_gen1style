SilphCo_5F_Text_RocketsInUproarAboutIntruder::
    .string "TEAM ROCKET is\n"
    .string "in an uproar over\l"
    .string "some intruder.\l"
    .string "ThatÙ you right?$"

SilphCo_5F_Text_YoureOurHeroThankYou::
    .string "TEAM ROCKET took\n"
    .string "off! Youàe our\l"
    .string "hero! Thank you!$"

SilphCo_5F_Text_Grunt1Intro::
    .string "I heard a kid was\n"
    .string "wandering around.$"

SilphCo_5F_Text_Grunt1Defeat::
    .string "ROCKET: Boom!$"

SilphCo_5F_Text_Grunt1PostBattle::
    .string "ItÙ not smart\n"
    .string "to pick a fight\l"
    .string "with TEAM ROCKET!$"

SilphCo_5F_Text_BeauIntro::
    .string "We study POKé\n"
    .string "BALL technology\l"
    .string "on this floor!$"

SilphCo_5F_Text_BeauDefeat::
    .string "SCIENTIST: Dang!\n"
    .string "Blast it!$"

SilphCo_5F_Text_BeauPostBattle::
    .string "We worked on the\n"
    .string "ultimate POKé\l"
    .string "BALL which would\l"
    .string "catch anything!$"

SilphCo_5F_Text_DaltonIntro::
    .string "Whaaat? There\n"
    .string "shouldnÑ be any\l"
    .string "children here?$"

SilphCo_5F_Text_DaltonDefeat::
    .string "JUGGLER: Oh\n"
    .string "goodness!$"

SilphCo_5F_Text_DaltonPostBattle::
    .string "Youàe only on 5F.\n"
    .string "ItÙ a long way\l"
    .string "to my BOSS!$"

SilphCo_5F_Text_Grunt2Intro::
    .string "Show TEAM ROCKET\n"
    .string "a little respect!$"

SilphCo_5F_Text_Grunt2Defeat::
    .string "ROCKET: Cough...\n"
    .string "Cough...$"

SilphCo_5F_Text_Grunt2PostBattle::
    .string "Which reminds me.\p"
    .string "KOFFING evolves\n"
    .string "into WEEZING!$"

SilphCo_5F_Text_PorygonFirstVRMon::
    .string "ItÙ a POKéMON\n"
    .string "REPORT!\p"
    .string "POKéMON LAB\n"
    .string "created PORYGON,\l"
    .string "the first virtual\l"
    .string "reality POKéMON.$"

SilphCo_5F_Text_Over350TechniquesConfirmed::
    .string "ItÙ a POKéMON\n"
    .string "REPORT!\p"
    .string "Over 160 POKéMON\n"
    .string "techniques have\l"
    .string "been confirmed.$"

SilphCo_5F_Text_SomeMonsEvolveWhenTraded::
    .string "ItÙ a POKéMON\n"
    .string "REPORT!\p"
    .string "4 POKéMON evolve\n"
    .string "only when traded\l"
    .string "by link-cable.$"

SilphCo_5F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "5F$"

SilphCo_5F_Text_RocketBossLookingForStrongMons::
    .string "Those thugs that took over our\n"
    .string "building…\p"
    .string "Their BOSS said he was looking for\n"
    .string "strong POKéMON.\p"
    .string "I hope our PRESIDENT managed to\n"
    .string "avoid trouble…$"
