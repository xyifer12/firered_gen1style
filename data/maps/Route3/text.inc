Route3_Text_TunnelFromCeruleanTiring::
    .string "Whew... I better\n"
    .string "take a rest...\l"
    .string "Groan...\p"
    .string "That tunnel from\n"
    .string "CERULEAN takes a\l"
    .string "lot out of you!$"

Route3_Text_ColtonIntro::
    .string "Hey! I met you in\n"
    .string "VIRIDIAN FOREST!$"

Route3_Text_ColtonDefeat::
    .string "BUG CATCHER: You\n"
    .string "beat me again!$"

Route3_Text_ColtonPostBattle::
    .string "There are other\n"
    .string "kinds of POKéMON\l"
    .string "than those found\l"
    .string "in the forest!$"

Route3_Text_BenIntro::
    .string "Hi! I like shorts!\n"
    .string "Theyàe comfy and\l"
    .string "easy to wear!$"

Route3_Text_BenDefeat::
    .string "YOUNGSTER: I donÑ\n"
    .string "believe it!$"

Route3_Text_BenPostBattle::
    .string "Are you storing\n"
    .string "your POKéMON on\l"
    .string "PC? Each BOX can\l"
    .string "hold 20 POKéMON!$"

Route3_Text_JaniceIntro::
    .string "You looked at me,\n"
    .string "didnÑ you?$"

Route3_Text_JaniceDefeat::
    .string "LASS: Youàe\n"
    .string "mean!$"

Route3_Text_JanicePostBattle::
    .string "Quit staring if\n"
    .string "you donÑ want to\l"
    .string "fight!$"

Route3_Text_GregIntro::
    .string "Are you a trainer?\n"
    .string "LetÙ fight!$"

Route3_Text_GregDefeat::
    .string "BUG CATCHER: If I\n"
    .string "had new POKéMON I\l"
    .string "wouldße won!$"

Route3_Text_GregPostBattle::
    .string "If a POKéMON BOX\n"
    .string "on the PC gets\l"
    .string "full, just switch\l"
    .string "to another BOX!$"

Route3_Text_SallyIntro::
    .string "That look you\n"
    .string "gave me, itÙ so\l"
    .string "intriguing!$"

Route3_Text_SallyDefeat::
    .string "LASS: Be nice!$"

Route3_Text_SallyPostBattle::
    .string "Avoid fights by\n"
    .string "not letting\l"
    .string "people see you!$"

Route3_Text_CalvinIntro::
    .string "Hey! Youàe not\n"
    .string "wearing shorts!$"

Route3_Text_CalvinDefeat::
    .string "YOUNGSTER: Lost!\n"
    .string "Lost! Lost!$"

Route3_Text_CalvinPostBattle::
    .string "I always wear\n"
    .string "shorts even in\l"
    .string "winter!$"

Route3_Text_JamesIntro::
    .string "You can fight my\n"
    .string "new POKéMON!$"

Route3_Text_JamesDefeat::
    .string "BUG CATCHER: Done\n"
    .string "like dinner!$"

Route3_Text_JamesPostBattle::
    .string "Trained POKéMON\n"
    .string "are stronger than\l"
    .string "the wild ones!$"

Route3_Text_RobinIntro::
    .string "Eek! Did you\n"
    .string "touch me?$"

Route3_Text_RobinDefeat::
    .string "LASS: ThatÙ it?$"

Route3_Text_RobinPostBattle::
    .string "ROUTE 4 is at the\n"
    .string "foot of MT.MOON.$"

Route3_Text_RouteSign::
    .string "ROUTE 3\n"
    .string "MT.MOON AHEAD$"

