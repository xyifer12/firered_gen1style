Route12_Text_MonSprawledOutInSlumber::
    .string "A sleeping POKéMON\n"
    .string "blocks the way!$"

Text_SnorlaxWokeUp::
    .string "SNORLAX woke up!\p"
    .string "It attacked in a\n"
    .string "grumpy rage!$"

Text_SnorlaxReturnedToMountains::
    .string "With a big yawn\n"
    .string "SNORLAX retuned\l"
    .string "to the mountains!$"

Text_WantToUsePokeFlute::
    .string "Want to use\n"
    .string "the FLUTE?$"

Text_PlayedPokeFlute::
    .string "Played the\n"
    .string "FLUTE.$"

Route12_Text_NedIntro::
    .string "Yeah! I got a\n"
    .string "bite, here!$"

Route12_Text_NedDefeat::
    .string "FISHERMAN: Tch!\n"
    .string "Just a small fry!$"

Route12_Text_NedPostBattle::
    .string "Hang on! My lineÙ\n"
    .string "snagged!$"

Route12_Text_ChipIntro::
    .string "Be patient!\n"
    .string "Fishing is a\l"
    .string "waiting game!$"

Route12_Text_ChipDefeat::
    .string "FISHERMAN: That\n"
    .string "one got away!$"

Route12_Text_ChipPostBattle::
    .string "With a better ROD,\n"
    .string "I could catch\l"
    .string "better POKéMON!$"

Route12_Text_JustinIntro::
    .string "Have you found a\n"
    .string "MOON STONE?$"

Route12_Text_JustinDefeat::
    .string "JR.TRAINER♂: Oww!$"

Route12_Text_JustinPostBattle::
    .string "I could have made\n"
    .string "my POKéMON evolve\l"
    .string "with MOON STONE!$"

Route12_Text_LucaIntro::
    .string "Electricity is my\n"
    .string "specialty!$"

Route12_Text_LucaDefeat::
    .string "ROCKER: Unplugged!$"

Route12_Text_LucaPostBattle::
    .string "Water conducts\n"
    .string "electricity, so\l"
    .string "you should zap\l"
    .string "sea POKéMON!$"

Route12_Text_HankIntro::
    .string "The FISHING FOOL\n"
    .string "vs. POKéMON KID!$"

Route12_Text_HankDefeat::
    .string "FISHERMAN: Too\n"
    .string "much!$"

Route12_Text_HankPostBattle::
    .string "You beat me at\n"
    .string "POKéMON, but Iá\l"
    .string "good at fishing!$"

Route12_Text_ElliotIntro::
    .string "IÚ rather be\n"
    .string "working!$"

Route12_Text_ElliotDefeat::
    .string "FISHERMAN: ItÙ\n"
    .string "not easy...$"

Route12_Text_ElliotPostBattle::
    .string "ItÙ all right.\n"
    .string "Losing doesnÑ\l"
    .string "bug me any more.$"

Route12_Text_AndrewIntro::
    .string "You never know\n"
    .string "what you could\l"
    .string "catch!$"

Route12_Text_AndrewDefeat::
    .string "FISHERMAN: Lost\n"
    .string "it!$"

Route12_Text_AndrewPostBattle::
    .string "I catch MAGIKARP\n"
    .string "all the time, but\l"
    .string "theyàe so weak!$"

Route12_Text_RouteSign::
    .string "ROUTE 12 \n"
    .string "North to LAVENDER$"

Route12_Text_SportfishingArea::
    .string "SPORT FISHING AREA$"

Route12_Text_JesIntro::
    .string "JES: If I win, Iá going to\n"
    .string "propose to GIA.$"

Route12_Text_JesDefeat::
    .string "JES: Oh, please, why couldnÑ you\n"
    .string "let us win?$"

Route12_Text_JesPostBattle::
    .string "JES: Oh, GIA, forgive me,\n"
    .string "my love!$"

Route12_Text_JesNotEnoughMons::
    .string "JES: GIA and I, weÛl be\n"
    .string "together forever.\p"
    .string "We wonÑ battle unless you have\n"
    .string "two POKéMON of your own.$"

Route12_Text_GiaIntro::
    .string "GIA: Hey, JES…\p"
    .string "If we win, IÛl marry you!$"

Route12_Text_GiaDefeat::
    .string "GIA: Oh, but why?$"

Route12_Text_GiaPostBattle::
    .string "GIA: JES, you silly!\n"
    .string "You ruined this!$"

Route12_Text_GiaNotEnoughMons::
    .string "GIA: I canÑ bear to battle\n"
    .string "without my JES!\p"
    .string "DonÑ you have one more POKéMON?$"
