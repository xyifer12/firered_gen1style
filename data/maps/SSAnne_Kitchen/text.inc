SSAnne_Kitchen_Text_BusyOutOfTheWay::
    .string "You, mon petit!\n"
    .string "Weàe busy here!\l"
    .string "Out of the way!$"

SSAnne_Kitchen_Text_SawOddBerryInTrash::
    .string "I saw an odd ball\n"
    .string "in the trash.$"

SSAnne_Kitchen_Text_SoBusyImDizzy::
    .string "Iá so busy Iá\n"
    .string "getting dizzy!$"

SSAnne_Kitchen_Text_PeelSpudsEveryDay::
    .string "Hum-de-hum-de-\n"
    .string "ho...\p"
    .string "I peel spuds\n"
    .string "every day!\l"
    .string "Hum-hum...$"

SSAnne_Kitchen_Text_HearAboutSnorlaxItsAGlutton::
    .string "Did you hear about\n"
    .string "SNORLAX?\p"
    .string "All it does is\n"
    .string "eat and sleep!$"

SSAnne_Kitchen_Text_OnlyGetToPeelOnions::
    .string "Snivel...Sniff...\p"
    .string "I only get to\n"
    .string "peel onions...\l"
    .string "Snivel...$"

SSAnne_Kitchen_Text_IAmLeChefMainCourseIs::
    .string "Er-hem! Indeed I\n"
    .string "am le CHEF!\p"
    .string "Le main course is$"

SSAnne_Kitchen_Text_SalmonDuSalad::
    .string "Salmon du Salad!\p"
    .string "Les guests may\n"
    .string "gripe itÙ fish\l"
    .string "again, however!$"

SSAnne_Kitchen_Text_EelsAuBarbecue::
    .string "Eels au Barbecue!\p"
    .string "Les guests will\n"
    .string "mutiny, I fear.$"

SSAnne_Kitchen_Text_PrimeBeefsteak::
    .string "Prime Beef Steak!\p"
    .string "But, have I enough\n"
    .string "fillets du beef?$"

