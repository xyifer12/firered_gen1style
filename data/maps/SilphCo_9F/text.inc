SilphCo_9F_Text_YouShouldTakeQuickNap::
    .string "You look tired!\n"
    .string "You should take a\l"
    .string "quick nap!$"

SilphCo_9F_Text_DontGiveUp::
    .string "DonÑ give up!$"

SilphCo_9F_Text_ThankYouSoMuch::
    .string "Thank you so\n"
    .string "much!$"

SilphCo_9F_Text_Grunt1Intro::
    .string "Your POKéMON seem\n"
    .string "to adore you, kid!$"

SilphCo_9F_Text_Grunt1Defeat::
    .string "ROCKET: Ghaaah!$"

SilphCo_9F_Text_Grunt1PostBattle::
    .string "If I had started\n"
    .string "as a trainer at\l"
    .string "your age...$"

SilphCo_9F_Text_EdIntro::
    .string "Your POKéMON have\n"
    .string "weak points! I\l"
    .string "can nail them!$"

SilphCo_9F_Text_EdDefeat::
    .string "SCIENTIST: You\n"
    .string "hammered me!$"

SilphCo_9F_Text_EdPostBattle::
    .string "Exploiting weak\n"
    .string "spots does work!\l"
    .string "Think about\l"
    .string "element types!$"

SilphCo_9F_Text_Grunt2Intro::
    .string "I am one of the 4\n"
    .string "ROCKET BROTHERS!$"

SilphCo_9F_Text_Grunt2Defeat::
    .string "ROCKET: Warg!\n"
    .string "Brothers, I lost!$"

SilphCo_9F_Text_Grunt2PostBattle::
    .string "My brothers will\n"
    .string "avenge me!$"

SilphCo_9F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "9F$"

