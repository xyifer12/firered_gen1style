Route21_South_Text_JackIntro::
	.string "I caught all my\n"
	.string "POKéMON at sea!$"

Route21_South_Text_JackDefeat::
	.string "SWIMMER: Diver!!\n"
	.string "Down!!$"

Route21_South_Text_JackPostBattle::
	.string "WhereÚ you catch\n"
	.string "your POKéMON?$"

Route21_South_Text_JeromeIntro::
	.string "Right now, Iá in\n"
	.string "a triathlon meet!$"

Route21_South_Text_JeromeDefeat::
	.string "SWIMMER: Pant...\n"
	.string "pant...pant...$"

Route21_South_Text_JeromePostBattle::
	.string "Iá beat!\n"
	.string "But, I still have\l"
	.string "the bike race and\l"
	.string "marathon left!$"

Route21_South_Text_RolandIntro::
	.string "Ahh!\n"
	.string "Feel the sun\l"
	.string "and the wind!$"

Route21_South_Text_RolandDefeat::
	.string "SWIMMER: Yow!\n"
	.string "I lost!$"

Route21_South_Text_RolandPostBattle::
	.string "Iá sunburnt to a\n"
	.string "crisp!$"

Route21_South_Text_ClaudeIntro::
	.string "Hey, donÑ scare\n"
	.string "away the fish!$"

Route21_South_Text_ClaudeDefeat::
	.string "FISHERMAN: Sorry!\n"
	.string "I didnÑ mean it!$"

Route21_South_Text_ClaudePostBattle::
	.string "I was just angry\n"
	.string "that I couldnÑ\l"
	.string "catch anyhting.$"

Route21_South_Text_NolanIntro::
	.string "Keep me company\n"
	.string "Ñil I get a hit!$"

Route21_South_Text_NolanDefeat::
	.string "FISHERMAN: That\n"
	.string "burned some time.$"

Route21_South_Text_NolanPostBattle::
	.string "Oh wait! I got a\n"
	.string "bite! Yeah!$"

Route21_North_Text_LilIntro::
	.string "LIL: Huh? A battle?\n"
	.string "IAN, canÑ you do it alone?$"

Route21_North_Text_LilDefeat::
	.string "LIL: Oh, see?\n"
	.string "We lost. Happy now?$"

Route21_North_Text_LilPostBattle::
	.string "LIL: Iá tired.\n"
	.string "CanÑ we go home already?$"

Route21_North_Text_LilNotEnoughMons::
	.string "LIL: Huh? A battle?\n"
	.string "I canÑ be bothered to do it alone.\l"
	.string "Bring two POKéMON, wonÑ you?$"

Route21_North_Text_IanIntro::
	.string "IAN: My sis doesnÑ get enough\n"
	.string "exercise, so I made her come.$"

Route21_North_Text_IanDefeat::
	.string "IAN: Awww, Sis!\n"
	.string "Get it together!$"

Route21_North_Text_IanPostBattle::
	.string "IAN: Come on, Sis!\p"
	.string "Youàe not going to lose weight\n"
	.string "like this!$"

Route21_North_Text_IanNotEnoughMons::
	.string "IAN: We want a two-on-two battle.\n"
	.string "So can you bring two POKéMON?$"
