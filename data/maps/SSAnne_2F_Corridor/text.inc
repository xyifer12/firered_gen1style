SSAnne_2F_Corridor_Text_ThisShipIsLuxuryLiner::
    .string "This ship, she is\n"
    .string "a luxury liner\l"
    .string "for trainers!\p"
    .string "At every port, we\n"
    .string "hold parties with\l"
    .string "invited trainers!$"

SSAnne_2F_Corridor_Text_RivalIntro::
    .string "{RIVAL}: Bonjour!\n"
    .string "{PLAYER}!\p"
    .string "Imagine seeing\n"
    .string "you here!\p"
    .string "{PLAYER}, were you\n"
    .string "really invited?\p"
    .string "So howÙ your\n"
    .string "POKéDEX coming?\p"
    .string "I already caught\n"
    .string "40 kinds, pal!\p"
    .string "Different kinds\n"
    .string "are everywhere!\p"
    .string "Crawl around in\n"
    .string "grassy areas!$"

SSAnne_2F_Corridor_Text_RivalDefeat::
    .string "{RIVAL}: Humph!\p"
    .string "At least youàe\n"
    .string "raising your\l"
    .string "POKéMON!$"

SSAnne_2F_Corridor_Text_RivalVictory::
    .string "{PLAYER}‥！\n"
    .string "ふなよい　してるのか！\p"
    .string "もっと　からだ\n"
    .string "きたえた　ほうが　いいぜ！$"

SSAnne_2F_Corridor_Text_RivalPostBattle::
    .string "{RIVAL}: I heard\n"
    .string "there was a CUT\l"
    .string "master on board.\p"
    .string "But, he was just a\n"
    .string "seasick, old man!\p"
    .string "But, CUT itself is\n"
    .string "really useful!\p"
    .string "You should go see\n"
    .string "him! Smell ya!$"

