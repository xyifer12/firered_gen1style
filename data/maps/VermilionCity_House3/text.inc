VermilionCity_House3_Text_PidgeyFlyLetterToSaffron::
    .string "Iá getting my\n"
    .string "PIDGEY to fly a\l"
    .string "letter to SAFFRON\l"
    .string "in the north!$"

VermilionCity_House3_Text_Pidgey::
    .string "PIDGEY: Kurukkoo!$"

VermilionCity_House3_Text_DearPippiLetter::
    .string "Dear PIPPI, I hope\n"
    .string "to see you soon.\p"
    .string "I heard SAFFRON\n"
    .string "has problems with\l"
    .string "TEAM ROCKET.\p"
    .string "VERMILION appears\n"
    .string "to be safe.$"

VermilionCity_House3_Text_SendMyPidgeyToUnionRoom::
    .string "I want to exchange MAIL with all\n"
    .string "sorts of people.\p"
    .string "I send my PIDGEY to a UNION ROOM\n"
    .string "to exchange MAIL for me.$"

