SixIsland_WaterPath_House1_Text_LoveItNeedItHeracross::
    .string "Hera, hera, HERACROSS!\n"
    .string "Big and shiny, itÙ the bug boss!\l"
    .string "Love it, need it, HERACROSS!$"

SixIsland_WaterPath_House1_Text_MayIMeasureHeracross::
    .string "Eeeeek!\n"
    .string "ThatÙ a HERACROSS!\p"
    .string "Please, please, may I measure how\n"
    .string "big it is?$"

SixIsland_WaterPath_House1_Text_ItsXInchesDeserveReward::
    .string "Eeeek, itÙ {STR_VAR_2} inches!\n"
    .string "Iße never seen anything like this!\l"
    .string "You deserve a reward!$"

SixIsland_WaterPath_House1_Text_WantToSeeBiggerOne::
    .string "I want to see a much, much bigger\n"
    .string "HERACROSS than that one.\p"
    .string "Oh, how I adore them, big\n"
    .string "HERACROSS!$"

SixIsland_WaterPath_House1_Text_ItsXInchesSameAsBefore::
    .string "Huh? This {STR_VAR_2}-inch measurement…\n"
    .string "Oh, boo! ItÙ the same as before.$"

SixIsland_WaterPath_House1_Text_ItsXInchesYInchesWasBiggest::
    .string "Oh, itÙ just {STR_VAR_2} inches.\n"
    .string "The HERACROSS before was bigger.\p"
    .string "It was {STR_VAR_3} inches, the biggest\n"
    .string "HERACROSS youße brought me.$"

SixIsland_WaterPath_House1_Text_ThisWontDo::
    .string "Oh, no! This wonÑ do!\p"
    .string "HERACROSS looks much more macho\n"
    .string "and cool, and has a lovely horn!$"

SixIsland_WaterPath_House1_Text_YourBagIsFull::
    .string "Your BAG is full.\n"
    .string "My reward wonÑ fit.$"

SixIsland_WaterPath_House1_Text_BiggestHeracrossIsXInches::
    .string "The biggest HERACROSS that I have\n"
    .string "ever seen measured so far is:\p"
    .string "{STR_VAR_3} inches!$"

SixIsland_WaterPath_House1_Text_BlankChartOfSomeSort::
    .string "ItÙ a blank chart of some sort.\p"
    .string "It has spaces for writing in\n"
    .string "records of some kind.$"

