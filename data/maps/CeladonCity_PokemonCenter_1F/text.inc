CeladonCity_PokemonCenter_1F_Text_PokeFluteAwakensSleepingMons::
    .string "POKé FLUTE awakens\n"
    .string "POKéMON with a\l"
    .string "sound that only\l"
    .string "they can hear!$"

CeladonCity_PokemonCenter_1F_Text_RodeHereFromFuchsia::
    .string "I rode uphill on\n"
    .string "CYCLING ROAD from\l"
    .string "FUCHSIA!$"

CeladonCity_PokemonCenter_1F_Text_GoToCyclingRoadIfIHadBike::
    .string "If I had a BIKE,\n"
    .string "I would go to\l"
    .string "CYCLING ROAD!$"

