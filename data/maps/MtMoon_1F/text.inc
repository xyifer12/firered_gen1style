MtMoon_1F_Text_MarcosIntro::
    .string "WHOA! You shocked\n"
    .string "me! Oh, youàe\l"
    .string "just a kid!$"

MtMoon_1F_Text_MarcosDefeat::
    .string "HIKER: Wow!\n"
    .string "Shocked again!$"

MtMoon_1F_Text_MarcosPostBattle::
    .string "Kids like you\n"
    .string "shouldnÑ be\l"
    .string "here!$"

MtMoon_1F_Text_JoshIntro::
    .string "Did you come to\n"
    .string "explore too?$"

MtMoon_1F_Text_JoshDefeat::
    .string "YOUNGSTER: Losing\n"
    .string "stinks!$"

MtMoon_1F_Text_JoshPostBattle::
    .string "I came down here\n"
    .string "to show off to\l"
    .string "girls.$"

MtMoon_1F_Text_MiriamIntro::
    .string "Wow! ItÙ way\n"
    .string "bigger in here\l"
    .string "than I thought!$"

MtMoon_1F_Text_MiriamDefeat::
    .string "LASS: Oh!\n"
    .string "I lost it!$"

MtMoon_1F_Text_MiriamPostBattle::
    .string "How do you get\n"
    .string "out of here?$"

MtMoon_1F_Text_JovanIntro::
    .string "What! DonÑ sneak\n"
    .string "up on me!$"

MtMoon_1F_Text_JovanDefeat::
    .string "SUPER NERD: My\n"
    .string "POKéMON wonÑ do!$"

MtMoon_1F_Text_JovanPostBattle::
    .string "I have to find\n"
    .string "stronger POKéMON.$"

MtMoon_1F_Text_IrisIntro::
    .string "What? Iá waiting\n"
    .string "for my friends to\l"
    .string "find me here.$"

MtMoon_1F_Text_IrisDefeat::
    .string "LASS: I lost?$"

MtMoon_1F_Text_IrisPostBattle::
    .string "I heard there are\n"
    .string "some very rare\l"
    .string "fossils here.$"

MtMoon_1F_Text_KentIntro::
    .string "Suspicious men\n"
    .string "are in the cave.\l"
    .string "What about you?$"

MtMoon_1F_Text_KentDefeat::
    .string "BUG CATCHER: You\n"
    .string "got me!$"

MtMoon_1F_Text_KentPostBattle::
    .string "I saw them! Iá\n"
    .string "sure theyàe from\l"
    .string "TEAM ROCKET!$"

MtMoon_1F_Text_RobbyIntro::
    .string "Go through this\n"
    .string "cave to get to\l"
    .string "CERULEAN CITY!$"

MtMoon_1F_Text_RobbyDefeat::
    .string "BUG CATCHER: I\n"
    .string "lost.$"

MtMoon_1F_Text_RobbyPostBattle::
    .string "ZUBAT is tough!\n"
    .string "But, it can be\l"
    .string "useful if you\l"
    .string "catch one.$"

MtMoon_1F_Text_ZubatIsABloodsucker::
    .string "Beware! ZUBAT is\n"
    .string "a blood sucker!$"

MtMoon_1F_Text_BrockHelpsExcavateFossils::
    .string "Hi, Iá excavating for fossils here\n"
    .string "under MT. MOON.\p"
    .string "Sometimes, BROCK of PEWTER GYM\n"
    .string "lends me a hand.$"

