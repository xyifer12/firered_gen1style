IndigoPlateau_PokemonCenter_1F_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"

    .string "At POKéMON LEAGUE,\n"
    .string "you have to face\l"
    .string "the ELITE FOUR in\l"
    .string "succession.\p"

    .string "If you lose, you\n"
    .string "have to start all\l"
    .string "over again! This\l"
    .string "is it! Go for it!$"

IndigoPlateau_PokemonCenter_1F_Text_FaceEliteFourGoodLuck::
    .string "From here on, you\n"
    .string "face the ELITE\l"
    .string "FOUR one by one!\p"

    .string "If you win, a\n"
    .string "door opens to the\l"
    .string "next TRAINER!\l"
    .string "Good luck!$"

IndigoPlateau_PokemonCenter_1F_Text_LoreleiIsAbsentClosedForTimeBeing::
    .string "I am so sorry, but youße wasted\n"
    .string "your time coming here.\p"

    .string "LORELEI of the ELITE FOUR is\n"
    .string "absent, you see.\p"

    .string "As a result, the POKéMON LEAGUE is\n"
    .string "closed for the time being.$"

IndigoPlateau_PokemonCenter_1F_Text_AgathaWhuppedUs::
    .string "AGATHAÙ GHOST-type POKéMON are\n"
    .string "horrifically terrifying in toughness.\p"

    .string "I took my FIGHTING-type POKéMON\n"
    .string "and raised them to the max.\p"

    .string "I went at AGATHA feeling pretty\n"
    .string "confident, but she whupped us.\p"

    .string "That old ladyÙ also got a really\n"
    .string "short fuse, too.\p"

    .string "It doesnÑ take anything to get\n"
    .string "that scary lady hollering.$"

IndigoPlateau_PokemonCenter_1F_Text_LancesCousinGymLeaderFarAway::
    .string "Maybe becoming an ELITE FOUR\n"
    .string "member is in the blood.\p"
    
    .string "From what Iße heard, LANCE has\n"
    .string "a cousin whoÙ a GYM LEADER\l"
    .string "somewhere far away.$"

