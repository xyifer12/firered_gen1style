ThreeIsland_DunsparceTunnel_Text_ProspectingForGold::
    .string "Hey, there!\n"
    .string "HowÙ it going?\p"
    .string "What am I doing here, you ask?\n"
    .string "Why, Iá prospecting for gold!\p"
    .string "When I strike it rich, Iá buying\n"
    .string "a house in KANTO.$"

ThreeIsland_DunsparceTunnel_Text_StruckGoldThisIsForYou::
    .string "Hey, there! HowÙ it going?\n"
    .string "Did you hear? Iße struck gold!\p"
    .string "You remembered that I was\n"
    .string "prospecting, didnÑ you?\p"
    .string "You canÑ begin to imagine just\n"
    .string "how happy I am.\p"
    .string "So, IÛl show you!\n"
    .string "This is for you!$"

ThreeIsland_DunsparceTunnel_Text_ThatsANugget::
    .string "ThatÙ a NUGGET!\p"
    .string "I canÑ give you any nuggets of\n"
    .string "wisdom, so thatÛl have to do!$"

ThreeIsland_DunsparceTunnel_Text_WhoopsBagCrammedFull::
    .string "Whoops, your BAG is crammed full!\n"
    .string "IÛl give this to you later.$"

