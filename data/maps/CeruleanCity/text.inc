CeruleanCity_Text_RivalIntro::
    .string "{RIVAL}: Yo!\n"
    .string "{PLAYER}!\p"
    
    .string "Youàe still\n"
    .string "struggling along\l"
    .string "back here?\p"

    .string "Iá doing great!\n"
    .string "I caught a bunch\l"
    .string "of strong and\l"
    .string "smart POKéMON!\p"

    .string "Here, let me see\n"
    .string "what you caught,\l"
    .string "{PLAYER}!$"

CeruleanCity_Text_RivalDefeat::
    .string "{RIVAL}: Hey!\n"
    .string "Take it easy!\l"
    .string "You won already!$"

CeruleanCity_Text_RivalPostBattle::
    .string "{RIVAL}: Hey,\n"
    .string "guess what?\p"

    .string "I went to BILLÙ\n"
    .string "and got him to\l"
    .string "show me his rare\l"
    .string "POKéMON!\p"

    .string "That added a lot\n"
    .string "of pages to my\l"
    .string "POKéDEX!\p"

    .string "After all, BILLÙ\n"
    .string "world famous as a\l"
    .string "POKéMANIAC!\p"

    .string "He invented the\n"
    .string "POKéMON Storage\l"
    .string "System on PC!\p"

    .string "Since youàe using\n"
    .string "his system, go\l"
    .string "thank him!\p"

    .string "Well, I better\n"
    .string "get rolling!\l"
    .string "Smell ya later!$"

CeruleanCity_Text_OhRightLittlePresentAsFavor::
    .string "Oh, yeah, right.\p"

    .string "I feel sorry for you. No, really.\n"
    .string "Youàe always plodding behind me.\p"

    .string "So here, IÛl give you a little\n"
    .string "present as a favor.$"

CeruleanCity_Text_ExplainFameCheckerSmellYa::
    .string "A chatty gossip like you…\n"
    .string "That thingÙ perfect.\p"

    .string "I donÑ need it because I donÑ\n"
    .string "give a hoot about others.\p"

    .string "All right, this time I really am\n"
    .string "gone. Smell ya!$"

@ RB translation: "Heh! Youàe no match for my genius!"
CeruleanCity_Text_RivalVictory::
    .string "なんたって！\n"
    .string "おれは　てんさい　だからよ！$"

CeruleanCity_Text_GruntIntro::
    .string "Hey! Stay out!\n"
    .string "ItÙ not your\l"
    .string "yard! Huh? me?\p"

    .string "Iá an innocent\n"
    .string "bystander! DonÑ\l"
    .string "you believe me?{PLAY_BGM}{MUS_ENCOUNTER_ROCKET}$"

CeruleanCity_Text_GruntDefeat::
    .string "ROCKET: Stop!\n"
    .string "I give up! IÛl\l"
    .string "leave quietly!$"

CeruleanCity_Text_OkayIllReturnStolenTM::
    .string "OK! IÛl return\n"
    .string "the TM I stole!$"

CeruleanCity_Text_RecoveredTM28FromGrunt::
    .string "{PLAYER} recovered\n"
    .string "TM28!$"

CeruleanCity_Text_BetterGetMovingBye::
    .string "I better get\n"
    .string "moving! Bye!$"

CeruleanCity_Text_MakeRoomForThisCantRun::
    .string "Make room for\n"
    .string "this!\p"

    .string "I canÑ run until\n"
    .string "I give it to you!$"

CeruleanCity_Text_TrainerLifeIsToughIsntIt::
    .string "Youàe a trainer\n"
    .string "too? Collecting,\l"
    .string "fighting, itÙ a\l"
    .string "tough life.$"

CeruleanCity_Text_YouCanCutDownSmallTrees::
    .string "Did you know that you can CUT down\n"
    .string "small trees?\p"

    .string "Even that small tree in front of\n"
    .string "the shop can be CUT down.\p"

    .string "I think thereÙ a way around it,\n"
    .string "though.$"

CeruleanCity_Text_IfSlowbroWasntThereCouldCutTree::
    .string "That bush in\n"
    .string "front of the shop\l"
    .string "is in the way.\p"

    .string "There might be a\n"
    .string "way around.$"

CeruleanCity_Text_PokemonEncyclopediaAmusing::
    .string "Youàe making an\n"
    .string "encyclopedia on\l"
    .string "POKéMON? That\l"
    .string "sounds amusing.$"

CeruleanCity_Text_PeopleHereWereRobbed::
    .string "The people here\n"
    .string "were robbed.\p"

    .string "ItÙ obvious that\n"
    .string "TEAM ROCKET is\l"
    .string "behind this most\l"
    .string "heinous crime!\p"

    .string "Even our POLICE\n"
    .string "FORCE has trouble\l"
    .string "with the ROCKETs!$"

CeruleanCity_Text_SlowbroUseSonicboom::
    .string "Okay! SLOWBRO!\n"
    .string "Use SONICBOOM!$"

CeruleanCity_Text_SlowbroPayAttention::
    .string "Come on, SLOWBRO\n"
    .string "pay attention!$"

CeruleanCity_Text_SlowbroPunch::
    .string "SLOWBRO, punch!$"

CeruleanCity_Text_NoYouBlewItAgain::
    .string "No! You blew it\n"
    .string "again!$"

CeruleanCity_Text_SlowbroWithdraw::
    .string "SLOWBRO, WITHDRAW!$"

CeruleanCity_Text_HardToControlMonsObedience::
    .string "No! ThatÙ wrong!\p"

    .string "ItÙ so hard to\n"
    .string "control POKéMON!\p"

    .string "Your POKéMONÙ\n"
    .string "obedience depends\l"
    .string "on your abilities\l"
    .string "as a trainer!$"

CeruleanCity_Text_SlowbroTookSnooze::
    .string "SLOWBRO took a\n"
    .string "snooze...$"

CeruleanCity_Text_SlowbroLoafingAround::
    .string "SLOWBRO is\n"
    .string "loafing around...$"

CeruleanCity_Text_SlowbroTurnedAway::
    .string "SLOWBRO turned\n"
    .string "away...$"

CeruleanCity_Text_SlowbroIgnoredOrders::
    .string "SLOWBRO\n"
    .string "ignored orders...$"

CeruleanCity_Text_WantBrightRedBicycle::
    .string "I want a bright\n"
    .string "red bicycle!\p"

    .string "IÛl keep it at\n"
    .string "home, so it wonÑ\l"
    .string "get dirty!$"

CeruleanCity_Text_ThisIsCeruleanCave::
    .string "This is CERULEAN\n"
    .string "CAVE! Horribly\l"
    .string "strong POKéMON\l"
    .string "live in there!\p"

    .string "The POKéMON LEAGUE\n"
    .string "champion is the\l"
    .string "only person who\l"
    .string "is allowed in!$"

CeruleanCity_Text_CitySign::
    .string "CERULEAN CITY\n"
    .string "A Mysterious,\l"
    .string "Blue Aura\l"
    .string "Surrounds It$"

CeruleanCity_Text_TrainerTipsHeldItems::
    .string "TRAINER TIPS\p"

    .string "Pressing B Button\n"
    .string "during evolution\l"
    .string "cancels the whole\l"
    .string "process.$"

CeruleanCity_Text_BikeShopSign::
    .string "Grass and caves\n"
    .string "handled easily!\l"
    .string "BIKE SHOP$"

CeruleanCity_Text_GymSign::
    .string "CERULEAN CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: MISTY\p"

    .string "The Tomboyish\n"
    .string "Mermaid!$"

