SaffronCity_CopycatsHouse_2F_Text_MimickingFavoriteHobby::
    .string "{PLAYER}: Hi! Do\n"
    .string "you like POKéMON?\p"

    .string "{PLAYER}: Uh no, I\n"
    .string "just asked you.\p"

    .string "{PLAYER}: Huh?\n"
    .string "Youàe strange!\p"

    .string "COPYCAT: Hmm?\n"
    .string "Quit mimicking?\p"

    .string "But, thatÙ my\n"
    .string "favorite hobby!$"

SaffronCity_CopycatsHouse_2F_Text_TakeTM31::
    .string "Oh wow!\n"
    .string "A POKé DOLL!\p"

    .string "For me?\n"
    .string "Thank you!\p"

    .string "You can have\n"
    .string "this, then!$"

SaffronCity_CopycatsHouse_2F_Text_TM31_Contains::
    .string "{PLAYER}: Hi!\n"
    .string "Thanks for TM31!\p"

    .string "{PLAYER}: Pardon?\p"

    .string "{PLAYER}: Is it\n"
    .string "that fun to mimic\l"
    .string "my every move?\p"
    
    .string "COPYCAT: You bet!\n"
    .string "ItÙ a scream!$"

SaffronCity_CopycatsHouse_2F_Text_TooMuchInBag::
    .string "にもつが　いっぱいね！$"

SaffronCity_CopycatsHouse_2F_Text_MimickingFavoriteHobbyMaleJP::
    .string "{PLAYER}“やあ！　こんにちは！\n"
    .string "きみ　ポケモン　すきかい？\p"
    .string "{PLAYER}“ぼく　じゃなくって\n"
    .string "きみに　きいてるんだ　けど\p"
    .string "{PLAYER}“‥‥　えー　なんだよ！\n"
    .string "ヘンな　やつ　だなあ！\p"
    .string "モノマネむすめ“‥‥　なに？\n"
    .string "ひとの　マネ　すんなって？\p"
    .string "だって　あたし　\n"
    .string "ものまね　しゅみ　なんだ　もん！”$"

SaffronCity_CopycatsHouse_2F_Text_MimickingFavoriteHobbyFemaleJP::
    .string "{PLAYER}“こんにちは！\n"
    .string "ポケモン　すきなのね？\p"
    .string "{PLAYER}“わたし　じゃなくって\n"
    .string "あなたに　きいてるんだ　けど\p"
    .string "{PLAYER}“‥　えー　なんなの！\n"
    .string "おかしなこ　ねー！\p"
    .string "モノマネむすめ“‥　なに？\n"
    .string "ひとの　マネ　すんなって？\p"
    .string "だって　あたし\n"
    .string "ものまね　しゅみ　なんだ　もん！$"

SaffronCity_CopycatsHouse_2F_Text_Doduo::
    .string "DODUO: Giiih!\p"

    .string "MIRROR MIRROR ON\n"
    .string "THE WALL, WHO IS\l"
    .string "THE FAIREST ONE\l"
    .string "OF ALL?$"

SaffronCity_CopycatsHouse_2F_Text_RareMonOnlyDoll::
    .string "This is a rare\n"
    .string "POKéMON! Huh?\l"
    .string "ItÙ only a doll!$"

SaffronCity_CopycatsHouse_2F_Text_MarioWearingABucket::
    .string "A game with MARIO\n"
    .string "wearing a bucket\l"
    .string "on his head!$"

SaffronCity_CopycatsHouse_2F_Text_MySecrets::
    .string "...\p"

    .string "My Secrets!\p"

    .string "Skill: Mimicry!\n"
    .string "Hobby: Collecting\l"
    .string "dolls!\l"
    .string "Favorite POKéMON:\l"
    .string "CLEFAIRY!$"

SaffronCity_CopycatsHouse_2F_Text_HuhCantSee::
    .string "？　みえないぞ‥$"
