SSAnne_Deck_Text_ShipDepartingSoon::
    .string "The partyÙ over.\n"
    .string "The ship will be\l"
    .string "departing soon.$"

SSAnne_Deck_Text_ScrubbingDecksHardWork::
    .string "Scrubbing decks\n"
    .string "is hard work!$"

SSAnne_Deck_Text_FeelSeasick::
    .string "Urf. I feel ill.\p"
    .string "I stepped out to\n"
    .string "get some air.$"

SSAnne_Deck_Text_EdmondIntro::
    .string "Hey matey!\p"
    .string "LetÙ do a little\n"
    .string "jig!$"

SSAnne_Deck_Text_EdmondDefeat::
    .string "SAILOR: Youàe\n"
    .string "impressive!$"

SSAnne_Deck_Text_EdmondPostBattle::
    .string "How many kinds of\n"
    .string "POKéMON do you\l"
    .string "think there are?$"

SSAnne_Deck_Text_TrevorIntro::
    .string "Ahoy there!\n"
    .string "Are you seasick?$"

SSAnne_Deck_Text_TrevorDefeat::
    .string "SAILOR: I was\n"
    .string "just careless!$"

SSAnne_Deck_Text_TrevorPostBattle::
    .string "My pa said there\n"
    .string "are 100 kinds of\l"
    .string "POKéMON. I think\l"
    .string "there are more.$"

