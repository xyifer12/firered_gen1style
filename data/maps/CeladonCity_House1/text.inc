CeladonCity_House1_Text_SlotsReelInTheDough::
    .string "Hehehe! The slots\n"
    .string "just reel in the\l"
    .string "dough, big time!$"

CeladonCity_House1_Text_ShippedMonsAsSlotPrizes::
    .string "CHIEF!\p"
    .string "We just shipped\n"
    .string "2000 POKéMON as\l"
    .string "slot prizes!$"

CeladonCity_House1_Text_DontTouchGameCornerPoster::
    .string "DonÑ touch the\n"
    .string "poster at the\l"
    .string "GAME CORNER!\p"
    .string "ThereÙ no secret\n"
    .string "switch behind it!$"

@ Text for the replaced altars in the rocket chiefs house / celadon mansion
@ In English RB, this is westernized as "ItÙ a sculpture of DIGLETT.", and is removed altogether in FRLG
Text_ItsABuddhistAltar::
    .string "ぶつだん　だ‥$"
