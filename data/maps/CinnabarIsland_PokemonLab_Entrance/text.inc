CinnabarIsland_Gym_Text_PhotoOfBlaineAndFuji::
    .string "A photo of the\n"
    .string "LABÙ founder,\l"
    .string "DR.FUJI!$"

CinnabarIsland_PokemonLab_Entrance_Text_StudyMonsExtensively::
    .string "We study POKéMON\n"
    .string "extensively here.\p"
    .string "People often bring\n"
    .string "us rare POKéMON\l"
    .string "for examination.$"

CinnabarIsland_PokemonLab_Entrance_Text_PhotoOfLabFounderDrFuji::
    .string "A photo of the\n"
    .string "LABÙ founder,\l"
    .string "DR.FUJI!$"

CinnabarIsland_PokemonLab_Entrance_Text_MeetingRoomSign::
    .string "POKéMON LAB\n"
    .string "Meeting Room$"

CinnabarIsland_PokemonLab_Entrance_Text_RAndDRoomSign::
    .string "POKéMON LAB\n"
    .string "R-and-D Room$"

CinnabarIsland_PokemonLab_Entrance_Text_TestingRoomSign::
    .string "POKéMON LAB\n"
    .string "Testing Room$"

