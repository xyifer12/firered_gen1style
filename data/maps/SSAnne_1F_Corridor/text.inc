SSAnne_1F_Corridor_Text_LeStrongSilentType::
    .string "Bonjour!\n"
    .string "I am le waiter on\l"
    .string "this ship!\p"
    .string "I will be happy\n"
    .string "to serve you any-\l"
    .string "thing you please!\p"
    .string "Ah! Le strong\n"
    .string "silent type!$"

SSAnne_1F_Corridor_Text_PassengersAreRestless::
    .string "The passengers\n"
    .string "are restless!\p"
    .string "You might be\n"
    .string "challenged by the\l"
    .string "more bored ones!$"

