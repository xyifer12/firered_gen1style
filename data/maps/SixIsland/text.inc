SixIsland_Text_IslandSign::
    .string "SIX ISLAND\n"
    .string "Fortune Island of Aged Wisdom$"

SixIsland_Text_ThatWayToWaterPathRuinValley::
    .string "Keep going this way and youÛl get\n"
    .string "to the WATER PATH.\p"
    .string "Stay on that and youÛl get to\n"
    .string "the RUIN VALLEY.$"

SixIsland_Text_SkyAtNightIsFantastic::
    .string "When youàe this far away from the\n"
    .string "city, the sky at night is fantastic.$"

