SafariZone_North_Text_RestHouse::
    .string "REST HOUSE$"

SafariZone_North_Text_SecretHouseStillAhead::
    .string "TRAINER TIPS\p"
    .string "The SECRET HOUSE\n"
    .string "is still ahead!$"

SafariZone_North_Text_Area2::
    .string "AREA 2$"

SafariZone_North_Text_ZigzagThroughTallGrass::
    .string "TRAINER TIPS\p"
    .string "POKéMON hide in\n"
    .string "tall grass!\p"
    .string "Zigzag through\n"
    .string "grassy areas to\l"
    .string "flush them out.$"

SafariZone_North_Text_WinFreeHMFindSecretHouse::
    .string "TRAINER TIPS\p"
    .string "Win a free HM for\n"
    .string "finding the\l"
    .string "SECRET HOUSE!$"

