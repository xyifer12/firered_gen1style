Route25_Text_JoeyIntro::
    .string "Local trainers\n"
    .string "come here to\l"
    .string "practice!$"

Route25_Text_JoeyDefeat::
    .string "YOUNGSTER: Youàe\n"
    .string "decent.$"

Route25_Text_JoeyPostBattle::
    .string "All POKéMON have\n"
    .string "weaknesses. ItÙ\l"
    .string "best to raise\l"
    .string "different kinds.$"

Route25_Text_DanIntro::
    .string "Dad took me to a\n"
    .string "great party on\l"
    .string "S.S.ANNE at\l"
    .string "VERMILION CITY!$"

Route25_Text_DanDefeat::
    .string "YOUNGSTER: Iá\n"
    .string "not mad!$"

Route25_Text_DanPostBattle::
    .string "On S.S.ANNE, I\n"
    .string "saw trainers from\l"
    .string "around the world.$"

Route25_Text_FlintIntro::
    .string "Iá a cool guy.\n"
    .string "Iße got a girl\l"
    .string "friend!$"

Route25_Text_FlintDefeat::
    .string "JR.TRAINER♂: Aww,\n"
    .string "darn...$"

Route25_Text_FlintPostBattle::
    .string "Oh well. My girl\n"
    .string "will cheer me up.$"

Route25_Text_KelseyIntro::
    .string "Hi! My boy\n"
    .string "friend is cool!$"

Route25_Text_KelseyDefeat::
    .string "LASS: I was in\n"
    .string "bad condition!$"

Route25_Text_KelseyPostBattle::
    .string "I wish my guy was\n"
    .string "as good as you!$"

Route25_Text_ChadIntro::
    .string "I knew I had to\n"
    .string "fight you!$"

Route25_Text_ChadDefeat::
    .string "YOUNGSTER: I knew\n"
    .string "IÚ lose too!$"

Route25_Text_ChadPostBattle::
    .string "If your POKéMON\n"
    .string "gets confused or\l"
    .string "falls asleep,\l"
    .string "switch it!$"

Route25_Text_HaleyIntro::
    .string "My friend has a\n"
    .string "cute POKéMON.\l"
    .string "Iá so jealous!$"

Route25_Text_HaleyDefeat::
    .string "LASS: Iá\n"
    .string "not so jealous!$"

Route25_Text_HaleyPostBattle::
    .string "You came from MT.\n"
    .string "MOON? May I have\l"
    .string "a CLEFAIRY?$"

Route25_Text_FranklinIntro::
    .string "I just got down\n"
    .string "from MT.MOON,\l"
    .string "but Iá ready!$"

Route25_Text_FranklinDefeat::
    .string "HIKER: You\n"
    .string "worked hard!$"

Route25_Text_FranklinPostBattle::
    .string "Drat!\n"
    .string "A ZUBAT bit me\l"
    .string "back in there.$"

Route25_Text_NobIntro::
    .string "Iá off to see a\n"
    .string "POKéMON collector\l"
    .string "at the cape!$"

Route25_Text_NobDefeat::
    .string "HIKER: You\n"
    .string "got me.$"

Route25_Text_NobPostBattle::
    .string "The collector has\n"
    .string "many rare kinds\l"
    .string "of POKéMON.$"

Route25_Text_WayneIntro::
    .string "Youàe going to\n"
    .string "see BILL? First,\l"
    .string "letÙ fight!$"

Route25_Text_WayneDefeat::
    .string "HIKER: Youàe\n"
    .string "something.$"

Route25_Text_WaynePostBattle::
    .string "The trail below\n"
    .string "is a shortcut to\l"
    .string "CERULEAN CITY.$"

Route25_Text_SeaCottageSign::
    .string "SEA COTTAGE\n"
    .string "BILL lives here!$"

Route25_Text_MistyHighHopesAboutThisPlace::
    .string "This cape is a famous date spot.\p"
    .string "MISTY, the GYM LEADER, has high\n"
    .string "hopes about this place.$"

Route25_Text_AreYouHereAlone::
    .string "Hello, are you here alone?\p"
    .string "If youàe out at CERULEANÙ cape…\n"
    .string "Well, it should be as a couple.$"

