SilphCo_8F_Text_WonderIfSilphIsFinished::
    .string "I wonder if SILPH\n"
    .string "is finished...$"

SilphCo_8F_Text_ThanksForSavingUs::
    .string "Thanks for saving\n"
    .string "us!$"

SilphCo_8F_Text_Grunt1Intro::
    .string "ThatÙ as far as\n"
    .string "youÛl go!$"

SilphCo_8F_Text_Grunt1Defeat::
    .string "ROCKET: Not\n"
    .string "enough grit!$"

SilphCo_8F_Text_Grunt1PostBattle::
    .string "If you donÑ turn\n"
    .string "back, IÛl call\l"
    .string "for backup!$"

SilphCo_8F_Text_ParkerIntro::
    .string "Youàe causing us\n"
    .string "problems!$"

SilphCo_8F_Text_ParkerDefeat::
    .string "SCIENTIST: Huh?\n"
    .string "I lost?$"

SilphCo_8F_Text_ParkerPostBattle::
    .string "So, what do you\n"
    .string "think of SILPH\l"
    .string "BUILDINGÙ maze?$"

SilphCo_8F_Text_Grunt2Intro::
    .string "I am one of the 4\n"
    .string "ROCKET BROTHERS!$"

SilphCo_8F_Text_Grunt2Defeat::
    .string "ROCKET: Whoo!\n"
    .string "Oh brothers!$"

SilphCo_8F_Text_Grunt2PostBattle::
    .string "IÛl leave you up\n"
    .string "to my brothers!$"

SilphCo_8F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "8F$"

SilphCo_8F_Text_ToRocketBossMonsAreTools::
    .string "TEAM ROCKETÙ BOSS is terribly\n"
    .string "cruel!\p"
    .string "To him, POKéMON are just tools to\n"
    .string "be used.\p"
    .string "What will happen if that tyrant\n"
    .string "takes over our company…$"
