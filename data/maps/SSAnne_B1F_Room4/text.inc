SSAnne_B1F_Room4_Text_LeonardIntro::
    .string "You know what they\n"
    .string "say about sailors\l"
    .string "and fighting!$"

SSAnne_B1F_Room4_Text_LeonardDefeat::
    .string "SAILOR: Right!\n"
    .string "Good fight, mate!$"

SSAnne_B1F_Room4_Text_LeonardPostBattle::
    .string "Haha! Want to be\n"
    .string "a sailor, mate?$"

SSAnne_B1F_Room4_Text_DuncanIntro::
    .string "My sailorÙ pride\n"
    .string "is at stake!$"

SSAnne_B1F_Room4_Text_DuncanDefeat::
    .string "SAIRLOR: Your\n"
    .string "spirit sank me!$"

SSAnne_B1F_Room4_Text_DuncanPostBattle::
    .string "Did you see the\n"
    .string "FISHING GURU in\l"
    .string "VERMILION CITY?$"

