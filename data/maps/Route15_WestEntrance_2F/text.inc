Route15_WestEntrance_2F_Text_GiveItemIfCaughtEnough::
    .string "Hi! Remember me?\n"
    .string "Iá PROF.OAKÙ\l"
    .string "AIDE!\p"
    .string "If you caught {STR_VAR_1}\n"
    .string "kinds of POKéMON,\l"
    .string "Iá supposed to\l"
    .string "give you an\l"
    .string "EXP.ALL!\p"
    .string "So, {PLAYER}! Have\n"
    .string "you caught at\l"
    .string "least {STR_VAR_1}\l"
    .string "kinds of POKéMON?$"

Route15_WestEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have\n"
    .string "caught {STR_VAR_3} kinds\l"
    .string "of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route15_WestEntrance_2F_Text_ReceivedItemFromAide::
    .string "{PLAYER} got the\n"
    .string "{STR_VAR_2}!$"

Route15_WestEntrance_2F_Text_ExplainExpShare::
    .string "EXP.ALL gives\n"
    .string "EXP points to all\l"
    .string "the POKéMON with\l"
    .string "you, even if they\l"
    .string "donÑ fight.\p"
    .string "It does, however\n"
    .string "reduce the amount\l"
    .string "of EXP for each\l"
    .string "POKéMON.$"

Route15_WestEntrance_2F_Text_LargeShiningBird::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "A large, shining\n"
    .string "bird is flying\l"
    .string "toward the sea.$"

Route15_WestEntrance_2F_Text_SmallIslandOnHorizon::
    .string "Looked into the\n"
    .string "binoculars.\p"
    .string "It looks like a\n"
    .string "small island!$"

