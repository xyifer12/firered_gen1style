CeladonCity_DepartmentStore_4F_Text_GettingPokeDollAsPresent::
    .string "Iá getting a\n"
    .string "POKé DOLL for my\l"
    .string "girl friend!$"

CeladonCity_DepartmentStore_4F_Text_CanRunAwayWithPokeDoll::
    .string "I heard something\n"
    .string "useful.\p"
    .string "You can run from\n"
    .string "wild POKéMON by\l"
    .string "distracting them\l"
    .string "with a POKé DOLL!$"

CeladonCity_DepartmentStore_4F_Text_FloorSign::
    .string "Express yourself\n"
    .string "with gifts!\p"
    .string "4F: WISEMAN GIFTS\p"
    .string "Evolution Special!\n"
    .string "Element STONEs on\l"
    .string "sale now!$"
