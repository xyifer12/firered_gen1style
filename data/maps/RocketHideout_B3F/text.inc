RocketHideout_B3F_Text_Grunt1Intro::
    .string "Stop meddling in\n"
    .string "TEAM ROCKETÙ\l"
    .string "affairs!$"

RocketHideout_B3F_Text_Grunt1Defeat::
    .string "ROCKET: Oof!\n"
    .string "Taken down!$"

RocketHideout_B3F_Text_Grunt1PostBattle::
    .string "SILPH SCOPE?\n"
    .string "The machine the\l"
    .string "BOSS stole. ItÙ\l"
    .string "here somewhere.$"

RocketHideout_B3F_Text_Grunt2Intro::
    .string "We got word from\n"
    .string "upstairs that you\l"
    .string "were coming!$"

RocketHideout_B3F_Text_Grunt2Defeat::
    .string "ROCKET: What?\n"
    .string "I lost? No!$"

RocketHideout_B3F_Text_Grunt2PostBattle::
    .string "Go ahead and go!\n"
    .string "But, you need the\l"
    .string "LIFT KEY to run\l"
    .string "the elevator!$"

