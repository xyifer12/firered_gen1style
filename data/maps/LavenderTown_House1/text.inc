LavenderTown_House1_Text_Cubone::
    .string "CUBONE: Kyarugoo!$"

LavenderTown_House1_Text_RocketsKilledCubonesMother::
    .string "I hate those\n"
    .string "horrible ROCKETs!\p"
    .string "That poor CUBONEÙ\n"
    .string "mother...\p"
    .string "It was killed\n"
    .string "trying to escape\l"
    .string "from TEAM ROCKET!$"

LavenderTown_House1_Text_GhostOfPokemonTowerIsGone::
    .string "The ghost of\n"
    .string "POKéMON TOWER is\l"
    .string "gone!\p"
    .string "Someone must have\n"
    .string "soothed its\l"
    .string "restless spirit!$"

