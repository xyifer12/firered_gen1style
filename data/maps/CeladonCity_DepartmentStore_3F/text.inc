CeladonCity_DepartmentStore_3F_Text_OTStandsForOriginalTrainer::
    .string "Captured POKéMON\n"
    .string "are registered\l"
    .string "with an ID No.\l"
    .string "and OT, the name\l"
    .string "of the Original\l"
    .string "Trainer that\l"
    .string "caught it!$"

CeladonCity_DepartmentStore_3F_Text_BuddyTradingKangaskhanForHaunter::
    .string "All right!\p"
    .string "My buddyÙ going\n"
    .string "to trade me his\l"
    .string "KANGASKHAN for my\l"
    .string "GRAVELER!$"

CeladonCity_DepartmentStore_3F_Text_HaunterEvolvedOnTrade::
    .string "Come on GRAVELER!\p"
    .string "I love GRAVELER!\n"
    .string "I collect them!\p"
    .string "Huh?\p"
    .string "GRAVELER turned\n"
    .string "into a different\l"
    .string "POKéMON!$"

CeladonCity_DepartmentStore_3F_Text_CanIdentifyTradeMonsByID::
    .string "You can identify\n"
    .string "POKéMON you got\l"
    .string "in trades by\l"
    .string "their ID Numbers!$"

CeladonCity_DepartmentStore_3F_Text_ItsSuperNES::
    .string "ItÙ an SNES!$"

CeladonCity_DepartmentStore_3F_Text_AnRPG::
    .string "An RPG! ThereÙ\n"
    .string "no time for that!$"

CeladonCity_DepartmentStore_3F_Text_SportsGame::
    .string "A sports game!\n"
    .string "DadÛl like that!$"

CeladonCity_DepartmentStore_3F_Text_PuzzleGame::
    .string "A puzzle game!\n"
    .string "Looks addictive!$"

CeladonCity_DepartmentStore_3F_Text_FightingGame::
    .string "A fighting game!\n"
    .string "Looks tough!$"

CeladonCity_DepartmentStore_3F_Text_TVGameShop::
    .string "3F: TV GAME SHOP$"

CeladonCity_DepartmentStore_3F_Text_RedGreenBothArePokemon::
    .string "Red and Blue!\n"
    .string "Both are POKéMON!$"

Text_CounterTeach::
    .string "Oh, hi! I finally\n"
    .string "finished POKéMON!\p"
    .string "Not done yet?\n"
    .string "This might be\l"
    .string "useful!$"

Text_CounterDeclined::
    .string "Youàe not interested? Come see\n"
    .string "me if you change your mind.$"

Text_CounterWhichMon::
    .string "Which POKéMON should I teach\n"
    .string "COUNTER to?$"

Text_CounterTaught::
    .string "Are you using that COUNTER move\n"
    .string "I taught your POKéMON?$"
