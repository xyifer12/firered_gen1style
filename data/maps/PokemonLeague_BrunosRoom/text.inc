PokemonLeague_BrunosRoom_Text_Intro::
    .string "I am BRUNO of\n"
    .string "the ELITE FOUR!\p"
    .string "Through rigorous\n"
    .string "training, people\l"
    .string "and POKéMON can\l"
    .string "become stronger!\p"
    .string "Iße weight\n"
    .string "trained with\l"
    .string "my POKéMON!\p"
    .string "{PLAYER}!\p"
    .string "We will grind you\n"
    .string "down with our\l"
    .string "superior power!\p"
    .string "Hoo hah!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_BrunosRoom_Text_RematchIntro::
    .string "I am BRUNO of\n"
    .string "the ELITE FOUR!\p"
    .string "Through rigorous\n"
    .string "training, people\l"
    .string "and POKéMON can\l"
    .string "become stronger!\p"
    .string "Iße weight\n"
    .string "trained with\l"
    .string "my POKéMON!\p"
    .string "{PLAYER}!\p"
    .string "We will grind you\n"
    .string "down with our\l"
    .string "superior power!\p"
    .string "Hoo hah!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_BrunosRoom_Text_Defeat::
    .string "BRUNO: Why?\n"
    .string "How could I lose?$"

PokemonLeague_BrunosRoom_Text_PostBattle::
    .string "My job is done!\n"
    .string "Go face your next\l"
    .string "challenge!$"

