ThreeIsland_BondBridge_Text_NikkiIntro::
    .string "The salty tang of the sea…\n"
    .string "It tickles my heart.$"

ThreeIsland_BondBridge_Text_NikkiDefeat::
    .string "…Sniff, sniff…\n"
    .string "Something smells unpleasant…$"

ThreeIsland_BondBridge_Text_NikkiPostBattle::
    .string "Perhaps that reek is your sweaty\n"
    .string "POKéMONÙ body odor…$"

ThreeIsland_BondBridge_Text_VioletIntro::
    .string "Where are you off to in such\n"
    .string "a hurry?$"

ThreeIsland_BondBridge_Text_VioletDefeat::
    .string "Youàe raising some wonderful\n"
    .string "POKéMON.$"

ThreeIsland_BondBridge_Text_VioletPostBattle::
    .string "If you keep going this way, youÛl\n"
    .string "eventually reach BERRY FOREST.$"

ThreeIsland_BondBridge_Text_AmiraIntro::
    .string "My mommy said that I canÑ swim\n"
    .string "without my float ring.$"

ThreeIsland_BondBridge_Text_AmiraDefeat::
    .string "Waaah!\n"
    .string "Waaah!$"

ThreeIsland_BondBridge_Text_AmiraPostBattle::
    .string "This year, Iá going to finally\n"
    .string "learn how to swim!$"

ThreeIsland_BondBridge_Text_AlexisIntro::
    .string "Yay, yay!\n"
    .string "POKéMON!$"

ThreeIsland_BondBridge_Text_AlexisDefeat::
    .string "What happens now?$"

ThreeIsland_BondBridge_Text_AlexisPostBattle::
    .string "Did I just win?\n"
    .string "Or did I lose?$"

ThreeIsland_BondBridge_Text_TishaIntro::
    .string "Oh, no, donÑ come here!\n"
    .string "Please, stay away from me!$"

ThreeIsland_BondBridge_Text_TishaDefeat::
    .string "Okay, youße won!\n"
    .string "Now will you please go away?$"

ThreeIsland_BondBridge_Text_TishaPostBattle::
    .string "A POKéMON bit through my \n"
    .string "swimsuit.\p"
    .string "I canÑ get out of the water!$"

ThreeIsland_BondBridge_Text_JoyIntro::
    .string "JOY: WeÛl teach you what our\n"
    .string "favorite POKéMON is!$"

ThreeIsland_BondBridge_Text_JoyDefeat::
    .string "JOY: Ohh…\n"
    .string "MEG!$"

ThreeIsland_BondBridge_Text_JoyPostBattle::
    .string "JOY: WasnÑ that fun?\n"
    .string "I hope we can battle again!$"

ThreeIsland_BondBridge_Text_JoyNotEnoughMons::
    .string "JOY: I really want to battle with\n"
    .string "MEG.\p"
    .string "One POKéMON isnÑ enough.$"

ThreeIsland_BondBridge_Text_MegIntro::
    .string "MEG: WeÛl show you our favorite\n"
    .string "POKéMON.$"

ThreeIsland_BondBridge_Text_MegDefeat::
    .string "MEG: Ohh…\n"
    .string "JOY!$"

ThreeIsland_BondBridge_Text_MegPostBattle::
    .string "MEG: JOY, wasnÑ that fun?$"

ThreeIsland_BondBridge_Text_MegNotEnoughMons::
    .string "MEG: I want to battle together\n"
    .string "with JOY.\p"
    .string "One POKéMON isnÑ enough.$"

ThreeIsland_BondBridge_Text_BerryForestAhead::
    .string "BERRY FOREST AHEAD$"

ThreeIsland_BondBridge_Text_BondBridgeSign::
    .string "BOND BRIDGE\n"
    .string "Please be quiet when crossing.\p"
    .string "BERRY FOREST AHEAD$"

