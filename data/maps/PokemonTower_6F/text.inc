PokemonTower_6F_Text_AngelicaIntro::
    .string "Give...me...\n"
    .string "blood...$"

PokemonTower_6F_Text_AngelicaDefeat::
    .string "CHANNELER: Groan!$"

PokemonTower_6F_Text_AngelicaPostBattle::
    .string "I feel anemic and\n"
    .string "weak...$"

PokemonTower_6F_Text_EmiliaIntro::
    .string "Urff... Kwaah!$"

PokemonTower_6F_Text_EmiliaDefeat::
    .string "CHANNELER: Some\n"
    .string "thing fell out!$"

PokemonTower_6F_Text_EmiliaPostBattle::
    .string "Hair didnÑ fall\n"
    .string "out! It was an\l"
    .string "evil spirit!$"

PokemonTower_6F_Text_JenniferIntro::
    .string "Ke...ke...ke...\n"
    .string "ke..ke...ke!!$"

PokemonTower_6F_Text_JenniferDefeat::
    .string "CHANNELER: Keee!$"

PokemonTower_6F_Text_JenniferPostBattle::
    .string "WhatÙ going on\n"
    .string "here?$"

PokemonTower_6F_Text_BeGoneIntruders::
    .string "Be gone...\n"
    .string "Intruders...$"

PokemonTower_6F_Text_GhostWasCubonesMother::
    .string "The GHOST was the\n"
    .string "restless soul of\l"
    .string "CUBONEÙ mother!$"

PokemonTower_6F_Text_MothersSpiritWasCalmed::
    .string "The motherÙ soul\n"
    .string "was calmed.\p"
    .string "It departed to\n"
    .string "the afterlife!$"

