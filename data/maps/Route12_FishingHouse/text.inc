Route12_FishingHouse_Text_DoYouLikeToFish::
    .string "Iá the FISHING\n"
    .string "GURUÙ brother!\p"

    .string "I simply Looove\n"
    .string "fishing!\p"

    .string "Do you like to\n"
    .string "fish?$"

Route12_FishingHouse_Text_TakeThisAndFish::
    .string "Grand! I like\n"
    .string "your style!\p"

    .string "Take this and\n"
    .string "fish, young one!$"

Route12_FishingHouse_Text_ReceivedSuperRod::
    .string "{PLAYER} received\n"
    .string "a SUPER ROD!$"

Route12_FishingHouse_Text_IfYouCatchBigMagikarpShowMe::
    .string "Fishing is a way\n"
    .string "of life!\p"

    .string "From the seas to\n"
    .string "rivers, go out\l"
    .string "and land the big\l"
    .string "one!$"

Route12_FishingHouse_Text_OhThatsDisappointing::
    .string "Oh... ThatÙ so\n"
    .string "disappointing...$"

Route12_FishingHouse_Text_TryFishingBringMeMagikarp::
    .string "Hello there,\n"
    .string "{PLAYER}!\p"

    .string "Use the SUPER ROD\n"
    .string "in any water!\l"
    .string "You can catch\l"
    .string "different kinds\l"
    .string "of POKéMON.\p"

    .string "Try fishing\n"
    .string "wherever you can!$"

Route12_FishingHouse_Text_OhMagikarpAllowMeToSee::
    .string "Oh? {PLAYER}?\n"
    .string "Why, if it isnÑ a MAGIKARP!\p"

    .string "Allow me to see it, quick!$"

Route12_FishingHouse_Text_WhoaXInchesTakeThis::
    .string "… … …Whoa!\n"
    .string "{STR_VAR_2} inches!\p"

    .string "You have a rare appreciation for\n"
    .string "the fine, poetic aspects of fishing!\p"

    .string "You must take this.\n"
    .string "I insist!$"

Route12_FishingHouse_Text_LookForwardToGreaterRecords::
    .string "IÛl look forward to seeing greater\n"
    .string "records from you!$"

Route12_FishingHouse_Text_HuhXInchesSameSizeAsLast::
    .string "Huh?\n"
    .string "{STR_VAR_2} inches?\p"

    .string "This is the same size as the one\n"
    .string "I saw before.$"

Route12_FishingHouse_Text_HmmXInchesDoesntMeasureUp::
    .string "Hmm…\n"
    .string "This one is {STR_VAR_2} inches long.\p"

    .string "It doesnÑ measure up to the\n"
    .string "{STR_VAR_3}-inch one you brought before.$"

Route12_FishingHouse_Text_DoesntLookLikeMagikarp::
    .string "Uh… That doesnÑ look much like\n"
    .string "a MAGIKARP.$"

Route12_FishingHouse_Text_NoRoomForGift::
    .string "Oh, no!\p"

    .string "I had a gift for\n"
    .string "you, but you have\l"
    .string "no room for it.$"

Route12_FishingHouse_Text_MostGiganticMagikarpXInches::
    .string "The most gigantic MAGIKARP\n"
    .string "I have ever witnessed…\p"

    .string "{STR_VAR_3} inches!$"

Route12_FishingHouse_Text_BlankChartOfSomeSort::
    .string "ItÙ a blank chart of some sort.\p"
    
    .string "It has spaces for writing in\n"
    .string "records of some kind.$"

