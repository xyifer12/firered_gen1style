SilphCo_10F_Text_GruntIntro::
    .string "Welcome to the\n"
    .string "10F! So good of\l"
    .string "you to join me!$"

SilphCo_10F_Text_GruntDefeat::
    .string "ROCKET: Iá\n"
    .string "stunned!$"

SilphCo_10F_Text_GruntPostBattle::
    .string "Nice try, but the\n"
    .string "boardroom is up\l"
    .string "one more floor!$"

SilphCo_10F_Text_TravisIntro::
    .string "Enough of your\n"
    .string "silly games!$"

SilphCo_10F_Text_TravisDefeat::
    .string "SCIENTIST: No\n"
    .string "continues left!$"

SilphCo_10F_Text_TravisPostBattle::
    .string "Are you satisfied\n"
    .string "with beating me?\l"
    .string "Then go on home!$"

SilphCo_10F_Text_WaaaImScared::
    .string "Waaaaa!\n"
    .string "Iá scared!$"

SilphCo_10F_Text_KeepMeCryingASecret::
    .string "Please keep quiet\n"
    .string "about my crying!$"

SilphCo_10F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "10F$"

