CeladonCity_Restaurant_Text_TakingBreakRightNow::
    .string "Hi!\p"
    .string "Weàe taking a\n"
    .string "break now.$"

CeladonCity_Restaurant_Text_OftenGoToDrugstore::
    .string "My POKéMON are\n"
    .string "weak, so I often\l"
    .string "have to go to the\l"
    .string "DRUG STORE.$"

CeladonCity_Restaurant_Text_PsstBasementUnderGameCorner::
    .string "Psst! ThereÙ a\n"
    .string "basement under\l"
    .string "the GAME CORNER!$"

CeladonCity_Restaurant_Text_ManLostItAllAtSlots::
    .string "Munch...\p"
    .string "The man at that\n"
    .string "table lost it all\l"
    .string "at the slots.$"

CeladonCity_Restaurant_Text_TakeThisImBusted::
    .string "Go ahead! Laugh!\p"
    .string "Iá flat out\n"
    .string "busted!\p"
    .string "No more slots for\n"
    .string "me! Iá going\l"
    .string "straight!\p"
    .string "Here! I wonÑ be\n"
    .string "needing this any-\l"
    .string "more!$"

CeladonCity_Restaurant_Text_ReceivedCoinCaseFromMan::
    .string "{PLAYER} received\n"
    .string "COIN CASE!$"

CeladonCity_Restaurant_Text_MakeRoomForThis::
    .string "Make room for\n"
    .string "this!$"

CeladonCity_Restaurant_Text_ThoughtIdWinItBack::
    .string "I always thought\n"
    .string "I was going to\l"
    .string "win it back...$"

