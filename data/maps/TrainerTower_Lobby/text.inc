@ All Trainer Tower text (Lobby and Roof) is interleaved here

TrainerTower_Lobby_Text_WelcomeToTrainerTower::
    .string "Hello!\p"
    .string "Welcome to TRAINER TOWER where\n"
    .string "TRAINERS gather from all over!$"

TrainerTower_Lobby_Text_TrainersUpToFloorNum::
    .string "TRAINERS from all over the world\n"
    .string "gather here to battle.\p"
    .string "Let me see…\p"
    .string "Right now, there are TRAINERS only\n"
    .string "up to Floor {STR_VAR_1}.$"

TrainerTower_Lobby_Text_TrainersUpEighthFloor::
    .string "TRAINERS are awaiting your\n"
    .string "challenge up to the eighth floor.$"

TrainerTower_Lobby_Text_LikeToChallengeTrainers::
    .string "Would you like to challenge the\n"
    .string "waiting TRAINERS?$"

TrainerTower_Lobby_Text_StartClockGetSetGo::
    .string "Okay, IÛl get the clock started,\n"
    .string "so give it everything you have.\p"
    .string "On your marks…\p"
    .string "Get set…\p"
    .string "Go!$"

TrainerTower_Lobby_Text_PleaseVisitUsAgain::
    .string "Please do visit us again!$"

TrainerTower_Lobby_Text_TooBadComeBackTryAgain::
    .string "That was too bad.\p"
    .string "I think you put in a tremendous\n"
    .string "effort in your battling.\p"
    .string "Please come back and try again!$"

TrainerTower_Lobby_Text_GiveItYourBest::
    .string "I hope you give it your best.$"

TrainerTower_Lobby_Text_MoveCounterHereWhenTrainersSwitch::
    .string "When the TRAINERS switch places,\n"
    .string "the movement can be hectic.\p"
    .string "To avoid the stampede, we moved\n"
    .string "the reception counter here.\p"
    .string "Iá sorry for the inconvenience.$"

TrainerTower_Roof_Text_ImOwnerBattledPerfectly::
    .string "Hello…\p"
    .string "I am the owner of this TOWER…\p"
    .string "How the sky soars above this\n"
    .string "rooftop…\p"
    .string "The caress of the winds up here…\p"
    .string "ItÙ all so perfect…\p"
    .string "The way you battled…\n"
    .string "It, too, was perfection…$"

TrainerTower_Roof_Text_ThisIsForYou::
    .string "This is for you…$"

TrainerTower_Roof_Text_DoneItInRecordTime::
    .string "Oh!\n"
    .string "Stupendous!\p"
    .string "ItÙ marvelous how youße come up\n"
    .string "here so quickly.\p"
    .string "The fact is, youße done it in\n"
    .string "record time…\p"
    .string "IÛl have your record posted at\n"
    .string "the reception counter.$"

TrainerTower_Roof_Text_TookSweetTimeGettingHere::
    .string "You seem to have taken your sweet\n"
    .string "time getting here…$"

TrainerTower_Roof_Text_IdLikeToSeeBetterTime::
    .string "What I would like to see is a\n"
    .string "better time out of you…\p"
    .string "Iá counting on you.\p"
    .string "Until then, farewell…$"

TrainerTower_Text_XMinYZSec::
    .string "{STR_VAR_1} min. {STR_VAR_2}.{STR_VAR_3} sec.$"

TrainerTower_Lobby_Text_HereAreTheResults::
    .string "せいせきひょうです$"

TrainerTower_Lobby_Text_NeedTwoMonsForDouble::
    .string "This is a two-on-two battle.\p"
    .string "You may not battle unless you have\n"
    .string "at least two POKéMON.$"

TrainerTower_Lobby_Text_ExplainTrainerTower::
    .string "Here at TRAINER TOWER, there is an\n"
    .string "event called TIME ATTACK.\p"
    .string "You will be timed on how quickly\n"
    .string "you can get from the reception\l"
    .string "counter to the OWNER on the roof.\p"
    .string "The best times will be recorded on\n"
    .string "the Time Board.\p"
    .string "Try competing with friends to see\n"
    .string "who can beat it the fastest.\p"
    .string "You will not earn any EXP. Points\n"
    .string "or money by beating TRAINERS here.$"

TrainerTower_Lobby_Text_ThanksForCompeting::
    .string "Thank you for competing!$"

TrainerTower_Lobby_Text_WonderWhatKindsOfTrainers::
    .string "Iá here to see how good I am.\p"
    .string "I wonder what kinds of TRAINERS\n"
    .string "are waiting for me?\p"
    .string "ItÙ nerve-racking!$"

TrainerTower_Lobby_Text_StairsTougherThanAnyBattle::
    .string "Gasp, gasp…\n"
    .string "Gasp…\p"
    .string "Never mind battling! These stairs…\n"
    .string "Theyàe tougher than any battle…$"

