ViridianCity_House_Text_NicknamingIsFun::
    .string "Coming up with\n"
    .string "nicknames is fun,\l"
    .string "but hard.\p"
    .string "Simple names are\n"
    .string "the easiest to\l"
    .string "remember.$"

ViridianCity_House_Text_MyDaddyLovesMonsToo::
    .string "My Daddy loves\n"
    .string "POKéMON too.$"

ViridianCity_House_Text_Speary::
    .string "SPEARY: Tetweet!$"

ViridianCity_House_Text_SpearowNameSpeary::
    .string "SPEAROW\n"
    .string "Name: SPEARY$"

