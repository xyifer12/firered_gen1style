ThreeIsland_BerryForest_Text_HelpScaryPokemon::
    .string "LOSTELLE: Whimper… Sniff…\n"
    .string "Oh! Please, help!\p"
    .string "A scary POKéMON appeared there\n"
    .string "a little while ago.\p"
    .string "It kept scaring.\n"
    .string "It made LOSTELLE scared.\p"
    .string "Iá too scared to move!\n"
    .string "But I want to go home…$"

ThreeIsland_BerryForest_Text_HereItComesAgain::
    .string "Oh! Here it comes again!\n"
    .string "No! Go away! ItÙ scaring me!\p"
    .string "Waaaaaaah!\n"
    .string "I want my daddy!$"

ThreeIsland_BerryForest_Text_ThankYouHaveThis::
    .string "Ohh! That was so scary!\n"
    .string "Thank you!\p"
    .string "LOSTELLE came to pick some\n"
    .string "BERRIES.\p"
    .string "You can have this!$"

ThreeIsland_BerryForest_Text_LetsGoHome::
    .string "WhatÙ your name?\p"
    .string "LOSTELLEÙ scared, so can I go\n"
    .string "with you to my daddyÙ house?\p"
    .string "Okay!\n"
    .string "LetÙ go home!$"

ThreeIsland_BerryForest_Text_BerryPouchIsFull::
    .string "Your BERRY POUCH is full.\n"
    .string "I guess you donÑ want this.$"

ThreeIsland_BerryForest_Text_WelcomeToBerryForest::
    .string "Welcome to the BERRY FOREST\p"
    .string "Be friendly and share BERRIES with\n"
    .string "others and POKéMON.$"

ThreeIsland_BerryForest_Text_BewareWildBerryLovingMons::
    .string "Beware of wild, BERRY-loving\n"
    .string "POKéMON!$"

