PewterCity_House2_Text_MonsLearnTechniquesAsTheyGrow::
    .string "POKéMON learn new\n"
    .string "techniques as\l"
    .string "they grow!\p"
    .string "But, some moves\n"
    .string "must be taught by\l"
    .string "the trainer!$"

PewterCity_House2_Text_MonsEasierCatchIfStatused::
    .string "POKéMON become\n"
    .string "easier to catch\l"
    .string "when they are\l"
    .string "hurt or asleep!\p"
    .string "But, itÙ not a\n"
    .string "sure thing!$"
