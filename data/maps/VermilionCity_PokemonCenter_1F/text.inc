VermilionCity_PokemonCenter_1F_Text_TrainerMonsStrongerThanWild::
    .string "Even if they are\n"
    .string "the same level,\l"
    .string "POKéMON can have\l"
    .string "very different\l"
    .string "abilities.\p"
    .string "A POKéMON raised\n"
    .string "by a trainer is\l"
    .string "stronger than one\l"
    .string "in the wild.$"

VermilionCity_PokemonCenter_1F_Text_PoisonedMonFaintedWhileWalking::
    .string "My POKéMON was\n"
    .string "poisoned! It\l"
    .string "fainted while we\l"
    .string "were walking!$"

VermilionCity_PokemonCenter_1F_Text_AllMonWeakToSpecificTypes::
    .string "It is true that a\n"
    .string "higher level\l"
    .string "POKéMON will be\l"
    .string "more powerful...\p"
    .string "But, all POKéMON\n"
    .string "will have weak\l"
    .string "points against\l"
    .string "specific types.\p"
    .string "So, there is no\n"
    .string "universally\l"
    .string "strong POKéMON.$"

VermilionCity_PokemonCenter_1F_Text_UrgeToBattleSomeoneAgain::
    .string "The urge to battle with someone\n"
    .string "youße tangled with before…\p"
    .string "Have you ever had that urge?\n"
    .string "Iá sure you have.\p"
    .string "I wanted to battle certain people\n"
    .string "again over and over, too.\p"
    .string "So, Iße been giving these away.\n"
    .string "Please, take one!$"

VermilionCity_PokemonCenter_1F_Text_UseDeviceForRematches::
    .string "Use that device and youÛl find\n"
    .string "TRAINERS looking for a rematch.\p"
    .string "You have to charge its battery to\n"
    .string "use it, though.$"

VermilionCity_PokemonCenter_1F_Text_ExplainVSSeeker::
    .string "How do you use the VS SEEKER?\n"
    .string "ThereÙ nothing to it.\p"
    .string "Use it like beep-beep-beep, and\n"
    .string "TRAINERS around you will notice.\p"
    .string "If any TRAINER wants a rematch,\n"
    .string "it will let you know immediately.\p"
    .string "Charge its battery and use it\n"
    .string "while youàe on a road.$"
