VermilionCity_Gym_Text_LtSurgeIntro::
    .string "Hey, kid! What do\n"
    .string "you think youàe\l"
    .string "doing here?\p"
    .string "You wonÑ live\n"
    .string "long in combat!\l"
    .string "ThatÙ for sure!\p"
    .string "I tell you kid,\n"
    .string "electric POKéMON\l"
    .string "saved me during\l"
    .string "the war!\p"
    .string "They zapped my\n"
    .string "enemies into\l"
    .string "paralysis!\p"
    .string "The same as IÛl\n"
    .string "do to you!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

VermilionCity_Gym_Text_LtSurgePostBattle::
    .string "A little word of\n"
    .string "advice, kid!\p"
    .string "Electricity is\n"
    .string "sure powerful!\p"
    .string "But, itÙ useless\n"
    .string "against ground-\l"
    .string "type POKéMON!$"

VermilionCity_Gym_Text_ExplainThunderBadgeTakeThis::
    .string "The THUNDERBADGE\n"
    .string "cranks up your\l"
    .string "POKéMONÙ SPEED!\p"
    .string "It also lets your\n"
    .string "POKéMON FLY any\l"
    .string "time, kid!\p"
    .string "Youàe special,\n"
    .string "kid! Take this!$"

VermilionCity_Gym_Text_ReceivedTM24FromLtSurge::
    .string "{PLAYER} received\n"
    .string "TM24!$"

VermilionCity_Gym_Text_ExplainTM24::
    .string "TM24 contains\n"
    .string "THUNDERBOLT!\p"
    .string "Teach it to an\n"
    .string "electric POKéMON!$"

VermilionCity_Gym_Text_MakeRoomInYourBag::
    .string "Yo kid, make room\n"
    .string "in your pack!$"

VermilionCity_Gym_Text_LtSurgeDefeat::
    .string "LT.SURGE: Whoa!\p"
    .string "Youàe the real\n"
    .string "deal, kid!\p"
    .string "Fine then, take\n"
    .string "the THUNDERBADGE!$"

VermilionCity_Gym_Text_TuckerIntro::
    .string "When I was in the\n"
    .string "Army, LT.SURGE\l"
    .string "was my strict CO!$"

VermilionCity_Gym_Text_TuckerDefeat::
    .string "GENTLEMAN: Stop!\n"
    .string "Youàe very good!$"

VermilionCity_Gym_Text_TuckerPostBattle::
    .string "The door wonÑ\n"
    .string "open?\p"
    .string "LT.SURGE always\n"
    .string "was cautious!$"

VermilionCity_Gym_Text_BailyIntro::
    .string "Iá a lightweight,\n"
    .string "but Iá good with\l"
    .string "electricity!$"

VermilionCity_Gym_Text_BailyDefeat::
    .string "ROCKER: Fried!$"

VermilionCity_Gym_Text_BailyPostBattle::
    .string "OK, IÛl talk!\p"
    .string "LT.SURGE said he\n"
    .string "hid door switches\l"
    .string "inside something!$"

VermilionCity_Gym_Text_DwayneIntro::
    .string "This is no place\n"
    .string "for kids!$"

VermilionCity_Gym_Text_DwayneDefeat::
    .string "SAILOR: Wow!\n"
    .string "Surprised me!$"

VermilionCity_Gym_Text_DwaynePostBattle::
    .string "LT.SURGE set up\n"
    .string "double locks!\l"
    .string "HereÙ a hint!\p"
    .string "When you open the\n"
    .string "1st lock, the 2nd\l"
    .string "lock is right\l"
    .string "next to it!$"

VermilionCity_Gym_Text_GymGuyAdvice::
    .string "Yo! Champ in\n"
    .string "making!\p"
    .string "LT.SURGE has a\n"
    .string "nickname. People\l"
    .string "refer to him as\l"
    .string "the Lightning\l"
    .string "American!\p"
    .string "HeÙ an expert on\n"
    .string "electric POKéMON!\p"
    .string "Birds and water\n"
    .string "POKéMON are at\l"
    .string "risk! Beware of\l"
    .string "paralysis too!\p"
    .string "LT.SURGE is very\n"
    .string "cautious!\p"
    .string "YouÛl have to\n"
    .string "break a code to\l"
    .string "get to him!$"

VermilionCity_Gym_Text_GymGuyPostVictory::
    .string "Whew! That match\n"
    .string "was electric!$"

VermilionCity_Gym_Text_GymStatue::
    .string "VERMILION\n"
    .string "POKéMON GYM\l"
    .string "LEADER: LT.SURGE\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

VermilionCity_Gym_Text_GymStatuePlayerWon::
    .string "VERMILION\n"
    .string "POKéMON GYM\l"
    .string "LEADER: LT.SURGE\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

VermilionCity_Gym_Text_NopeOnlyTrashHere::
    .string "Nope, thereÙ\n"
    .string "only trash here.$"

VermilionCity_Gym_Text_SwitchUnderTrashFirstLockOpened::
    .string "Hey! ThereÙ a\n"
    .string "switch under the\l"
    .string "trash!\l"
    .string "Turn it on!\p"
    .string "The 1st electric\n"
    .string "lock opened!$"

VermilionCity_Gym_Text_AnotherSwitchInTrash::
    .string "ガサゴソ‥！\n"
    .string "おっと！　ゴミばこの　そこに\l"
    .string "また　スイッチが　あった！\l"
    .string "おして　みよう！　‥　ポチッとな$"

VermilionCity_Gym_Text_SecondLockOpened::
    .string "The 2nd electric\n"
    .string "lock opened!\p"
    .string "The motorized door\n"
    .string "opened!$"

VermilionCity_Gym_Text_OnlyTrashLocksWereReset::
    .string "Nope, thereÙ\n"
    .string "only trash here.\l"
    .string "Hey! The electric\l"
    .string "locks were reset!$"

