ViridianCity_Mart_Text_YouCameFromPallet::
    .string "Hey! You came from\n"
    .string "PALLET TOWN?$"

ViridianCity_Mart_Text_TakeThisToProfOak::
    .string "You know PROF.\n"
    .string "OAK, right?\p"
    
    .string "His order came in.\n"
    .string "Will you take it\l"
    .string "to him?$"

ViridianCity_Mart_Text_ReceivedOaksParcelFromClerk::
    .string "{PLAYER} got\n"
    .string "OAK'S PARCEL!$"

ViridianCity_Mart_Text_SayHiToOakForMe::
    .string "Okay! Say hi to\n"
    .string "PROF.OAK for me!$"

ViridianCity_Mart_Text_ShopDoesGoodBusinessInAntidotes::
    .string "This shop sells\n"
    .string "many ANTIDOTEs.$"

ViridianCity_Mart_Text_GotToBuySomePotions::
    .string "No! POTIONs are\n"
    .string "all sold out.$"

