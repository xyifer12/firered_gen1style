RocketHideout_B2F_Text_GruntIntro::
    .string "BOSS said you can\n"
    .string "see GHOSTs with\l"
    .string "the SILPH SCOPE!$"

RocketHideout_B2F_Text_GruntDefeat::
    .string "ROCKET: I\n"
    .string "surrender!$"

RocketHideout_B2F_Text_GruntPostBattle::
    .string "The TEAM ROCKET\n"
    .string "HQ has 4 basement\l"
    .string "floors. Can you\l"
    .string "reach the BOSS?$"

