Route10_PokemonCenter_1F_Text_EveryTypeStrongerThanOthers::
    .string "The element types\n"
    .string "of POKéMON make\l"
    .string "them stronger\l"
    .string "than some types\l"
    .string "and weaker than\l"
    .string "others!$"

Route10_PokemonCenter_1F_Text_NuggetUselessSoldFor5000::
    .string "I sold a useless\n"
    .string "NUGGET for ¥5000!$"

Route10_PokemonCenter_1F_Text_HeardGhostsHauntLavender::
    .string "I heard that\n"
    .string "GHOSTs haunt\l"
    .string "LAVENDER TOWN!$"

Route10_PokemonCenter_1F_Text_GiveEverstoneIfCaught20Mons::
    .string "Oh… {PLAYER}!\n"
    .string "Iße been looking for you!\p"
    .string "ItÙ me, one of the ever-present\n"
    .string "AIDES to PROF. OAK.\p"
    .string "If your POKéDEX has complete data\n"
    .string "on twenty species, Iá supposed to\p"
    .string "give you a reward from PROF. OAK.\l"
    .string "He entrusted me with this\n"
    .string "EVERSTONE.\p"
    .string "So, {PLAYER}, let me ask you.\n"
    .string "Have you gathered data on at least\p"
    .string "twenty kinds of POKéMON?$"

Route10_PokemonCenter_1F_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route10_PokemonCenter_1F_Text_ReceivedEverstoneFromAide::
    .string "{PLAYER} got the EVERSTONE$"

Route10_PokemonCenter_1F_Text_ExplainEverstone::
    .string "Making POKéMON evolve certainly\n"
    .string "can add to the POKéDEX.\p"
    .string "However, at times, you may not\n"
    .string "want a certain POKéMON to evolve.\p"
    .string "In that case, give the EVERSTONE\n"
    .string "to that POKéMON.\p"
    .string "It will prevent evolution according\n"
    .string "to the PROFESSOR.$"

