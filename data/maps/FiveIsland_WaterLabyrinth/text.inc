FiveIsland_WaterLabyrinth_Text_LetMeTakeLookAtMons::
    .string "You travel all over the place,\n"
    .string "donÑ you?\p"
    .string "Youàe not driving your POKéMON\n"
    .string "too harshly, are you?\p"
    .string "Let me take a look.\n"
    .string "… … …   … … …$"

FiveIsland_WaterLabyrinth_Text_HmmISeeIsee::
    .string "Ah, hmm…\n"
    .string "I see, I see…$"

FiveIsland_WaterLabyrinth_Text_TreatMonRightHaveThis::
    .string "Oh, impressive.\n"
    .string "You treat your POKéMON right.\p"
    .string "I think you can be entrusted with\n"
    .string "this.\p"
    .string "Please, IÚ like you to have this.$"

FiveIsland_WaterLabyrinth_Text_ReceivedEggFromMan::
    .string "{PLAYER} received an EGG!$"

FiveIsland_WaterLabyrinth_Text_DontHaveSpaceInYourParty::
    .string "…Unfortunately, you donÑ have\n"
    .string "space for this in your party.\p"
    .string "YouÚ better come back for it\n"
    .string "another time.$"

@ Unused
FiveIsland_WaterLabyrinth_Text_YouveComeBackForTheEgg::
    .string "やあ\n"
    .string "タマゴを　もらいに\l"
    .string "きて　くれたんだね$"

FiveIsland_WaterLabyrinth_Text_GladIMetSomeoneLikeYou::
    .string "I received that EGG while I was\n"
    .string "traveling.\p"
    .string "Iá glad I met someone like you.$"

FiveIsland_WaterLabyrinth_Text_CuteMonRemindsMeOfDaisy::
    .string "Oh, hello.\n"
    .string "ThatÙ a cute {STR_VAR_2}.\p"
    .string "Seeing it reminds me of a sweet \n"
    .string "little girl I met while traveling.\p"
    .string "She was gently grooming POKéMON…\n"
    .string "She was a little angel.\p"
    .string "That little girlÙ name…\n"
    .string "I think it was DAISY.$"

FiveIsland_WaterLabyrinth_Text_AlizeIntro::
    .string "This is an excellent environment\n"
    .string "for raising POKéMON.$"

FiveIsland_WaterLabyrinth_Text_AlizeDefeat::
    .string "Your POKéMON are growing up\n"
    .string "admirably well!$"

FiveIsland_WaterLabyrinth_Text_AlizePostBattle::
    .string "You know, I think you have the\n"
    .string "talent to be a good BREEDER.$"
