CeladonCity_GameCorner_PrizeRoom_Text_FancyThatPorygon::
    .string "I sure do fancy\n"
    .string "that PORYGON!\p"
    .string "But, itÙ hard to\n"
    .string "win at slots!$"

CeladonCity_GameCorner_PrizeRoom_Text_RakedItInToday::
    .string "I had a major\n"
    .string "haul today!$"

CeladonCity_GameCorner_PrizeRoom_Text_CoinCaseRequired::
    .string "A COIN CASE is\n"
    .string "required.$"

CeladonCity_GameCorner_PrizeRoom_Text_WeExchangeCoinsForPrizes::
    .string "We exchange your\n"
    .string "coins for prizes.$"

CeladonCity_GameCorner_PrizeRoom_Text_WhichPrize::
    .string "Which prize do\n"
    .string "you want?$"

CeladonCity_GameCorner_PrizeRoom_Text_HereYouGo::
    .string "はい どうぞ$"

CeladonCity_GameCorner_PrizeRoom_Text_YouWantPrize::
    .string "So, you want\n"
    .string "{STR_VAR_1}?$"

CeladonCity_GameCorner_PrizeRoom_Text_OhFine::
    .string "Oh, fine then.$"

CeladonCity_GameCorner_PrizeRoom_Text_YouWantTM::
    .string "So, you want\n"
    .string "{STR_VAR_2}?$"

CeladonCity_GameCorner_PrizeRoom_Text_NeedMoreCoins::
    .string "Sorry, you need\n"
    .string "more coins.$"

CeladonCity_GameCorner_PrizeRoom_Text_OopsNotEnoughRoom::
    .string "おきゃくさん もう もてないよ$"

CeladonCity_GameCorner_PrizeRoom_Text_OhFineThen::
    .string "あっ そう$"

