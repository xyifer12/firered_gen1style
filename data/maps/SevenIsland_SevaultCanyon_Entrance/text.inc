SevenIsland_SevaultCanyon_Entrance_Text_MiahIntro::
    .string "Kyahaha!\n"
    .string "IÛl flick you away effortlessly!$"

SevenIsland_SevaultCanyon_Entrance_Text_MiahDefeat::
    .string "Tch!\n"
    .string "Too strong for me!$"

SevenIsland_SevaultCanyon_Entrance_Text_MiahPostBattle::
    .string "WhatÙ that?\n"
    .string "I donÑ act the way I look?\p"
    .string "Heh, thatÙ a part of my strategy!$"

SevenIsland_SevaultCanyon_Entrance_Text_MasonIntro::
    .string "Howdy!\n"
    .string "Are you a member of my fan club?$"

SevenIsland_SevaultCanyon_Entrance_Text_MasonDefeat::
    .string "Oh, so youàe not a fan…\p"
    .string "Well, I can change that.\n"
    .string "Let me sing for you!$"

SevenIsland_SevaultCanyon_Entrance_Text_MasonPostBattle::
    .string "Lalalah…\n"
    .string "My POKéMON, I send them out,\l"
    .string "all the girls scream and shout!$"

SevenIsland_SevaultCanyon_Entrance_Text_NicolasIntro::
    .string "This island is too spread out…\n"
    .string "ItÙ not easy patrolling the place.$"

SevenIsland_SevaultCanyon_Entrance_Text_NicolasDefeat::
    .string "Uh-huh…$"

SevenIsland_SevaultCanyon_Entrance_Text_NicolasPostBattle::
    .string "TRAINERS like you from cities,\n"
    .string "they sure are tough.\p"
    .string "YouÛl be headed for the TOWER,\n"
    .string "wonÑ you?$"

SevenIsland_SevaultCanyon_Entrance_Text_MadelineIntro::
    .string "I punish people who abuse\n"
    .string "POKéMON!$"

SevenIsland_SevaultCanyon_Entrance_Text_MadelineDefeat::
    .string "You donÑ seem to be a problem\n"
    .string "TRAINER.$"

SevenIsland_SevaultCanyon_Entrance_Text_MadelinePostBattle::
    .string "If you treat your POKéMON with\n"
    .string "kindness, theyÛl understand.$"

SevenIsland_SevaultCanyon_Entrance_Text_EveIntro::
    .string "EVE: IÛl team up with JON and\n"
    .string "battle together.$"

SevenIsland_SevaultCanyon_Entrance_Text_EveDefeat::
    .string "EVE: Me and JON, we lost.\n"
    .string "Ehehe.$"

SevenIsland_SevaultCanyon_Entrance_Text_EvePostBattle::
    .string "EVE: Iá going to work harder with\n"
    .string "JON.$"

SevenIsland_SevaultCanyon_Entrance_Text_EveNotEnoughMons::
    .string "EVE: I want to team up with JON\n"
    .string "when I battle you.\p"
    .string "Come back with two POKéMON, okay?$"

SevenIsland_SevaultCanyon_Entrance_Text_JonIntro::
    .string "JON: When Iá with EVE, it feels\n"
    .string "like we could never lose.$"

SevenIsland_SevaultCanyon_Entrance_Text_JonDefeat::
    .string "JON: When Iá with EVE,\n"
    .string "it doesnÑ feel like Iße lost!$"

SevenIsland_SevaultCanyon_Entrance_Text_JonPostBattle::
    .string "JON: When Iá with EVE, I feel\n"
    .string "giddy whether I win or lose.\p"
    .string "ItÙ magical!$"

SevenIsland_SevaultCanyon_Entrance_Text_JonNotEnoughMons::
    .string "JON: If itÙ with EVE, IÛl be\n"
    .string "happy to battle with you.\p"
    .string "So, IÚ like to make it a\n"
    .string "two-on-two battle, if I may.$"

SevenIsland_SevaultCanyon_Entrance_Text_RouteSign::
    .string "SEVAULT CANYON ENTRANCE\p"
    .string "TRAINERS are asked to refrain from\n"
    .string "damaging plants in the CANYON.$"

