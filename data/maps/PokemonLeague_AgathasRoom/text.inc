PokemonLeague_AgathasRoom_Text_Intro::
    .string "I am AGATHA of\n"
    .string "the ELITE FOUR!\p"
    .string "OAKÙ taken a lot\n"
    .string "of interest in\l"
    .string "you, child!\p"
    .string "That old duff was\n"
    .string "once tough and\l"
    .string "handsome! That\l"
    .string "was decades ago!\p"
    .string "Now he just wants\n"
    .string "to fiddle with\l"
    .string "his POKéDEX! HeÙ\l"
    .string "wrong! POKéMON\l"
    .string "are for fighting!\p"
    .string "{PLAYER}! IÛl show\n"
    .string "you how a real\l"
    .string "trainer fights!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_AgathasRoom_Text_RematchIntro::
    .string "I am AGATHA of\n"
    .string "the ELITE FOUR!\p"
    .string "OAKÙ taken a lot\n"
    .string "of interest in\l"
    .string "you, child!\p"
    .string "That old duff was\n"
    .string "once tough and\l"
    .string "handsome! That\l"
    .string "was decades ago!\p"
    .string "Now he just wants\n"
    .string "to fiddle with\l"
    .string "his POKéDEX! HeÙ\l"
    .string "wrong! POKéMON\l"
    .string "are for fighting!\p"
    .string "{PLAYER}! IÛl show\n"
    .string "you how a real\l"
    .string "trainer fights!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_AgathasRoom_Text_Defeat::
    .string "Oh ho!\n"
    .string "Youàe something\l"
    .string "special, child!$"

PokemonLeague_AgathasRoom_Text_PostBattle::
    .string "You win! I see\n"
    .string "what the old duff\l"
    .string "sees in you now!\p"
    .string "I have nothing\n"
    .string "else to say! Run\l"
    .string "along now, child!$"

