CeladonCity_Condominiums_RoofRoom_Text_TheresNothingIDontKnow::
    .string "I know everything\n"
    .string "about the world\l"
    .string "of POKéMON in\l"
    .string "your GAME BOY!\p"
    .string "Get together with\n"
    .string "your friends and\l"
    .string "trade POKéMON!$"

CeladonCity_Condominiums_RoofRoom_Text_ObtainedAnEevee::
    .string "{PLAYER} got\n"
    .string "EEVEE!$"

CeladonCity_Condominiums_RoofRoom_Text_BoxIsFull::
    .string "ポケモンが　いっぱいだ\n"
    .string "ボックスを　かえて　きなさい$"

CeladonCity_Condominiums_RoofRoom_Text_WirelessAdapterLecture::
    .string "TRAINER TIPS\p"
    .string "Using a Game Link\n"
    .string "Cable$"

CeladonCity_Condominiums_RoofRoom_Text_ReadWhichHeading::
    .string "Which heading do\n"
    .string "you want to read?$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainWirelessClub::
    .string "When you have\n"
    .string "linked your GAME\l"
    .string "BOY with another\l"
    .string "GAME BOY, talk to\l"
    .string "the attendant on\l"
    .string "the right in any\l"
    .string "POKéMON CENTER.$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainDirectCorner::
    .string "COLOSSEUM lets\n"
    .string "you play against\l"
    .string "a friend.$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainUnionRoom::
    .string "TRADE CENTER is\n"
    .string "used for trading\l"
    .string "POKéMON.$"

CeladonCity_Condominiums_RoofRoom_Text_PamphletOnTMs::
    .string "ItÙ a pamphlet on TMs.\p"
    .string "...\p"
    .string "There are 50 TMs in all.\p"
    .string "There are also 5 HMs that\n"
    .string "can be used repeatedly.\p"
    .string "SILPH CO.$"

