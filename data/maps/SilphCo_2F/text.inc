SilphCo_2F_Text_ConnorIntro::
    .string "Help! Iá a SILPH\n"
    .string "employee.$"

SilphCo_2F_Text_ConnorDefeat::
    .string "SCIENTIST: How\n"
    .string "did you know I\l"
    .string "was a ROCKET?$"

SilphCo_2F_Text_ConnorPostBattle::
    .string "I work for both\n"
    .string "SILPH and TEAM\l"
    .string "ROCKET!$"

SilphCo_2F_Text_JerryIntro::
    .string "ItÙ off limits\n"
    .string "here! Go home!$"

SilphCo_2F_Text_JerryDefeat::
    .string "SCIENTIST: Youàe\n"
    .string "good.$"

SilphCo_2F_Text_JerryPostBattle::
    .string "Can you solve the\n"
    .string "maze in here?$"

SilphCo_2F_Text_Grunt1Intro::
    .string "No kids are\n"
    .string "allowed in here!$"

SilphCo_2F_Text_Grunt1Defeat::
    .string "ROCKET: Tough!$"

SilphCo_2F_Text_Grunt1PostBattle::
    .string "Diamond shaped\n"
    .string "tiles are\l"
    .string "teleport blocks!\p"
    .string "Theyàe hi-tech\n"
    .string "transporters!$"
SilphCo_2F_Text_Grunt2Intro::
    .string "Hey kid! What are\n"
    .string "you doing here?$"

SilphCo_2F_Text_Grunt2Defeat::
    .string "ROCKET: I goofed!$"

SilphCo_2F_Text_Grunt2PostBattle::
    .string "SILPH CO. will\n"
    .string "be merged with\l"
    .string "TEAM ROCKET!$"

SilphCo_2F_Text_TakeTM36::
    .string "Eeek!\n"
    .string "No! Stop! Help!\p"
    .string "Oh, youàe not\n"
    .string "with TEAM ROCKET.\l"
    .string "I thought...\l"
    .string "Iá sorry. Here,\l"
    .string "please take this!$"

SilphCo_2F_Text_PutTM36Away::
    .string "{PLAYER} put TM36 away in\n"
    .string "the BAGÙ TM CASE.$"

SilphCo_2F_Text_TM36_Contains::
    .string "TM36 is\n"
    .string "SELFDESTRUCT!\p"
    .string "ItÙ powerful, but\n"
    .string "the POKéMON that\l"
    .string "uses it faints!\l"
    .string "Be careful.$"

SilphCo_2F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "2F$"

