ViridianCity_PokemonCenter_1F_Text_FeelFreeToUsePC::
    .string "You can use that\n"
    .string "PC in the corner.\p"
    .string "The receptionist\n"
    .string "told me. So kind!$"

ViridianCity_PokemonCenter_1F_Text_PokeCenterInEveryTown::
    .string "ThereÙ a POKéMON\n"
    .string "CENTER in every\l"
    .string "town ahead.\p"
    .string "They donÑ charge\n"
    .string "any money either!$"

ViridianCity_PokemonCenter_1F_Text_PokeCentersHealMons::
    .string "POKéMON CENTERs\n"
    .string "heal your tired,\l"
    .string "hurt or fainted\l"
    .string "POKéMON!$"

