SilphCo_7F_Text_HaveMonForSavingUs::
    .string "Oh! Hi! Youàe\n"
    .string "not a ROCKET! You\l"
    .string "came to save us?\l"
    .string "Why, thank you!\p"
    .string "I want you to\n"
    .string "have this POKéMON\l"
    .string "for saving us.$"

SilphCo_7F_Text_ObtainedLaprasFromEmployee::
    .string "{PLAYER} got\n"
    .string "LAPRAS.$"

SilphCo_7F_Text_ExplainLapras::
    .string "ItÙ LAPRAS. ItÙ\n"
    .string "very intelligent.\p"
    .string "We kept it in our\n"
    .string "lab, but it will\l"
    .string "be much better\l"
    .string "off with you!\p"
    .string "I think you will\n"
    .string "be a good trainer\l"
    .string "for LAPRAS!\p"
    .string "ItÙ a good\n"
    .string "swimmer. ItÛl\l"
    .string "give you a lift!$"

SilphCo_7F_Text_RocketBossWentToBoardroom::
    .string "TEAM ROCKETÙ\n"
    .string "BOSS went to the\l"
    .string "boardroom! Is our\l"
    .string "PRESIDENT OK?$"

SilphCo_7F_Text_OhNo::
    .string "あ‥　もう　もてないぞ$"

SilphCo_7F_Text_SavedAtLast::
    .string "Saved at last!\n"
    .string "Thank you!$"

SilphCo_7F_Text_RocketsAfterMasterBall::
    .string "TEAM ROCKET was\n"
    .string "after the MASTER\l"
    .string "BALL which will\l"
    .string "catch any POKéMON!$"

SilphCo_7F_Text_CanceledMasterBallProject::
    .string "We canceled the\n"
    .string "MASTER BALL\l"
    .string "project because\l"
    .string "of TEAM ROCKET.$"

SilphCo_7F_Text_BadIfTeamRocketTookOver::
    .string "It would be bad\n"
    .string "if TEAM ROCKET\l"
    .string "took over SILPH\l"
    .string "or our POKéMON!$"

SilphCo_7F_Text_WowYouChasedOffTeamRocket::
    .string "Wow! You chased\n"
    .string "off TEAM ROCKET\l"
    .string "all by yourself?$"

SilphCo_7F_Text_ReallyDangerousHere::
    .string "You! ItÙ really\n"
    .string "dangerous here!\p"
    .string "You came to save\n"
    .string "me? You canÑ!$"

SilphCo_7F_Text_ThankYouSoMuch::
    .string "Safe at last!\n"
    .string "Oh thank you!$"

SilphCo_7F_Text_Grunt3Intro::
    .string "Oh ho! I smell a\n"
    .string "little rat!$"

SilphCo_7F_Text_Grunt3Defeat::
    .string "ROCKET: Lights\n"
    .string "out!$"

SilphCo_7F_Text_Grunt3PostBattle::
    .string "You wonÑ find my\n"
    .string "BOSS by just\l"
    .string "scurrying around!$"

SilphCo_7F_Text_JoshuaIntro::
    .string "Heheh!\p"
    .string "You mistook me for\n"
    .string "a SILPH worker?$"

SilphCo_7F_Text_JoshuaDefeat::
    .string "SCIENTIST: Iá\n"
    .string "done!$"

SilphCo_7F_Text_JoshuaPostBattle::
    .string "Despite your age,\n"
    .string "you are a skilled\l"
    .string "trainer!$"

SilphCo_7F_Text_Grunt1Intro::
    .string "I am one of the 4\n"
    .string "ROCKET BROTHERS!$"

SilphCo_7F_Text_Grunt1Defeat::
    .string "ROCKET: Aack!\n"
    .string "Brothers, I lost!$"

SilphCo_7F_Text_Grunt1PostBattle::
    .string "DoesnÑ matter.\n"
    .string "My brothers will\l"
    .string "repay the favor!$"

SilphCo_7F_Text_Grunt2Intro::
    .string "A child intruder?\n"
    .string "That must be you!$"

SilphCo_7F_Text_Grunt2Defeat::
    .string "ROCKET: Fine!\n"
    .string "I lost!$"

SilphCo_7F_Text_Grunt2PostBattle::
    .string "Go on home\n"
    .string "before my BOSS\l"
    .string "gets ticked off!$"

SilphCo_7F_Text_RivalWhatKeptYou::
    .string "{RIVAL}: What\n"
    .string "kept you {PLAYER}?$"

SilphCo_7F_Text_RivalIntro::
    .string "{RIVAL}: Hahaha!\n"
    .string "I thought youÚ\l"
    .string "turn up if I\l"
    .string "waited here!\p"
    .string "I guess TEAM\n"
    .string "ROCKET slowed you\l"
    .string "down! Not that I\l"
    .string "care!\p"
    .string "I saw you in\n"
    .string "SAFFRON, so I\l"
    .string "decided to see if\l"
    .string "you got better!$"

SilphCo_7F_Text_RivalDefeat::
    .string "Oh ho!\n"
    .string "So, you are ready\l"
    .string "for BOSS ROCKET!$"

SilphCo_7F_Text_RivalVictory::
    .string "{RIVAL}“おまえなあ‥\p"
    .string "こんな　うでまえじゃ\n"
    .string "まだまだ‥\l"
    .string "いちにんまえ　とは　いえないぜ$"

SilphCo_7F_Text_RivalPostBattle::
    .string "Well, {PLAYER}!\p"
    .string "Iá moving on up\n"
    .string "and ahead!\p"
    .string "By checking my\n"
    .string "POKéDEX, Iá\l"
    .string "starting to see\l"
    .string "whatÙ strong and\l"
    .string "how they evolve!\p"
    .string "Iá going to the\n"
    .string "POKéMON LEAGUE\l"
    .string "to boot out the\l"
    .string "ELITE FOUR!\p"
    .string "IÛl become the\n"
    .string "worldÙ most\l"
    .string "powerful trainer!\p"
    .string "{PLAYER}, well\n"
    .string "good luck to you!\l"
    .string "DonÑ sweat it!\l"
    .string "Smell ya!$"

SilphCo_7F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "7F$"

