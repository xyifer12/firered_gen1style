SaffronCity_Gym_Text_SabrinaIntro::
    .string "I had a vision of\n"
    .string "your arrival!\p"
    .string "I have had psychic\n"
    .string "powers since I\l"
    .string "was a child.\p"
    .string "I first learned\n"
    .string "to bend spoons\l"
    .string "with my mind.\p"
    .string "I dislike fight-\n"
    .string "ing, but if you\l"
    .string "wish, I will show\l"
    .string "you my powers!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

SaffronCity_Gym_Text_SabrinaDefeat::
    .string "SABRINA: Iá\n"
    .string "shocked!\l"
    .string "But, a loss is a\l"
    .string "loss.\p"
    .string "I admit I didnÑ\n"
    .string "work hard enough\l"
    .string "to win!\p"
    .string "You earned the\n"
    .string "MARSHBADGE!$"

SaffronCity_Gym_Text_SabrinaPostBattle::
    .string "Everyone has\n"
    .string "psychic power!\p"
    .string "People just donÑ\n"
    .string "realize it!$"

SaffronCity_Gym_Text_ExplainMarshBadgeTakeThis::
    .string "The MARSHBADGE\n"
    .string "makes POKéMON up\l"
    .string "to L70 obey you!\p"
    .string "Stronger POKéMON\n"
    .string "will become wild,\l"
    .string "ignoring your\l"
    .string "orders in battle!\p"
    .string "Just donÑ raise\n"
    .string "your POKéMON too\l"
    .string "too much!\p"
    .string "Wait, please take\n"
    .string "this TM with you!$"

SaffronCity_Gym_Text_ReceivedTM04FromSabrina::
    .string "{PLAYER} recieved\n"
    .string "TM46!$"

SaffronCity_Gym_Text_ExplainTM04::
    .string "TM04 is PSYWAVE!\n"
    .string "It uses powerful\l"
    .string "psychic waves to\l"
    .string "inflict damage!$"

SaffronCity_Gym_Text_BagFullOfOtherItems::
    .string "Your pack is full\n"
    .string "of other items!$"

SaffronCity_Gym_Text_AmandaIntro::
    .string "SABRINA is younger\n"
    .string "than I, but I\l"
    .string "respect her!$"

SaffronCity_Gym_Text_AmandaDefeat::
    .string "CHANNELER: Not\n"
    .string "good enough!$"

SaffronCity_Gym_Text_AmandaPostBattle::
    .string "In a battle of\n"
    .string "equals, the one\l"
    .string "with the stronger\l"
    .string "will wins!\p"
    .string "If you wish\n"
    .string "to beat SABRINA,\l"
    .string "focus on winning!$"

SaffronCity_Gym_Text_JohanIntro::
    .string "Does our unseen\n"
    .string "power scare you?$"

SaffronCity_Gym_Text_JohanDefeat::
    .string "PSYCHIC: I never\n"
    .string "foresaw this!$"

SaffronCity_Gym_Text_JohanPostBattle::
    .string "Psychic POKéMON\n"
    .string "fear only ghosts\l"
    .string "and bugs!$"

SaffronCity_Gym_Text_StacyIntro::
    .string "POKéMON take on\n"
    .string "the appearance of\l"
    .string "their trainers.\p"
    .string "Your POKéMON must\n"
    .string "be tough, then!$"

SaffronCity_Gym_Text_StacyDefeat::
    .string "CHANNELER: I knew\n"
    .string "it!$"

SaffronCity_Gym_Text_StacyPostBattle::
    .string "I must teach\n"
    .string "better techniques\l"
    .string "to my POKéMON!$"

SaffronCity_Gym_Text_TyronIntro::
    .string "You know that\n"
    .string "power alone isnÑ\l"
    .string "enough!$"

SaffronCity_Gym_Text_TyronDefeat::
    .string "PSYCHIC: I donÑ\n"
    .string "believe this!$"

SaffronCity_Gym_Text_TyronPostBattle::
    .string "SABRINA just wiped\n"
    .string "out the KARATE\l"
    .string "MASTER next door!$"

SaffronCity_Gym_Text_TashaIntro::
    .string "You and I, our\n"
    .string "POKéMON shall\l"
    .string "fight!$"

SaffronCity_Gym_Text_TashaDefeat::
    .string "CHANNELER: I lost\n"
    .string "after all!$"

SaffronCity_Gym_Text_TashaPostBattle::
    .string "I knew that this\n"
    .string "was going to take\l"
    .string "place.$"

SaffronCity_Gym_Text_CameronIntro::
    .string "SABRINA is young,\n"
    .string "but sheÙ also\l"
    .string "our LEADER!\p"
    .string "You wonÑ reach\n"
    .string "her easily!$"

SaffronCity_Gym_Text_CameronDefeat::
    .string "PSYCHIC: I lost\n"
    .string "my concentration!$"

SaffronCity_Gym_Text_CameronPostBattle::
    .string "There used to be\n"
    .string "2 POKéMON GYMs in\l"
    .string "SAFFRON.\p"
    .string "The FIGHTING DOJO\n"
    .string "next door lost\l"
    .string "its GYM status\l"
    .string "when we went and\l"
    .string "creamed them!$"

SaffronCity_Gym_Text_PrestonIntro::
    .string "SAFFRON POKéMON\n"
    .string "GYM is famous for\l"
    .string "its psychics!\p"
    .string "You want to see\n"
    .string "SABRINA!\l"
    .string "I can tell!$"

SaffronCity_Gym_Text_PrestonDefeat::
    .string "PSYCHIC: Arrrgh!$"

SaffronCity_Gym_Text_PrestonPostBattle::
    .string "ThatÙ right! I\n"
    .string "used telepathy to\l"
    .string "read your mind!$"

SaffronCity_Gym_Text_GymGuyAdvice::
    .string "Yo! Chemp in\n"
    .string "making!\p"
    .string "SABRINAÙ POKéMON\n"
    .string "use psychic power\l"
    .string "instead of force!\p"
    .string "Fighting POKéMON\n"
    .string "are weak against\l"
    .string "psychic POKéMON!\p"
    .string "They get creamed\n"
    .string "before they can\l"
    .string "even aim a punch!$"

SaffronCity_Gym_Text_GymGuyPostVictory::
    .string "Psychic power,\n"
    .string "huh?\p"
    .string "If I had that,\n"
    .string "IÚ make a bundle\l"
    .string "at the slots!$"

SaffronCity_Gym_Text_GymStatue::
    .string "SAFFRON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: SABRINA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

SaffronCity_Gym_Text_GymStatuePlayerWon::
    .string "SAFFRON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: SABRINA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

