FourIsland_IcefallCave_Back_Text_LoreleiKeepHandsOffMons::
    .string "LORELEI: Keep your filthy hands\n"
    .string "off the POKéMON in the cave!\p"
    .string "Do as I say, or youÛl have me to\n"
    .string "answer to!$"

FourIsland_IcefallCave_Back_Text_ShutItLadyLeaveUsBe::
    .string "Aww, shut it, lady, and leave\n"
    .string "us be.\p"
    .string "DonÑ let your glasses get all\n"
    .string "steamed up!$"

FourIsland_IcefallCave_Back_Text_LoreleiPlayerHelpMeKickPoachersOut::
    .string "LORELEI: {PLAYER}?!\n"
    .string "What are you doing here?\p"
    .string "No, we can catch up later.\n"
    .string "Right now, I need your help.\p"
    .string "Help me kick these poachers out\n"
    .string "before they do anything else.\p"
    .string "Theyße been catching POKéMON\n"
    .string "here, then selling them off!\p"
    .string "Are you ready?\n"
    .string "You take that one, please!$"

FourIsland_IcefallCave_Back_Text_GruntIntro::
    .string "W-what?!\p"
    .string "Who says we canÑ do what we want\n"
    .string "with the POKéMON we catch?$"

FourIsland_IcefallCave_Back_Text_GruntDefeat::
    .string "We didnÑ plan on this!$"

FourIsland_IcefallCave_Back_Text_LoreleiWhereHaveYouTakenMons::
    .string "LORELEI: Humph.\n"
    .string "So despicably weak.\p"
    .string "You!\n"
    .string "Tell me!\p"
    .string "Where have you taken the captured\n"
    .string "POKéMON?\p"
    .string "Iá smashing your ring once and\n"
    .string "for all!$"

FourIsland_IcefallCave_Back_Text_NotTellingYouThat::
    .string "N-no way!\n"
    .string "Iá not telling you that!$"

FourIsland_IcefallCave_Back_Text_LoreleiWellDeepFreezeYou::
    .string "LORELEI: If you wonÑ confess,\n"
    .string "weÛl deep-freeze you.\p"
    .string "My LAPRAS is furious for what\n"
    .string "youße done to its friends.\p"
    .string "Go, LAPRAS!\n"
    .string "ICE BEAM…$"

FourIsland_IcefallCave_Back_Text_OkayRocketWareHouseFiveIsland::
    .string "Wawaah! Okay!\n"
    .string "IÛl talk!\p"
    .string "The POKéMON are in the ROCKET\n"
    .string "WAREHOUSE on FIVE ISLAND.\p"
    .string "There! I said it!\n"
    .string "WeÛl be going now!\p"
    .string "…But I doubt youÛl ever make it\n"
    .string "into the ROCKET WAREHOUSE!\p"
    .string "Heheheheh!$"

FourIsland_IcefallCave_Back_Text_ThankYouThisIsAwful::
    .string "{PLAYER}, thank you.\n"
    .string "But this is awful…\p"
    .string "I was born and raised here on\n"
    .string "these islands.\p"
    .string "I had no idea that those horrible\n"
    .string "criminals were loose here…$"

