FuchsiaCity_PokemonCenter_1F_Text_CantBecomeGoodTrainerWithOneMon::
    .string "You canÑ win\n"
    .string "with just one\l"
    .string "strong POKéMON.\p"
    .string "ItÙ tough, but\n"
    .string "you have to raise\l"
    .string "them evenly.$"

FuchsiaCity_PokemonCenter_1F_Text_PokemonLeagueWestOfViridian::
    .string "ThereÙ a narrow\n"
    .string "trail west of\l"
    .string "VIRIDIAN CITY.\p"
    .string "It goes to POKéMON\n"
    .string "LEAGUE HQ.\l"
    .string "The HQ governs\l"
    .string "all TRAINERS.$"

FuchsiaCity_PokemonCenter_1F_Text_VisitSafariZoneForPokedex::
    .string "If youàe studying\n"
    .string "POKéMON, visit\l"
    .string "the SAFARI ZONE.\p"
    .string "It has all sorts\n"
    .string "of rare POKéMON.$"

