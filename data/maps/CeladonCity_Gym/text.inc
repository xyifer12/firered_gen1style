CeladonCity_Gym_Text_ErikaIntro::
    .string "Hello. Lovely\n"
    .string "weather isnÑ it?\l"
    .string "ItÙ so pleasant.\p"
    .string "...Oh dear...\n"
    .string "I must have dozed\l"
    .string "off. Welcome.\p"
    .string "My name is ERIKA.\n"
    .string "I am the LEADER\l"
    .string "of CELADON GYM.\p"
    .string "I teach the art of\n"
    .string "flower arranging.\l"
    .string "My POKéMON are of\l"
    .string "the grass-type.\p"
    .string "Oh, Iá sorry, I\n"
    .string "had no idea that\l"
    .string "you wished to\l"
    .string "challenge me.\p"
    .string "Very well, but I\n"
    .string "shall not lose.{PLAY_BGM MUS_ENCOUNTER_GYM_LEADER}$"

CeladonCity_Gym_Text_ErikaDefeat::
    .string "ERIKA: Oh!\n"
    .string "I concede defeat.\p"
    .string "You are remarkably\n"
    .string "strong.\p"
    .string "I must confer you\n"
    .string "the RAINBOWBADGE.$"

CeladonCity_Gym_Text_ErikaPostBattle::
    .string "You are cataloging\n"
    .string "POKéMON? I must\l"
    .string "say Iá impressed.\p"
    .string "I would never\n"
    .string "collect POKéMON\l"
    .string "if they were\l"
    .string "unattractive.$"

CeladonCity_Gym_Text_ExplainRainbowBadgeTakeThis::
    .string "The RAINBOWBADGE\n"
    .string "will make POKéMON\l"
    .string "up to L50 obey.\p"
    .string "It also allows\n"
    .string "POKéMON to use\l"
    .string "STRENGTH in and\l"
    .string "out of battle.\p"
    .string "Please also take\n"
    .string "this with you.$"

CeladonCity_Gym_Text_ReceivedTM21FromErika::
    .string "{PLAYER} received\n"
    .string "TM21!$"

CeladonCity_Gym_Text_ExplainTM21::
    .string "TM21 contains\n"
    .string "MEGA DRAIN.\p"
    .string "Half the damage\n"
    .string "it inflicts is\l"
    .string "drained to heal\l"
    .string "your POKéMON!$"

CeladonCity_Gym_Text_ShouldMakeRoomForThis::
    .string "You should make\n"
    .string "room for this.$"

CeladonCity_Gym_Text_KayIntro::
    .string "Hey!\p"
    .string "You are not\n"
    .string "allowed in here!$"

CeladonCity_Gym_Text_KayDefeat::
    .string "LASS: Youàe\n"
    .string "too rough!$"

CeladonCity_Gym_Text_KayPostBattle::
    .string "Bleaah!\n"
    .string "I hope ERIKA\l"
    .string "wipes you out!$"

CeladonCity_Gym_Text_BridgetIntro::
    .string "I was getting\n"
    .string "bored.$"

CeladonCity_Gym_Text_BridgetDefeat::
    .string "BEAUTY: My\n"
    .string "makeup!$"

CeladonCity_Gym_Text_BridgetPostBattle::
    .string "Grass-type POKéMON\n"
    .string "are tough against\l"
    .string "the water-type!\p"
    .string "They also have an\n"
    .string "edge on rock and\l"
    .string "ground POKéMON!$"

CeladonCity_Gym_Text_TinaIntro::
    .string "ArenÑ you the\n"
    .string "peeping Tom?$"

CeladonCity_Gym_Text_TinaDefeat::
    .string "JR.TRAINER♀: Iá\n"
    .string "in shock!$"

CeladonCity_Gym_Text_TinaPostBattle::
    .string "Oh, you werenÑ\n"
    .string "peeping? We get a\l"
    .string "lot of gawkers!$"

CeladonCity_Gym_Text_TamiaIntro::
    .string "Look at my grass\n"
    .string "POKéMON!\p"
    .string "Theyàe easy\n"
    .string "to raise!$"

CeladonCity_Gym_Text_TamiaDefeat::
    .string "BEAUTY: No!$"

CeladonCity_Gym_Text_TamiaPostBattle::
    .string "We only use grass-\n"
    .string "type POKéMON at\l"
    .string "our GYM!\p"
    .string "We also use them\n"
    .string "for making flower\l"
    .string "arrangements!$"

CeladonCity_Gym_Text_LisaIntro::
    .string "DonÑ bring any\n"
    .string "bugs or fire\l"
    .string "POKéMON in here!$"

CeladonCity_Gym_Text_LisaDefeat::
    .string "LASS: Oh!\n"
    .string "You!$"

CeladonCity_Gym_Text_LisaPostBattle::
    .string "Our LEADER, ERIKA,\n"
    .string "might be quiet,\l"
    .string "but sheÙ also\l"
    .string "very skilled!$"

CeladonCity_Gym_Text_LoriIntro::
    .string "Pleased to meet\n"
    .string "you. My hobby is\l"
    .string "POKéMON training.$"

CeladonCity_Gym_Text_LoriDefeat::
    .string "BEAUTY: Oh!\n"
    .string "Splendid!$"

CeladonCity_Gym_Text_LoriPostBattle::
    .string "I have a blind\n"
    .string "date coming up.\l"
    .string "I have to learn\l"
    .string "to be polite.$"

CeladonCity_Gym_Text_MaryIntro::
    .string "Welcome to\n"
    .string "CELADON GYM!\p"
    .string "You better not\n"
    .string "underestimate\l"
    .string "girl power!$"

CeladonCity_Gym_Text_MaryDefeat::
    .string "COOLTRAINER♀: Oh!\n"
    .string "Beaten!$"

CeladonCity_Gym_Text_MaryPostBattle::
    .string "I didnÑ bring my\n"
    .string "best POKéMON!\p"
    .string "Wait Ñil next\n"
    .string "time!$"

CeladonCity_Gym_Text_GymStatue::
    .string "CELADON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: ERIKA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CeladonCity_Gym_Text_GymStatuePlayerWon::
    .string "CELADON CITY\n"
    .string "POKéMON GYM\l"
    .string "LEADER: ERIKA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

