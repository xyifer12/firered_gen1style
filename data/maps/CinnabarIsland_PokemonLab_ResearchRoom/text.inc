CinnabarIsland_PokemonLab_ResearchRoom_Text_EeveeCanEvolveIntroThreeMons::
    .string "EEVEE can evolve\n"
    .string "into 1 of 3 kinds\l"
    .string "of POKéMON.$"

CinnabarIsland_PokemonLab_ResearchRoom_Text_LegendaryBirdEmail::
    .string "ThereÙ an e-mail\n"
    .string "message!\p"
    .string "...\p"
    .string "The 3 legendary\n"
    .string "bird POKéMON are\l"
    .string "ARTICUNO, ZAPDOS\l"
    .string "and MOLTRES.\p"
    .string "Their whereabouts\n"
    .string "are unknown.\p"
    .string "We plan to explore\n"
    .string "the cavern close\l"
    .string "to CERULEAN.\p"
    .string "From: POKéMON\n"
    .string "RESEARCH TEAM\p"
    .string "...$"

CinnabarIsland_PokemonLab_ResearchRoom_Text_AnAmberPipe::
    .string "An amber pipe!$"

