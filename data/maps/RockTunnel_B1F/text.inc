RockTunnel_B1F_Text_SofiaIntro::
    .string "Hikers leave twigs\n"
    .string "as trail markers.$"

RockTunnel_B1F_Text_SofiaDefeat::
    .string "JR.TRAINER♂: Ohhh!\n"
    .string "I did my best!$"

RockTunnel_B1F_Text_SofiaPostBattle::
    .string "I want to go\n"
    .string "home!$"

RockTunnel_B1F_Text_DudleyIntro::
    .string "Hahaha! Can you\n"
    .string "beat my power?$"

RockTunnel_B1F_Text_DudleyDefeat::
    .string "HIKER: Oops!\n"
    .string "Out-muscled!$"

RockTunnel_B1F_Text_DudleyPostBattle::
    .string "I go for power\n"
    .string "because I hate\l"
    .string "thinking!$"

RockTunnel_B1F_Text_CooperIntro::
    .string "You have a\n"
    .string "POKéDEX?\l"
    .string "I want one too!$"

RockTunnel_B1F_Text_CooperDefeat::
    .string "POKéMANIAC: Shoot!\n"
    .string "Iá so jealous!$"

RockTunnel_B1F_Text_CooperPostBattle::
    .string "When you finish\n"
    .string "your POKéDEX, can\l"
    .string "I have it?$"

RockTunnel_B1F_Text_SteveIntro::
    .string "Do you know about\n"
    .string "costume players?$"

RockTunnel_B1F_Text_SteveDefeat::
    .string "POKéMANIAC: Well,\n"
    .string "thatÙ that.$"

RockTunnel_B1F_Text_StevePostBattle::
    .string "Costume players\n"
    .string "dress up as\l"
    .string "POKéMON for fun.$"

RockTunnel_B1F_Text_AllenIntro::
    .string "My POKéMON\n"
    .string "techniques will\l"
    .string "leave you crying!$"

RockTunnel_B1F_Text_AllenDefeat::
    .string "HIKER: I give!\n"
    .string "Youàe a better\l"
    .string "technician!$"

RockTunnel_B1F_Text_AllenPostBattle::
    .string "In mountains,\n"
    .string "youÛl often find\l"
    .string "rock-type POKéMON.$"

RockTunnel_B1F_Text_MarthaIntro::
    .string "I donÑ often\n"
    .string "come here, but I\l"
    .string "will fight you.$"

RockTunnel_B1F_Text_MarthaDefeat::
    .string "JR.TRAINER♂: Oh!\n"
    .string "I lost!$"

RockTunnel_B1F_Text_MarthaPostBattle::
    .string "I like tiny\n"
    .string "POKéMON, big ones\l"
    .string "are too scary!$"

RockTunnel_B1F_Text_EricIntro::
    .string "Hit me with your\n"
    .string "best shot!$"

RockTunnel_B1F_Text_EricDefeat::
    .string "HIKER: Fired\n"
    .string "away!$"

RockTunnel_B1F_Text_EricPostBattle::
    .string "IÛl raise my\n"
    .string "POKéMON to beat\l"
    .string "yours, kid!$"

RockTunnel_B1F_Text_WinstonIntro::
    .string "I draw POKéMON\n"
    .string "when Iá home.$"

RockTunnel_B1F_Text_WinstonDefeat::
    .string "POKéMANIAC: Whew!\n"
    .string "Iá exhausted!$"

RockTunnel_B1F_Text_WinstonPostBattle::
    .string "Iá an artist,\n"
    .string "not a fighter.$"

