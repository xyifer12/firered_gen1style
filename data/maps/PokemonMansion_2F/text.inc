PokemonMansion_Text_PressSecretSwitch::
    .string "A secret switch!\p"
    .string "Press it?$"

PokemonMansion_Text_WhoWouldnt::
    .string "Who wouldnÑ?$"

PokemonMansion_Text_NotQuiteYet::
    .string "Not quite yet!$"

PokemonMansion_1F_Text_ArnieIntro::
    .string "I canÑ get out!\n"
    .string "This old place is\l"
    .string "one big puzzle!$"

PokemonMansion_1F_Text_ArnieDefeat::
    .string "BURGLAR: Oh no!\n"
    .string "My bag of loot!$"

PokemonMansion_1F_Text_ArniePostBattle::
    .string "Switches open and\n"
    .string "close alternating\l"
    .string "sets of doors!$"

PokemonMansion_1F_Text_NewMonDiscoveredInGuyanaJungle::
    .string "Diary: July 5\n"
    .string "Guyana,\l"
    .string "South America\p"
    .string "A new POKéMON was\n"
    .string "discovered deep\l"
    .string "in the jungle.$"

PokemonMansion_1F_Text_ChristenedDiscoveredMonMew::
    .string "Diary: July 10\n"
    .string "We christened the\l"
    .string "newly discovered\l"
    .string "POKéMON, MEW.$"

