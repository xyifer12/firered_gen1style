CeruleanCity_Mart_Text_RepelWorksOnWeakMons::
    .string "Use REPEL to keep\n"
    .string "bugs and weak\l"
    .string "POKéMON away.\p"
    .string "Put your strongest\n"
    .string "POKéMON at the\l"
    .string "top of the list\l"
    .string "for best results!$"

CeruleanCity_Mart_Text_DoYouKnowAboutRareCandy::
    .string "Have you seen any\n"
    .string "RARE CANDY?\p"
    .string "ItÙ supposed to\n"
    .string "make POKéMON go\l"
    .string "up one level!$"

