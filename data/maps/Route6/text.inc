Route6_Text_RickyIntro::
    .string "WhoÙ there?\n"
    .string "Quit listening in\l"
    .string "on us!$"

Route6_Text_RickyDefeat::
    .string "JR.TRAINER♂: I\n"
    .string "just canÑ win!$"

Route6_Text_RickyPostBattle::
    .string "Whisper...\n"
    .string "Whisper...$"

Route6_Text_NancyIntro::
    .string "Excuse me! This\n"
    .string "This is a private\l"
    .string "conversation!$"

Route6_Text_NancyDefeat::
    .string "JR.TRAINER♀: Ugh!\n"
    .string "I hate losing!$"

Route6_Text_NancyPostBattle::
    .string "Whisper...\n"
    .string "Whisper...$"

Route6_Text_KeigoIntro::
    .string "There arenÑ many\n"
    .string "bugs out here.$"

Route6_Text_KeigoDefeat::
    .string "BUG CATCHER: No!\n"
    .string "Youàe kidding!$"

Route6_Text_KeigoPostBattle::
    .string "I like bugs, so\n"
    .string "Iá going back to\l"
    .string "VIRIDIAN FOREST.$"

Route6_Text_JeffIntro::
    .string "Huh? You want\n"
    .string "to talk to me?$"

Route6_Text_JeffDefeat::
    .string "JR.TRAINER♂: I\n"
    .string "didnÑ start it!$"

Route6_Text_JeffPostBattle::
    .string "I should carry\n"
    .string "more POKéMON with\l"
    .string "me for safety.$"

Route6_Text_IsabelleIntro::
    .string "Me? Well, OK.\n"
    .string "IÛl play!$"

Route6_Text_IsabelleDefeat::
    .string "JR.TRAINER♀: Just\n"
    .string "didnÑ work!$"

Route6_Text_IsabellePostBattle::
    .string "I want to get\n"
    .string "stronger! WhatÙ\l"
    .string "your secret?$"

Route6_Text_ElijahIntro::
    .string "Iße never seen\n"
    .string "you around!\l"
    .string "Are you good?$"

Route6_Text_ElijahDefeat::
    .string "BUG CATCHER: You\n"
    .string "are too good!$"

Route6_Text_ElijahPostBattle::
    .string "Are my POKéMON\n"
    .string "weak? Or, am I\l"
    .string "just bad?$"

Route6_Text_UndergroundPathSign::
    .string "UNDERGROUND PATH\n"
    .string "CERULEAN CITY \l"
    .string "VERMILION CITY$"

