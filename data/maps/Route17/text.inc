Route17_Text_RaulIntro::
    .string "ThereÙ no money in\n"
    .string "fighting kids.$"

Route17_Text_RaulDefeat::
    .string "CUE BALL: Burned\n"
    .string "out!$"

Route17_Text_RaulPostBattle::
    .string "Good stuff is\n"
    .string "lying around on\l"
    .string "CYCLING ROAD!$"

Route17_Text_IsaiahIntro::
    .string "What do you want,\n"
    .string "kiddo?$"

Route17_Text_IsaiahDefeat::
    .string "CUE BALL: Whoo!$"

Route17_Text_IsaiahPostBattle::
    .string "I could belly-\n"
    .string "bump you outta\l"
    .string "here!$"

Route17_Text_VirgilIntro::
    .string "You heading to\n"
    .string "FUCHSIA?$"

Route17_Text_VirgilDefeat::
    .string "BIKER: Crash and\n"
    .string "burn!$"

Route17_Text_VirgilPostBattle::
    .string "I love racing\n"
    .string "downhill!$"

Route17_Text_BillyIntro::
    .string "Weàe BIKERs!\n"
    .string "Highway stars!$"

Route17_Text_BillyDefeat::
    .string "BIKER: Smoked!$"

Route17_Text_BillyPostBattle::
    .string "Are you looking\n"
    .string "for adventure?$"

Route17_Text_NikolasIntro::
    .string "Let VOLTORB\n"
    .string "electrify you!$"

Route17_Text_NikolasDefeat::
    .string "BIKER: Grounded\n"
    .string "out!$"

Route17_Text_NikolasPostBattle::
    .string "I got my VOLTORB\n"
    .string "at the abandoned\l"
    .string "POWER PLANT.$"

Route17_Text_ZeekIntro::
    .string "My POKéMON wonÑ\n"
    .string "evolve! Why?$"

Route17_Text_ZeekDefeat::
    .string "BIKER: Why,\n"
    .string "you!$"

Route17_Text_ZeekPostBattle::
    .string "Maybe some POKéMON\n"
    .string "need element\l"
    .string "STONEs to evolve.$"

Route17_Text_JamalIntro::
    .string "I need a little\n"
    .string "exercise!$"

Route17_Text_JamalDefeat::
    .string "CUE BALL: Whew!\n"
    .string "Good workout!$"

Route17_Text_JamalPostBattle::
    .string "Iá sure I lost\n"
    .string "weight there!$"

Route17_Text_CoreyIntro::
    .string "Be a rebel!$"

Route17_Text_CoreyDefeat::
    .string "CUE BALL: Aaaargh!$"

Route17_Text_CoreyPostBattle::
    .string "Be ready to fight\n"
    .string "for your beliefs!$"

Route17_Text_JaxonIntro::
    .string "Nice BIKE!\n"
    .string "HowÙ it handle?$"

Route17_Text_JaxonDefeat::
    .string "BIKER: Shoot!$"

Route17_Text_JaxonPostBattle::
    .string "The slope makes\n"
    .string "it hard to steer!$"

Route17_Text_WilliamIntro::
    .string "Get lost kid!\n"
    .string "Iá bushed!$"

Route17_Text_WilliamDefeat::
    .string "BIKER: Are\n"
    .string "you satisfied?$"

Route17_Text_WilliamPostBattle::
    .string "I need to catch\n"
    .string "a few Zs!$"

Route17_Text_WatchOutForDiscardedItems::
    .string "ItÙ a notice!\p"

    .string "Watch out for\n"
    .string "discarded items!$"

Route17_Text_SameSpeciesGrowDifferentRates::
    .string "TRAINER TIPS\p"

    .string "All POKéMON are\n"
    .string "unique.\p"

    .string "Even POKéMON of\n"
    .string "the same type and\l"
    .string "level grow at\l"
    .string "different rates.$"

Route17_Text_PressBToStayInPlace::
    .string "TRAINER TIPS\p"

    .string "Press the A or B\n"
    .string "Button to stay in\l"
    .string "place while on a\l"
    .string "slope.$"

Route17_Text_RouteSign::
    .string "ROUTE 17\n"
    .string "CELADON CITY -\l"
    .string "FUCHSIA CITY$"

Route17_Text_DontThrowGameThrowBalls::
    .string "ItÙ a notice!\p"
    
    .string "DonÑ throw the\n"
    .string "game, throw POKé\l"
    .string "BALLs instead!$"

Route17_Text_CyclingRoadSign::
    .string "CYCLING ROAD\n"
    .string "Slope ends here!$"

