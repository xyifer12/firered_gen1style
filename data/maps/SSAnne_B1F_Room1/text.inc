SSAnne_B1F_Room1_Text_PhillipIntro::
    .string "Matey, youàe\n"
    .string "walking the plank\l"
    .string "if you lose!$"

SSAnne_B1F_Room1_Text_PhillipDefeat::
    .string "SAILOR: Argh!\n"
    .string "Beaten by a kid!$"

SSAnne_B1F_Room1_Text_PhillipPostBattle::
    .string "Jellyfish some-\n"
    .string "times drift into\l"
    .string "the ship.$"

SSAnne_B1F_Room1_Text_BarnyIntro::
    .string "Hello stranger!\n"
    .string "Stop and chat!\p"
    .string "All my POKéMON\n"
    .string "are from the sea!$"

SSAnne_B1F_Room1_Text_BarnyDefeat::
    .string "FISHERMAN: Darn!\n"
    .string "I let that one\l"
    .string "get away!$"

SSAnne_B1F_Room1_Text_BarnyPostBattle::
    .string "I was going to\n"
    .string "make you my\l"
    .string "assistant, too!$"

