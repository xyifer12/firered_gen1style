PokemonMansion_1F_Text_TedIntro::
    .string "Who are you? There\n"
    .string "shouldnÑ be\l"
    .string "anyone here.$"

PokemonMansion_1F_Text_TedDefeat::
    .string "SCIENTIST: Ouch!$"

PokemonMansion_1F_Text_TedPostBattle::
    .string "A key? I donÑ\n"
    .string "know what youàe\l"
    .string "talking about.$"

PokemonMansion_1F_Text_JohnsonIntro::
    .string "W-w-waah! You startled me!\n"
    .string "I thought you were a ghost.$"

PokemonMansion_1F_Text_JohnsonDefeat::
    .string "Tch!\n"
    .string "I canÑ get any wins.$"

PokemonMansion_1F_Text_JohnsonPostBattle::
    .string "I was exploring here by myself,\n"
    .string "but I feel spooked.\p"
    .string "I ought to go soon.$"

