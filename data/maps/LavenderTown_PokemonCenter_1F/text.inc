LavenderTown_PokemonCenter_1F_Text_RocketsDoAnythingForMoney::
    .string "TEAM ROCKET will\n"
    .string "do anything for\l"
    .string "the sake of gold!$"

LavenderTown_PokemonCenter_1F_Text_CubonesMotherKilledByRockets::
    .string "I saw CUBONEÙ\n"
    .string "mother die trying\l"
    .string "to escape from\l"
    .string "TEAM ROCKET!$"

LavenderTown_PokemonCenter_1F_Text_PeoplePayForCuboneSkulls::
    .string "CUBONEs wear\n"
    .string "skulls, right?\p"
    .string "People will pay a\n"
    .string "lot for one!$"

