FourIsland_LoreleisHouse_Text_IfAnythingWereToHappenToIsland::
    .string "LORELEI: ThereÙ something weighing\n"
    .string "heavily on my mind.\p"
    .string "If anything were to happen on\n"
    .string "the island where I was born…\p"
    .string "I wouldnÑ know about it if I were\n"
    .string "in the POKéMON LEAGUE.\p"
    .string "I wonder if that would make me\n"
    .string "irresponsible to my home…$"

FourIsland_LoreleisHouse_Text_IllReturnToLeagueInShortWhile::
    .string "LORELEI: So, you managed to solve\n"
    .string "all the problems here?\p"
    .string "ThatÙ wonderful.\p"
    .string "That means there isnÑ any reason\n"
    .string "for me to be here all the time.\p"
    .string "Thank you…\p"
    .string "IÛl return to the POKéMON LEAGUE\n"
    .string "in a short while.$"

FourIsland_LoreleisHouse_Text_WillDoWhatICanHereAndNow::
    .string "I donÑ know what will happen in\n"
    .string "the future, but…\p"
    .string "I will do what I can here and now.\n"
    .string "ThatÙ all I can do.$"

FourIsland_LoreleisHouse_Text_StuffedMonDollsGalore::
    .string "Stuffed POKéMON dolls galore!$"

