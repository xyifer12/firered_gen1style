ViridianCity_Text_CanCarryMonsAnywhere::
    .string "Those POKé BALLs\n"
    .string "at your waist!\l"
    .string "You have POKéMON!\p"
    
    .string "ItÙ great that\n"
    .string "you can carry and\l"
    .string "use POKéMON any\l"
    .string "time, anywhere!$"

ViridianCity_Text_GymClosedWonderWhoLeaderIs::
    .string "This POKéMON GYM\n"
    .string "is always closed.\p"

    .string "I wonder who the\n"
    .string "LEADER is?$"

ViridianCity_Text_ViridiansGymLeaderReturned::
    .string "VIRIDIAN GYMÙ\n"
    .string "LEADER returned!$"

ViridianCity_Text_WantToKnowAboutCaterpillarMons::
    .string "You want to know\n"
    .string "about the 2 kinds\l"
    .string "of caterpillar\l"
    .string "POKéMON?$"

ViridianCity_Text_OhOkayThen::
    .string "Oh, okay then!$"

ViridianCity_Text_ExplainCaterpieWeedle::
    .string "CATERPIE has no\n"
    .string "poison, but\l"
    .string "WEEDLE does.\p"

    .string "Watch out for its\n"
    .string "POISON STING!$"

ViridianCity_Text_GrandpaHasntHadCoffeeYet::
    .string "Oh Grandpa! DonÑ\n"
    .string "be so mean!\l"
    .string "He hasnÑ had his\l"
    .string "coffee yet.$"

ViridianCity_Text_GoShoppingInPewterOccasionally::
    .string "When I go shop in\n"
    .string "PEWTER CITY, I\l"
    .string "have to take the\l"
    .string "winding trail in\l"
    .string "VIRIDIAN FOREST.$"

ViridianCity_Text_ThisIsPrivateProperty::
    .string "You canÑ go\n"
    .string "through here!\p"

    .string "This is private\n"
    .string "property!$"

ViridianCity_Text_HadMyCoffee::
    .string "Ahh, Iße had my\n"
    .string "coffee now and I\l"
    .string "feel great!\p"
    
    .string "Sure you can go\n"
    .string "through!\p"

    .string "Are you in a\n"
    .string "hurry?$"

ViridianCity_Text_ShowYouHowToCatchMons::
    .string "I see youàe using\n"
    .string "a POKéDEX.\p"

    .string "When you catch a\n"
    .string "POKéMON, POKéDEX\l"
    .string "is automatically\l"
    .string "updated.\p"

    .string "What? donÑ you\n"
    .string "know how to catch\l"
    .string "POKéMON?\p"

    .string "IÛl show you\n"
    .string "how to then.$"

ViridianCity_Text_DeclineTutorial::
    .string "Time is money...\n"
    .string "Go along then.$"

ViridianCity_Text_FirstWeaken::
    .string "First, you need\n"
    .string "to weaken the\l"
    .string "target POKéMON.$"

ViridianCity_Text_CitySign::
    .string "VIRIDIAN CITY\n"
    .string "The Eternally\l"
    .string "Green Paradise$"

ViridianCity_Text_CatchMonsForEasierBattles::
    .string "TRAINER TIPS\p"

    .string "Catch POKéMON\n"
    .string "and expand your\l"
    .string "collection!\p"

    .string "The more you have,\n"
    .string "the easier it is\l"
    .string "to fight!$"

ViridianCity_Text_MovesLimitedByPP::
    .string "TRAINER TIPS\p"

    .string "The battle moves\n"
    .string "of POKéMON are\l"
    .string "limited by their\l"
    .string "POWER POINTs, PP.\p"

    .string "To replenish PP,\n"
    .string "rest your tired\l"
    .string "POKéMON at a\l"
    .string "POKéMON CENTER!$"

ViridianCity_Text_GymSign::
    .string "VIRIDIAN CITY\n"
    .string "POKéMON GYM$"

ViridianCity_Text_GymDoorsAreLocked::
    .string "The GYMÙ doors\n"
    .string "are locked...$"

ViridianCity_Text_TakeTM42::
    .string "Yawn!\n"
    .string "I must have dozed\l"
    .string "off in the sun.\p"

    .string "I had this dream\n"
    .string "about a DROWZEE\l"
    .string "eating my dream.\l"
    .string "WhatÙ this?\l"
    .string "Where did this TM\l"
    .string "come from?\p"

    .string "This is spooky!\n"
    .string "Here, you can\l"
    .string "have this TM.$"

ViridianCity_Text_PutTM42Away::
    .string "{PLAYER} put TM42 away in\n"
    .string "the BAGÙ TM CASE.$"

ViridianCity_Text_TM42_Contains::
    .string "TM42 contains\n"
    .string "DREAM EATER...\l"
    .string "...Snore...$"

