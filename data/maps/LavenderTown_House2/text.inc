LavenderTown_House2_Text_WantMeToRateNicknames::
    .string "Hello, hello!\n"
    .string "I am the official\l"
    .string "NAME RATER!\p"
    .string "Want me to rate\n"
    .string "the nicknames of\l"
    .string "your POKéMON?$"

LavenderTown_House2_Text_CritiqueWhichMonsNickname::
    .string "Which POKéMON\n"
    .string "should I look at?$"

LavenderTown_House2_Text_GiveItANicerName::
    .string "{STR_VAR_1}, is it?\n"
    .string "That is a decent\l"
    .string "nickname!\p"
    .string "But, would you\n"
    .string "like me to give\l"
    .string "it a nicer name?\p"
    .string "How about it?$"

LavenderTown_House2_Text_WhatShallNewNicknameBe::
    .string "Fine! What should\n"
    .string "we name it?$"

LavenderTown_House2_Text_FromNowOnShallBeKnownAsName::
    .string "OK! This POKéMON\n"
    .string "has been renamed\l"
    .string "{STR_VAR_1}!\p"
    .string "ThatÙ a better\n"
    .string "name than before!$"

LavenderTown_House2_Text_ISeeComeVisitAgain::
    .string "Fine! Come any\n"
    .string "time you like!$"

LavenderTown_House2_Text_FromNowOnShallBeKnownAsSameName::
    .string "Done! From now on, this POKéMON\n"
    .string "shall be known as {STR_VAR_1}!\p"
    .string "It looks no different from before,\n"
    .string "and yet, this is vastly superior!\p"
    .string "How fortunate for you!$"

LavenderTown_House2_Text_TrulyImpeccableName::
    .string "{STR_VAR_1}, is it?\n"
    .string "That is a truly\l"
    .string "impeccable name!\p"
    .string "Take good care of\n"
    .string "{STR_VAR_1}!$"

LavenderTown_House2_Text_ThatIsMerelyAnEgg::
    .string "Now, now.\n"
    .string "That is merely an EGG!$"

