Route16_Text_LaoIntro::
    .string "What do you want?$"

Route16_Text_LaoDefeat::
    .string "BIKER: DonÑ you\n"
    .string "dare laugh!$"

Route16_Text_LaoPostBattle::
    .string "We like just\n"
    .string "hanging here,\l"
    .string "whatÙ it to you?$"

Route16_Text_KojiIntro::
    .string "Nice BIKE!\n"
    .string "Hand it over!$"

Route16_Text_KojiDefeat::
    .string "CUE BALL: Knock\n"
    .string "out!$"

Route16_Text_KojiPostBattle::
    .string "Forget it, who\n"
    .string "needs your BIKE!$"

Route16_Text_LukeIntro::
    .string "Come out and play,\n"
    .string "little mouse!$"

Route16_Text_LukeDefeat::
    .string "CUE BALL: You\n"
    .string "little rat!$"

Route16_Text_LukePostBattle::
    .string "I hate losing!\n"
    .string "Get away from me!$"

Route16_Text_HideoIntro::
    .string "Hey, you just\n"
    .string "bumped me!$"

Route16_Text_HideoDefeat::
    .string "BIKER: Kaboom!$"

Route16_Text_HideoPostBattle::
    .string "You can also get\n"
    .string "to FUCHSIA from\l"
    .string "VERMILION using a\l"
    .string "coastal road.$"

Route16_Text_CamronIntro::
    .string "Iá feeling\n"
    .string "hungry and mean!$"

Route16_Text_CamronDefeat::
    .string "CUE BALL: Bad,\n"
    .string "bad, bad!$"

Route16_Text_CamronPostBattle::
    .string "I like my POKéMON\n"
    .string "ferocious! They\l"
    .string "tear up enemies!$"

Route16_Text_RubenIntro::
    .string "Sure, IÛl go!$"

Route16_Text_RubenDefeat::
    .string "BIKER: DonÑ make\n"
    .string "me mad!$"

Route16_Text_RubenPostBattle::
    .string "I like harassing\n"
    .string "people with my\l"
    .string "vicious POKéMON!$"

Route16_Text_MonSprawledOutInSlumber::
    .string "A sleeping POKéMON\n"
    .string "blocks the way!$"

Route16_Text_CyclingRoadSign::
    .string "Enjoy the slope!\n"
    .string "CYCLING ROAD$"

Route16_Text_RouteSign::
    .string "ROUTE 16\n"
    .string "CELADON CITY -\l"
    .string "FUCHSIA CITY$"

Route16_Text_JedIntro::
    .string "JED: Our love knows no bounds.\n"
    .string "Weàe in love and we show it!$"

Route16_Text_JedDefeat::
    .string "JED: Oh, no!\n"
    .string "My love has seen me as a loser!$"

Route16_Text_JedPostBattle::
    .string "JED: Listen, LEA.\n"
    .string "You need to focus less on me.$"

Route16_Text_JedNotEnoughMons::
    .string "JED: You have just one POKéMON?\n"
    .string "Is there no love in your heart?$"

Route16_Text_LeaIntro::
    .string "LEA: Sometimes, the intensity of\n"
    .string "our love scares me.$"

Route16_Text_LeaDefeat::
    .string "LEA: Ohh! But JED looks cool\n"
    .string "even in a loss!$"

Route16_Text_LeaPostBattle::
    .string "LEA: Ehehe, Iá sorry.\n"
    .string "JED is so cool.$"

Route16_Text_LeaNotEnoughMons::
    .string "LEA: Oh, you donÑ have two\n"
    .string "POKéMON with you?\p"
    .string "DoesnÑ it feel lonely for you or\n"
    .string "your POKéMON?$"
