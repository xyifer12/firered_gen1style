Route25_SeaCottage_Text_ImBillHelpMeOutPal::
    .string "Hiya! Iá a\n"
    .string "POKéMON...\l"
    .string "...No Iá not!\p"
    .string "Call me BILL!\n"
    .string "Iá a true blue\l"
    .string "POKéMANIAC! Hey!\l"
    .string "WhatÙ with that\l"
    .string "skeptical look?\p"
    .string "Iá not joshing\n"
    .string "you, I screwed up\l"
    .string "an experiment and\l"
    .string "got combined with\l"
    .string "a POKéMON!\p"
    .string "So, how about it?\n"
    .string "Help me out here!$"

Route25_SeaCottage_Text_ImBillHelpMeOutLady::
    .string "Hiya! Iá a\n"
    .string "POKéMON...\l"
    .string "...No Iá not!\p"
    .string "Call me BILL!\n"
    .string "Iá a true blue\l"
    .string "POKéMANIAC! Hey!\l"
    .string "WhatÙ with that\l"
    .string "skeptical look?\p"
    .string "Iá not joshing\n"
    .string "you, I screwed up\l"
    .string "an experiment and\l"
    .string "got combined with\l"
    .string "a POKéMON!\p"
    .string "So, how about it?\n"
    .string "Help me out here!$"

Route25_SeaCottage_Text_RunCellSeparationOnPC::
    .string "When Iá in the\n"
    .string "TELEPORTER, go to\l"
    .string "my PC and run the\l"
    .string "Cell Separation\l"
    .string "System!$"

Route25_SeaCottage_Text_NoPleaseChief::
    .string "No!? Come on, you\n"
    .string "gotta help a guy\l"
    .string "in deep trouble!\p"
    .string "What do you say,\n"
    .string "chief? Please?\l"
    .string "OK? All right!$"

Route25_SeaCottage_Text_NoPleaseBeautiful::
    .string "No!? Come on, you\n"
    .string "gotta help a guy\l"
    .string "in deep trouble!\p"
    .string "What do you say,\n"
    .string "chief? Please?\l"
    .string "OK? All right!$"

Route25_SeaCottage_Text_ThanksBudTakeThis::
    .string "BILL: Yeehah!\n"
    .string "Thanks, bud! I\l"
    .string "owe you one!\p"
    .string "So, did you come\n"
    .string "to see my POKéMON\l"
    .string "collection?\l"
    .string "You didnÑ?\l"
    .string "ThatÙ a bummer.\p"
    .string "Iße got to thank\n"
    .string "you... Oh here,\l"
    .string "maybe thisÛl do.$"

Route25_SeaCottage_Text_ThanksLadyTakeThis::
    .string "BILL: Yeehah!\n"
    .string "Thanks, bud! I\l"
    .string "owe you one!\p"
    .string "So, did you come\n"
    .string "to see my POKéMON\l"
    .string "collection?\l"
    .string "You didnÑ?\l"
    .string "ThatÙ a bummer.\p"
    .string "Iße got to thank\n"
    .string "you... Oh here,\l"
    .string "maybe thisÛl do.$"

Route25_SeaCottage_Text_ReceivedSSTicketFromBill::
    .string "{PLAYER} received\n"
    .string "an S.S.TICKET!$"

Route25_SeaCottage_Text_YouveGotTooMuchStuff::
    .string "Youße got too\n"
    .string "much stuff, bud!$"

Route25_SeaCottage_Text_SSAnnePartyYouGoInstead::
    .string "That cruise ship,\n"
    .string "S.S.ANNE, is in\l"
    .string "VERMILION CITY.\l"
    .string "ItÙ passengers\l"
    .string "are all trainers!\p"
    .string "They invited me\n"
    .string "to their party,\l"
    .string "but I canÑ stand\l"
    .string "fancy doÙ. Why\l"
    .string "donÑ you go\l"
    .string "instead of me?$"

Route25_SeaCottage_Text_CheckOutRareMonsOnPC::
    .string "BILL: Look, bud,\n"
    .string "just check out\l"
    .string "some of my rare\l"
    .string "POKéMON on my PC!$"

Route25_SeaCottage_Text_TeleporterIsDisplayed::
    .string "TELEPORTER is\n"
    .string "displayed on the\l"
    .string "PC monitor.$"

Route25_SeaCottage_Text_InitiatedTeleportersCellSeparator::
    .string "{PLAYER} initiated\n"
    .string "TELEPORTERÙ Cell\l"
    .string "Separator!$"

Route25_SeaCottage_Text_BillsFavoriteMonList::
    .string "BILLÙ favorite\n"
    .string "POKéMON list!$"

Route25_SeaCottage_Text_SeeWhichMon::
    .string "Which POKéMON do\n"
    .string "you want to see?$"

