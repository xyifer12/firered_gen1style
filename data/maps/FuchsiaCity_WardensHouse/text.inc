FuchsiaCity_WardensHouse_Text_HifFuffHefifoo::
    .string "WARDEN: Hif fuff\n"
    .string "hefifoo!\p"
    .string "Ha lof ha feef ee\n"
    .string "hafahi ho. Heff\l"
    .string "hee fwee!$"

FuchsiaCity_WardensHouse_Text_AhHowheeHoHoo::
    .string "Ah howhee ho hoo!\n"
    .string "Eef ee hafahi ho!$"

FuchsiaCity_WardensHouse_Text_HeOhayHeHaHoo::
    .string "Ha? He ohay heh\n"
    .string "ha hoo ee haheh!$"

FuchsiaCity_WardensHouse_Text_GaveGoldTeethToWarden::
    .string "{PLAYER} gave the\n"
    .string "GOLD TEETH to the\l"
    .string "WARDEN!$"

FuchsiaCity_WardensHouse_Text_WardenPoppedInHisTeeth::
    .string "The WARDEN popped\n"
    .string "in his teeth!$"

FuchsiaCity_WardensHouse_Text_ThanksSonGiveYouSomething::
    .string "WARDEN: Thanks,\n"
    .string "kid! No one could\l"
    .string "understand a word\l"
    .string "that I said.\p"
    .string "I couldnÑ work\n"
    .string "that way.\p"
    .string "Let me give you\n"
    .string "something for\l"
    .string "your trouble.$"

FuchsiaCity_WardensHouse_Text_ThanksLassieGiveYouSomething::
    .string "WARDEN: Thanks,\n"
    .string "kid! No one could\l"
    .string "understand a word\l"
    .string "that I said.\p"
    .string "I couldnÑ work\n"
    .string "that way.\p"
    .string "Let me give you\n"
    .string "something for\l"
    .string "your trouble.$"

FuchsiaCity_WardensHouse_Text_ReceivedHM04FromWarden::
    .string "{PLAYER} received\n"
    .string "HM04!$"

FuchsiaCity_WardensHouse_Text_ExplainStrength::
    .string "WARDEN: HM04\n"
    .string "teaches STRENGTH!\p"
    .string "It lets POKéMON\n"
    .string "move boulders\l"
    .string "when youàe out-\l"
    .string "side of battle.\p"
    .string "Oh yes, did you\n"
    .string "find SECRET HOUSE\l"
    .string "in SAFARI ZONE?\p"
    .string "If you do, you\n"
    .string "win an HM!\p"
    .string "I hear itÙ the\n"
    .string "rare SURF HM.$"

@ Unused
FuchsiaCity_WardensHouse_Text_YouHaveTooMuchStuff::
    .string "なんや　にもつ\n"
    .string "いっぱいやんけ！$"

FuchsiaCity_WardensHouse_Text_MonPhotosFossilsOnDisplay::
    .string "POKéMON photos\n"
    .string "and fossils.$"

FuchsiaCity_WardensHouse_Text_OldMonMerchandiseOnDisplay::
    .string "Old POKéMON\n"
    .string "merchandise.$"

