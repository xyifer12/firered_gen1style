VermilionCity_Mart_Text_TeamRocketAreWickedPeople::
    .string "There are evil\n"
    .string "people who will\l"
    .string "use POKéMON for\l"
    .string "criminal acts.\p"
    .string "TEAM ROCKET\n"
    .string "traffics in rare\l"
    .string "POKéMON.\p"
    .string "They also abandon\n"
    .string "POKéMON that they\l"
    .string "consider not to\l"
    .string "be popular or\l"
    .string "useful.$"

VermilionCity_Mart_Text_MonsGoodOrBadDependingOnTrainer::
    .string "I think POKéMON\n"
    .string "can be good or\l"
    .string "evil. It depends\l"
    .string "on the trainer.$"

