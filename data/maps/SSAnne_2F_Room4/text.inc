SSAnne_2F_Room4_Text_LamarIntro::
    .string "Which do you like,\n"
    .string "a strong or a\l"
    .string "rare POKéMON?$"

SSAnne_2F_Room4_Text_LamarDefeat::
    .string "GENTLEMAN: I must\n"
    .string "salute you!$"

SSAnne_2F_Room4_Text_LamarPostBattle::
    .string "I prefer strong\n"
    .string "and rare POKéMON.$"

SSAnne_2F_Room4_Text_DawnIntro::
    .string "I never saw you at\n"
    .string "the party.$"

SSAnne_2F_Room4_Text_DawnDefeat::
    .string "LASS: Take\n"
    .string "it easy!$"

SSAnne_2F_Room4_Text_DawnPostBattle::
    .string "Oh, I adore your\n"
    .string "strong POKéMON!$"

