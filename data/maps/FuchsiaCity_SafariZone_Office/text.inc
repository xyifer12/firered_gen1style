FuchsiaCity_SafariZone_Office_Text_NicknamedWardenSlowpoke::
    .string "We nicknamed the\n"
    .string "WARDEN SLOWPOKE.\p"
    .string "He and SLOWPOKE\n"
    .string "both look vacant!$"

FuchsiaCity_SafariZone_Office_Text_WardenIsVeryKnowledgeable::
    .string "SLOWPOKE is very\n"
    .string "knowledgeable\l"
    .string "about POKéMON!\p"
    .string "He even has some\n"
    .string "fossils of rare,\l"
    .string "extinct POKéMON!$"

FuchsiaCity_SafariZone_Office_Text_CouldntUnderstandWarden::
    .string "SLOWPOKE came in,\n"
    .string "but I coulndÑ\l"
    .string "understand him.\p"
    .string "I think heÙ got\n"
    .string "a speech problem!$"

FuchsiaCity_SafariZone_Office_Text_PrizeInSafariZone::
    .string "WARDEN SLOWPOKE is running a\n"
    .string "promotion campaign right now.\p"
    .string "Try to get to the farthest corner\n"
    .string "of the SAFARI ZONE.\p"
    .string "If you can make it, youÛl win a\n"
    .string "very convenient prize.$"

