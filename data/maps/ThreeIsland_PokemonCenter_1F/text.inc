ThreeIsland_PokemonCenter_1F_Text_PCNetworkCanLinkWithKanto::
    .string "Hey, did you hear the news?\p"
    .string "The PC network here can now link\n"
    .string "with PCs in KANTO.\p"
    .string "I donÑ know how that came about,\n"
    .string "but itÙ fantastic!$"

ThreeIsland_PokemonCenter_1F_Text_ImpossibleToSurfBetweenIslands::
    .string "ItÙ impossible to SURF between the\n"
    .string "islands around these parts.\p"
    .string "The tides are too fast and\n"
    .string "treacherous.$"

ThreeIsland_PokemonCenter_1F_Text_AlwaysBerriesInBerryForest::
    .string "LetÙ crush BERRIES!\n"
    .string "…ThatÚ be wasting BERRIES?\p"
    .string "You can always find some BERRIES\n"
    .string "on the ground in BERRY FOREST.$"

