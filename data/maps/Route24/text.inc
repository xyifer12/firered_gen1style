Route24_Text_JustEarnedFabulousPrize::
    .string "Congratulations!\n"
    .string "You beat our 5\l"
    .string "contest trainers!\p"
    .string "You just earned a\n"
    .string "fabulous prize!$"

Route24_Text_ReceivedNuggetFromMysteryTrainer::
    .string "{PLAYER} received\n"
    .string "a NUGGET!$"

Route24_Text_YouDontHaveAnyRoom::
    .string "You donÑ have\n"
    .string "any room!$"

Route24_Text_JoinTeamRocket::
    .string "By the way, would\n"
    .string "you like to join\l"
    .string "TEAM ROCKET?\p"
    .string "Weàe a group\n"
    .string "dedicated to evil\l"
    .string "using POKéMON!\p"
    .string "Want to join?\p"
    .string "Are you sure?\p"
    .string "Come on, join us!\p"
    .string "Iá telling you\n"
    .string "to join!\p"
    .string "OK, you need\n"
    .string "convincing!\p"
    .string "IÛl make you an\n"
    .string "offer you canÑ\l"
    .string "refuse!$"

Route24_Text_RocketDefeat::
    .string "ROCKET: Arrgh!\n"
    .string "You are good!$"

Route24_Text_YoudBecomeTopRocketLeader::
    .string "With your ability,\n"
    .string "you could become\l"
    .string "a top leader in\l"
    .string "TEAM ROCKET!$"

Route24_Text_ShaneIntro::
    .string "I saw your feat\n"
    .string "from the grass!$"

Route24_Text_ShaneDefeat::
    .string "JR.TRAINER♂: I\n"
    .string "thought not!$"

Route24_Text_ShanePostBattle::
    .string "I hid because the\n"
    .string "people on the\l"
    .string "bridge scared me!$"

Route24_Text_EthanIntro::
    .string "OK! Iá No. 5!\n"
    .string "IÛl stomp you!$"

Route24_Text_EthanDefeat::
    .string "JR.TRAINER♂: Whoa!\n"
    .string "Too much!$"

Route24_Text_EthanPostBattle::
    .string "I did my best. I\n"
    .string "have no regrets!$"

Route24_Text_ReliIntro::
    .string "Iá No. 4!\n"
    .string "Getting tired?$"

Route24_Text_ReliDefeat::
    .string "LASS: I lost\n"
    .string "too!$"

Route24_Text_ReliPostBattle::
    .string "I did my best, so\n"
    .string "I have no regrets!$"

Route24_Text_TimmyIntro::
    .string "HereÙ No. 3!\n"
    .string "I wonÑ be easy!$"

Route24_Text_TimmyDefeat::
    .string "YOUNGSTER: Ow!\n"
    .string "Stomped flat!$"

Route24_Text_TimmyPostBattle::
    .string "I did my best, I\n"
    .string "have no regrets!$"

Route24_Text_AliIntro::
    .string "Iá second!\n"
    .string "Now itÙ serious!$"

Route24_Text_AliDefeat::
    .string "LASS: How could I\n"
    .string "lose?$"

Route24_Text_AliPostBattle::
    .string "I did my best, I\n"
    .string "have no regrets!$"

Route24_Text_CaleIntro::
    .string "This is NUGGET\n"
    .string "BRIDGE! Beat us 5\l"
    .string "trainers and win\l"
    .string "a fabulous prize!\p"
    .string "Think you got\n"
    .string "what it takes?$"

Route24_Text_CaleDefeat::
    .string "BUG CATCHER: Whoo!\n"
    .string "Good stuff!$"

Route24_Text_CalePostBattle::
    .string "I did my best, I\n"
    .string "have no regrets!$"
