PowerPlant_MapScripts::
	map_script MAP_SCRIPT_ON_RESUME, PowerPlant_OnResume
	map_script MAP_SCRIPT_ON_TRANSITION, PowerPlant_OnTransition
	.byte 0

PowerPlant_OnResume::
	call_if_set FLAG_SYS_SPECIAL_WILD_BATTLE, PowerPlant_EventScript_TryRemoveStaticMon
	end

PowerPlant_EventScript_TryRemoveStaticMon::
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_ne VAR_RESULT, B_OUTCOME_CAUGHT, EventScript_Return
	removeobject VAR_LAST_TALKED
	return

PowerPlant_OnTransition::
	setworldmapflag FLAG_WORLD_MAP_POWER_PLANT
	call_if_unset FLAG_FOUGHT_ZAPDOS, PowerPlant_EventScript_ShowZapdos
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_1, PowerPlant_EventScript_ShowVoltorb1
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_2, PowerPlant_EventScript_ShowVoltorb2
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_3, PowerPlant_EventScript_ShowVoltorb3
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_4, PowerPlant_EventScript_ShowVoltorb4
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_5, PowerPlant_EventScript_ShowVoltorb5
	call_if_unset FLAG_FOUGHT_POWER_PLANT_VOLTORB_6, PowerPlant_EventScript_ShowVoltorb6
	call_if_unset FLAG_FOUGHT_POWER_PLANT_ELECTRODE_1, PowerPlant_EventScript_ShowElectrode1
	call_if_unset FLAG_FOUGHT_POWER_PLANT_ELECTRODE_2, PowerPlant_EventScript_ShowElectrode2
	end

PowerPlant_EventScript_ShowZapdos::
	clearflag FLAG_HIDE_ZAPDOS
	return

PowerPlant_EventScript_ShowVoltorb1::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_1
	return

PowerPlant_EventScript_ShowVoltorb2::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_2
	return

PowerPlant_EventScript_ShowVoltorb3::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_3
	return

PowerPlant_EventScript_ShowVoltorb4::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_4
	return
	
PowerPlant_EventScript_ShowVoltorb5::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_5
	return
	
PowerPlant_EventScript_ShowVoltorb6::
	clearflag FLAG_HIDE_POWER_PLANT_VOLTORB_6
	return

PowerPlant_EventScript_ShowElectrode1::
	clearflag FLAG_HIDE_POWER_PLANT_ELECTRODE_1
	return

PowerPlant_EventScript_ShowElectrode2::
	clearflag FLAG_HIDE_POWER_PLANT_ELECTRODE_2
	return

PowerPlant_EventScript_Zapdos::
	goto_if_questlog EventScript_ReleaseEnd
	special QuestLog_CutRecording
	lock
	faceplayer
	setwildbattle SPECIES_ZAPDOS, 50
	waitse
	playmoncry SPECIES_ZAPDOS, CRY_MODE_ENCOUNTER
	message Text_Gyaoo
	waitmessage
	waitmoncry
	delay 10
	playbgm MUS_ENCOUNTER_GYM_LEADER, 0
	waitbuttonpress
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special StartLegendaryBattle
	waitstate
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_DefeatedZapdos
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_RanFromZapdos
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_RanFromZapdos
	setflag FLAG_FOUGHT_ZAPDOS
	release
	end

PowerPlant_EventScript_DefeatedZapdos::
	setflag FLAG_FOUGHT_ZAPDOS
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_RanFromZapdos::
	setvar VAR_0x8004, SPECIES_ZAPDOS
	goto EventScript_MonFlewAway
	end

PowerPlant_EventScript_Voltorb1::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb1
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb1
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb1
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_1
	release
	end

PowerPlant_EventScript_FoughtVoltorb1::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_1
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Voltorb2::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb2
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb2
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb2
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_2
	release
	end

PowerPlant_EventScript_FoughtVoltorb2::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_2
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Voltorb3::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb3
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb3
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb3
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_3
	release
	end

PowerPlant_EventScript_FoughtVoltorb3::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_3
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Voltorb4::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb4
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb4
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb4
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_4
	release
	end

PowerPlant_EventScript_FoughtVoltorb4::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_4
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Voltorb5::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb5
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb5
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb5
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_5
	release
	end

PowerPlant_EventScript_FoughtVoltorb5::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_5
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Voltorb6::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_VOLTORB, 40
	waitse
	playmoncry SPECIES_VOLTORB, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtVoltorb6
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtVoltorb6
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtVoltorb6
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_6
	release
	end

PowerPlant_EventScript_FoughtVoltorb6::
	setflag FLAG_FOUGHT_POWER_PLANT_VOLTORB_6
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Electrode1::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_ELECTRODE, 43
	waitse
	playmoncry SPECIES_ELECTRODE, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtElectrode1
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtElectrode1
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtElectrode1
	setflag FLAG_FOUGHT_POWER_PLANT_ELECTRODE_1
	release
	end

PowerPlant_EventScript_FoughtElectrode1::
	setflag FLAG_FOUGHT_POWER_PLANT_ELECTRODE_1
	goto EventScript_RemoveStaticMon
	end

PowerPlant_EventScript_Electrode2::
	goto_if_questlog EventScript_ReleaseEnd
	lock
	faceplayer
	setwildbattle SPECIES_ELECTRODE, 43
	waitse
	playmoncry SPECIES_ELECTRODE, CRY_MODE_ENCOUNTER
	msgbox PowerPlant_Text_Bzzzt
	delay 40
	waitmoncry
	setflag FLAG_SYS_SPECIAL_WILD_BATTLE
	dowildbattle
	clearflag FLAG_SYS_SPECIAL_WILD_BATTLE
	special QuestLog_CutRecording
	specialvar VAR_RESULT, GetBattleOutcome
	goto_if_eq VAR_RESULT, B_OUTCOME_WON, PowerPlant_EventScript_FoughtElectrode2
	goto_if_eq VAR_RESULT, B_OUTCOME_RAN, PowerPlant_EventScript_FoughtElectrode2
	goto_if_eq VAR_RESULT, B_OUTCOME_PLAYER_TELEPORTED, PowerPlant_EventScript_FoughtElectrode2
	setflag FLAG_FOUGHT_POWER_PLANT_ELECTRODE_2
	release
	end

PowerPlant_EventScript_FoughtElectrode2::
	setflag FLAG_FOUGHT_POWER_PLANT_ELECTRODE_2
	goto EventScript_RemoveStaticMon
	end

PowerPlant_Text_Bzzzt::
	.string "Bzzzt!$"
