# Pokémon Red & Blue Gen1Style
#        V7 Out now!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoTitleAndBattle.png "Title And Battle Screens")

**Why?**

This project started when I decided that I dislike the vanilla FireRed art style enough to create a sprite replacement hack.
This idea eventually grew through feature creep into a total re-de-make that aims to bring Red & Blue to the GBA, complete with widescreen and menus that don't suck!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoTowns.png "Battle UI, Move Selection UI.")

**Why not play a colorization hack?**

The tiny screen of the GBC makes me sad, your view is very limited and menus are necessarily cramped. I wanted widescreen, I wanted the ability to connect to DS projects, I wanted good menus and full color.
This is to Red and Blue what Rayman 2: Revolution was to Rayman 2: The Great Escape!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoAdventure.png "Save Screen, Dialogue Box.")

**Is this yet another eternal beta?**

The current version is playable all the way through Cerulean Cave. It's 99% done.
Too many projects promised greatness then died due to disinterest, showed amazing work and then fizzled out.
I decided to develop G1S in secret until it was ready for the general public to play.
Development isn't particularly quick, but as you can see by the commit history, I put in a ton of work in the past 2 years!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoMenu.png "Status Screen Pokemon Overview.")

**What's changed?**

Trainer parties, move stats, item prices and availability, learnsets, TMs, world layout, scripted events, dialogue, and more all match NTSC Red & Blue as closely as I can manage. I have played through the original games and G1S side-by-side 4 times in order to match the experience as closely as possible.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoPokedex.png "Status screen Pokemon Stats.")

**Why the Progressing Hacks category?**

There's a saying about the second 99% taking as long as the first 99%, I think that applies here!
Although the project is 99% done, there are still some very important changes to be made before declaring the project fully finished. 
I cannot bring myself to declare the project properly finished until all audio and move animations are replaced.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromoParty.png "Summary Screen Move Description.")

**FAQ**
Will Yellow be remade? * I have no plans to replicate Yellow. I've tried to keep the project easy for people to use as a base for future projects, going as far as developing with the idea of people enabling disabled systems (such as player and pokemon gender) in their own projects.

Will this be translated? *  Yes! A long-term goal is for it to be relatively easy to translate this project to other (left-to-right) languages. There is a fair amount of text baked directly into graphics by default, I've been working on replacing this with actual text strings printed in actual textboxes.

Can I play with vanilla games? * 50/50. Trading is possible and confirmed to work with Ruby, however, battles will rarely work due to changes to moves and pokemon. Pokemon are fully PkHex compatible so feel free to transfer them that way as well!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/Overworld_V1.png "Kanto Map")

**Ensure your base ROM matches one of the hashes below. Rev0 patches are for FRLG 1.0 while Rev1 patches are for FRLG 1.1. Red is for FireRed and Blue is for LeafGreen!**
- _pokefirered sha1_: 41cb23d8dccc8ebd7c649cd8fbb58eeace6e2fdc
- _pokefirered_rev1_ sha1: dd5945db9b930750cb39d00c84da8571feebf417
- _pokeleafgreen sha1_: 574fa542ffebb14be69902d1d36f1ec0a4afd71e
- _pokeleafgreen_rev1_ sha1: 7862c67bdecbe21d1d69ce082ce34327e1c6ed5e

Build options: 'red' 'blue'
Defaults to Rev1, modern not supported.

All credits moved to CREDITS.md!

https://www.pokecommunity.com/threads/red-blue-g1s.525470/